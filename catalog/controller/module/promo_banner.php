<?php  
class ControllerModulePromoBanner extends Controller {
	protected function index($setting) {

		static $module = 0;
		
		if ($setting['language_id'] != $this->config->get('config_language_id') && $setting['language_id'] != 0) {
			// do not continue if banner is not available on this language
			return false;
		}
		
		if ($module > 0) {
			// can't load up more than one promo banners at the same time!
			return false;
		}
		
		// if display_always option is set, do not perform cookie check, just continue showing the banner
		if (!isset($setting['display_always'])) {
			// serialize the settings array and hash it in order to make a signature
			$pm_signature = md5( serialize( $setting ) );
		
			// set up the cookie name
			$cookie_name = 'pb_signature_' . $pm_signature;
		
			// do not proceed if signature already exists as a cookie
			if (isset($this->request->cookie[$cookie_name])) {		
				return false;
			}
			
			// add a signature cookie
			$res = setcookie($cookie_name, '1', time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);	
		}
		
		$this->language->load('module/promo_banner');
		
    	$this->data['text_close'] = $this->language->get('text_close');
		
		$this->load->model('tool/image');
		
		$this->document->addScript('catalog/view/javascript/promo_banners.js');
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/stylesheet/promo_banners.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/promo_banners.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/promo_banners.css');
		}
		
		$this->data['link'] = $setting['link'];
		$this->data['new_window'] = isset($setting['new_window']);
		
		$this->data['width'] = $setting['width'];
		$this->data['height'] = $setting['height'];
		
		$this->data['banner_image'] =  $this->model_tool_image->resize($setting['image'], $setting['width'], $setting['height']);
		
		$this->data['module'] = $module++;
						
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/promo_banner.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/promo_banner.tpl';
		} else {
			$this->template = 'default/template/module/promo_banner.tpl';
		}
		
		$this->render();
	}
}
?>
