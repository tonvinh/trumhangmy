<?php  
class ControllerModuleGoogleAdsense extends Controller {
	protected function index() {
		$this->language->load('module/googleadsense');
		
    $this->data['heading_title'] = $this->language->get('heading_title');
	$this->data['googleadsense_google_client'] = $this->config->get('googleadsense_google_client');	
	$this->data['googleadsense_google_slot'] = $this->config->get('googleadsense_google_slot');	
	$this->data['googleadsense_google_width'] = $this->config->get('googleadsense_google_width');	
	$this->data['googleadsense_google_height'] = $this->config->get('googleadsense_google_height');	
				
		$this->id       = 'googleadsense';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/googleadsense.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/googleadsense.tpl';
		} else {
			$this->template = 'default/template/module/googleadsense.tpl';
		}
				
		$this->render();
	}
}
?>