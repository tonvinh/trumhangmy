<?php echo $header; ?>
<div id="content-info"><?php echo $content_top; ?>


  <div class="breadcrumb">
  <?php $i=0;?>
    <?php foreach ($breadcrumbs as $breadcrumb) { $i++; ?>
  <span class="bread <?php echo 'bread'.$i ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></span>
    <?php } ?>
  </div>

  <h1><?php echo $heading_title; ?></h1>
  <?php if ($products) { ?>


 
  <div class="product-list">
    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>
      <div class="info-ca-spec">
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <div class="description"><?php echo $product['description']; ?></div></div>
       <div class="info-gia">
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
   
      </div>
      <?php } ?>


      <span><?php echo $text_stock; ?></span> <div class="stock"><?php echo $product['stock']; ?></div>



       <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>

       
       <div class="cart"><div class="bor-gio">


        <div class="gio"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" style="border:0px solid #CCC !important"/></div></div></div>
      
    </div>
  </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="content-info"><?php echo $text_empty; ?></div>
  <?php }?>
  <?php echo $content_bottom; ?></div>

<?php echo $footer; ?>