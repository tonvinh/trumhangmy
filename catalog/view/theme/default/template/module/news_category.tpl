
<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
 
      <ul class="box-category">
        <?php foreach ($categories as $category) { ?>
        <li>
          <?php if ($category['news_category_id'] == $news_category_id) { ?>
          <a ><?php echo $category['name']; ?></a>
          <?php } else { ?>
          <a ><?php echo $category['name']; ?></a>
          <?php } ?>
          <?php if ($category['children']) { ?>
      <ul>
            <?php foreach ($category['children'] as $child) { ?>
            <li>
              <?php if ($child['news_category_id'] == $child_id) { ?>
              <a href="<?php echo $child['href']; ?>" class="active">  <?php echo $child['name']; ?></a>
              <?php } else { ?>
              <a href="<?php echo $child['href']; ?>">  <?php echo $child['name']; ?></a>
              <?php } ?>
            </li>
            <?php } ?>
       </ul>
         <?php } ?>
      </li>
       <?php } ?> 
      </ul>

  </div>
</div>


<div class="box">
  <div class="box-heading">ĐỐI TÁC</div>
  <div class="box-content">
 <marquee behavior="scroll" direction="up" scrollamount="2" height="481" width="100%">
      <div id="banner" >
  <?php foreach ($banners1 as $banner) { ?>
  <?php if ($banner['link']) { ?>
  <div><a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></a></div>
  <?php } else { ?>
  <div><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" title="<?php echo $banner['title']; ?>" /></div>
  <?php } ?>
  <?php } ?>
</div>
</marquee>
  </div>
</div>
<script type="text/javascript"><!--
var banner = function() {
	$('#banner').cycle({
		before: function(current, next) {
			$(next).parent().height($(next).outerHeight());
		}
	});
}

setTimeout(banner, 200);
//--></script>
<script type="text/javascript" src="catalog/view/theme/default/stylesheet/jquery.marquee.js"></script>
<script type="text/javascript">
 $(function () {
        
        $('marquee').marquee('pointer').mouseover(function () {
            $(this).trigger('stop');
        }).mouseout(function () {
            $(this).trigger('start');
        }).mousemove(function (event) {
            if ($(this).data('drag') == true) {
                this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
            }
        }).mousedown(function (event) {
            $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
        }).mouseup(function () {
            $(this).data('drag', false);
        });
    });

</script>

