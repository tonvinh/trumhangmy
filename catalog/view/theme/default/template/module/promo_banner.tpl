<div class='promo-banner' id="promo_banner-<?php echo $module;?>" style="margin-left:<?php echo -$width/2;?>px;">
	<div class="close_container"><a class="close" href="javascript:"><?php echo $text_close;?></a></div>
	<?php if (!empty($link)):?>
		<?php if ($new_window):?>
			<a href="<?php echo $link;?>" title="<?php echo $link;?>" target="_blank"><img id="promo_banner" src="<?php echo $banner_image;?>" width="<?php echo $width;?>" height="<?php echo $height;?>"/></a>
		<?php else: ?>
			<a href="<?php echo $link;?>" title="<?php echo $link;?>"><img id="promo_banner" src="<?php echo $banner_image;?>" width="<?php echo $width;?>" height="<?php echo $height;?>"/></a>
		<?php endif; ?>
	<?php else: ?>
	<img id="promo_banner" src="<?php echo $banner_image;?>" width="<?php echo $width;?>" height="<?php echo $height;?>"/>
	<?php endif; ?>
</div>