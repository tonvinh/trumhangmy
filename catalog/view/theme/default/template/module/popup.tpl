<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen">
<script>
			$(document).ready(function(){
				
				$(".inline").colorbox({inline:true, width:"850px" , open:true});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});
			});
		</script>
		<p><a class='inline' href="#inline_content"></a></p>
		<div style='display:none'>
			<div id='inline_content' style='padding:10px; background:#fff;'>
			<?php echo $message; ?>
			</div>
		</div>

