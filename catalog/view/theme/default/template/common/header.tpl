<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>"  itemscope itemtype="http://schema.org/Product">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<meta name="p:domain_verify" content="8f87157cc74cc9e1fa5563ff1377b2ea"/>
<meta itemprop="name" content="Shop mỹ phẩm xách tay Trùm Hàng Mỹ">
<meta itemprop="description" content="Shop chuyên cung cấp sỉ và lẻ mỹ phẩm xách tay,thuốc giảm cân an toàn,rượu Tây. Nhận ship hàng từ tất cả các website Mỹ.">
<meta itemprop="image" content="http://trumhangmy.com/image/data/logo/lo.png">
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/responsive.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>



<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5462bd636aabec5a" async="async"></script>


</head>
<body>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&appId=1493823304214846&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="container">
<div id="page">
<div class="nav-header">
<div id="menu-button">&#9776;</div>
<div class="callus"> <!--Gọi ngay : 098 3035 311 - 098 9999 096-->
Hotline : 093.8088.893 - 098.3035.311
</div>
</div>
<div id="header">
  <?php if ($logo) { ?>
  <div id="logo"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
  <?php } ?>
  <div class="callus-mobile"> <!--Gọi ngay : 098 3035 311 - 098 9999 096-->
  Hotline : 093.8088.893 - 098.3035.311
  </div>
  <?php echo $cart; ?>
  <div id="search">

    <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
        <div class="button-search"></div>
  </div>
  <div id="welcome">
  <span style="color:#000;"><span class="header-welcome">Welcome to TrumHangMy</span>
    <?php if (!$logged) { ?>
    (<?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
    </span>
  </div>
</div>

<div id="menu">
  <ul>
     <li><a href="index.php">Trang chủ</a></li>
     <li><a href="tin-tuc">Tin tức</a>

           <div>
     <ul>


    <?php foreach ($categoriesnews as $cat) { ?>
   <?php if($cat['cat_id'] == $cat_id){ ?>
    <li><a class="active" href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a>
   <?php }else{ ?>
    <li><a href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a>
  <?php } ?>
    <?php if ($cat['children']) { ?>
      <div>
	    <ul>
        <?php for ($i = 0; $i < count($cat['children']);) { ?>

          <?php $j = $i + ceil(count($cat['children']) / $cat['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($cat['children'][$i])) { ?>
          <li><a href="<?php echo $cat['children'][$i]['href']; ?>"><?php echo $cat['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>

        <?php } ?>
		    </ul>
      </div>

      <?php } ?>



    </li>
    <?php } ?>



    </ul>
</div>

     </li>
     <li><a>Sản phẩm</a>
     <div>
     <ul>

    <?php foreach ($categories as $category) { ?>

  <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>
      <?php } ?>
    </li>

    <?php } ?>

    </ul>
    </div>
     </li>
     <li><a href="huong-dan-mua-hang">Hướng dẫn mua hàng</a></li>
     <li class="latest"><a href="lien-he">Liên hệ</a></li>


    <!--
    <?php foreach ($categories as $category) { ?>
    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>
      <?php } ?>
    </li>
    <?php } ?> -->

   <!-- <li><a href="index.php?route=information/contact">Contact</a></li>-->
  </ul>
</div>

<?php if ($error) { ?>

    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>

<?php } ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- TrumHangMy_Top*970*90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-9579207990306895"
     data-ad-slot="1106207365"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<div id="content-out">
