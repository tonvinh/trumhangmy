<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>

  <div class="breadcrumb">
  <?php $i=0;?>
    <?php foreach ($breadcrumbs as $breadcrumb) { $i++; ?>
  <span class="bread <?php echo 'bread'.$i ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></span>
    <?php } ?>
  </div>
<!--<h1><?php echo $heading_title; ?></h1>-->
  <?php if ($thumb || $description) { ?>
  <div class="category-info">
    <?php if ($thumb) { ?>
    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <?php if ($description) { ?>
    <?php echo $description; ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php if ($categories) { ?>
  <div class="category-list">
   <?php foreach ($categories as $category) { ?>
      <div style="width:49%; display:inline-block;text-align: center;">
       <?php if ($category['image']) { ?>
      <div style=""> <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" /></a></div>
       <?php } ?>
     <div style="text-transform:uppercase; text-align:center; font-weight:bold; margin-bottom:20px;"> <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>
      </div>
      <?php } ?>
  </div>
  <?php } ?>
  <?php if ($news) { ?>

  <div class="product-list">
    <?php foreach ($news as $news_item) { ?>
    <div>
      <?php if ($news_item['thumb']) { ?>
      <div class="image"><a href="<?php echo $news_item['href']; ?>"><img src="<?php echo $news_item['thumb']; ?>" title="<?php echo $news_item['name']; ?>" alt="<?php echo $news_item['name']; ?>" <?php echo $news_item['width']; ?> <?php echo $news_item['height']; ?> /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $news_item['href']; ?>" style="font-size:20px;"><?php echo $news_item['name']; ?></a></div>
      <div style="margin-top: 4px;	margin-bottom: 5px;	color: gray; font-size: 11px;"><?php echo $text_post_on; ?> <?php echo $news_item['date_added']; ?> - <?php echo $text_viewed; ?> <?php echo $news_item['viewed']; ?></div>
      <div class="description" style="font-size:16px;"><?php echo $news_item['short_description']; ?></div>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  <?php if (!$categories && !$news) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>