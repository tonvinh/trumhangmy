<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div class="news" id="content"><?php echo $content_top; ?>


  <div class="breadcrumb">
  <?php $i=0;?>
    <?php foreach ($breadcrumbs as $breadcrumb) { $i++; ?>
  <span class="bread <?php echo 'bread'.$i ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></span>
    <?php } ?>
  </div>


  <h1><?php echo $heading_title; ?></h1>
  <div style="margin-bottom: 10px;	color: gray; font-size: 11px;margin-top: 50px;"><?php echo $text_post_on; ?> <?php echo $date_added; ?> - <?php echo $text_viewed; ?> <?php echo $viewed; ?></div>


       <div class="share1" >
<div class="addthis_native_toolbox"></div>
</div>


  <div style="margin-top: 15px; margin-bottom: 20px; font-weight: bold;"><?php echo $short_description; ?></div>
  <?php echo $description; ?>

  <?php if ($tags) { ?>
  <br />
  <br />
  <div class="tags"><!--<b><?php echo $text_tags; ?></b>-->
    <?php foreach ($tags as $tag) { ?>
    <a href="<?php echo $tag['href']; ?>"><?php echo $tag['tag']; ?></a>
    <?php } ?>
  </div>
  <?php } ?>
  <!--
  <div class="share" style="margin-top: 30px;">
    <div class="addthis_default_style"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>


  </div><br />
  -->
  <?php if ($news) { ?>
      <div style="font-size: 13px; font-weight: bold; margin-top: 30px;"><?php echo $tab_related; ?></div>
      <div><ul>
      	<?php foreach ($news as $related_news) { ?>
      		<li style="padding: 2px;"><a href="<?php echo $related_news['href']; ?>"><?php echo $related_news['name']; ?></a> <span style="color: gray; font-size: 11px;">(<?php echo $related_news['date_added']; ?>)</span></li>
      	<?php } ?>
     	</ul></div>
   <?php } ?>

   <?php if ($other_news) { ?>
      <div style="font-size: 15px; font-weight: bold; margin-top: 30px;" ><?php echo $tab_others; ?></div>
      <div class="tin-khac"><ul>
      	<?php foreach ($other_news as $other_news_item) { ?>
      		<li style="padding: 2px;"><a href="<?php echo $other_news_item['href']; ?>"><?php echo $other_news_item['name']; ?></a> <span style="color: gray; font-size: 11px;">(<?php echo $other_news_item['date_added']; ?>)</span></li>
      	<?php } ?>
     	</ul></div>
   <?php } ?>
   <br/>
   <br/>

  <br/>
    <br/>
<br />
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 600*90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:600px;height:90px"
     data-ad-client="ca-pub-9579207990306895"
     data-ad-slot="6569364566"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<fieldset name="fbcommentBorder">
  <legend>Bình luận facebook</legend>
<div class="fb-comments" data-href="http://trumhangmy.com<?=$_SERVER['REQUEST_URI']?>" data-width="627" data-numposts="5" data-colorscheme="light">


</fieldset>


<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 600*90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:600px;height:90px"
     data-ad-client="ca-pub-8285805548838755"
     data-ad-slot="9732875026"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
   <div style="margin-bottom: 20px;"></div>

  <?php echo $content_bottom; ?></div>
  <script type="text/javascript"><!--
$('.fancybox').fancybox({cyclic: true});
//--></script>
<?php if ($allow_comment) { ?>
<script type="text/javascript"><!--
$('#comment .pagination a').live('click', function() {
	$('#comment').slideUp('slow');

	$('#comment').load(this.href);

	$('#comment').slideDown('slow');

	return false;
});

$('#comment').load('index.php?route=news/news/comment&news_id=<?php echo $news_id; ?>');

$('#button-comment').bind('click', function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=news/news/write&news_id=<?php echo $news_id; ?>&approved=<?php echo $approved; ?>',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&email=' + encodeURIComponent($('input[name=\'email\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-comment').attr('disabled', true);
			$('#comment-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-comment').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data.error) {
				$('#comment-title').after('<div class="warning">' + data.error + '</div>');
			}

			if (data.success) {
				$('#comment-title').after('<div class="success">' + data.success + '</div>');

				$('input[name=\'name\']').val('');
				$('input[name=\'email\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
jQuery(document).ready(function($) {

	$(".scroll").click(function(event){
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
	});
});
//--></script>
<?php } ?>
<?php echo $footer; ?>
