<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content-info"><?php echo $content_top; ?>
 
  <div class="breadcrumb">
  <?php $i=0;?>
    <?php foreach ($breadcrumbs as $breadcrumb) { $i++; ?>
  <span class="bread <?php echo 'bread'.$i ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></span>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2><?php echo $text_location; ?></h2>
    <div class="contact-info">
      <div class="content"><div class="left"><b><?php echo $text_address; ?></b><br />
      <!--  <?php echo $store; ?><br />-->
        <?php echo $address; ?><br />
		
		
		
		</div>
      <div class="right">
        <?php if ($telephone) { ?>
        <b><?php echo $text_telephone; ?></b><br />
        <?php echo $telephone; ?><br />
     
        <?php } ?>
       <b>Email :</b><br />
        trumhangmy@gmail.com<br /><br />
      </div>
    </div>
    </div>
    <br/>
    <h2><?php echo $text_contact; ?></h2>
    <div class="content box-contact" style="
    line-height: 20px;
">
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="<?php echo $name; ?>" />
    <br />
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_email; ?></b><br />
    <input type="text" name="email" value="<?php echo $email; ?>" />
    <br />
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_enquiry; ?></b><br />
    <textarea name="enquiry" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>
    <br />
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" />
    <br />   <br/>
    <img src="index.php?route=information/contact/captcha" alt="" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>

     <div class="buttons">
      <div ><input type="submit" value="Gửi" class="button" /></div>
    </div>
    </div>
   
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>