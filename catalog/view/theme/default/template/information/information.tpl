<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content-info"><?php echo $content_top; ?>

  <div class="breadcrumb">
  <?php $i=0;?>
    <?php foreach ($breadcrumbs as $breadcrumb) { $i++; ?>
  <span class="bread <?php echo 'bread'.$i ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></span>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php echo $description; ?>

  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>