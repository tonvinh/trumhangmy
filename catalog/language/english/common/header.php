<?php
// Text
$_['text_home']     = 'Trang Chủ';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_cart']     = 'Giỏ Hàng';
$_['text_items']    = '%s sản phẩm';
$_['text_search']   = 'Tìm kiếm';
$_['text_welcome']  = '<a href="%s">Đăng Nhập</a>|&nbsp;&nbsp;<a href="%s">Đăng Ký)</a>';
$_['text_logged']   = '<a href="%s">%s</a> <b>(</b> <a href="%s">Đăng xuất</a> <b>)</b>';
$_['text_account']  = 'Tài Khoản';
$_['text_checkout'] = 'Thanh Toán';
$_['text_language'] = 'Ngôn Ngữ';
$_['text_currency'] = 'Tiền Tệ';
?>