<?php
// Text Language Settings 
$_['heading_title'] = 'Đăng nhập';
$_['heading_title_logged'] = 'Thành công';
$_['text_username'] = 'E-mail';
$_['text_password'] = 'Mật khẩu';
$_['text_password_reminder'] = 'Quên mật khẩu ?';
$_['text_submit'] = 'Đăng nhập';
$_['text_logged'] = 'Bạn đang đăng nhập với tài khoản ';
$_['text_logout'] = 'Thoát';
?>