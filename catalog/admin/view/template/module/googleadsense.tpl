<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="left"></div>
  <div class="right"></div>
  <div class="heading">
    <h1 style="background-image: url('view/image/module.png');"><?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
        <tr>
          <td><?php echo $entry_google_client; ?></td>
          <td><input name="googleadsense_google_client" type="text" value="<?php echo $googleadsense_google_client; ?>" size="50" /></td>
        </tr>
        <tr>
          <td><?php echo $entry_google_slot; ?></td>
          <td><input name="googleadsense_google_slot" type="text" value="<?php echo $googleadsense_google_slot; ?>" size="50" /></td>
        </tr>
        <tr>
          <td><?php echo $entry_width; ?></td>
          <td><input name="googleadsense_google_width" type="text" value="<?php echo $googleadsense_google_width; ?>" size="1" /></td>
        </tr>
        <tr>
          <td><?php echo $entry_height; ?></td>
          <td><input name="googleadsense_google_height" type="text" value="<?php echo $googleadsense_google_height; ?>" size="1" /></td>
        </tr>

        <tr>
          <td><?php echo $entry_position; ?></td>
          <td><select name="googleadsense_position">
              <?php if ($googleadsense_position == 'left') { ?>
              <option value="left" selected="selected"><?php echo $text_left; ?></option>
              <?php } else { ?>
              <option value="left"><?php echo $text_left; ?></option>
              <?php } ?>
              <?php if ($googleadsense_position == 'right') { ?>
              <option value="right" selected="selected"><?php echo $text_right; ?></option>
              <?php } else { ?>
              <option value="right"><?php echo $text_right; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td><select name="googleadsense_status">
              <?php if ($googleadsense_status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td><?php echo $entry_sort_order; ?></td>
          <td><input type="text" name="googleadsense_sort_order" value="<?php echo $googleadsense_sort_order; ?>" size="1" /></td>
        </tr>
      </table>
    </form>
  </div>
</div>
<?php echo $footer; ?>