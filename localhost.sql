-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 12, 2016 at 03:44 PM
-- Server version: 5.5.45
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_trum`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(32) NOT NULL,
  `company_id` varchar(32) NOT NULL,
  `tax_id` varchar(32) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `company_id`, `tax_id`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`) VALUES
(2, 2, 'Hứa Hiếu', '', '', '', '', '13/9M Thống Nhất P11 Gò Vấp HCM', '', '', '', 0, 0),
(3, 3, 'Hoàng Thanh Nhàn', '', '', '', '', '89b, nguyễn chí thanh, thị trấn sa Pa, huyện sa Pa, Lào cai', '', '', '', 0, 0),
(4, 4, 'Phạm Hải Yến', '', '', '', '', 'KĐT Xa La, Hà Nội', '', '', '', 0, 0),
(5, 5, 'Nguyễn Hữu Thắng', '', '', '', '', 'DT402 Khánh Long-Tân Phước Khánh-Tân Uyên-Bình Dương', '', '', '', 0, 0),
(6, 6, 'nguyễn quốc huy', '', '', '', '', '78 đường số 1 f13 gò vấp', '', '', '', 0, 0),
(7, 7, 'Nguyen Huy Thuy An', '', '', '', '', '36/70/14 Duong D2, F25, Q Binh Thanh, Tp HCM', '', '', '', 0, 0),
(8, 8, 'Trinh', '', '', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', 0, 0),
(9, 9, 'Tran Phuong Thuy', '', '', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', 0, 0),
(10, 10, 'Lê Nguyễn Ngọc Diệu', '', '', '', '', '105/7 khóm 6 phường 7 đường Hòa Bình, thành phố Bạc Liêu', '', '', '', 0, 0),
(11, 11, 'Hà Thị Ái Thanh', '', '', '', '', '275/3 Lâm Thị Hố, Phường Tân Chánh Hiệp Q12', '', '', '', 0, 0),
(12, 12, 'Trương Thị Hoa Lý', '', '', '', '', '180-192 Nguyễn Công Trứ, Phường Nguyễn Thái Bình, Quận 1, TP. Hồ Chí Minh', '', '', '', 0, 0),
(13, 13, 'Tran le bich dao', '', '', '', '', '85-86 đường 3/2 tp.rach gia-kiên giang', '', '', '', 0, 0),
(14, 14, 'Lý Tố Nhi', '', '', '', '', 'Thủ Đức - TPHCM', '', '', '', 0, 0),
(15, 15, 'Hoàng Phương Hảo', '', '', '', '', '232 Lý Thường Kiệt, Đồng Hới, Quảng Bình', '', '', '', 0, 0),
(16, 16, 'Huệ', '', '', '', '', 'So 18 tt trường đh công nghệ gtvt ngõ 50 Trung Văn Từ Liêm ( gần cao đẳng xây dựng)', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `affiliate`
--

CREATE TABLE IF NOT EXISTS `affiliate` (
  `affiliate_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(32) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_transaction`
--

CREATE TABLE IF NOT EXISTS `affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE IF NOT EXISTS `attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_description`
--

CREATE TABLE IF NOT EXISTS `attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_description`
--

INSERT INTO `attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(4, 1, 'test 1'),
(5, 1, 'test 2'),
(6, 1, 'test 3'),
(7, 1, 'test 4'),
(8, 1, 'test 5'),
(9, 1, 'test 6'),
(10, 1, 'test 7'),
(11, 1, 'test 8'),
(3, 1, 'Clockspeed');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group`
--

CREATE TABLE IF NOT EXISTS `attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_group`
--

INSERT INTO `attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_group_description`
--

CREATE TABLE IF NOT EXISTS `attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attribute_group_description`
--

INSERT INTO `attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `name`, `status`) VALUES
(8, 'Thuong Hieu', 1),
(11, 'Dich vu order', 1),
(12, 'Khuyen mai', 1),
(13, 'Hoá đơn', 1),
(14, 'Y kien khach hang', 1),
(15, 'Đối Tác', 0);

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

CREATE TABLE IF NOT EXISTS `banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=378 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_image`
--

INSERT INTO `banner_image` (`banner_image_id`, `banner_id`, `link`, `image`) VALUES
(371, 8, 'http://trumhangmy.com/my-pham/Eyes-Liner', 'data/banner/Chi-Ke-Mat.png'),
(372, 8, 'http://trumhangmy.com/my-pham/son', 'data/banner/Son.png'),
(370, 8, 'http://trumhangmy.com/my-pham/mascara', 'data/banner/Mascara.png'),
(355, 13, '', 'data/Hoa don/Hoa don mua hang tu My 8.JPG'),
(369, 8, 'http://trumhangmy.com/my-pham/kem-chong-nang', 'data/banner/KCN.png'),
(319, 14, '', 'data/Y kien khach hang/Chinh.png'),
(318, 14, '', 'data/Y kien khach hang/em-be.png'),
(317, 14, '', 'data/Y kien khach hang/phan-loan_c.png'),
(316, 14, '', 'data/Y kien khach hang/vy-nguyen_c.png'),
(312, 14, '', 'data/Y kien khach hang/Toner.png'),
(313, 14, '', 'data/Y kien khach hang/Hana.png'),
(314, 14, '', 'data/Y kien khach hang/Ha.png'),
(354, 13, '', 'data/Hoa don/Hoa don mua hang tu My 7.JPG'),
(353, 13, '', 'data/Hoa don/Hoa don mua hang tu My 5.JPG'),
(356, 12, '', 'data/banner/GiamGia10.png'),
(352, 13, '', 'data/Hoa don/Hoa don mua hang tu My 1.JPG'),
(351, 13, '', 'data/Hoa don/Hoa don mua hang tu My 10.JPG'),
(350, 13, '', 'data/Hoa don/Hoa don mua hang tu My 11.JPG'),
(349, 13, '', 'data/Hoa don/Hoa don mua hang tu My 12.JPG'),
(315, 14, '', 'data/Y kien khach hang/Thanh-Thao_c.png'),
(311, 14, '', 'data/Y kien khach hang/banh-bong-lan.png'),
(310, 14, '', 'data/Y kien khach hang/may-rua-mat.png'),
(377, 11, 'http://trumhangmy.com/shipping', 'data/banner/shipping.png'),
(348, 13, '', 'data/Hoa don/Hoa don mua hang tu My 2.JPG'),
(347, 13, '', 'data/Hoa don/Hoa don mua hang tu My 3.JPG'),
(346, 13, '', 'data/Hoa don/Hoa don mua hang tu My 4.JPG'),
(368, 8, 'http://trumhangmy.com/my-pham/sua-rua-mat-toner', 'data/banner/Sua-Rua-Mat.png');

-- --------------------------------------------------------

--
-- Table structure for table `banner_image_description`
--

CREATE TABLE IF NOT EXISTS `banner_image_description` (
  `banner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_image_description`
--

INSERT INTO `banner_image_description` (`banner_image_id`, `language_id`, `banner_id`, `title`, `description`) VALUES
(356, 1, 12, 'Giam gia 10%', NULL),
(371, 1, 8, 'EyesLiner', NULL),
(355, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(370, 1, 8, 'Mascara', NULL),
(369, 1, 8, 'Kem chống nắng', NULL),
(319, 1, 14, 'Chinh', NULL),
(318, 1, 14, 'Em be', NULL),
(317, 1, 14, 'Phan Loan', NULL),
(316, 1, 14, 'Vy Nguyen', NULL),
(315, 1, 14, 'Thanh Thao', NULL),
(314, 1, 14, 'Ha', NULL),
(313, 1, 14, 'Hana', NULL),
(312, 1, 14, 'Toner', NULL),
(311, 1, 14, 'Banh bong lan', NULL),
(354, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(353, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(310, 1, 14, 'May rua mat', NULL),
(377, 1, 11, 'banner', NULL),
(352, 1, 13, 'Hoá đơn mua hàng', NULL),
(351, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(350, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(349, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(348, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(347, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(346, 1, 13, 'Hoá đơn mua hàng 1', NULL),
(368, 1, 8, 'Sữa rửa mặt', NULL),
(372, 1, 8, 'Son', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(25, 'data/BBW/2011_01_BathBodyWorksCarriedAway.jpg', 0, 1, 1, 4, 1, '2009-01-31 01:04:25', '2015-01-05 05:06:49'),
(59, 'data/brand/VS.png', 0, 1, 1, 2, 1, '2014-08-29 11:35:09', '2015-01-05 05:05:42'),
(60, 'data/brand/PINK.jpg', 0, 1, 1, 3, 1, '2014-08-29 11:35:22', '2015-01-05 05:06:05'),
(61, 'data/BBW/bath-body-work-01.jpg', 25, 0, 1, 0, 1, '2014-10-27 23:53:54', '2014-11-09 23:38:26'),
(62, 'data/BBW/1.png', 25, 0, 1, 1, 1, '2014-10-28 00:33:49', '2014-11-09 23:38:40'),
(63, '', 0, 1, 1, 0, 1, '2014-11-01 13:02:53', '2015-01-09 23:49:37'),
(64, 'data/23-6-15 hinh dai dien tin tuc/KCN.png', 63, 1, 1, 0, 1, '2014-11-01 13:04:00', '2015-06-23 14:28:49'),
(66, 'data/BBW/bath-body-works-antibacterial.jpg', 25, 0, 1, 0, 1, '2014-11-09 23:38:10', '2014-11-09 23:39:28'),
(67, 'data/23-6-15 hinh dai dien tin tuc/Son.png', 63, 1, 1, 0, 1, '2014-12-30 09:00:02', '2015-06-23 14:31:51'),
(68, 'data/23-6-15 hinh dai dien tin tuc/Mascara.png', 63, 1, 1, 0, 1, '2014-12-30 09:00:54', '2015-06-23 14:29:45'),
(69, 'data/23-6-15 hinh dai dien tin tuc/Chi-Ke-Mat.png', 63, 1, 1, 0, 1, '2014-12-30 09:01:37', '2015-06-23 14:27:44'),
(70, 'data/23-6-15 hinh dai dien tin tuc/Phan-Phu.png', 63, 1, 1, 0, 1, '2014-12-30 09:02:40', '2015-06-23 14:31:31'),
(71, 'data/iBlue/thanhphaniblue.png', 0, 1, 1, 5, 1, '2015-01-04 23:13:49', '2015-01-05 05:07:15'),
(73, 'data/23-6-15 hinh dai dien tin tuc/Sua-Rua-Mat.png', 63, 1, 1, 0, 1, '2015-01-13 01:09:05', '2015-06-23 14:32:33');

-- --------------------------------------------------------

--
-- Table structure for table `category_description`
--

CREATE TABLE IF NOT EXISTS `category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_description`
--

INSERT INTO `category_description` (`category_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`) VALUES
(59, 1, 'VICTORIA''S SECRET', '&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;Shop chuyên cung cấp sỉ và lẻ tất cả các sản phẩm chính hãng của Victoria''s Secret với giá cả cạnh tranh nhất. Cam đoan 100% hàng chính hãng nhập tự Mỹ. Có hóa đơn mua hàng tự Mỹ. Các sản phẩm của Shop bao gồm : Body Lotion , Body Cream, Hand Cream, Body Mist, Perfume.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Các bạn cần tư vấn sản phẩm&amp;nbsp;và mua lẻ liên hệ : &lt;span style=&quot;background-color:#FF0000;&quot;&gt;Ms.Thi&lt;/span&gt; &lt;span style=&quot;color:#FF0000;&quot;&gt;098.9999.096&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Mua sỉ, hỗ trợ đặt hàng, giao hàng liên hệ : &lt;span style=&quot;background-color:#FF0000;&quot;&gt;Mr. Hiếu&lt;/span&gt; &lt;span style=&quot;color:#FF0000;&quot;&gt;098.3035.311&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Hoặc Pm qua facebook : &lt;span style=&quot;color:#FFFFFF;&quot;&gt;&lt;span style=&quot;background-color:#0000CD;&quot;&gt;www.facebook.com/trumhangusa&lt;/span&gt;&lt;/span&gt; 24/7&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Chuyên cung cấp sỉ và lẻ tất cả sản phẩm của Victoria''s Secret như : body lotion, body cream, body mist, perfume, đồ lót, áo ngực, quần lót...Với giá cả cạnh tranh nhất, Cam kết hàng nhập từ Mỹ 100%, không phải hàn g VNXK hay TQ', 'Body Lotion, Body Cream, Body Mist, Perfume, Quần lót, Áo ngực, Đồ lót, Đồ ngủ, Quần chip'),
(60, 1, 'PINK - VICTORIA''S SECRECT', '&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;em&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;span style=&quot;color: rgb(238, 130, 238);&quot;&gt;&lt;strong&gt;Shop chuyên cung cấp sỉ và lẻ tất cả các sản phẩm chính hãng của PINK -&amp;nbsp;Victoria''s Secret với giá cả cạnh tranh nhất. Cam đoan 100% hàng chính hãng nhập tự Mỹ. Có hóa đơn mua hàng tự Mỹ. Các sản phẩm của Shop bao gồm : Quần lót, Áo ngực, Đồ ngủ, Mỹ phẩm..&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;strong&gt;Các bạn cần tư vấn sản phẩm&amp;nbsp;và mua lẻ liên hệ :&amp;nbsp;&lt;span style=&quot;background-color: rgb(255, 0, 0);&quot;&gt;Ms.Thi&lt;/span&gt;&amp;nbsp;&lt;span style=&quot;color: rgb(255, 0, 0);&quot;&gt;098.9999.096&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;strong&gt;Mua sỉ, hỗ trợ đặt hàng, giao hàng liên hệ :&amp;nbsp;&lt;span style=&quot;background-color: rgb(255, 0, 0);&quot;&gt;Mr. Hiếu&lt;/span&gt;&amp;nbsp;&lt;span style=&quot;color: rgb(255, 0, 0);&quot;&gt;098.3035.311&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;strong&gt;Hoặc Pm qua facebook :&amp;nbsp;&lt;span style=&quot;color: rgb(255, 255, 255);&quot;&gt;&lt;span style=&quot;background-color: rgb(0, 0, 205);&quot;&gt;www.facebook.com/trumhangusa&lt;/span&gt;&lt;/span&gt;&amp;nbsp;24/7&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Chuyên cung cấp sỉ và lẻ tất cả sản phẩm của Pink như : đồ lót, áo ngực, quần lót, đồ ngủ, đồ tập thể thao, nước hoa, mỹ phẩm...Với giá cả cạnh tranh nhất, Cam kết hàng nhập từ Mỹ 100%, không phải hàng VNXK hay TQ', 'Pink, quần lót Pink, đồ ngủ Pink, áo ngực Pink, áo lót Pink, đồ lót Victoria''s Secret, đồ lót gợi cảm, đồ lót sexy, đồ lót xách tay, đồ lót trẻ trung, đồ lót khuyến mãi'),
(25, 1, 'BATH AND BODY WORKS', '&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;em&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;span style=&quot;color: rgb(238, 130, 238);&quot;&gt;&lt;strong&gt;Shop chuyên cung cấp sỉ và lẻ tất cả các sản phẩm chính hãng của Bath&amp;amp;Body Works&amp;nbsp;với giá cả cạnh tranh nhất. Cam đoan 100% hàng chính hãng nhập tự Mỹ. Có hóa đơn mua hàng tự Mỹ. Các sản phẩm của Shop bao gồm : Body Lotion , Body Cream, Hand Cream, Hand Soáp,&amp;nbsp;Body Mist, Perfume.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;strong&gt;Các bạn cần tư vấn sản phẩm&amp;nbsp;và mua lẻ liên hệ :&amp;nbsp;&lt;span style=&quot;background-color: rgb(255, 0, 0);&quot;&gt;Ms.Thi&lt;/span&gt;&amp;nbsp;&lt;span style=&quot;color: rgb(255, 0, 0);&quot;&gt;098.9999.096&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;strong&gt;Mua sỉ, hỗ trợ đặt hàng, giao hàng liên hệ :&amp;nbsp;&lt;span style=&quot;background-color: rgb(255, 0, 0);&quot;&gt;Mr. Hiếu&lt;/span&gt;&amp;nbsp;&lt;span style=&quot;color: rgb(255, 0, 0);&quot;&gt;098.3035.311&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&lt;strong&gt;Hoặc Pm qua facebook :&amp;nbsp;&lt;span style=&quot;color: rgb(255, 255, 255);&quot;&gt;&lt;span style=&quot;background-color: rgb(0, 0, 205);&quot;&gt;www.facebook.com/trumhangusa&lt;/span&gt;&lt;/span&gt;&amp;nbsp;24/7&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, l'),
(61, 1, 'Body Lotion - Kem dưỡng da', '&lt;p&gt;&lt;span style=&quot;color:#0000CD;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Arial; line-height: 20px; text-align: justify;&quot;&gt;Sử dụng sản phẩm dưỡng thể là một phần quan trọng trong việc làm đẹp của chị em phụ nữ và điều này càng thực sự cần thiết với khí trời thay đổi &amp;nbsp;bất thường như những năm gần đây rất dễ gây ảnh hưởng xấu đến làn da của phái &amp;nbsp;đẹp. Bạn muốn chăm sóc làn da của mình nhưng lại chưa tìm thấy loại kem hay sữa dưỡng nào vừa hiệu quả trong việc dưỡng ẩm vừa có tác dụng cho làn da trẻ trung lâu dài.&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, l'),
(62, 1, 'Body Mist - Xịt toàn thân', '&lt;p&gt;&lt;strong&gt;&lt;span style=&quot;font-family: Arial; font-size: 15px; line-height: 20px; text-align: justify;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;Body Mist – NƯỚC HOA XỊT THƠM TOÀN THÂN&lt;/span&gt; của thương hiệu &lt;span style=&quot;color:#0000FF;&quot;&gt;Bath and Body Works&lt;/span&gt; – Hàng xách tay từ Mỹ – đảm bảo chất lượng 100% Mỗi bộ sưu tập mới mang lại hương thơm quyến rũ và ngọt ngào càng làm tăng thêm vẻ đẹp cho phái nữ chúng ta. Đây là một món quà đầy ý nghĩa của các bạn trai tặng cho người yêu thương của mình. Bất kể là bạn xịt toàn thân hay chỉ là xịt 1 chút xíu hương thơm yêu thích, chắc chắn bạn sẽ yêu ngay những hương thơm này! Chai và bơm xịt được thiết kể tỉ mỉ để có thể xịt lượng lớn hương bụi bao phủ toàn thân! Bạn sẽ đắm chìm trong hương thơm ngây ngất.&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;&lt;span style=&quot;font-family: Arial; font-size: 15px; line-height: 20px; text-align: justify;&quot;&gt;Shop cung cấp đầy đủ các mùi, các mùi mới nhất để các bạn có nhiều sự lựa chọn&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, l'),
(63, 1, 'MỸ PHẨM', '', '', ''),
(64, 1, 'Kem chống nắng', '', '', ''),
(66, 1, 'Hand Gel - Nước rửa tay khô', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath &amp;amp; Body Works&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;là thương hiệu nổi tiếng của Mỹ chuyên về lĩnh vực chăm sóc và bảo vệ da. Luôn hướng tới mục tiêu mang lại cho khách hàng của mình những dòng sản phẩm tốt nhất, cùng sự tiện lợi tối đa, các dòng sản phẩm của &lt;span style=&quot;color:#0000FF;&quot;&gt;Bath &amp;amp; Body Works&lt;/span&gt; được người tiêu dùng khắp thế giới ưa chuộng đặc biệt là giới văn phòng.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;Sản phẩm Gel rửa tay khô&lt;/span&gt;&lt;/strong&gt;&amp;nbsp;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath &amp;amp; Body Works&amp;nbsp;&lt;/strong&gt;&lt;/span&gt;(Sản phẩm dạng gel tinh chất đậm đặc chứ không phải dạng nước như nhiều bạn đã lầm tưởng nhé) vốn được tinh chế từ các thành phần tự nhiên, vừa giúp nuôi dưỡng cho bạn làn da mịn màng, khỏe mạnh, vừa tiệt trùng, diệt khuẩn cho bạn đôi tay sạch sẽ.&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m'),
(67, 1, 'Son', '', '', ''),
(68, 1, 'Mascara', '', '', ''),
(69, 1, 'Eyes Liner', '', '', ''),
(70, 1, 'Phấn phủ', '', '', ''),
(71, 1, 'SẢN PHẨM GIẢM CÂN', '', 'Shop cung cấp các sản phẩm hổ trợ giảm cân từ Mỹ với chất lượng và giá cả cạnh tranh nhất trên thị trường, các sản phẩm giảm cân an toàn, giảm cân công nghệ nano', ''),
(73, 1, 'Sửa rữa mặt - Toner', '', 'Shop cung cấp sỉ và lẻ các loại sữa rửa mặt,toner của các hãng earth science,neutrogena, với chất lượng và giá cả cạnh tranh nhất', '');

-- --------------------------------------------------------

--
-- Table structure for table `category_filter`
--

CREATE TABLE IF NOT EXISTS `category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_path`
--

CREATE TABLE IF NOT EXISTS `category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_path`
--

INSERT INTO `category_path` (`category_id`, `path_id`, `level`) VALUES
(25, 25, 0),
(60, 60, 0),
(59, 59, 0),
(61, 25, 0),
(61, 61, 1),
(62, 25, 0),
(62, 62, 1),
(63, 63, 0),
(64, 63, 0),
(64, 64, 1),
(66, 66, 1),
(66, 25, 0),
(67, 63, 0),
(67, 67, 1),
(68, 63, 0),
(68, 68, 1),
(69, 63, 0),
(69, 69, 1),
(70, 63, 0),
(70, 70, 1),
(71, 71, 0),
(73, 63, 0),
(73, 73, 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_to_layout`
--

CREATE TABLE IF NOT EXISTS `category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_to_store`
--

CREATE TABLE IF NOT EXISTS `category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_to_store`
--

INSERT INTO `category_to_store` (`category_id`, `store_id`) VALUES
(25, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(73, 0);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(240, 'Jersey', 'JE', 'JEY', '', 1, 1),
(241, 'Guernsey', 'GG', 'GGY', '', 1, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(10) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_category`
--

CREATE TABLE IF NOT EXISTS `coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_history`
--

CREATE TABLE IF NOT EXISTS `coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_product`
--

CREATE TABLE IF NOT EXISTS `coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'VND', 'GBP', '', ' Đ', '', 1.00000000, 1, '2016-05-12 06:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `custom_field`
--

CREATE TABLE IF NOT EXISTS `custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `location` varchar(32) NOT NULL,
  `position` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_description`
--

CREATE TABLE IF NOT EXISTS `custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_to_customer_group`
--

CREATE TABLE IF NOT EXISTS `custom_field_to_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_value`
--

CREATE TABLE IF NOT EXISTS `custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_value_description`
--

CREATE TABLE IF NOT EXISTS `custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `store_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `customer_group_id`, `ip`, `status`, `approved`, `token`, `date_added`) VALUES
(2, 0, 'Hứa Hiếu', '', 'huatronghieu@gmail.com', '0983035311', '', '0883569f9c0ffc00c9da6e957499617035fcb120', '00726229c', 'a:0:{}', '', 1, 2, 1, '42.118.109.155', 1, 1, '', '2014-11-07 05:09:37'),
(3, 0, 'Hoàng Thanh Nhàn', '', 'Nhan.huang@gmail.com', '0903462902', '', '57b144e1c2e5dee5681ea533d2e6aa3f98926c9f', 'e89eff08f', 'a:1:{s:5:"101::";i:1;}', '', 0, 3, 1, '113.160.151.205', 1, 1, '', '2015-07-01 08:54:55'),
(4, 0, 'Phạm Hải Yến', '', 'haiyen504@yahoo.com', '0903240681', '', '290867eedc5c95997d61662ec8b21f2cee4f90f9', 'a71789f8a', 'a:0:{}', '', 1, 4, 1, '113.20.116.121', 1, 1, '', '2015-07-06 14:17:45'),
(5, 0, 'Nguyễn Hữu Thắng', '', 'nguyenhuuthang2190@gmail.com', '0972100972', '', '260923bd39b445475b16e51594f7603223a19dc8', '0bf901925', 'a:0:{}', '', 1, 5, 1, '27.74.70.159', 1, 1, '', '2015-07-27 02:59:40'),
(6, 0, 'nguyễn quốc huy', '', 'huynguyen0905@gmail.com', '0903090594', '', '58e81b385b8d88cd6281d4fd42a8e024fc09cdd0', '68ff0c048', 'a:0:{}', '', 1, 6, 1, '123.21.187.65', 1, 1, '', '2015-08-18 22:30:27'),
(7, 0, 'Nguyen Huy Thuy An', '', 'thuyan0707@yahoo.com', '0938078202', '', '7a89588f8806bed45f8c40cdbb52976ef2c07297', 'e4d0b556e', 'a:1:{s:4:"58::";i:1;}', '', 0, 7, 1, '123.20.248.136', 1, 1, '', '2015-09-23 21:00:00'),
(8, 0, 'Trinh', '', 'trinh114@yahoo.com', '0906913786', '', 'd32df6571de3d2e652fd317d546378d790e9e5ed', 'bdc0db616', 'a:0:{}', '', 0, 8, 1, '171.248.53.208', 1, 1, '', '2015-10-05 12:01:29'),
(9, 0, 'Tran Phuong Thuy', '', 'phuongthuy_67@yahoo.com', '0945 050 052', '', 'b90d0dd7622e65519181d2be844b9589681a0781', '988804117', 'a:0:{}', '', 0, 9, 1, '210.86.231.190', 1, 1, '', '2015-11-08 21:55:38'),
(10, 0, 'Lê Nguyễn Ngọc Diệu', '', 'ngocdieu0907@yahoo.com', '0947962152', '', '64328c7bc9820c34d36408a81ad3f47c16bd414d', '8da083ec5', 'a:7:{s:4:"62::";i:2;s:4:"63::";i:1;s:4:"64::";i:2;s:4:"65::";i:1;s:4:"57::";i:1;s:4:"61::";i:2;s:4:"59::";i:1;}', '', 1, 10, 1, '14.164.231.74', 1, 1, '', '2015-11-16 15:15:45'),
(11, 0, 'Hà Thị Ái Thanh', '', 'haaithanh2905@yahoo.com.vn', '0967328041', '', 'b6b6e6d7e7f29245fc4859197b4218fd6d2d6db6', 'fced4e512', 'a:0:{}', '', 1, 11, 1, '116.106.193.36', 1, 1, '', '2015-11-27 21:27:34'),
(12, 0, 'Trương Thị Hoa Lý', '', 'hoalykt@yahoo.com.vn', '0932562678', '', '50701b786bc19aca26feb9bd0bae1b01d5df02bb', '1119e0d2a', 'a:0:{}', '', 0, 12, 1, '27.75.160.6', 1, 1, '', '2015-12-14 22:41:29'),
(13, 0, 'Tran le bich dao', '', 'Daotran168@icloud.com', '0939446996', '', 'd9d4b59ae5be14662333ab7ebadd83f9c6274d59', '0c56d10b6', 'a:0:{}', '', 1, 13, 1, '113.169.30.165', 1, 1, '', '2015-12-16 20:05:36'),
(14, 0, 'Lý Tố Nhi', '', 'tonhi275@gmail.com', '01699444235', '', '911ec65f9929efc1ca1d4d17c76174b91767fca7', '0e32ddc64', 'a:0:{}', '', 1, 14, 1, '115.75.4.27', 1, 1, '', '2016-01-15 22:47:48'),
(15, 0, 'Hoàng Phương Hảo', '', 'phuonghao214@gmail.com', '0949423377', '', '6987bd0e73031aee63c4d24eb08ce551e5f7b22b', 'b8af63382', 'a:0:{}', '', 1, 15, 1, '123.25.116.33', 1, 1, '', '2016-01-27 14:20:52'),
(16, 0, 'Huệ', '', 'lfkimhue@gmaik.com', '0988425736', '', 'f262540064664da66720fb2761a6454d4078d6ef', '3172d3f4a', 'a:1:{s:4:"80::";i:2;}', '', 1, 16, 1, '58.187.5.224', 1, 1, '', '2016-05-04 23:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `customer_ban_ip`
--

CREATE TABLE IF NOT EXISTS `customer_ban_ip` (
  `customer_ban_ip_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_field`
--

CREATE TABLE IF NOT EXISTS `customer_field` (
  `customer_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` int(128) NOT NULL,
  `value` text NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_group`
--

CREATE TABLE IF NOT EXISTS `customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `company_id_display` int(1) NOT NULL,
  `company_id_required` int(1) NOT NULL,
  `tax_id_display` int(1) NOT NULL,
  `tax_id_required` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_group`
--

INSERT INTO `customer_group` (`customer_group_id`, `approval`, `company_id_display`, `company_id_required`, `tax_id_display`, `tax_id_required`, `sort_order`) VALUES
(1, 0, 1, 0, 0, 1, 1),
(2, 1, 1, 0, 1, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `customer_group_description`
--

CREATE TABLE IF NOT EXISTS `customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_group_description`
--

INSERT INTO `customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Khách', 'Khách hàng thông thường'),
(2, 1, 'VIP', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer_history`
--

CREATE TABLE IF NOT EXISTS `customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_ip`
--

CREATE TABLE IF NOT EXISTS `customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_ip`
--

INSERT INTO `customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(5, 2, '1.53.135.129', '2014-11-07 05:09:38'),
(6, 2, '183.80.175.214', '2015-01-13 00:44:27'),
(7, 2, '42.118.109.155', '2015-01-13 07:48:43'),
(8, 3, '113.160.151.205', '2015-07-01 08:54:55'),
(9, 4, '113.20.116.121', '2015-07-06 14:17:45'),
(10, 5, '27.74.70.159', '2015-07-27 02:59:41'),
(11, 6, '42.119.68.233', '2015-08-18 22:30:28'),
(12, 6, '123.21.187.65', '2015-08-18 22:32:25'),
(13, 7, '123.20.248.136', '2015-09-23 21:00:01'),
(14, 8, '171.248.53.208', '2015-10-05 12:01:29'),
(15, 9, '14.177.218.137', '2015-11-08 21:55:39'),
(16, 9, '210.86.231.190', '2015-11-09 12:25:01'),
(17, 10, '113.188.132.113', '2015-11-16 15:15:45'),
(18, 10, '113.170.46.134', '2015-11-16 21:59:43'),
(19, 10, '115.79.126.34', '2015-11-17 10:27:48'),
(20, 10, '14.164.231.74', '2015-11-17 12:16:07'),
(21, 11, '116.106.193.36', '2015-11-27 21:27:35'),
(22, 12, '27.75.160.6', '2015-12-14 22:41:30'),
(23, 13, '113.169.30.165', '2015-12-16 20:05:37'),
(24, 14, '115.75.4.27', '2016-01-15 22:47:48'),
(25, 15, '222.255.144.74', '2016-01-27 14:20:53'),
(26, 15, '123.25.116.33', '2016-01-27 14:31:28'),
(27, 16, '58.187.5.224', '2016-05-04 23:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `customer_online`
--

CREATE TABLE IF NOT EXISTS `customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_reward`
--

CREATE TABLE IF NOT EXISTS `customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction`
--

CREATE TABLE IF NOT EXISTS `customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE IF NOT EXISTS `download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `remaining` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download_description`
--

CREATE TABLE IF NOT EXISTS `download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `extension`
--

CREATE TABLE IF NOT EXISTS `extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=463 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extension`
--

INSERT INTO `extension` (`extension_id`, `type`, `code`) VALUES
(23, 'payment', 'cod'),
(449, 'shipping', 'free'),
(436, 'total', 'sub_total'),
(59, 'total', 'total'),
(429, 'module', 'banner'),
(426, 'module', 'carousel'),
(387, 'shipping', 'flat'),
(433, 'module', 'category'),
(431, 'module', 'featured1'),
(434, 'module', 'simple'),
(437, 'total', 'tax'),
(407, 'payment', 'free_checkout'),
(427, 'module', 'featured'),
(432, 'module', 'featured2'),
(428, 'module', 'pavsliderlayer'),
(440, 'module', 'latest'),
(441, 'module', 'bestseller'),
(442, 'module', 'yahoomessenger'),
(443, 'module', 'fblikebox'),
(444, 'module', 'news_category'),
(445, 'module', 'tnt_newscat'),
(446, 'payment', 'bank_transfer'),
(451, 'module', 'newslatest'),
(452, 'total', 'voucher'),
(453, 'payment', 'nganluong'),
(454, 'module', 'carousel1'),
(456, 'module', 'popup'),
(458, 'module', 'promo_banner'),
(460, 'feed', 'google_sitemap'),
(461, 'module', 'googleadsense'),
(462, 'module', 'special');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_description`
--

CREATE TABLE IF NOT EXISTS `filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_group`
--

CREATE TABLE IF NOT EXISTS `filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_group_description`
--

CREATE TABLE IF NOT EXISTS `filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `geo_zone`
--

CREATE TABLE IF NOT EXISTS `geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geo_zone`
--

INSERT INTO `geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2010-02-26 22:33:24', '2009-01-06 23:26:25'),
(4, 'UK Shipping', 'UK Shipping Zones', '2010-12-15 15:18:13', '2009-06-23 01:14:53'),
(5, 'Ho Chi Minh', 'Ho Chi Minh', '0000-00-00 00:00:00', '2015-07-17 15:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE IF NOT EXISTS `information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(7, 0, -1, 1),
(8, 0, 0, 1),
(9, 0, 0, 1),
(10, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `information_description`
--

CREATE TABLE IF NOT EXISTS `information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information_description`
--

INSERT INTO `information_description` (`information_id`, `language_id`, `title`, `description`) VALUES
(3, 1, 'Khuyến mãi ', '&lt;p&gt;Khuyến mãi&amp;nbsp;&lt;/p&gt;\r\n');
INSERT INTO `information_description` (`information_id`, `language_id`, `title`, `description`) VALUES
(7, 1, 'Danh sách Website chọn lọc', '&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;CÁC WEBSITE BÁN ĐỒ THỂ THAO:&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 14px; color: rgb(119, 119, 119); box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;Footlocker.com&amp;nbsp;&amp;nbsp;chuyên về các sản phẩm giày , phụ kiện thể thao&lt;/span&gt;&lt;span style=&quot;font-size: 14px; box-sizing: border-box; line-height: 16.1000003814697px; color: rgb(20, 24, 35);&quot;&gt;&amp;nbsp;:&amp;nbsp;&lt;span style=&quot;box-sizing: border-box; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;&quot;&gt;Basketball Shoes, Casual Shoes, Sneakers, Running Shoes - New Releases &amp;amp; Exclusive Styles from Jordan, Nike, adidas, Under Armour &amp;amp; more.&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); font-size: 14px; box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;Golflocker.com&amp;nbsp;chuyên về các sản phẩm chơi GOLF : Golf shoes, golf clothes, rainwear, apparel, golf shirts, golf gloves and more from FootJoy, PUMA, Nike, Adidas, Under Armour, Ecco&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); font-size: 14px; box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;Tennisware-house.com&amp;nbsp;chuyên về giày , quần áo , phụ kiện tenis&amp;nbsp; : Rackets, strings, balls, shoes and other tennis gear.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); font-size: 14px; box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;Zappos.com&amp;nbsp;chuyên về các sản phẩm leo núi, cắm trại ngoài trờiTHE NORTH FACE.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); font-size: 14px; box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;Prodirectrunning.com&amp;nbsp;chuyên về các sản phẩm running, gym : Great Offers on Running Shoes &amp;amp; Running Clothing with up to 65% off Leading Brands Asics, Nike, adidas, Brooks, Mizuno, Saucony &amp;amp; More&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); font-size: 14px; box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;Shoes.com&amp;nbsp;- website bán giày lớn nhất tại Mỹ&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 14px; color: rgb(119, 119, 119); box-sizing: border-box; line-height: 16.1000003814697px;&quot;&gt;6PM.com&amp;nbsp;là website bán giày và phụ kiện với giá tốt nhất trên thị trường : Free shipping and savings of up to 75% off top brands. Shop discounted shoes, clothing, accessories, and more&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;CÁC WEBSITE PHỤ KIỆN ÔTÔ, XE MÁY USA:&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; text-align: center; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ US APPLIANCE&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; text-align: center; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;&amp;nbsp;RA ĐỜI NĂM 1963 LÀ NHÀ BÁN LẺ ĐỒ GIA DỤNG CAO &amp;nbsp;CẤP LỚN NHẤT Ở MICHIGAN.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; text-align: center; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ US APPLIANCE&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; text-align: center; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;&amp;nbsp;RA ĐỜI NĂM 1963 LÀ NHÀ BÁN LẺ ĐỒ GIA DỤNG CAO &amp;nbsp;CẤP LỚN NHẤT Ở MICHIGAN.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+ AJ MADISON &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;là&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial, Tahoma; padding: 0px; margin: 0px; font-size: 12px; border: 0px; outline: 0px; vertical-align: baseline; color: rgb(51, 51, 51); line-height: 22px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;&quot;&gt;&amp;nbsp;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color:#808080;&quot;&gt;&lt;span style=&quot;line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; line-height: 22px; font-family: Arial !important;&quot;&gt;nhà cung cấp hàng đầu các sản phẩm gia dụng thương hiệu lớn với giá cả hợp lý.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ BOSCH HOME &lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;là nhà sản xuất đồ gia dụng lớn thứ 3 thế giới.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;+&amp;nbsp;&lt;/span&gt;JUST DEAL &lt;/span&gt;&lt;/span&gt;&lt;font style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px; box-sizing: border-box;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-size: 14px;&quot;&gt;nhà bán lẻ hàng đầu với các sản phẩm luôn được giảm giá từ &amp;nbsp;40% đến 90%.&amp;nbsp;&lt;/span&gt;&lt;/font&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ WAYFAIR &lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;thế giới đồ nội thất.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;Harley&amp;nbsp;DaVIDSON&amp;nbsp;là thương hiệu nổi tiếng về moto.&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;SOLOMOTO là cửa hàng online đầy đủ các thương hiệu nổi &amp;nbsp;tiếng về &amp;nbsp;phụ tùng cho moto.&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ ETRAILER&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;&amp;nbsp;phụ tùng phụ kiện ôtô chính hãng.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ AUTO ANYTHING &lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;&amp;nbsp;là cửa store online tập hợp nội thất các hãng &amp;nbsp;xe &amp;nbsp;hơi nổi tiếng.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;+ USAUTOPARTS &lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-family: arial, helvetica, sans-serif; font-size: 14px;&quot;&gt;&amp;nbsp;cung cấp phụ tùng, phụ kiện ôtô chính hãng.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;CÁC SIÊU THỊ ONLINE LỚN NHẤT TẠI MỸ:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;Amazon.com&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là một website nổi tiếng tại Mỹ, bán rất nhiều mặt hàng. Bạn có thể hoàn toàn yên tâm về độ tin cậy khi mua hàng tại đây.&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+ Cũng như&amp;nbsp;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;Amazon&lt;/strong&gt;,&amp;nbsp;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;eBay.com&lt;/strong&gt;&amp;nbsp;cũng là website bán hàng hàng đầu tại Mỹ với nhiều mặt hàng. Đặc biệt bạn có thể mua hàng bằng hình thức đấu giá.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+ Lại thêm một website uy tín nữa cho bạn.&amp;nbsp;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;Target.com&lt;/strong&gt;&amp;nbsp;có rất nhiều mặt hàng được bán tại đây.&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;&amp;nbsp; &amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+ Là một trong những website bán hàng trực tuyến hàng đâu, tại&amp;nbsp;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;Walmart.com&lt;/strong&gt;&amp;nbsp;bạn có thể tìm thấy nhiều sự lựa chọn cho việc mua hàng.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+ Đến với&amp;nbsp;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;Bestbuy.com&lt;/strong&gt;&amp;nbsp;bạn có thể tìm kiếm cho mình nhiều sản phẩm khác nhau, đặc biệt là các sản phẩm, thiết bị điện tử.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box; font-size: 14px;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+ Một sự lựa chọn tốt cho bạn khi muốn tìm kiếm các sản phẩm nội thất trong gia đình. Ngoài ra&amp;nbsp;&lt;strong style=&quot;box-sizing: border-box; font-family: Arial !important;&quot;&gt;Overstock.com&lt;/strong&gt;&amp;nbsp;cũng bán nhiều mặt hàng khác.&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(119, 119, 119); line-height: 17.142858505249px; font-size: 13px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;Costo.com&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;bán nhiều mặt hàng khác nhau, đặc biệt là các mặt hàng dành cho gia đình. Đây cũng là một website được nhiều người lựa chọn.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; font-family: Arial; margin-right: 0px; margin-left: 0px; font-size: 16px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;Zappos.com&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là một webstie mua hàng lớn và uy tín tại U.S.A với rất nhiều món hàng đa dạng.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;CÁC WEBSITE MỸ PHẨM VÀ NƯỚC HOA:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;&lt;span style=&quot;font-weight: bold; text-transform: uppercase; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;&lt;span style=&quot;box-sizing: border-box;&quot;&gt;&lt;font face=&quot;Arial&quot;&gt;+&lt;/font&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; box-sizing: border-box;&quot;&gt; Một sự lựa chọn tốt cho các chị em,&amp;nbsp;&lt;span style=&quot;box-sizing: border-box; color: rgb(0, 0, 255); font-family: Arial !important;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Sephora.com&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;bán nhiều loại mỹ phẩm, các loại kem dưỡng da...&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-weight: bold; text-transform: uppercase; font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-weight: bold; text-transform: uppercase; font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Lancome.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-weight: bold; text-transform: uppercase; font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cung cấp cho khách hàng các sản phẩm dưỡng da, nước hoa và trang điểm.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;YvesRocher.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là thương hiệu với những sản phẩm mỹ phẩm thiên nhiên có nguồn gốc từ thực vật, có rất nhiều sản phẩm làm đẹp tự nhiên cho các bạn tại đây. &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Một thương hiệu đã khẳng định được tên tuổi trên toàn thế giới, ngoài các sản phẩm nội y, tại&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Victorias Secret.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;còn cung cấp nhiều sản phẩm làm đẹp và nước hoa cao cấp.&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Eyeslipsface.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là một thương hiệu mỹ phẩm quốc tế có mặt tại nhiều nước,&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-family: Arial; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;E.L.F&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cung cấp nhiều dòng mỹ phẩm cao cấp, chất lượng với giá phải chăng.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;MACCosmetics.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;nổi tiếng với dòng trang điểm cao cấp và chuyên nghiệp. Ngoài các mỹ phẩm MAC Comestic còn cung cấp các phụ kiện make up &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Được nhiều người tiêu dùng lựa chọn,&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Drugstore.com&lt;/strong&gt;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;là website bán nhiều mặt hàng cho gia đình như thực phẩm chức năng, mỹ phẩm, các loại thuốc bổ...&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-family: Arial; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;Fragrancenet.com&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là website bán nhiều mặt hàng mỹ phẩm, đặc biệt là nước hoa.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Perfume.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;chuyên về nước hoa cho cả nam lẫn nữ, tại đây bạn có thể tìm được khá nhiều Sale Off từ shop.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Website&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Macys.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;tại Mỹ, chuyên bán các loại quần áo thời trang, phụ kiện, mỹ phẩm cho cả nam và nữ. &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Website&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Strawberrynet.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;chuyên về nước hoa tại Mỹ, web thường hay có những sự kiện Sale Off cho khách hàng. &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Như&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-family: Arial; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;Drugstore&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;,&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Walgreen.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là một website cho gia đình với các mặt hàng thực phẩm chức năng, thuốc bổ, mỹ phẩm và các máy dùng trong gia đình.&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Esteelauder.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là website chuyên cung cấp các loại mỹ phẩm, dụng cụ make up và nước hoa&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Rocskincare.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;chuyên cung cấp các loại sữa dưỡng thể, kem dưỡng da và nhiều loại mỹ phẩm khác&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Olay.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;website nổi tiếng với các loại kem dưỡng da, sữa dưỡng da.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;+&amp;nbsp;&lt;span style=&quot;box-sizing: border-box; color: rgb(0, 0, 255); font-family: Arial !important;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Dior.com&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;là website của hãng nước hoa nổi tiếng DIOR tại U.S.A, bên cạnh đó website còn cung cấp các &amp;nbsp;loại mỹ phẩm&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;..&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;CÁC WEBSITE THỜI TRANG VÀ GIÀY DÉP PHỤ KIỆN:&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; font-family: arial, helvetica, sans-serif;&quot;&gt;&amp;nbsp;&lt;span style=&quot;box-sizing: border-box; color: rgb(0, 0, 255); font-family: Arial !important;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Forever 21.com&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;bán rất nhiều mặt hàng quần áo, phụ kiện, giày dép thời trang cho nam và nữ. Website thường có những sự kiện Sale Off cho khách hàng. &amp;nbsp; &amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;HM.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;chuyên bán quần áo thời trang cho cả nam, nữ và trẻ em, đồng thời Shop còn cung cấp các sản phẩm trang trí trong nhà.&amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;6PM.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cung cấp các loại mặt hàng quần áo, phụ kiện, túi xách. &amp;nbsp; &amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Một thương hiệu đã khẳng định được tên tuổi trên toàn thế giới, ngoài các sản phẩm nội y, tại&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Victorias Secret.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;còn cung cấp nhiều sản phẩm làm đẹp và nước hoa cao cấp. &amp;nbsp; &amp;nbsp;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Oldnavy.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cung cấp mặt hàng quần áo, phụ kiện cho nam, nữ, trẻ em và các bé.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-family: Arial; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;&lt;span style=&quot;box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;Gap.com&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cung cấp mặt hàng quần áo, phụ kiện cho nam, nữ, trẻ em và các bé.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Bananarepublic.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cung cấp mặt hàng quần áo, phụ kiện cho nam, nữ, trẻ em và các bé.&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Website chuyên bán các loại quần áo, phụ kiên, giày dép cho nam và nữ. &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Abercrombie.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;một website bán các mặt hàng quần áo nổi tiếng tại Mỹ. &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Hollisterco.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là thương hiệu nổi tiếng được nhiều người lựa chọn, chuyên các mặt hàng quần áo cho nam và nữ.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Mango.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;chuyên cung cấp các mặt hàng quần áo, phụ kiện, giày dép, túi xách.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;NineWest.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;nổi tiếng với các mặt hàng phụ kiện, túi xách, giày dép, ngoài ra website thường có các sự kiện Sale Off cho khách hàng.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Nhãn hàng thời trang nổi tiếng&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;LACOSTE.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;với các mặt hàng thời trang cao cấp.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+ Trang web của hãng thời trang&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Levi.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;lừng danh và nổi tiếng trên toàn thế giới tại Mỹ.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Us.LouisVuitton.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là website của hãng thời trang nổi tiếng Louis Vuitton.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Gucci.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là website chính thức của hãng thời trang cao cấp GUCCI&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Nike.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là website của hãng đồ thể thao nổi tiếng thế giới NIKE tại U.S.A&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 14px; line-height: 17.142858505249px; box-sizing: border-box; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Converse.com&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 14px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;với nhiều mẫu mã quần áo, phụ kiện và giày dép thời trang của hãng CONVERSE&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;CÁC WEBSITE CHO MẸ VÀ BÉ:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&lt;/span&gt;&lt;span style=&quot;box-sizing: border-box;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Crazy 8&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là thương hiệu nổi tiếng với các mặt hàng quần áo, phụ kiện cho bé. &lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+Tương tự như&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px; box-sizing: border-box;&quot;&gt;Crazy 8&lt;/strong&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;,&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;box-sizing: border-box;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Gymboree&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;cũng là một thương hiệu nổi tiếng về các mặt hàng dành cho trẻ em.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;+&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;box-sizing: border-box;&quot;&gt;&lt;strong style=&quot;box-sizing: border-box;&quot;&gt;Zulily&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial; font-size: 13px; color: rgb(119, 119, 119); line-height: 17.142858505249px;&quot;&gt;&amp;nbsp;là website chuyên cung cấp các mặt hàng thời trang cho trẻ em.&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h1 class=&quot;heading&quot; style=&quot;box-sizing: border-box; margin-right: 0px; margin-left: 0px; font-weight: bold; line-height: 1.1; color: rgb(22, 107, 170); text-transform: uppercase; margin-top: 20px !important; margin-bottom: 10px !important;&quot;&gt;&amp;nbsp;&lt;/h1&gt;\r\n');
INSERT INTO `information_description` (`information_id`, `language_id`, `title`, `description`) VALUES
(8, 1, 'Hỗ trợ khởi nghiệp Kinh doanh Online', '&lt;p&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/logo/lo.png&quot; style=&quot;width: 311px; height: 130px; float: left;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:22px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;strong&gt;Kinh doanh Online-Xu hướng thời đại-Tại sao không thử?&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;Bill Gates đã phát biểu: “Từ 5 cho đến 10 năm nữa nếu Bạn không Kinh doanh thông qua Internet thì tốt hơn hết Bạn đừng nên Kinh doanh gì nữa…”&lt;/span&gt;&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;TRUMHANGMY&lt;/strong&gt;&lt;/span&gt; sẽ giúp bạn có tất cả công cụ để bắt đầu con đường Kinh doanh với chi phí rẻ nhất, nhưng hiệu quả cao nhất.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;strong&gt;Kinh doanh Online&lt;/strong&gt; đang là một trong những xu thế kinh doanh phổ biến nhất mà không kém phần hiệu quả. Từ những bạn Sinh viên, những bà nội trợ cho đến những công ty lớn, từ những bạn có vốn cho đến những bạn chập chững bước vào kinh doanh – &lt;strong&gt;Kinh doanh online&lt;/strong&gt; đều thích hợp.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;Bạn sẽ được gì với &lt;strong&gt;Kinh doanh Online&lt;/strong&gt;?&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;-&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;strong&gt;&lt;em&gt;Không tốn chi phí mặt bằng&lt;/em&gt;&lt;/strong&gt; – điều khiến các bạn mới bước vào kinh doanh e ngại nhất.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;-&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;strong&gt;&lt;em&gt;Không tốn chi phí nhân viên&lt;/em&gt;.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;-&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;strong&gt;&lt;em&gt;Không tốn thời gian quá nhiều&lt;/em&gt;&lt;/strong&gt;&lt;strong&gt;:&lt;/strong&gt; tất cả mọi đối tượng đều có thể kinh doanh online, từ những bạn sinh viên, bà nội trợ, các bạn đang đi làm cho đến những bạn có vốn ít nhưng không biết làm gì để kiếm tiền…&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;-&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;strong&gt;&lt;em&gt;Đưa sản phẩm đến với mọi đối tượng khách hàng&lt;/em&gt;&lt;/strong&gt;&lt;strong&gt; :&lt;/strong&gt; Khách hàng từ mọi nơi, mọi độ tuổi, mọi ngành nghề…đều có thể tìm đến với sản phẩm của các bạn.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;Chỉ cần tính nhẩm Bạn sẽ thấy bạn sẽ giảm bớt được bao nhiêu chi phí khi khởi nghiệp với &lt;strong&gt;Kinh Doanh Online&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;TRUMHANGMY&lt;/strong&gt;&lt;/span&gt; sẽ cung cấp cho bạn một trong những dịch vụ tốt nhất để bạn thực hiện đam mê Kinh doanh của mình:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;1. Một website Thương mại điện tử đầy đủ tất cả chức năng : Giới thiệu sản phẩm, Tin tức, Mua hàng trực tuyến, Tạo Khuyến mại, Coupon, Liên hệ, Theo dõi Google, SEO.. ( &lt;span style=&quot;color:#FFFF00;&quot;&gt;&lt;strong&gt;Trị giá 12 triệu&lt;/strong&gt;&lt;/span&gt; )&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;Trang tham khảo : www.trumhangmy.com&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;2. Hỗ trợ tạo và tăng Like,Sub cho Fanpage Facebook : 2000 Like ( &lt;span style=&quot;color:#FFFF00;&quot;&gt;&lt;strong&gt;Trị giá 4 triệu&lt;/strong&gt;&lt;/span&gt; )&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;3. Cung cấp nguồn hàng cho các bạn Kinh doanh (nguồn hàng từ Mỹ, có hóa đơn mua hàng, chứng từ đầy đủ) với giá cả cạnh tranh nhất, từ những thương hiệu tốt nhất cho phái đẹp : Victoria’s Secret, Bath and Body Works, Pink, Lancome, Loreal, M.A.C, NYX, Earth Science, tất cả các thương hiệu nước hoa và các sản phẩm chăm sóc sắc đẹp khác..Nguồn hàng sẽ được cập nhật mỗi tuần để đảm bảo đa dạng, phù hợp với xu hướng Khách hàng..&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&amp;nbsp;&lt;strong&gt;Các gói hỗ trợ thích hợp với nhu cầu và nguồn vốn các bạn có:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong&gt;10 triệu : &lt;/strong&gt;&lt;/span&gt;1 website (*) + 1 Fanpage FB 2000Like (**) (trị giá 15 triệu) + 5 triệu tiền sản phẩm (***): Lotion, Mỹ phẩm, Nước hoa giá sĩ + Hóa đơn mua hàng từ các Store bên Mỹ, cam kết hàng chính hãng 100% (Hỗ trợ 10 triệu)&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong&gt;20 triệu&lt;/strong&gt; :&lt;/span&gt; 10 triệu : 1 website (*) + 1 Fanpage FB 2000Like (**) (trị giá 15 triệu) + 16 triệu tiền sản phẩm: Lotion, Mỹ phẩm, Nước hoa giá sĩ + Hóa đơn mua hàng từ các Store bên Mỹ, cam kết hàng chính hãng 100% (Hỗ trợ 11 triệu)&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong&gt;30 triệu&lt;/strong&gt; :&lt;/span&gt; 1 website (*) + 1 Fanpage FB 2000Like (**) (trị giá 14 triệu) + 27 triệu tiền sản phẩm: Lotion, Mỹ phẩm, Nước hoa giá sĩ + Hóa đơn mua hàng từ các Store bên Mỹ, cam kết hàng chính hãng 100% (Hỗ trợ 12 triệu)&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;em&gt;&amp;nbsp;(*) : Website tương tự &lt;a href=&quot;http://www.trumhangmy.com/&quot;&gt;www.trumhangmy.com&lt;/a&gt;, mỗi năm các bạn chỉ phải đóng phí duy trì Domain + Hosting là 1 triệu 5, Các bạn không được quyền nhân bản hay copy source web cho bên thứ 3 mà chưa được sự đồng ý của TrumHangMy. Thời gian setup website khoảng 5-7 ngày&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;em&gt;(**)Like thật, không phải like ảo, Thời gian tăng đến 2000 Like khoảng 2 tuần. Shop sẽ hỗ trợ các bạn cách tăng Sub,Like để các bạn tiếp tục phát triển.&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;em&gt;(***) Hàng TrumHangMy cung cấp là hàng có chứng từ, hóa đơn từ Mỹ, đảm bảo hàng Mỹ 100%..Nếu các bạn phát hiện hàng giả TrumHangMy sẽ đền bù gấp 10 lần giá trị hàng hóa.&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;u&gt;&lt;strong&gt;Lưu ý:&lt;/strong&gt;&lt;/u&gt;&lt;/span&gt; Khi sử dụng gói hỗ trợ này, để đảm bảo chất lượng hàng hóa các bạn không được cung cấp hàng của đối tác thứ 3 khi chưa được sự cho phép của chúng tôi.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;strong&gt;Nếu đam mê Kinh doanh, có 1 số vốn ít mà vẫn đang loay hoay không biết phải làm gì, hoặc muốn tìm kiếm nguồn hàng tốt để Kinh doanh, Tìm 1 Nhà cung cấp Chất lượng thì tại sao không bắt đầu với TRUMHANGMY ngay bây giờ? &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;strong&gt;Chúng tôi cam kết hỗ trợ các bạn tối đa nếu các bạn có đam mê với công việc Kinh doanh Online.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:22px;&quot;&gt;&lt;span style=&quot;font-family:times new roman,times,serif;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;background-color:#FFFF00;&quot;&gt;Hãy là Người tiên phong – Bạn sẽ là Người thành công !!!&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n'),
(9, 1, 'Tưng bừng Khai trương - Giáng Sinh và Năm mới 2015', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;Chào mừng khai trương, cùng đó là lễ Giáng Sinh và Tết 2015, Shop TRÙM HÀNG MỸ xin trân trọng gửi tới Quý khách hàng chương trình khuyến mãi đặc biệt &lt;strong&gt;“ Mua hàng bằng LIKE – Bao nhiêu LIKE giảm bấy nhiêu tiền”&lt;/strong&gt;&lt;/span&gt;&lt;br style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;&lt;strong&gt;Thời gian Share và tính Like:&lt;/strong&gt; từ ngày 24/12/2014 đến hết ngày 01/01/2015)&lt;/span&gt;&lt;br style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;&lt;strong&gt;Đối tượng tham gia :&lt;/strong&gt; Tất cả các khách hàng có Facebook.&lt;/span&gt;&lt;br style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;+Bước 1: Like fanpage TRÙM HÀNG MỸ :&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;a href=&quot;https://www.facebook.com/trumhangusa&quot; rel=&quot;nofollow&quot; style=&quot;color: rgb(59, 89, 152); cursor: pointer; text-decoration: none; font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; font-size: 13px; line-height: 18px;&quot;&gt;https://www.facebook.com/&lt;span class=&quot;text_exposed_show&quot; style=&quot;display: inline;&quot;&gt;trumhangusa&lt;/span&gt;&lt;/a&gt;&lt;/span&gt;&lt;br /&gt;\r\n&lt;span class=&quot;text_exposed_show&quot; style=&quot;display: inline; color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;+Bước 2: Share 1 sản phẩm bất kỳ mà bạn mua của TRÙM HÀNG MỸ hoặc Fanpage Trùm Hàng Mỹ lên trang cá nhân của các bạn. Sau đó Inbox cho Shop link FB của bạn đã Share, Họ tên của các bạn.&lt;br /&gt;\r\n+Bước 3: Mời bạn bè like. Bạn thu hút được bao nhiêu like trong bài share thì sẽ được giảm giá với số tiền tương đương 1 like = 500đ&lt;br /&gt;\r\n+Bước 4: Sau 24h ngày 01/01/2015 Inbox hoặc nt cho shop vs cú pháp &lt;strong&gt;“ TÊN SẢN PHẨM – SỐ LƯỢT LIKE – THÔNG TIN: TÊN, ĐỊA CHỈ, ĐIỆN THOẠI NGÀY GIỜ GIAO HÀNG- LINK BÀI SHARE”&lt;/strong&gt; để shop kiểm tra và giao hàng (Chậm nhất là 24h ngày 02/01/2015)&lt;/span&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span class=&quot;text_exposed_show&quot; style=&quot;display: inline; font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;&lt;strong&gt;***Điều kiện áp dụng:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;br /&gt;\r\n&lt;span class=&quot;text_exposed_show&quot; style=&quot;display: inline; color: rgb(20, 24, 35); font-family: Helvetica, Arial, ''lucida grande'', tahoma, verdana, arial, sans-serif; line-height: 18px;&quot;&gt;- Bài share phải để chế độ public&lt;br /&gt;\r\n- Lượt like phải là lượt like thật. Người like phải nằm trong friend list của người tham gia.&lt;br /&gt;\r\n- Áp dụng cho tất cả các sản phẩm tại shop TRÙM HÀNG MỸ&amp;nbsp;&lt;br /&gt;\r\n- Áp dụng từ sản phẩm thứ 2 trở đi.&lt;br /&gt;\r\n- Chương trình không áp dụng chung với các Chương trình Khuyến mãi khác của Shop.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n'),
(4, 1, 'Hướng Dẫn Mua  Hàng', '&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;line-height: 1.6em;&quot;&gt;Cách 1: Đặt hàng offline :&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;B1: Chọn sản phẩm&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;B2:&amp;nbsp;Chuyển khoản cho shop số tiền của sản phẩm&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;&lt;span style=&quot;font-size: 13px; line-height: 20.7999992370605px;&quot;&gt;B3:&amp;nbsp;&lt;/span&gt;Điện thoại hoặc nhắn tin cho SHOP:&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;SẢN PHẨM BẠN CHỌN - NGÂN HÀNG – HỌ TÊN – ĐỊA CHỈ - NGÀY GIAO HÀNG&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;(Tiền Ship sẽ thanh toán khi nhận hàng)&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;Cách 2: Đặt hàng online :&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;B1: Bạn đăng nhập vào website:&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;B2: Chọn món hàng bạn cần -&amp;gt; Thêm vào giỏ -&amp;gt; Thanh toán -&amp;gt; Đăng nhập (Hoặc đăng ký nếu bạn chưa là thành viên) -&amp;gt; Điền thông tin đầy đủ vào mẫu -&amp;gt; Chọn một trong 2 hình thức (Giao hàng nhận tiền hoặc Thanh toán qua Ngân lượng)&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;&lt;strong&gt;+Shop sẽ xác nhận đơn hàng của bạn và tiến hành giao hàng.&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;hr /&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;em&gt;&lt;strong&gt;***Hướng dẫn thanh toán bằng Ngân lượng:&lt;/strong&gt;&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p&gt;http://help.nganluong.vn/chi-tiet-83/2/433/Huong-dan-thanh-toan-truc-tuyen.html&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;***Thông tin Ngân hàng:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot;&gt;\r\n&lt;p&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: green; font-family: Arial !important; font-style: normal !important;&quot;&gt;VIETCOMBANK : 0441003735055&lt;/span&gt;&amp;nbsp;HỨA TRỌNG HIẾU CN TÂN BÌNH&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot;&gt;\r\n&lt;p&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: blue; font-family: Arial !important; font-style: normal !important;&quot;&gt;SACOMBANK&amp;nbsp;&amp;nbsp; &amp;nbsp;: 060061313454&lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;HỨA TRỌNG HIẾU PGD GÒ VẤP&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot;&gt;\r\n&lt;p&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: cyan; font-family: Arial !important; font-style: normal !important;&quot;&gt;ACB&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;: 143867749&lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; HỨA TRỌNG HIẾU PGD GÒ VẤP&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot;&gt;\r\n&lt;p&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: mediumblue; font-family: Arial !important; font-style: normal !important;&quot;&gt;ĐÔNG Á&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;: 0103816446&lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; HỨA TRONG HIẾU PGD GÒ VẤP&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;&amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n'),
(5, 1, 'NHẬN ORDER VÀ SHIP HÀNG MỸ TỪ MỸ VỀ VIỆT NAM', '&lt;p&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/logo/lo.png&quot; style=&quot;width: 311px; height: 130px; float: left;&quot; /&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/banner/shipping.png&quot; style=&quot;opacity: 0.9; line-height: 20.7999992370605px; width: 435px; height: 260px; float: right;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp; &amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;meta content=&quot;Trong thời gian gần đây, dịch vụ mua hàng tại mỹ đang nở rộ hơn bao giờ hết. Nắm bắt tình hình đó Trumhangmy có cung cấp dịch vụ nhận order, ship hàng từ mỹ với giá cạnh tranh, thời gian nhanh…cùng phương châm khách hàng là thượng đế... 0933383093&quot; name=&quot;description&quot; /&gt;\r\n&lt;h1&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;em&gt;Trong thời gian gần đây, dịch vụ mua hàng tại mỹ đang nở rộ hơn bao giờ hết. Nắm bắt tình hình đó&amp;nbsp;&lt;/em&gt;&lt;a href=&quot;http://trumhangmy.com&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong&gt;&lt;em&gt;Trumhang&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;&lt;em&gt;my&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/a&gt;&amp;nbsp;&lt;em&gt;có cung cấp dịch vụ nhận order, ship hàng từ web mỹ&amp;nbsp;với giá cạnh tranh, thời gian nhanh…cùng phương châm khách hàng là thượng đế, và sau đây shop xin gởi tới các bạn bảng giá mua hàng từ mỹ để các bạn dễ dàng hợp tác.&lt;/em&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;A. Cách tính tiền:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;em&gt;Công thức :&amp;nbsp;&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;&lt;em&gt;Total&amp;nbsp;= Giá USD web * (1)Tỷ giá * (2)Công mua + (3)Thuế và Ship Mỹ nếu có + (4)Ship về VN.&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FFFF00;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;em&gt;&lt;span style=&quot;background-color:#FF0000;&quot;&gt;***Lưu ý***&lt;/span&gt;&lt;/em&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color:#FFFF00;&quot;&gt;&lt;span style=&quot;background-color:#000000;&quot;&gt;Số tiền đặt cọc chỉ là tiền hàng và công mua. Tiền ship hàng từ Mỹ về VN chỉ có thể tính chính xác khi hàng về VN và shop cân Kg món hàng (hoặc các bạn biết chính xác hàng các bạn bao nhiêu kg).&amp;nbsp;Vì thế Shop không thể báo chính xác 100% hàng về đến VN là bao nhiêu tiền được. Mong các KH lưu ý giúp.&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;em&gt;1/ Tỷ giá chuyển từ VND sang USD theo VISA quốc tế quy định :&amp;nbsp;22.600&amp;nbsp;(Từ 24/07/15-31/07/2015)&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;em&gt;2/&amp;nbsp;Công mua&amp;nbsp;:&amp;nbsp;5%&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;em&gt;3/&amp;nbsp;Thuế tại mỹ và phí vận chuyển nội địa Mỹ:&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Hai phí trên là do web tự động thông báo khi check out tới bước cuối cùng khi có nhu cầu mua hàng online mà không cần shop can thiệp vào. Trừ trường hợp web thu tax sau khi order , thì shop sẽ thông báo cho các bạn sau khi nhận được thông báo từ web.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em style=&quot;color: rgb(255, 140, 0); font-size: 18px; line-height: 1.6em;&quot;&gt;4/ Ship về VN:&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong style=&quot;font-size: 18px; line-height: 1.6em;&quot;&gt;I.&amp;nbsp;Không phụ thu&amp;nbsp;(hàng về cân kg tính tiền: cân nặng bao nhiêu tính bấy nhiêu)&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+&amp;nbsp;Quần áo &amp;nbsp; &amp;nbsp; :&amp;nbsp;6/lbs&amp;nbsp;~ 12$/kg&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+&amp;nbsp;Thuốc &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; : &amp;nbsp;6$/lbs ~ 12$/kg&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+&amp;nbsp;Mỹ phẩm &amp;nbsp; &amp;nbsp; :&amp;nbsp;7$/lbs ~ 14$/kg&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+&amp;nbsp;Nước hoa &amp;nbsp;:&amp;nbsp;7$/lbs ~ 14$/kg&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;II. Có&amp;nbsp;phụ thu (vẫn tính khối lượng 12$/kg&amp;nbsp;+ phụ thu)&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+ Đồng hồ&amp;nbsp;: 5%/cái&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+ Laptop – Điện thoại&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&amp;nbsp;Old 40$- New 45$/cái&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+ Túi xách &amp;nbsp;: 5%/cái&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+ Giày : 5%/cái&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+ Mắt kính : 5%/cái&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;+&amp;nbsp;Trang sức&amp;nbsp;hoặc son: 1$/cái&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;em&gt;5/ Free shipping HCM và chính sách vận chuyển nội địa VN:&lt;/em&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Shop sẽ freeship nội thành tp.hcm cho các bạn vào thứ 7 và cn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Các bạn ở tỉnh sẽ nhận hàng tại địa chỉ shop hoặc shop sẽ ship ra đến bưu điện hoặc các nhà xe để chuyển hàng về tận nhà cho các bạn (chi phí cho bưu điện hoặc xe các bạn chi trả)&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;B. Lưu ý đặt hàng:&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 18px; line-height: 1.6em;&quot;&gt;- Không nhận cancel, đổi trả hàng sau khi đã được order.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Không nhận order hàng với trường hợp chưa đặt cọc&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size: 18px; line-height: 1.6em;&quot;&gt;- Các bạn vui lòng ghi rõ cần order sản phẩm màu gì, kích cỡ bao nhiêu&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color:#FFD700;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;background-color:#FF0000;&quot;&gt;Quy trình order:&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;Download&lt;strong&gt; File Tính tiền&lt;/strong&gt;&amp;nbsp;tại đây &lt;a href=&quot;https://dl.dropboxusercontent.com/u/103599779/Shipping.xlsx&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;https://dl.dropboxusercontent.com/u/103599779/Download.gif&quot; style=&quot;width: 200px; border-width: 0px; border-style: solid; height: 62px;&quot; /&gt;&lt;/a&gt;&amp;nbsp;&amp;nbsp;-&amp;gt; Điền thông tin trong file yêu cầu -&amp;gt; File tính giá -&amp;gt;&amp;nbsp;Gửi lại File cho shop theo email :&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 18px; line-height: 1.6em;&quot;&gt;trumhangmy@gmail.com -&amp;gt; Chuyển khoản (Số tài khoản bên dưới)&amp;nbsp;-&amp;gt; SMS cho Shop 098.3035.311 báo đã chuyển khoản -&amp;gt;&amp;nbsp;Shop Order và thông báo ngày hàng về.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Shop báo giá trong vòng 24h từ khi nhận được thông tin của các bạn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Shop sẽ chốt order vào 12h trưa thứ 2 và thư 6 hàng tuần.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;C. Thanh toán:&lt;/strong&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Thanh toán 100% giá trị món hàng. &lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Tiền Ship về VN sẽ thanh toán khi nhận hang tại VN.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;&lt;em&gt;D. Thời gian hàng về:&lt;/em&gt;&lt;/strong&gt;&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Thời gian hàng về khoản 10-14 ngày (không tình thời gian web giao tới đại lý shop mở tại usa) hoặc 3 tuần kể từ lúc quý khách đặt hàng nhưng không quá 30 ngày.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Không tính ngày ngày thứ 7 và cn theo giờ địa phương ở mỹ và các sự cố khác như delay chuyến bay, web tạm thời hết hàng, bão tuyết và những lý do khách quan khác từ phía đối tác fedex, ups, usps, dhl,...&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;- Nếu quá 30 ngày theo như cam kết trên, shop xin hoàn lại tiền mua hàng cho quý khách hoặc sẽ thỏa thuận 1 số ưu đãi cho quý khách.&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;E. Thông tin chuyển khoản:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;br /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot; style=&quot;color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;&quot;&gt;\r\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px;&quot;&gt;&lt;span style=&quot;color:#008000;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: green; font-family: Arial !important; font-style: normal !important;&quot;&gt;VIETCOMBANK : 0441003735055&lt;/span&gt;&amp;nbsp;HỨA TRỌNG HIẾU CN TÂN BÌNH&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot; style=&quot;color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;&quot;&gt;\r\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px;&quot;&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: blue; font-family: Arial !important; font-style: normal !important;&quot;&gt;SACOMBANK&amp;nbsp;&amp;nbsp; &amp;nbsp;: 060061313454&lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;HỨA TRỌNG HIẾU PGD GÒ VẤP&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot; style=&quot;color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;&quot;&gt;\r\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px;&quot;&gt;&lt;span style=&quot;color:#33ccff;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: cyan; font-family: Arial !important; font-style: normal !important;&quot;&gt;ACB&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;: 143867749&lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; HỨA TRỌNG HIẾU PGD GÒ VẤP&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot; style=&quot;color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;&quot;&gt;\r\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px;&quot;&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;font-family: Arial !important; font-size: 18px; font-style: normal !important;&quot;&gt;&lt;strong data-blogger-escaped-style=&quot;font-family: Arial !important; font-style: normal !important;&quot;&gt;&lt;span data-blogger-escaped-style=&quot;color: mediumblue; font-family: Arial !important; font-style: normal !important;&quot;&gt;ĐÔNG Á&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;: 0103816446&lt;/span&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; HỨA TRONG HIẾU PGD GÒ VẤP&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div data-blogger-escaped-style=&quot;-webkit-text-stroke-width: 0px; background-color: white; color: black; font-family: Arial; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; margin-bottom: 20px; margin-top: 0px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px;&quot; style=&quot;color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;&quot;&gt;\r\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px;&quot;&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n'),
(10, 1, 'Tuyển cộng tác viên và đại lý bán hàng Mỹ', '&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Nhằm mở rộng quy mô kinh doanh, Trùm Hàng Mỹ tuyển các CTV và Đại Lý bán hàng trên cả nước.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Đây là cơ hội dành cho các bạn trẻ thích kinh doanh nhưng không có nguồn vốn mạnh.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;Giới thiệu:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Trùm Hàng Mỹ chuyên cung cấp các sản phẩm nhập trực tiếp và xách tay từ Mỹ như : Nước hoa, Mỹ phẩm, Hàng tiêu dùng, Thực phẩm chức năng...Và nhận order hàng trên toàn bộ các website của Mỹ về Việt Nam.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Chúng tôi đảm bảo nguồn hàng Mỹ chính hãng ,chất lượng. Nói không với hàng Fake và hàng Trung Quốc.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;Điều kiện:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;+ Có Facebook trên 4000 friend/ follow.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;+ Đăng 10 sản phẩm của Trùm Hàng Mỹ trên trang cá nhân hoặc Fanpage.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;hoặc (Nếu không đủ 1 trong 2 điều kiện trên)&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;+ Lấy trước 5 sản phẩm bất kỳ của Shop.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;&lt;strong&gt;Quyền lợi khi làm Đại lý hoặc CTV của Trùm Hàng Mỹ:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;+ &lt;span style=&quot;line-height: 20.8px;&quot;&gt;Không phải ôm hàng:&amp;nbsp;&lt;/span&gt;Các bạn sẽ được giá sĩ dù chỉ order một sản phẩm (Tất nhiên giá sẽ cao hơn 1 xíu so với khi các bạn bỏ vốn ra lấy hàng)&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;+ Được hỗ trợ khởi tạo một Website bán hàng tương tự trumhangmy.com với giá chỉ 2triệu3 (Không bắt buộc). Tư vấn và hướng dẫn cách SEO 1 website lên TOP Google.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;+ Được hướng dẫn tạo Fanpage và chạy quảng cáo FB với giá rẻ nhất, hiệu quả nhất.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:20px;&quot;&gt;Liên hệ:&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-size: 20px;&quot;&gt;trumhangmy@gmail.com&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-size: 20px;&quot;&gt;FB.com/huatronghieu&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-size: 20px;&quot;&gt;0938088893 / 0983035311&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `information_to_layout`
--

CREATE TABLE IF NOT EXISTS `information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `information_to_store`
--

CREATE TABLE IF NOT EXISTS `information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `information_to_store`
--

INSERT INTO `information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `filename` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `filename`, `sort_order`, `status`) VALUES
(1, 'English', 'en', 'en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `layout`
--

CREATE TABLE IF NOT EXISTS `layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `layout`
--

INSERT INTO `layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information');

-- --------------------------------------------------------

--
-- Table structure for table `layout_route`
--

CREATE TABLE IF NOT EXISTS `layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `layout_route`
--

INSERT INTO `layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(30, 6, 0, 'account'),
(17, 10, 0, 'affiliate/'),
(29, 3, 0, 'product/category'),
(26, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(22, 5, 0, 'product/manufacturer'),
(23, 7, 0, 'checkout/'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap');

-- --------------------------------------------------------

--
-- Table structure for table `length_class`
--

CREATE TABLE IF NOT EXISTS `length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `length_class`
--

INSERT INTO `length_class` (`length_class_id`, `value`) VALUES
(1, 1.00000000),
(2, 10.00000000),
(3, 0.39370000);

-- --------------------------------------------------------

--
-- Table structure for table `length_class_description`
--

CREATE TABLE IF NOT EXISTS `length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `length_class_description`
--

INSERT INTO `length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(16, 'Earth Science', '', 0),
(12, 'Bath&amp;Body Works ', '', 2),
(13, 'Pink', '', 3),
(14, 'Clarins', '', 0),
(15, 'Neutrogena', '', 0),
(11, 'Victoria''s Secret', '', 1),
(17, 'Minerals', '', 0),
(18, 'NYX', '', 0),
(19, 'Loreal ', '', 0),
(20, 'Blue Lizard', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer_to_store`
--

CREATE TABLE IF NOT EXISTS `manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturer_to_store`
--

INSERT INTO `manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date_available` date NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `comment` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `image`, `date_available`, `sort_order`, `comment`, `approved`, `status`, `date_added`, `date_modified`, `viewed`) VALUES
(9, 'data/Tin tuc/Duong da/Anh-5.jpg', '2014-11-09', 1, 1, 1, 1, '2014-11-10 06:14:48', '2015-01-13 18:29:40', 7693),
(10, 'data/Tin tuc/Duong da/dap-mat-na-tranh.jpg', '2014-11-09', 1, 1, 1, 1, '2014-11-10 06:25:52', '0000-00-00 00:00:00', 7044),
(11, 'data/Tin tuc/Duong da/23-7e817.jpg', '2014-11-09', 1, 1, 1, 1, '2014-11-10 08:55:41', '2014-11-10 17:35:07', 6871),
(12, 'data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-3.jpg', '2014-11-15', 1, 1, 1, 1, '2014-11-16 08:46:32', '2015-03-22 23:33:50', 6538),
(13, 'data/Tin tuc/Duong toc/bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc-4.jpg', '2014-11-17', 1, 1, 1, 1, '2014-11-18 09:00:13', '2015-05-03 23:00:33', 4485),
(21, 'data/Tin tuc/hugh-jackman-26a.jpg', '2015-05-10', 1, 1, 1, 1, '2015-05-10 21:16:24', '2016-04-28 15:51:40', 1858),
(15, 'data/Tin tuc/loi-chuc-8-3-danh-cho-vo-hay-va-y-nghia.jpg', '2015-03-07', 1, 1, 1, 1, '2015-03-07 20:18:04', '2016-04-28 15:56:01', 2832),
(16, 'data/Tin tuc/qua-tang-8-3-12.jpg', '2015-03-07', 1, 1, 1, 1, '2015-03-07 20:20:48', '2016-04-28 15:55:26', 2878),
(17, 'data/Tin tuc/i-love-mom.jpg', '2015-03-07', 1, 1, 1, 1, '2015-03-07 20:23:06', '2016-04-28 15:52:37', 2923),
(22, 'data/Tin tuc/Duong da/22-7e817.jpg', '2015-05-11', 1, 1, 1, 1, '2015-05-12 02:21:42', '2016-04-28 15:46:09', 1325);

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE IF NOT EXISTS `news_category` (
  `news_category_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(4) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`news_category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(9, '', 0, 1, 1, 2, 1, '2014-11-03 17:48:26', '2014-11-10 18:58:49'),
(7, '', 0, 1, 1, 0, 1, '2014-10-31 10:59:06', '2015-03-20 00:57:43'),
(8, 'data/23-6-15 hinh dai dien tin tuc/Duong-Da.png', 7, 1, 1, 0, 1, '2014-10-31 10:59:33', '2015-06-23 11:42:33'),
(10, 'data/23-6-15 hinh dai dien tin tuc/Duong-Toc.png', 7, 1, 1, 1, 1, '2014-11-10 17:40:52', '2015-06-23 11:43:10'),
(11, 'data/23-6-15 hinh dai dien tin tuc/Dang-Dep.png', 7, 1, 1, 2, 1, '2014-11-18 07:16:32', '2015-06-23 11:41:17'),
(12, 'data/23-6-15 hinh dai dien tin tuc/MPTD.png', 7, 1, 1, 3, 1, '2014-11-18 07:34:46', '2015-06-23 11:55:38'),
(13, 'data/23-6-15 hinh dai dien tin tuc/PTTM.png', 7, 1, 1, 4, 1, '2014-11-18 07:58:28', '2015-06-23 11:56:27'),
(14, '', 9, 0, 1, 0, 1, '2014-11-18 08:19:27', '2015-06-23 15:01:22'),
(15, '', 9, 0, 1, 1, 1, '2014-11-18 08:22:55', '2014-11-18 08:22:55'),
(16, 'data/23-6-15 hinh dai dien tin tuc/Giam-Can.png', 7, 1, 1, 0, 1, '2015-01-12 12:17:01', '2015-06-23 11:54:27'),
(17, '', 0, 1, 1, 5, 1, '2015-03-07 20:11:01', '2015-03-07 20:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `news_category_description`
--

CREATE TABLE IF NOT EXISTS `news_category_description` (
  `news_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_category_description`
--

INSERT INTO `news_category_description` (`news_category_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`) VALUES
(9, 1, 'THỜI TRANG', '', '', ''),
(7, 1, 'LÀM ĐẸP', '', 'Hướng dẫn, chia sẻ các phương pháp làm đẹp cho bạn gái như: phương pháp làm đẹp cho da,tóc..', ''),
(8, 1, 'Dưỡng da - Làm đẹp da', '', 'Trang thông tin về làm đẹp da cho phụ nữ, chia sẻ kinh nghiệm làm đẹp và bảo vệ da,chia sẻ các sản phẩm làm đẹp da, chia sẻ các shop bán mỹ phẩm chăm sóc da uy tín,', 'sửa rửa mặt,rửa mặt đúng cách,làm đẹp da,hướng dẫn rửa mặt,rửa mặt ,bảo vệ da,vệ sinh da,hướng dẫn sử dụng kem chống nắng,kem chống nắng,toner,dưỡng ẩm,dưỡng da,tẩy tế bào chết,serum,lotion,cream,cân bằng da,trắng da,chống lão hóa da,giảm nếp nhăn,đặc trị'),
(10, 1, 'Dưỡng tóc - Làm đẹp tóc', '', 'Trang thông tin về làm đẹp tóc cho phụ nữ, chia sẻ kinh nghiệm làm đẹp và bảo vệ tóc,chia sẻ các sản phẩm làm đẹp tóc, chia sẻ các shop bán mỹ phẩm chăm sóc tóc uy tín,', 'dưỡng tóc,làm đẹp tóc,tóc suông dài,tóc mềm,tóc đen,xã tóc,uốn tóc,duỗi tóc,sấy tóc,gội đầu,ủ tóc,kem ủ tóc,sản phẩm tóc,chăm sóc tóc,nối tốc,hấp dầu,'),
(11, 1, 'Dáng đẹp', '', '', ''),
(12, 1, 'Mỹ phẩm - trang điểm', '', 'cung cấp các thông tin về mỹ phẩm và trang điểm mới nhất dành cho phái đẹp', 'mỹ phẩm,trang điểm,hướng dẫn dùng mỹ phẩm,hướng dẫn trang điểm,mẹo trang điểm'),
(13, 1, 'Phẫu thuật thẩm mỹ', '', 'Cung cấp thông tin về phẩu thuật thẩm mỹ mới nhất, hiện đại, an toàn nhất', 'phẫu thuật thẩm mỹ,nâng mũi, cắt mắt,xăm môi,xăm chân mày,thẩm mỹ hàn quốc'),
(14, 1, 'Nội y', '', 'Các bài viết giới thiệu, tư  vấn chọn lựa nội y phù hợp với chị em phụ nữ,', 'Đồ lót,quần lót,áo ngực,nội y,đồ ngủ,đồ lót sexy,đồ ngủ sexy,gợi cảm'),
(15, 1, 'Phụ kiện', '', 'Bài viết về các trang sức, phụ kiện thời trang, hướng dẫn phối phụ kiện phù hợp trang phục', 'phụ kiện thời trang,lắc tay,vòng cổ,nhẫn,dây chuyền,dây nịt,thắt lưng,giỏ xách,túi xách,giày,dép,ví,bóp,nón,mắt kính,đồng hồ'),
(16, 1, 'Giảm cân', '', 'chia sẻ,cung cấp các phương pháp giảm cân an toàn,các sản phẩm hỗ trợ giảm cân an toàn,hiệu quả nhập từ Mỹ', ''),
(17, 1, 'TIN XU HƯỚNG', '', 'tin mới về phụ nữ,thời trang,làm đẹp được cập nhật liên tục', '');

-- --------------------------------------------------------

--
-- Table structure for table `news_category_to_layout`
--

CREATE TABLE IF NOT EXISTS `news_category_to_layout` (
  `news_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `news_category_to_store`
--

CREATE TABLE IF NOT EXISTS `news_category_to_store` (
  `news_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_category_to_store`
--

INSERT INTO `news_category_to_store` (`news_category_id`, `store_id`) VALUES
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_comment`
--

CREATE TABLE IF NOT EXISTS `news_comment` (
  `comment_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` varchar(96) COLLATE utf8_bin NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `news_description`
--

CREATE TABLE IF NOT EXISTS `news_description` (
  `news_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `short_description` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_description`
--

INSERT INTO `news_description` (`news_id`, `language_id`, `name`, `short_description`, `description`, `meta_description`, `meta_keyword`) VALUES
(9, 1, 'Hướng dẫn sử dụng sữa rửa mặt đúng cách', 'Sữa rửa mặt sẽ phản tác dụng và tác động xấu đến làn da bạn nếu bạn không biết sử dụng đúng cách!\r\nSữa rửa mặt là một trong những loại mỹ phẩm thông dụng và được sử dụng nhiều nhất trong các sản phẩm chăm sóc sắc đẹp. Nó không chỉ tác động trực tiếp đến da mà còn ảnh hưởng đến sức sống lâu dài của da bạn...', '&lt;p&gt;&amp;nbsp;&lt;strong style=&quot;text-align: justify; font-family: Arial; line-height: 18px; font-size: 16px; color: rgb(255, 0, 0);&quot;&gt;1.Lựa chọn sản phẩm phù hợp:&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px;&quot;&gt;\r\n&lt;div style=&quot;text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Hiện nay, đa số chị em đã ít nhiều biết và lưu tâm đến điều này khi chọn sửa rửa mặt cho mình. Đây là một điều tối thiểu cần biết và là điều đầu tiên bạn cần áp dụng cho mỗi sản phẩm cần mua: Lựa chọn sản phẩm thích hợp.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Các hãng mỹ phẩm liên tục tung ra các dòng sản phẩm mới, đa dạng và phù hợp với các loại da khác nhau: da nhờn, da khô, da tổng hợp và da thường vì vậy không quá khó khăn để bạn lựa chọn sản phẩm theo đúng làn da của mình.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;text-align: justify;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Tuy nhiên, với dòng sản phẩm sữa rửa mặt có chứa các hạt li ti, khi sử dụng sẽ có nguy cơ làm rách da và phá vỡ cấu trúc da nếu bạn không đánh bọt sản phẩm trước khi sử dụng và điều này cần đặc biệt lưu ý đối với làn da mỏng, nhạy cảm.&lt;/span&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n&amp;nbsp;\r\n\r\n&lt;div&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;2. Cách sử dụng&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Bạn vẫn thường lấy một lượng bất kỳ và thoa lung tung lên mặt, sau đó rửa sạch lại với nước, bạn vẫn làm thế hàng ngày đúng không nào? Đây là cách làm chưa đúng! Hãy tham khảo các bước sau để tối ưu tác dụng của sữa rửa mặt:&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Bước 1:&lt;/strong&gt;&amp;nbsp;Làm ướt da mặt.&lt;/span&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/fdfs.jpg&quot; style=&quot;width: 490px; height: 416px;&quot; /&gt;&lt;/div&gt;\r\n&amp;nbsp;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Bước 2:&lt;/strong&gt;&amp;nbsp;Lấy một lượng sữa rửa mặt bằng hạt ngô, cho ra tay hoặc miếng tạo bọt đánh bông.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Anh-3.jpg&quot; style=&quot;opacity: 0.9; width: 321px; height: 402px;&quot; /&gt;&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;\r\n&lt;p style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Bước 3:&amp;nbsp;&lt;/strong&gt;Lấy phần bọt thoa đều nhẹ nhàng lên khuôn mặt và lưu ý thoa xuôi theo chiều lỗ chân lông để chất bẩn dễ dàng theo đà bị quấn ra ngoài. Thời gian khoảng 20-30 giây, bạn không nên để sữa lâu quá trên da mặt.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Bước 4:&lt;/strong&gt;&amp;nbsp;Rửa sạch với nước và thấm khô da bằng khăn mềm&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/u9sa5.jpg&quot; style=&quot;width: 405px; height: 480px;&quot; /&gt;&lt;/div&gt;\r\n&amp;nbsp;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Lưu ý:&amp;nbsp;&lt;/strong&gt;dùng sữa rửa mặt quá 2 lần/ngày gây tác động xấu cho da. Rửa mặt trước khi đi ngủ là vô cùng quan trọng, tạo điều kiện thông thoáng và tái tạo tế bào da, lấy đi các chất bẩn bám trên da sau một ngày dài.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;3. Cách bảo quản sản phẩm&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Trên các sản phẩm đều nghi rõ: Bảo quản ở nơi khô thoáng và tránh ánh nắng trực tiếp. Bạn lưu ý thêm với các sản phẩm dạng tuýp, phần đầu của sản phẩm sau thời gian sử dụng sẽ có thể lắng bẩn, nên thường xuyên vệ sinh nắp và đầu tuýp sản phẩm nữa nhé.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Bây giờ thì thật dễ dàng để rửa mặt đúng cách rồi đúng không nào, chúc các bạn luôn biết sử dụng hiệu quả những sản phẩm mình đang có để có được vẻ đẹp tối ưu!&lt;/span&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div class=&quot;div_creator&quot; style=&quot;font-family: Arial; padding-top: 10px; text-align: right; font-weight: bold; color: rgb(0, 0, 0); line-height: 18px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Nguồn: Nước ngoài&lt;/span&gt;&lt;/div&gt;\r\n', 'Hướng dẫn sử dụng sữa rửa mặt đúng cách,hiệu quả của shop mỹ phẩm xách tay trùm hàng mỹ', ''),
(12, 1, 'Phương pháp cho da mặt trắng hồng tự nhiên với mặt nạ trà xanh', 'Như các chị em đa biết thì trà xanh là loại nguyên liệu thiên nhiên chứa khá nhiều công dụng hữu ích, hỗ trợ chị em phụ nữ giữ dáng thành công cũng như làm đẹp da, ngăn ngừa lão hóa hiệu quả. Chính vì thế mà bột trà xanh – sản phẩm của trà xanh cũng mang trong mình những công dụng làm đẹp hữu hiệu. Cho da mặt trắng hồng tự nhiên với mặt nạ trà xanh chính là một trong những công dụng vượt trội hàng đầu mà loại nguyên liệu này mang lại cho phái đẹp chúng mình.\r\n', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Bản chất của trà xanh là chứa khá nhiều vitamin, chất diệp lục, chứa nhiều axit có lợi và còn chứa thêm thành phần chất chống oxy hóa… nên được khá nhiều chị em phụ nữ sử dụng để làm đẹp. Chính vì thế mà hiện nay có khá nhiều dòng sản phẩm chăm sóc da, chăm sóc sắc đẹp được chiết xuất từ trà xanh. Và bột trà xanh chính là một trong số những sản phẩm làm đẹp da hiệu quả từ trà xanh.&lt;/span&gt;&lt;br style=&quot;margin: 0px; padding: 0px; color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 22.5px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Chị em chúng mình có thể sử dụng bột trà xanh để tạo thành các loại mặt nạ dưỡng da sử dụng hàng ngày để sớm phục hồi và làm đẹp làn da của mình hơn nhé!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h2 style=&quot;margin: 20px 0px 0.4em; padding: 0px; line-height: 1.4em; font-family: ''Times New Roman'', Times, serif; color: rgb(16, 55, 87); font-size: 1.9em;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;Mặt nạ làm sáng da&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-3.jpg&quot; style=&quot;opacity: 0.9; line-height: 20.7999992370605px; width: 600px; height: 400px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Để có thể làm mặt nạ trà xanh cho làn da sáng thì bạn chỉ cần sử dụng 2 loại nguyên liệu quen thuộc là sữa chua với bột trà xanh. Bạn trộn 2 nguyên liệu này với nhau tạo thành một hỗn hợp mặt nạ. Sau đó, bạn cho hỗn hợp này đắp lên mặt trong khoảng 15 – 20 phút rồi rửa mặt thật sạch. Sự kết hợp giữa trà xanh với sữa chua chính là công thức để bạn làm sáng da và trị các vết thâm hiệu quả. Những cô nàng có làn da nhạy cảm, có nhiều mụn thì vẫn có thể sử dụng loại mặt nạ này để bỏ bớt mụn hiệu quả hơn nhé&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;Mặt nạ trà xanh chống nếp nhăn&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-4.jpg&quot; style=&quot;width: 600px; height: 400px;&quot; /&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Để chế biến thành mặt nạ trà xanh chống nếp nhăn thì bạn cần nguyên liệu chính là bột trà xanh và bột cám gạo. Trộn đều hỗn hợp 2 loại nguyên liệu này với nhau và hòa thêm 1 chút nước để tạo thành hỗn hợp đặc sánh. Sau đó, bạn cho hỗn hợp này đắp lên mặt khoảng 15 – 20 phút rồi rửa mặt thật sạch. Chỉ cần bạn thực hiện cách dưỡng da này đều đặn mỗi tuần 1 lần thì chẳng mấy chốc, bạn sẽ cảm nhận được sự thay đổi rõ rệt trên chính da mặt của mình. Những chị em trên 30 tuổi thì hãy tham khảo và áp dụng ngay bí quyết này nhé! Hiệu quả lắm đấy.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;Mặt nạ trà xanh trị mụn&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-3.jpg&quot; style=&quot;width: 600px; height: 400px;&quot; /&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Nếu như mụn là kẻ thù của sắc đẹp thì giờ đây, với mặt nạ trà xanh trị mụn thì các cô nàng không cần phải lo lắng về vấn đề này nữa nhé! Chỉ cần bạn cho một thìa bột trà xanh trộn đều với một ít sữa chua cùng với một ít bột cám gạo nếp theo tỉ lệ thích hợp thì bạn đã có thể loại bỏ mụn nhanh chóng rồi. Sau khi trộn đều hỗn hợp này thì bạn đắp hỗn hợp lên mặt trong khoảng từ 15 – 20 phút rồi rửa mặt thật sạch thì bạn sẽ thấy được hiệu quả tức thì.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-6.jpg&quot; style=&quot;width: 600px; height: 450px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Axit lactic có trong sữa chua làm cho da mịn màng tự nhiên, đồng thời còn làm se lỗ chân lông cũng như giúp bảo vệ và tái tạo tế bào da. Còn bột cám gạo nếp thì có công dụng làm sạch bụi bẩn tích tụ trên da mặt, đồng thời cung cấp một lượng vitamin để nuôi dưỡng, tái sinh làn da của bạn một cách hiệu quả.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;Mặt nạ trà xanh làm nhỏ lỗ chân lông&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-7.jpg&quot; style=&quot;width: 600px; height: 400px;&quot; /&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Bạn sẽ không cần phải lo lắng về những lỗ chân lông trên da mặt của mình nữa nếu như bạn sử dụng mặt nạ trà xanh làm se lỗ chân lông này. Bạn có thể thực hiện một trong 2 cách chế biến mặt nạ trà xanh mà chúng tôi giới thiệu dưới đây nhé!&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-1.jpg&quot; style=&quot;width: 600px; height: 449px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Cách 1: Cách làm này khá đơn giản, chỉ cần bạn cho một muỗng bột trà xanh hòa tan với 3 muỗng nước, sau đó, bạn đợi thêm khoảng 5 phút thì tiếp tục cho một ít nước vào hỗn hợp này. Trộn đều hỗn hợp đến khi nó trở nên nhão và sánh. Bạn chỉ cần dùng hỗn hợp này đắp lên mặt hoặc đắp lên những vùng da bị mụn và để trong khoảng 1 tiếng, sau đó bạn rửa mặt thật sạch với nước là đã có thể làm se lỗ chân lông hiệu quả.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-9.jpg&quot; style=&quot;width: 600px; height: 400px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; line-height: 22.5px;&quot;&gt;Cách 2: Với cách làm này thì bạn cần nhiều nguyên liệu hơn, bao gồm trà xanh, lòng trắng trứng và một ít bột mì. Đầu tiên bạn hòa tan bột trà xanh với ít nước, sau đó bạn cho tiếp lòng trắng trứng và một ít bột mì vào, trộn đều hỗn hợp này. Sau đó, bạn đắp hỗn hợp này lên da mặt và để trong vòng khoảng 10 – 15 phút, rồi rửa mặt thật sạch là đã có thể cảm nhận được những lỗ chân lông trên da mặt đang dần se lại.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/Cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh-3.jpg&quot; style=&quot;width: 600px; height: 400px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;color: rgb(15, 12, 12); font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 22.5px;&quot;&gt;Tuy chỉ là loại nguyên liệu đơn giản, dân gian và khá quen thuộc nhưng trà xanh lại mang đến những công dụng tuyệt vời mà có thể bạn chưa biết. Bạn đã sẵn sàng để làm đẹp làn da của mình bằng các loại mặt nạ trà xanh chưa? Chắc chắn rằng bạn sẽ không phải thất vọng về những công dụng mà loại mặt nạ này mang lại cho bạn.&lt;/span&gt;&lt;/p&gt;\r\n', 'Trang thông tin về làm đẹp da cho phụ nữ, chia sẻ kinh nghiệm làm đẹp và bảo vệ da,chia sẻ các sản phẩm làm đẹp da, chia sẻ các shop bán mỹ phẩm chăm sóc da uy tín,', 'sửa rửa mặt,rửa mặt đúng cách,làm đẹp da,hướng dẫn rửa mặt,rửa mặt ,bảo vệ da,vệ sinh da,hướng dẫn sử dụng kem chống nắng,kem chống nắng,toner,dưỡng ẩm,dưỡng da,tẩy tế bào chết,serum,lotion,cream,cân bằng da,trắng da,chống lão hóa da,giảm nếp nhăn,đặc trị'),
(10, 1, 'Phương pháp làm trắng da mặt trong 15 ngày', 'Phương pháp làm trắng dưới đây sẽ giúp bạn nhanh chóng lấy lại vẻ đẹp tự nhiên và sức sống cho làn da của mình chỉ trong 15 ngày.', '&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Đầu tiên sử dụng sữa rửa mặt làm trắng da hằng ngày&lt;/strong&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Hiện nay, trên thị trường có rất nhiều loại sữa rửa mặt với tác dụng làm trắng da. Hãy lựa chọn loại sữa rửa mặt có thành phần phù hợp với loại da của bạn, đặc biệt là loại có nguồn gốc từ những nguyên liệu thiên nhiên như Sữa rửa mặt DABO Trà Xanh để liệu trình làm trắng da trong vòng 15 ngày của bạn thêm hiệu quả.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Rửa mặt 2 lần vào buổi sáng thức dậy và buổi tối trước khi đi ngủ với sữa rửa mặt làm trắng da để giúp da được sạch sẽ, loại sạch bụi bẩn và dễ dàng tiếp nhận những bước chăm sóc đặc biệt sau đó.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Tuần 1: Sử dụng chanh để làm trắng da&lt;/strong&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Với thành phần tự nhiên có tính chất tẩy nhẹ, chanh có tác dụng tẩy đi tế bào chết, tái tạo và làm mới để da bạn được sáng hồng một cách tự nhiên. Ngoài ra, chanh còn có tác dụng trẻ hóa làn da và làm&amp;nbsp;se khít lỗ chân lông.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: center;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/dap-mat-na-tranh.jpg&quot; style=&quot;width: 450px; height: 337px;&quot; /&gt;&lt;br /&gt;\r\nMặt nạ chanh có tác dụng tẩy trắng da khá hiệu quả.&lt;/span&gt;&lt;br /&gt;\r\n&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Vì chanh có tính chất lột nhẹ nên bạn chỉ nên sử dụng khoảng 2 - 3 lần trong tuần để đắp mặt nạ. Tùy theo loại da của mình mà bạn nên chọn loại mặt nạ từ quả chanh để làm trắng da cho phù hợp:&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Mặt nạ làm trắng cho da dầu: Lấy một bát nhỏ và thêm một muỗng cà phê bột yến mạch. Tiếp tục cho thêm 2 muỗng cafe nước ép chanh pha loãng để tạo ra một hỗn hợp sền sệt. Mặt nạ này giúp làm sạch da hiệu quả và giúp loại bỏ dầu để cho bạn làn da tươi sáng trong một thời gian dài.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Mặt nạ làm trắng da cho da thường: Nếu bạn có một làn da không mấy trắng sáng, bạn hãy kết hợp một muỗng cafe nước cốt chanh với một thìa cafe bột nghệ và trộn đều. Bổ sung thêm vài giọt nước hoa hồng để tạo thành hỗn hợp dày và đắp trên mặt. Bột nghệ và nước chanh vừa giúp trong việc loại bỏ tế bào da chết và làm sáng da tự nhiên. Nước hoa hồng được bổ sung vào đây để dưỡng da và giúp da mịn màng, se khít lỗ chân lông.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Làm trắng cho làn da khô: Để chăm sóc da khô với chanh thật hiệu quả mà không khiến da bạn bị khô hoặc dị ứng, bạn có thể kết hợp một muỗng cafe dầu ô liu với một muỗng cafe mật ong và vài giọt nước cốt chanh tươi. Sau khi làm sạch mặt, bạn áp dụng thoa hỗn hợp này đồng đều trên mặt và massage khoảng 2-3 phút. Sau đó, để hỗn hợp trên da 10-15 phút rồi rửa sạch. Chanh sẽ làm sáng da và dầu ô liu sẽ giúp ngăn ngừa sự mất độ ẩm của da. Mật ong cũng là một thành phần tự nhiên tuyệt vời để cải thiện làn da.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Tuần 2: Mặt nạ đậu phụ&lt;/strong&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Đậu phụ không chỉ là món ăn ngon, bổ, rẻ mà nó còn có tác dụng làm trắng da nhanh chóng và hiệu quả. Lấy khoảng 50g đậu phụ thêm 15g bột mỳ và 5g mật ông trộn đều hỗn hợp và đắp lên mặt. Sau 20 phút rửa lại mặt bằng nước ấm. Làm liên tục trong 1 tuần để cảm nhận rõ sự biến đổi về sắc tố trên làn da mặt của bạn nhé. Mặt nạ đậu phụ không chỉ làm sáng da mà còn thu nhỏ lỗ chân lông giúp da bạn căng mịn, đầy sức sống.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Bôi kem làm trắng da ban đêm&lt;/strong&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Bôi kem làm trắng da ban đêm My Jin Gold Hàn Quốc sẽ giúp da bạn được nuôi dưỡng và tái tạo mới cùng với nước da sáng hồng hiệu quả.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Trên thực tế có cả loại kem dưỡng da ban ngày và ban đêm có chứa thành phần làm trắng da. Tuy nhiên, theo một nghiên cứu mới công bố, việc sử dụng kem dưỡng da có tác dụng làm trắng da vào ban đêm đạt hiệu quả hơn nhiều so với loại kem sử dụng ban ngày. Vì ban đêm là khoảng thời gian bạn nghỉ ngơi và thư giãn hoàn toàn, do vậy loại kem dưỡng da làm trắng sẽ có tác dụng tối ưu nhất. Nếu muốn, bạn vẫn có thể sử dụng kem dưỡng làm trắng da ban ngày nhưng trên thực tế, chỉ cần bạn sử dụng loại kem dưỡng làm trắng da ban đêm là đủ.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Chăm sóc da bằng vitamin E&lt;/strong&gt;&lt;br /&gt;\r\nMột tuần một lần, sau khi rửa sạch mặt, bạn hãy massage mặt với một loại kem dưỡng ẩm có chứa vitamin E trong khoảng 10-15 phút. Sau đó để cho da khô tự nhiên và rửa sạch mặt bằng nước ấm.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Chống nắng cho da&lt;/strong&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\nTrước khi bạn đi ra ngoài nắng, bạn nên bôi kem chống nắng trên những vùng da hở như mặt, cổ, bàn tay của mình. Nên mua kem chống nắng có chỉ số chống nắng SPF từ 30 trở lên để chống nắng thật hiệu quả trong mùa hè này. Việc sử dụng các loại kem chống nắng có chỉ số chống nắng thấp cũng có thể để lại những tác động tiêu cực của ánh nắng mặt trời đến làn da bạn như nám da, cháy nắng, xạm đen...&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; color: rgb(0, 0, 0); line-height: 18px; text-align: justify;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;Để giữ gìn &quot;thành quả&quot; làm trắng da của mình, bên cạnh việc bôi kem chống nắng, bạn cũng cần đội mũ rộng vành và che chắn cẩn thận để tránh những nguy hại do ánh nắng mặt trời gây ra.&lt;/span&gt;&lt;/div&gt;\r\n', 'Trang thông tin về làm đẹp da cho phụ nữ, chia sẻ kinh nghiệm làm đẹp và bảo vệ da,chia sẻ các sản phẩm làm đẹp da, chia sẻ các shop bán mỹ phẩm chăm sóc da uy tín,', 'sửa rửa mặt,rửa mặt đúng cách,làm đẹp da,hướng dẫn rửa mặt,rửa mặt ,bảo vệ da,vệ sinh da,hướng dẫn sử dụng kem chống nắng,kem chống nắng,toner,dưỡng ẩm,dưỡng da,tẩy tế bào chết,serum,lotion,cream,cân bằng da,trắng da,chống lão hóa da,giảm nếp nhăn,đặc trị'),
(11, 1, 'Làm trắng da từ A đến Z cùng bột yến mạch', 'Phù hợp với mọi loại da, đặc biệt là da nhạy cảm; bột yến mạch với công dụng tẩy trắng da, ngăn ngừa mụn và làm căng da mặt sẽ là gợi ý tuyệt vời của bạn trong mùa Thu này.', '&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Tiết trời giao mùa với không khí hanh khô đặc trưng sẽ khiến da bạn trở nên sần sùi, khô ráp hay thậm chí là nổi mẩn đỏ. Cũng từ đó, những đốm mụn lấm tấm xuất hiện, từng mảng da bong tróc (đặc biệt là nơi đầu mũi, gò má) sẽ khiến bạn mất tự tin. Vì thời tiết đặc trưng, tất nhiên bạn cũng phải &quot;trang bị&quot; cho mình những phương pháp chăm sóc da khác hẳn mùa Xuân/Hè.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Bột yến mạch là một phương pháp chăm sóc da được các chuyên gia khuyên bạn nên sử dụng cho những ngày đầu Thu. Đơn giản, dễ thực hiện mà phù hợp với mọi loại da khác nhau (đặc biệt là da nhạy cảm), các &quot;biến tấu&quot; mặt nạ chăm sóc da từ bột yến mạch không chỉ giúp bạn tẩy trắng da, loại bỏ tế bào chết, làm căng mịn làn da mà còn ngăn ngừa các đốm mụn hay hiện tượng nổi mẩn hiệu quả. Hơn nữa, bột yến mạch là nguyên liệu bạn có thể dễ dàng mua được từ siêu thị, các cửa hiệu tạp hóa với giá thành khá rẻ.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px; text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/23-7e817.jpg&quot; style=&quot;width: 500px; height: 500px;&quot; /&gt;&lt;br style=&quot;margin: 0px; padding: 0px;&quot; /&gt;\r\n&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;1. Rửa mặt bằng bột yến mạch&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Trộn 5 thìa bột yến mạch (đã xay) cùng 3 thìa cà phê nước ấm, tạo thành một hỗn hợp sền sệt. Sau khi rửa mặt sạch bằng nước ấm, thoa hỗn hợp bột yến mạch lên khắp mặt. Sau đó nhẹ nhàng mát - xa khuôn mặt theo chiều vòng tròn. Bạn có thể sử dụng khăn lông hay bàn chải để nhẹ nhàng mát - xa; nhưng việc sử dụng ngón tay sẽ mang đến hiệu quả cao nhất. Mát - xa trong khoảng từ 5 - 10 phút, sau đó rửa mặt bằng nước.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Nước ấm giúp làm giãn nở các lỗ chân lông, trong khi bột yến mạch sẽ thấm sâu vào da mặt để loại bỏ mọi bụi bẩn, cung cấp dưỡng chất sâu để làn da bạn luôn mềm mại, trắng mịn và ngăn ngừa mụn. Bạn có thể áp dụng phương pháp này từ 2 - 4 lần/tháng để duy trì nét đẹp làn da mình.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px; text-align: center;&quot;&gt;\r\n&lt;div style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/24-7e817.jpg&quot; style=&quot;width: 500px; height: 333px;&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;2. Tẩy tế bào chết&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Trộn 5 thìa bột yến mạch đã xay cùng mật ong để tạo thành một hỗn hợp sền sệt như mặt nạ. Sau đó đắp lên mặt, mặt nạ&amp;nbsp;bột yến mạch mật ong này sẽ giúp tẩy tế bào chết, giúp tế bào da chết bong ra. Bên cạnh đó, bột yến mạch hấp thu vào da dễ dàng nên nó có thể nhanh chóng làm dịu da và chống viêm da mùa hanh khô.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;3. Làm mịn da và &quot;trị&quot; mụn đầu đen&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Trộn bột yến mạch cùng lòng đỏ trứng, tùy vào lượng bột yến mạch mà bạn sẽ điều chỉnh số lòng trứng cho phù hợp để tạo thành hỗn hợp như mặt nạ. Đắp hỗn hợp này lên mặt, đợi khoảng 20 phút cho mặt nạn khô, sau đó rửa sạch mặt bằng nước ấm. Bạn có thể đắp hỗn hợp mặt nạ này 2 lần/tháng để trị mụn đầu đen và làm mềm mịn da.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px; text-align: center;&quot;&gt;\r\n&lt;div style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong da/22-7e817.jpg&quot; style=&quot;width: 500px; height: 598px;&quot; /&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;4. Tẩy trắng da&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;Yến mạch còn có tác dụng tẩy trắng da an toàn cho các bạn gái. Cho lòng trắng trứng, sữa chua và mật ong vào bát, đánh đều lên đến khi nổi bọt. Trộn bột yến mạch vào nước để chúng nở ra, sau đó trộn hỗn hợp lòng trắng trứng, sữa chua, mật ong vào bát bột yến mạch. Đắp lên mặt, đợi từ 20 - 30 phút cho mặt nạ khô hẳn, rửa lại mặt bằng nước ấm. Với loại mặt nạ, bạn có thể cho vào tủ lạnh để dùng dần.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;margin: 0px; padding: 0px; font-family: ''Times New Roman''; font-size: 16px; line-height: 20px; -webkit-text-stroke-color: rgba(255, 255, 255, 0.00784314); -webkit-text-stroke-width: 0.100000001490116px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;margin: 0px; padding: 0px; font-weight: bold;&quot;&gt;- Mách bạn&lt;/span&gt;: trước khi đắp mặt nạ tẩy trắng, bạn có thể mát - xa mặt bằng bột yến mạch trước để tẩy tế bào chết và làm sạch sâu cho làn da. Chắc chắn hiệu quả tẩy trắng sẽ tuyệt vời hơn cho bạn.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n', 'Trang thông tin về làm đẹp da cho phụ nữ, chia sẻ kinh nghiệm làm đẹp và bảo vệ da,chia sẻ các sản phẩm làm đẹp da, chia sẻ các shop bán mỹ phẩm chăm sóc da uy tín,', 'sửa rửa mặt,rửa mặt đúng cách,làm đẹp da,hướng dẫn rửa mặt,rửa mặt ,bảo vệ da,vệ sinh da,hướng dẫn sử dụng kem chống nắng,kem chống nắng,toner,dưỡng ẩm,dưỡng da,tẩy tế bào chết,serum,lotion,cream,cân bằng da,trắng da,chống lão hóa da,giảm nếp nhăn,đặc trị'),
(13, 1, 'Bí quyết sử dụng dầu xả đúng cách mang lại vẻ đẹp suôn mượt và óng ả cho tóc', 'Dầu xả không chỉ có tác dụng làm tóc suôn mượt, duy trì độ bóng mà còn bảo vệ mái tóc khỏi những tác hại từ các hóa chất uốn, duỗi, nhuộm, nhiệt độ và bụi bẩn từ môi trường. Tuy nhiên, bạn phải sử dụng dầu xả đúng cách thì mới phát huy hiệu quả của nó được', '&lt;h2 style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; font-size: 24px; color: rgb(34, 34, 34); line-height: 24px;&quot;&gt;1. Có thể dùng hằng ngày&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;&lt;img alt=&quot;Bí quyết sử dụng dầu xả đúng cách mang lại vẻ đẹp suôn mượt và óng ả cho tóc&quot; class=&quot;aligncenter size-full wp-image-683&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong toc/bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc.jpg&quot; style=&quot;margin: 0px auto 20px; padding: 0px; border: 0px; display: block; max-width: 100%; height: 466px; width: 350px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;Dụng cụ uốn, máy sấy, máy ép là, hoặc môi trường ô nhiễm … đều có thể khiến tóc khô xơ. Dầu xả sẽ hoạt động như một lớp bảo vệ tóc hàng ngày trước những tác nhân đó. Bạn không nên gội đầu bằng dầu gội hàng ngày vì dầu gội có chất tẩy rất mạnh, có thể khiến tóc khô xơ. Nhưng với dầu xả, bạn có thể dùng chúng hàng ngày. Nghĩa là những hôm không dùng dầu gội, bạn vẫn có thể xả qua tóc với dầu xả để dưỡng tóc. Việc thay dầu gội bằng dầu xả sẽ giúp giữ lại độ ẩm cho tóc, cho tóc dễ chải. Tất nhiên bạn không thể thay thế hoàn toàn dầu gội, bởi dầu xả không thể lấy đi chất bẩn trên tóc bạn được.&lt;/p&gt;\r\n\r\n&lt;h2 style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; font-size: 24px; color: rgb(34, 34, 34); line-height: 24px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;2.Chọn dầu xả theo cấu trúc tóc&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;&lt;img alt=&quot;Bí quyết sử dụng dầu xả đúng cách mang lại vẻ đẹp suôn mượt và óng ả cho tóc&quot; class=&quot;aligncenter size-full wp-image-684&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong toc/bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc-1.jpg&quot; style=&quot;margin: 0px auto 20px; padding: 0px; border: 0px; display: block; max-width: 100%; height: 733px; width: 792px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;“Nếu bạn nghĩ dầu xả sẽ khiến tóc phẳng thì bạn đang dùng sai công thức”, Damian Santiago, chủ salon Mizu ở New York nói. “Chìa khóa để chọn được một loại dầu xả phù hợp đó là dựa trên cấu trúc tóc của bạn”.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;Phụ nữ có sợi tóc nhỏ, mảnh sẽ thích hợp với dầu xả làm dày tóc, tạo độ dày cho thân tóc. Những người có mái tóc dày lại cần dầu xả sâu có chứa axit béo (loại chứa dầu quả hạnh nhân, dầu dừa, dầu jojoba) để giúp sợi tóc khỏi khô và trở nên bóng khỏe. Tóc quăn thường hay bị khô bởi dầu từ da đầu và không dễ dàng tới được chân tóc như với tóc thẳng, vì thế những người tóc quăn nên dùng loại dầu xả tăng cường độ ẩm.&lt;/p&gt;\r\n\r\n&lt;h2 style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; font-size: 24px; color: rgb(34, 34, 34); line-height: 24px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;3.Tóc nhuộm phải dùng loại dầu xả riêng&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;Tóc nhuộm&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong toc/bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc-2.jpg&quot; style=&quot;width: 259px; height: 194px;&quot; /&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;“Khi bạn nhuộm tóc, các biểu bì phải mở ra rồi đóng lại để tiếp nhận các sắc tố, và theo thời gian, việc này khiến sợi tóc yếu đi”. Francis Mousseron, chuyên gia màu nhuộm tại Frederic Fekkai Salon ở New York nói. “Lúc này, hãy tìm những loại dầu xả có thêm protein để giúp tóc có sức khỏe và chịu đựng tốt hơn các loại hóa chất”.&lt;/p&gt;\r\n\r\n&lt;h2 style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; font-size: 24px; color: rgb(34, 34, 34); line-height: 24px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;4.Tóc cũng cần đắp mặt nạ&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;text-align: center;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;img alt=&quot;Tóc cũng cần đắp mặt nạ&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong toc/bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc-3.jpg&quot; style=&quot;width: 225px; height: 225px;&quot; /&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;Mặt nạ không chỉ dành cho tóc khô và hư tổn, bởi tóc bạn dù đẹp đến mấy thì mỗi tuần cũng nên được dưỡng ẩm một lần. Để tận dụng tối đa tác dụng của mặt nạ tóc, hãy tìm loại có chứa keratin, axit amino hoặc các axit béo thiết yếu. Bạn cũng có thẻ tăng cường độ ẩm cho tóc bằng ủ nóng hoặc tự chế mặt nạ cho tóc từ thực phẩm (quả bơ và mayonnaise đều rất giàu axit béo). Thoa mặt nạ lên tóc, dùng khăn ẩm ủ tóc trong 20 phút, sau đó để tóc nguội hoàn toàn trước khi xả. Đừng vội xả tóc khi còn ẩm bởi lúc đó lớp biểu bì vẫn mở và bạn sẽ xả đi gần như toàn bộ các dưỡng chất vừa cung cấp cho tóc.&lt;/p&gt;\r\n\r\n&lt;h2 style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; font-size: 24px; color: rgb(34, 34, 34); line-height: 24px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;5.Không xoa dầu xả gần da đầu&lt;/strong&gt;&lt;/h2&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;Nếu kiểu tóc của bạn đòi hỏi độ phồng hoặc bạn cảm thấy tóc gần da đầu thường bị dầu nhanh chóng, nên tránh dùng dầu xả hoặc ác loại dầu dưỡng ẩm ở chân tóc. Theo Kattia Solano, chủ Butterfly Studio Salon ở New York: “Phần chân tóc tự nó có thể làm ẩm bởi dầu tự nhiên từ da đầu. Đó cũng là phần tóc khỏe hơn do không phải tiếp xúc nhiều với nhiệt tạo kiểu hoặc màu nhuộm. Do đó, khi dùng dầu xả hàng ngày, hãy thoa từ ngọn tóc (nơi bị tổn hại nhiều nhất), sau đó thoa ngược lên phần còn lại của tóc, tránh phần tóc cách da đầu khoảng 2cm. Để tóc ngấm dầu xả khoảng 3-5 phút trước khi xả sạch”.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px; text-align: center;&quot;&gt;&lt;img alt=&quot;Không xoa dầu xả gần da đầu&quot; src=&quot;http://trumhangmy.com/image/data/Tin tuc/Duong toc/bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc-4.jpg&quot; style=&quot;width: 305px; height: 406px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 0px 0px 20px; padding: 0px; font-family: Arial; line-height: 21px; color: rgb(34, 34, 34); font-size: 14px;&quot;&gt;&lt;em style=&quot;margin: 0px; padding: 0px;&quot;&gt;Hiểu rõ và sử dụng dầu xả đúng cách sẽ giúp tóc bạn luôn khỏe đẹp. Chúc các bạn thành công!&lt;/em&gt;&lt;/p&gt;\r\n', 'Trang thông tin về làm đẹp tóc cho phụ nữ, chia sẻ kinh nghiệm làm đẹp và bảo vệ tóc,chia sẻ các sản phẩm làm đẹp tóc, chia sẻ các shop bán mỹ phẩm chăm sóc tóc uy tín,', 'dưỡng tóc,làm đẹp tóc,tóc suông dài,tóc mềm,tóc đen,xã tóc,uốn tóc,duỗi tóc,sấy tóc,gội đầu,ủ tóc,kem ủ tóc,sản phẩm tóc,chăm sóc tóc,nối tốc,hấp dầu,');
INSERT INTO `news_description` (`news_id`, `language_id`, `name`, `short_description`, `description`, `meta_description`, `meta_keyword`) VALUES
(21, 1, '“Người sói” Hugh Jackman mắc bệnh ung thư da và khuyên mọi người nên sử dụng kem chống nắng', 'Nam diễn viên nổi tiếng Hollywood - Hugh Jackman đã phát hiện mình bị bệnh ung thư da từ tháng 11/2013. Lúc ấy, anh đã đăng tải bức ảnh chụp mình với chiếc mũi dán băng và gửi lời chia sẻ đến người hâm mộ. Anh viết: “Vợ tôi bảo tôi nên đi khám cái vết ở trên mũi của tôi. Và cô ấy đã đúng! Bác sỹ nói tôi bị mắc bệnh ung thư da. Xin đừng dại dột như tôi! Bạn hãy nên đi kiểm tra. Và hãy SỬ DỤNG KEM CHỐNG NẮNG thường xuyên mỗi khi đi ra trời nắng”', '&lt;h3&gt;&lt;span style=&quot;line-height: 1.6em; font-size: 13px;&quot;&gt;Trong một đoạn video được đăng tải trên YouTube, anh kể lại tình huống ban đầu, trước khi phát hiện căn bệnh:&lt;/span&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;Khi tôi tham gia bộ phim X Men: Ngày cũ của tương lai, chuyên viên trang điểm của tôi nói: &quot;&lt;em&gt;Anh có một khối u máu nhỏ, ngay trên mũi&lt;/em&gt;&quot;, và tôi trả lời &quot;&lt;em&gt;Đúng thế, tôi biết nó xuất hiện sau khi tôi quay một cảnh chiến đấu và đôi khi tôi cũng hay sờ vào nó&lt;/em&gt;&quot;&lt;/p&gt;\r\n\r\n&lt;p&gt;Thời điểm đó anh đã chủ quan và cho rằng do anh bị rất nhiều thương tích trong 17 năm tham gia các bộ phim về “Người sói”.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sau đó một tuần, chuyên viên trang điểm lại nhắc anh, và lần này anh bảo:&lt;/p&gt;\r\n\r\n&lt;p&gt;-&amp;nbsp;&lt;em&gt;Vâng, nó đã đóng vảy khô, nhưng tôi lại lấy tay chà xát trong khi tắm, có lẽ vì thế nó ngày càng nặng hơn.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Một tuần sau nữa, người này khuyên anh:&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;-&amp;nbsp;Tôi nghĩ anh nên đi khám.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Cũng trong thời điểm đó vợ tôi&amp;nbsp;-&amp;nbsp;Deborra Lee Furness&amp;nbsp;- và hai con liên tục nhắc nhở tôi&amp;nbsp;đi khám bác sĩ cho đến khi tôi&amp;nbsp;đồng ý.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;http://kenhduongda.com/wp-content/uploads/2015/05/hugh-jackman.jpg&quot;&gt;&lt;img alt=&quot;Năm 2013, Hugh Jackman thông báo trên mạng xã hội về căn bệnh ung thư da mà anh mắc phải. Và nhắc mọi người nên sử dụng kem chống nắng mỗi khi ra đường&quot; src=&quot;http://kenhduongda.com/wp-content/uploads/2015/05/hugh-jackman.jpg&quot; style=&quot;width: 600px; height: 320px;&quot; /&gt;&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Ngôi sao của loạt phim về dị nhân Người sói cho hay anh đã sốc khi nghe đến hai chữ “ung thư”. Lúc đầu, anh không thể giữ được bình tĩnh cho đến khi tiến sĩ Michael Albom&amp;nbsp;-&amp;nbsp;vị bác sĩ điều trị cho anh&amp;nbsp;-&amp;nbsp;giải thích cặn kẽ với anh rằng anh buộc phải chấp nhận sự thật và vượt qua nó.&lt;/p&gt;\r\n\r\n&lt;p&gt;Bác sĩ Albom nói với anh rằng: “&lt;em&gt;Ung thư biểu mô tế bào đáy là điều mà anh phải đối phó, nó là ung thư, nó sẽ phát triển. Anh chỉ cần phẫu thuật để cắt bỏ các tế bào ung thư&lt;/em&gt;”&lt;/p&gt;\r\n\r\n&lt;p&gt;Tính đến nay, sau 18 tháng, Jackman đã trải qua 4 lần phẫu thuật để loại bỏ tế bào ung thư, 3 lần với khối u ở mũi và 1 lần ở vai.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img alt=&quot;Hugh Jack man được vợ khuyên sử dụng kem chống nắng mỗi khi ra đường&quot; src=&quot;http://kenhduongda.com/wp-content/uploads/2015/05/hugh-jackman-2.jpg&quot; style=&quot;width: 600px; height: 320px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Ngoài việc tiếp tục điều trị ung thư, Jackman không ngừng giúp mọi người nâng cao nhận thức về căn bệnh này, vốn khá phổ biến tại quê hương anh ở Úc. &lt;strong&gt;Anh khuyến khích mọi người nên dùng kem chống nắng nhằm phòng tránh những biến chứng nghiêm trọng khi để làn da tiếp xúc trực tiếp với ánh nắng mặt trời.&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Nam tài tử luôn cố gắng giữ tinh thần lạc quan, tích cực về tình hình sức khỏe.&lt;/p&gt;\r\n\r\n&lt;p&gt;Đồng thời, anh vẫn phải thường xuyên kiểm tra định kỳ 3 tháng một lần để theo dõi và phát hiện kịp thời tế bào ung thư nếu tái phát. Bác sĩ Albom nói với anh rằng: “Nếu đây là nỗi đau duy nhất mà anh phải chịu đựng trong đời, thì anh quả là quá may mắn.”&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;Cần nói thêm: Ung thư biểu mô tế bào đáy chiếm khoảng hơn 70% các loại ung thư da không hắc tố. Đây là loại ung thư tiến triển chậm và hiếm khi lan sang các bộ phận khác của cơ thể. Căn bệnh này thường thấy ở các vùng da hở, tiếp xúc với ánh sáng mặt trời như ở mặt, chân, tay… Biểu hiện ban đầu của bệnh thường là một khối u hơi nổi gờ cao hơn bề mặt da, phát triển to dần, thay đổi màu sắc, có thể kèm theo đau hoặc ngứa tại chỗ. Sùi, loét da là biểu hiện thường gặp tiếp theo, có thể biểu hiện là một vết trợt, loét lâu liền kèm theo chảy máu hoặc dịch mủ do bệnh nhân cào, gãi vào tổn thương. Bệnh cũng có thể biểu hiện qua một vết thương tổn màu trắng, gồ cao, cứng hoặc loét hay nổi cục ở sẹo cũ.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Nhằm&amp;nbsp;hạn chế cũng như phòng tránh những hệ luỵ từ việc để làn da tiếp xúc trực tiếp với ánh nắng mặt trời thì các bạn nên dùng kem chống nắng thường xuyên hơn mỗi khi ra đường. Đây là một trong những bước cực kỳ quan trọng trong quá trình chăm sóc và bảo về da của các bạn. Khẩu trang hay kính chỉ có thể giúp da ít đen, sạm một phần, còn để da hoàn toàn miễn nhiễm với các tia UV&amp;nbsp;độc hại thì chỉ có một cách duy nhất là sử dụng kem chống nắng.&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Qua trường hợp của &quot;Người sói&quot; - Hugh Jackman, Thi hy vọng các bạn đã phần nào hiểu được tầm quan trọng của kem chống nắng và bắt đầu chuẩn bị ngay cho bản thân 1 tuýp &lt;a href=&quot;http://trumhangmy.com/my-pham/kem-chong-nang&quot; rel=&quot;dofollow &quot; target=&quot;_blank&quot;&gt;kem chống nắng phù hợp&lt;/a&gt; để sánh bước mỗi khi ra đường nhé.&amp;nbsp;Chúc các bạn luôn xinh tươi và trẻ đẹp !!!&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;text-align: right;&quot;&gt;&lt;strong&gt;(Nguồn KenhDuongDa.com)&lt;/strong&gt;&lt;/p&gt;\r\n', 'Nam diễn viên nổi tiếng Hollywood - Hugh Jackman đã phát hiện mình bị bệnh ung thư da từ tháng 11/2013. Lúc ấy, anh đã đăng tải bức ảnh chụp mình với chiếc mũi dán băng và gửi lời chia sẻ đến người hâm mộ. Anh viết: “Vợ tôi bảo tôi nên đi khám cái vết ở t', ''),
(15, 1, 'Những lời chúc 8/3 dành cho người yêu hay và ngọt ngào nhất', 'Lời chúc hay và ngọt ngào nhất dành tặng người yêu dịp 8-3', '&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Lời chúc 8-3 dành tặng người yêu&lt;/strong&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Em yêu, hôm nay là ngày quốc tế phụ nữ ngày dành riêng để thể hiện lòng biết ơn sự quan tâm đến phụ nữ. Anh muốn gửi tới em trọn vẹn khối óc và trái tim anh, gửi tới em trọn vẹn tấm lòng anh, gửi tới em tất cả những gì cao quý và thiêng liêng nhất của anh.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Chúc mừng 8/3 người tôi yêu. Chúc mừng người tôi yêu ngày 8/3. Chúc em luôn thành đạt, học giỏi và xinh xắn nữa chứ! Anh luôn nhớ và yêu em nhiều. Lúc nào trái tim anh cũng nhớ về em, và chỉ yêu mình em mà thôi.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;- Từ nơi xa xôi này anh chỉ muốn được về bên em, về với yêu dấu, về với thương yêu và hạnh phúc.&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Nhân dịp 8/3, anh chúc em những lời tốt đẹp nhất và cả tình yêu lớn nhất trong cuộc đời này, anh sẽ dành tặng cho em! Anh cảm ơn em rất nhiều vì những gì em đã đem đến trong cuộc sống của anh… Yêu em thật nhiều và thật nhiều.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Ngày 8-3, anh tặng em ba bông hồng. Một bông tượng trưng cho tình cảm của anh. Một bông tượng trưng cho hơi thở của anh. Một bông tượng trưng cho suy nghĩ của anh. Tặng em 3 bông hồng này, anh đã tặng em cuộc đời anh!&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;- Thông tấn xã Tình Yêu xin thông báo , chủ thuê bao 09xxx vừa nhận được 1 lời chúc mừng ngày QTPN cùng 1 nụ hôn gió từ thuê bao 09xxxx: love:&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;- Chúc bạn có một tình yêu đẹp và sống hạnh phúc bên gia đình chúc bạn vạn sự như ý.&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;- Mặc dù ta đã xa nhau nhưng tình cảm của anh dành cho em sẽ không bao giờ thay đổi, nhớ nha em! Anh bao giờ cũng bên em! Hy vọng em sẽ được hạnh phúc. Vậy là sắp tới 8-3 rồi, anh chúc em vui vẻ và tràn đầy hạnh phúc&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;- Gửi người phương xa: Hỡi người tôi yêu! Em có biết rằng anh yêu em nhiều lắm không! Dẫu rằng anh không ở bên em nhưng em hãy luôn nhớ rằng ở phương trời xa nào trái tim anh luôn hướng về em!&lt;/div&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Hôm nay là 7/3 rùi, mai là ngày 8/3 mà anh không thể gần bên em được, và vì khoảng cách địa lý anh cũng không biết phải tặng hoa cho em thế nào nữa! Chúc em luôn luôn xinh đẹp, học giỏi, đáng yêu, sẽ đạt được những điều mình mong muốn&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px; text-align: center;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Dậy đi bé, sáng 8/3 rồi kìa dậy đi bé ơi! Anh sai một thiên thần tới gọi bé dậy nhưng nó bảo nó không thể đánh thức một thiên thần khác… Vì thế anh đành tự đánh thức bé vậy. Have a nice 8/3!&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Chúc em khỏe – trẻ – đẹp, luôn vui vẻ và hồn nhiên, mãi mãi là người anh yêu nhất trong cuộc đời anh!&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Ngày 8/3 luôn là ngày thật đặc biệt với em và đặc biệt với cả anh nữa.Vì đó lại thêm ngày anh được yêu thương chăm sóc em. Anh chúc em tràn đầy hạnh phúc ngọt ngào hãy gìn giữ và nuôi dưỡng tình yêu của chúng mình em nhé. Anh mong rằng ngày 8/3 gần đây nhất anh được nói với em rằng chúc bà xã của anh mọi sự tốt lành.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Ngày 8-3, anh vẫn nhớ ngày này hằng năm, có lẽ, anh không kịp gửi một đóa hoa hồng tươi nhất của buổi sáng khi ngày 8/3 năm nay đến, nhưng anh vẫn nhớ! Nhớ em rất nhiều, nhớ mái tóc thề ngang vai che nghiêng nụ cười duyên dáng khi anh trao một nụ hồng. Khi anh trao nụ hồng, chỉ một nụ hồng dành cho em buổi sáng 8-3 năm đó.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Nhân ngày 8-3, chúc em có một ngày thật nhiều tươi vui và hạnh phúc hơn những ngày khác. Ở bên em 365 ngày trong năm đều là những ngày thật ngọt ngào với anh. Mãi mãi yêu em.&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Gửi người phương xa: Hỡi người tôi yêu! Em có biết rằng anh yêu em nhiều lắm không! Dẫu rằng anh không ở bên em nhưng em hãy luôn nhớ rằng ở phương trời xa nào trái tim anh luôn hướng về em!&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;- Em yêu! Đã có lần em nói với anh, anh là người đem đến cho em những niềm vui trong cuộc sống nhưng cũng đem đến cho em rất nhiều nỗi buồn. Nếu có một điều ước thì anh sẽ ước &amp;nbsp;rằng mỗi ngày trôi qua đều là một ngày 8-3 thật đẹp đối với em!&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;a href=&quot;http://trumhangmy.com/khuyen-mai-mung-8-3-qua-tuyet-voi-thay-loi-yeu-thuong&quot;&gt;&lt;img alt=&quot;Lời chúc 8-3 dành cho người yêu&quot; src=&quot;http://trumhangmy.com/image/data/banner/khuyen mai 83.png&quot; style=&quot;width: 700px; height: 481px;&quot; /&gt;&lt;/a&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;\r\n&lt;p style=&quot;margin: 10px 0px 0px; padding: 10px 0px 0px; text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;Lời chúc 8/3 dành tặng người yêu cũ hay và ý nghĩa&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 10px 0px 0px; padding: 10px 0px 0px; text-align: justify;&quot;&gt;- Dù em đã ở xa anh nhưng trong trái tim anh luôn thầm mong cho em được hạnh phúc, đặc biệt là trong ngày hôm nay, 8/3. Hãy sống thật vui vẻ em nhé, vì ở đâu đó vẫn có 1 người muốn được nhìn thấy em cười.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 10px 0px 0px; padding: 10px 0px 0px; text-align: justify;&quot;&gt;- Mặc dù đã không còn ở bên nhau nhưng vào ngày này anh vẫn rất nhớ em. Không biết em sống ra sao, giờ như thế nào. Cầu chúc cho em một ngày 8-3 thật hạnh phúc, vui vẻ.&lt;/p&gt;\r\n\r\n&lt;p style=&quot;margin: 10px 0px 0px; padding: 10px 0px 0px; text-align: justify;&quot;&gt;- Lại một ngày 8/3 nữa đến rồi, dẫu biết rằng tình yêu đôi lúc không như mong đợi và tôi không thể có được bạn. Cảm ơn bạn vì đã cho tôi biết yêu một người trọn vẹn, dù người ấy sinh ra không phải dành cho mình. Chúc bạn 8/3 vui vẻ và hạnh phúc.&lt;/p&gt;\r\n&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;em&gt;Bài viết trên đây &lt;strong&gt;&lt;a href=&quot;http://trumhangmy.com&quot;&gt;Trùm Hàng Mỹ&lt;/a&gt;&lt;/strong&gt; đã&amp;nbsp;tổng hợp những&amp;nbsp;lời chúc 8-3 dành cho người yêu&amp;nbsp;cực hay và ngọt ngào, bạn hãy xem và lựa chọn lời chúc mà bạn cảm thấy hay nhất để người tặng người ấy nhân ngày quốc tế phụ nữ 8-3 này nhé.&lt;/em&gt;&lt;/span&gt;&lt;/div&gt;\r\n', 'Lời chúc hay và ngọt ngào nhất dành tặng người yêu nhân dịp 8-3\r\n', ''),
(16, 1, 'Lời chúc hay và ý nghĩa tặng vợ nhân dịp 8-3', 'Những lời chúc tặng vợ hay và ngọt ngào nhất dịp 8-3', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Arial; margin: 0px; padding: 0px; line-height: 16.7999992370605px;&quot;&gt;Gửi tặng những lời chúc 8/3 hay nhất dành cho vợ yêu, chắc chắn bà xã của bạn sẽ cảm thấy cực kỳ cảm động với những lời chúc của bạn dành cho cô ấy.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Để thể hiện tình cảm với một nửa thế giới trong ngày quốc tế phụ nữ 8-3 hãy cùng gửi những lời yêu thương nhất tới những người phụ nữ đặc biệt là người vợ thân yêu của bạn những câu chúc thật ý nghĩa này. Hãy làm ngày 8/3 thêm đặc biệt hơn với&lt;strong&gt; những&amp;nbsp;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;lời chúc 8-3 hay và ý nghĩa&lt;/strong&gt;&lt;/strong&gt;&amp;nbsp;, không chỉ có vậy &lt;a href=&quot;http://trumhangmy.com&quot;&gt;Trùm Hàng Mỹ&lt;/a&gt;&amp;nbsp;còn tổng hợp những lời chúc khá hay và độc đáo pha lẫn một chút dí dỏm, hài hước mà bạn sẽ cảm nhận đầy đủ nhât trong bài viết này.&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;Những lời chúc hay và ý nghĩa tặng vợ nhân dịp 8-3:&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Em cứ ngồi ngắm hoa. Em cứ ca cứ hát. Anh sẽ lo rửa bát. Anh sẽ lo quét nhà. Anh sẽ lo giặt là. Em uống gì anh pha. Chợ gần hay chợ xa. Anh lần ra được hết. Món em ưa anh biết. Em cứ chờ mà xem. Em đánh phấn xoa kem. Anh nhặt rau vo gạo. Em ung dung đọc báo. Anh tay nấu tay xào. Anh tự làm không sao. Đừng lo gì em nhé. Tà áo em tuột chỉ. Đưa anh khâu lại giùm. Nho anh mua cả chùm. Buồn mồm em cứ nếm. Bạn gái em mà đến. Cứ vô tư chuyện trò. Anh tắm cho thằng cu. Rồi anh ru nó ngủ. Màn hình bao cầu thủ. Nghe em hét “vào rồi”. Hết một ngày em ơi. 24h thôi nhé. (8/3) .&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;-&amp;nbsp;Hôm nay là ngày 8/3. Vợ yêu, Anh muốn gửi tới Em tình cảm yêu thương nhất từ sâu thẳm trong trái tim Anh. Với Anh, Em là tất cả, tài sản vô giá của cuộc đời Anh. Anh luôn cầu nguyện và hằng mong cho Em có sức khoẻ để được bên Anh trên cuộc đời này. Anh luôn chúc Em thành công trong sự nghiệp, nhưng với Anh sức khoẻ của Em là quan trọng nhất để Anh luôn có Em bên cạnh. Anh yêu Em nhiều và nhất, chỉ một mình Em thôi!!!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Gửi vợ: Dù ở xa, anh không trực tiếp tặng quà và hoa hồng cho em được, nhưng anh rất nhớ em và yêu em nhiều. Ngày 8-3 sắp đến, anh chúc em luôn mạnh khỏe và công tác tốt, mãi mãi là người vợ tốt của anh và là người mẹ hiền của các con. Tình yêu mà anh dành cho em cũng đỏ thắm như hoa hồng và ngọt ngào như hương vị của sô cô la vậy!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Hôm nay là ngày 8/3. Vợ yêu, Anh muốn gửi tới Em tình cảm yêu thương nhất từ sâu thẳm trong trái tim Anh. Với Anh, Em là tất cả, tài sản vô giá của cuộc đời Anh. Anh luôn cầu nguyện và hằng mong cho Em có sức khoẻ để được bên Anh trên cuộc đời này. Anh luôn chúc Em thành công trong sự nghiệp, nhưng với Anh sức khoẻ của Em là quan trọng nhất để Anh luôn có Em bên cạnh. Anh yêu Em nhiều và nhất, chỉ một mình Em thôi!!!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Chúc Vợ yêu ngày 8 tháng 3 đầy hạnh phúc bên chồng và Em mãi mãi &amp;nbsp;xinh đẹp, mãi tươi trẻ, đáng yêu, đằm thắm như thủa ban đầu và mãi là như thế ... &amp;nbsp;Anh đã rất hạnh phúc lắm lắm Em biết không? anh cám ơn Em về tất cả những gì Em đã mang lại cho anh...anh vẫn luôn lấy đó là niềm tự hào hãnh diện vì có Em, một người vợ xinh đẹp, ngoan ngoãn và chu đáo, hết mực yêu thương chồng.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Gửi vợ yêu! Có được em là may mắn lớn nhất trong cuộc đời anh. Anh mong muốn vơ chồng mình sẽ luôn sống hạnh phúc và vui vẻ bên nhau. Hôm nay là một ngày thật đặc biệt để anh có thể nói : Anh yêu em thật nhiều.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px; text-align: center;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;a href=&quot;http://trumhangmy.com/khuyen-mai-mung-8-3-qua-tuyet-voi-thay-loi-yeu-thuong&quot; target=&quot;_blank&quot;&gt;&lt;img alt=&quot;Những lời chúc hay và ý nghĩa tặng vợ yêu&quot; src=&quot;http://trumhangmy.com/image/data/banner/khuyen mai 83.png&quot; style=&quot;width: 700px; height: 481px;&quot; /&gt;&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Mỗi người đều có hạnh phúc của riêng mình. Đối với anh, hạnh phúc lớn nhất là có em. Anh mong rằng mỗi ngày đều có em bên cạnh, trọn kiếp này và cả kiếp sau nữa. Happy Women’s Day!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Em yêu hôm nay là ngày quốc tế phụ nữ ngày dành riêng để thể hiện lòng biết ơn sự quan tâm đến phụ nữ. Anh muốn gửi tới em trọn vẹn khối óc và trái tim anh, gửi tới em trọn vẹn tấm lòng anh, gửi tới em tất cả những gì cao quý và thiêng liêng nhất của anh.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Nhân dịp ngày 8/3, Anh muốn gửi tới em và các con tình cảm yêu thương nhất từ sâu thẳm trong trái tim Anh. Với Anh, em và các con là tất cả, tài sản vô giá của cuộc đời Anh. Anh luôn cầu nguyện và hằng mong cho em và các con có sức khoẻ để được bên Anh trên cuộc đời này. Anh luôn chúc Em thành công trong sự nghiệp, nhưng với Anh sức khoẻ của em là quan trọng nhất để Anh luôn có em bên cạnh. Anh yêu Em nhiều và nhất, chỉ một mình Em thôi! Happy Women’s Day.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Chúc em một ngày thật nhiều t­ươi vui và hạnh phúc hơn những ngày khác. Ở bên em 365 ngày trong năm đều là những ngày thật ngọt ngào với anh. Mãi mãi yêu em, bà xã của anh.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;\r\n&lt;div&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;- Em yêu hôm nay là ngày phụ nữ Việt Nam ngày dành riêng để thể hiện lòng biết ơn sự quan tâm đến phụ nữ. Anh muốn gửi tới em, người vợ yêu quý nhất của anh trọn vẹn khối óc và trái tim anh, gửi tới em trọn vẹn tấm lòng anh, gửi tới em tất cả những gì cao quý và thiêng liêng nhất của anh.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div&gt;&lt;strong&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Với bài viết&amp;nbsp;lời chúc 8-3 dành cho vợ hay và ý nghĩa&amp;nbsp;này, &lt;a href=&quot;http://trumhangmy.com&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;Trùm Hàng Mỹ&lt;/span&gt;&lt;/a&gt; hy vọng&amp;nbsp;các bạn có thể lựa chọn được lời chúc hay nhất để gửi tặng người vợ thân yêu của bạn.&lt;/span&gt;&lt;/strong&gt;&lt;/div&gt;\r\n&lt;/div&gt;\r\n', 'Những lời chúc hay,ngọt ngào và ý nghĩa nhất tặng vợ dịp 8-3', ''),
(17, 1, 'Những lời chúc tặng mẹ hay và ý nghĩa nhân dịp 8-3', 'Những lời chúc hay và ý nghĩa tặng mẹ nhân ngày 8-3', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Arial; margin: 0px; padding: 0px; line-height: 16.7999992370605px;&quot;&gt;Có lẽ là dù yêu mẹ nhiều tới bao nhiêu thì bạn vẫn ngại ngùng khi nói những lời yêu thương với mẹ. Ngày 8/3 đang tới gần rồi, bạn hãy dành ngay những&amp;nbsp;&lt;a href=&quot;http://thuthuat.taimienphi.vn/loi-chuc-8-3-hay-va-y-nghia-3396n.aspx&quot; style=&quot;text-decoration: none; color: black;&quot;&gt;lời chúc 8/3&lt;/a&gt;&amp;nbsp;dành cho mẹ thật hay và ý nghĩa có ở ngay trong bài viết này.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Không quản khó khăn, vất vả mẹ nuôi chúng ta khôn lớn. Ngày quốc tế phụ nữ đang tới gần, để thể hiện tình cảm với mẹ, bạn đang băn khoăn lựa chọn một món quà thích hợp nhất. Ngoài hoa và quà bạn còn hãy lựa chọn một lời chúc thật hay và ý nghĩa để tặng mẹ nhân ngày 8-3, những&amp;nbsp;lời chúc 8/3&amp;nbsp;hay và ý nghĩa có ở bài viết này chắc chắn sẽ mang lại sự hài lòng cho bạn&amp;nbsp;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px; text-align: center;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong style=&quot;margin: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;text-transform: uppercase; padding-top: 25px; padding-bottom: 15px; clear: both;&quot;&gt;Những lời chúc hay và ý nghĩa dành cho mẹ nhân ngày 8-3&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;1. Hẳn mẹ sẽ bất ngờ khi nhận được món quà này của con ! Mẹ à, con chưa từng nói con yêu mẹ nhưng sâu thẳm đáy lòng, con luôn biết ơn và coi mẹ là động lực sống đấy.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;2. Cám ơn Mẹ, đã sinh ra con và nuôi dưỡng con cho đến ngày trưởng thành. Cám ơn Mẹ, về những tháng ngày nhọc nhằn đã làm lưng Mẹ còng xuống, đôi mắt mẹ thâm quầng vì những đêm không ngủ, về những nỗi buồn lo mà Mẹ đã từng âm thầm chịu đựng suốt hơn 30 năm qua.... Con chúc Mẹ của con mỗi ngày đều vui vẻ và sống khỏe, con yêu Mẹ nhiều.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;3. Mẹ, mẹ là dòng suối dịu hiền/Mẹ, mẹ là bài hát thần tiên/Là bóng mát trên cao/Là mắt sáng trăng sao/Là ánh đuốc trong đêm khi lạc lối... mẹ là điều tốt đẹp nhất con có. 8/3, con chúc mẹ luôn vui khỏe. Con yêu mẹ!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;4. Con nhớ những ngày bé thơ rong ruổi cùng mẹ trên những chặng đường dài. Nhớ chiếc bánh mẹ nhịn ăn phần con ngày mưa bão. Nhớ những đêm đông mẹ thức trắng đêm đan áo len cho con... Con nhớ tất cả và càng kính trọng, thương mẹ hơn.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;5. &amp;nbsp;Ngày của mẹ mà con không thể ở gần để trực tiếp tặng hoa, quà cho mẹ hay đơn giản là dọn dẹp giúp mẹ căn nhà nhỏ thân yêu. Con xin lỗi mẹ! Con rất nhớ mẹ, mẹ là nguồn sống của con. Mẹ luôn theo sát những bước đi của con. Cảm ơn mẹ và chúc mẹ luôn khoẻ mạnh.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;6. Mẹ ơi, 8-3 này chắc con không về nhà được. Con nhớ mẹ và con mong mẹ sẽ có một ngày 8-3 với hoa và một bữa ăn ngon của bố!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;7. 8-3 sắp đến rồi mà con vẫn chưa có gì để tặng cho mẹ cả. Mẹ là người quan trọng nhất trong cuộc đời của con. Mẹ là người đã che chở cho con, những lúc con buồn, mẹ luôn an ủi cho con... Nhưng con chưa bao giờ tặng một món quà gì cho mẹ. Có lẽ con vẫn chưa ý thức được mẹ quan trọng như thế nào đối với con.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;8. Nhân ngày 8-3 con chúc mẹ nhiều sức khỏe và nụ cười luôn nở trên môi mẹ. Con ở xa mẹ quá, nên không thể chăm sóc cho mẹ được. Mẹ nhớ giữ gìn sức khỏe! Mong cho mọi điều tốt lành sẽ đến với mẹ! Con yêu mẹ!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;9. Má ơi, con học xa nhà mới thấy nhớ má và gia đình như thế nào, chẳng đợi đến dịp này con mới thốt lên ba tiếng &quot;con yêu má&quot;, đừng lo cho con, hãy &quot;cát tường&quot; má nhé.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;10. Ngày 8-3 đã tới thật mau. Con chúc mẹ có nhiều sức khỏe để có thể dẫn dắt chúng con đi trên đường đời. Mẹ mãi mãi là người mẹ yêu quý của con, con rất mong luôn có mẹ bên cạnh!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px; text-align: center;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;a href=&quot;http://trumhangmy.com/khuyen-mai-mung-8-3-qua-tuyet-voi-thay-loi-yeu-thuong&quot; target=&quot;_blank&quot;&gt;&lt;img alt=&quot;Những lời chúc hay và ý nghĩa tặng mẹ nhân dịp 8-3&quot; src=&quot;http://trumhangmy.com/image/data/banner/khuyen mai 83.png&quot; style=&quot;width: 700px; height: 481px;&quot; /&gt;&lt;/a&gt;&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;11. Lại một năm nữa chúng con ở xa nhà, nhưng đối với chúng con Mẹ luôn ở trong trái tim. Chúng con chúc Mẹ luôn được Hạnh Phúc. Gởi tặng Mẹ bó hoa tình cảm từ trái tim của các anh em con.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;12. Một mình mẹ tôi nuôi tôi khôn lớn trưởng thành. Mẹ tôi đã phải chịu đựng biết bao gian khó nhưng mẹ không hề lùi bước, không bị khuất phục. Tất cả những gì mẹ đã làm cũng chỉ vì tôi. Bây giờ tôi đã trở thành một sinh viên đại học rồi, đó là niềm tự hào của mẹ tôi. Tôi muốn gửi mọi điều tốt đẹp nhất đến mẹ tôi ở quê. Mong mẹ giữ gìn sức khỏe và mong mẹ không bao giờ phải buồn vì con hay là bất cứ chuyện gì. Con sẽ đem đến cho mẹ vạn niềm vui. Con biết mẹ nhớ con nhiều và điều đó sẽ thôi thúc con trong con đường đi phía trước. CON YÊU MẸ NHIỀU.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;13. Nhân ngày 8-3, con chúc mẹ luôn vui vẻ - sức khoẻ dồi dào - luôn xinh đẹp trong mắt bố con và mọi người.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px; text-align: center;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;14. Mẹ à, mẹ là món quà quý giá nhất mà ông trời đã ban cho con. Con kính chúc mẹ sống lâu trăm tuổi và mãi là ng mẹ tốt nhất của chúng con! Con yêu mẹ!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;15. Mẹ yêu ơi! Con chúc mẹ yêu luôn khỏe, trẻ đẹp mãi trong mắt của cha, bao la tình thương khi con lầm lỗi, mỗi khi mẹ cười là đời con không còn lạc lối. Bên mẹ cha sum vầy là hạnh phúc nhất của đời con. Con hôn mẹ!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;16. Kính chúc mẹ! Không chỉ là trong này 8-3 mà tất cả 365 ngày đều luôn luôn vui vẻ, hạnh phúc! Để cho con luôn được nhìn thấy nụ cười, ánh mắt, cho con được cảm nhận tình yêu thương của mẹ suốt cuộc đời... Và nhiều hơn thế nữa.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;17. Gửi mẹ yêu của con! Con chẳng biết nói gì hơn, nhân ngày 8-3 con chỉ biết chúc mẹ luôn luôn mạnh khoẻ, hạnh phúc và luôn luôn là người mẹ mà con yêu quý nhất!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;18. Chúc mừng mẹ yêu quý của con! Mẹ ơi, con yêu mẹ nhiều lắm. Có những lúc con đã làm mẹ phải bận lòng về con phải không mẹ. Nhưng con luôn yêu mẹ, yêu mẹ rất nhiều. Mẹ hãy luôn mạnh khỏe và ở bên con, giúp con vượt qua mọi khó khăn trong cuộc sống, mẹ nhé!&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;19. Xin gửi mẹ yêu: Ngày của mẹ mà con không có mặt để mua cho mẹ một món quà gì đó. Con rất nhớ mẹ, mẹ là nguồn sống của con. Mẹ luôn theo sát những bước đi của con. Cảm ơn mẹ và chúc mẹ luôn khoẻ mạnh.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;20. Mẹ! cám ơn mẹ rất nhiều vì mẹ đã sinh ra con và nuôi con khôn lớn. Con luôn cầu mong bố mẹ mạnh khoẻ và hạnh phúc không chỉ riêng ngày 8/3. Lúc nào mẹ cũng ở trong trái tim con. Con yêu mẹ nhiều, mẹ kính yêu của con.&lt;/span&gt;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;div style=&quot;font-family: Arial; line-height: 16.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;strong&gt;Bài viết này &lt;a href=&quot;http://trumahngmy.com&quot;&gt;Trùm Hàng Mỹ&lt;/a&gt;&amp;nbsp;đã tổng hợp những&amp;nbsp;lời chúc 8-3 tặng mẹ&amp;nbsp;để bạn có thể lựa chọn cho riêng mình. Chắc chắn mẹ bạn sẽ rất bất ngờ về món quà mà bạn gửi tặng.&amp;nbsp;&lt;/strong&gt;&lt;/span&gt;&lt;/div&gt;\r\n', 'Những lời chúc hay và ý nghĩa nhất tặng mẹ ngày 8-3', '');
INSERT INTO `news_description` (`news_id`, `language_id`, `name`, `short_description`, `description`, `meta_description`, `meta_keyword`) VALUES
(22, 1, 'Hướng dẫn chọn kem chống nắng cho từng loại da', 'Hiện nay trên thị trường có ti tỉ loại kem chống nắng, khiến chúng ta bấn loạn chả biết loại nào mới thật sự phù hợp. Nếu để chỉ chính xác 1 loại nào tuyệt đối tốt thì e rằng phiến diện và rất khó vì bản thân Thi cũng phải thử qua cả hơn chục loại kem chống nắng mới tìm được loại mình thích. Nhưng thậm chí loại này vào mùa này thấy thích nhưng sang mùa sau lại phải đổi sang loại khác. Sau đây là 1 số kinh nghiệm khi lựa chọn kem chống nắng chia sẻ cùng các bạn để tham khảo', '&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;color: #ff30ee;&quot; style=&quot;color: rgb(255, 48, 238);&quot;&gt;&lt;strong&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;CÁCH LỰA CHỌN KEM CHỐNG NẮNG&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;em&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Hiện nay trên thị trường có ti tỉ loại kem chống nắng, khiến chúng ta bấn loạn chả biết loại nào mới thật sự phù hợp. Nếu để chỉ chính xác 1 loại nào tuyệt đối tốt thì e rằng phiến diện và rất khó vì bản thân Thi cũng phải thử qua cả hơn chục loại kem chống nắng mới tìm được loại mình thích. Nhưng thậm chí loại này vào mùa này thấy thích nhưng sang mùa sau lại phải đổi sang loại khác. Sau đây là 1 số kinh nghiệm khi lựa chọn kem chống nắng chia sẻ cùng các bạn để tham khảo&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;em&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Chọn tính chất của kem chống nắng : Đầu tiên bạn cần quan tâm tới nhu cầu da mình phù hợp với loại chống nắng nào. Sunblock hay Sunscreen.&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;strong style=&quot;line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;A. Lựa chọn chỉ số SPF và PA phù hợp với nhu cầu:&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Tùy vào mục đích, nhu cầu là dùng hằng ngày, đi chơi, đi biển và thời gian ở ngoài nắng của bạn mà có thể chọn kem chống nắng có SPF phù hợp.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;&amp;nbsp;*** Theo quan điểm cá nhân Thi, Với điều kiện khí hậu nắng gắt ô nhiễm như hiện này nếu bạn sử dụng hàng ngày đi làm công sở và dùng cho vùng mặt thì bạn có thể dùng kem chống nắng có chỉ só SPF từ 25 – 40&amp;nbsp; là có thể lọc đươc 95% các tia UV từ ánh nắng mặt trời rồi. Còn nếu bạn thường xuyên tiếp xúc với ánh nắng thì chỉ số SPF tuyệt vời cho bạn là 50. Nếu bạn đi biển, làn da sẽ được bảo vệ tốt với chỉ số&amp;nbsp;SPF từ 50 trở lên&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;strong&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;B. Cần có kem chống nắng riêng cho mặt và body:&lt;/span&gt;&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Da mặt và cơ thể là khác nhau. Vì vậy, bạn không nên dùng kem chống nắng cho body để thoa lên mặt vì da mặt thường rất mỏng và nhạy cảm hơn da body rất nhiều. Ngược lại, kem chống nắng cho da mặt mà thoa body chắc hẳn phải rất tốn kém. Với da mặt bạn nên chọn các loại kem chống nắng SPF từ 25-40. Body SPF có thể từ 40 trở lên&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;&lt;strong&gt;C. Chọn kem chống nắng phù hợp với từng loại da&lt;/strong&gt;:&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;&lt;em&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;1. Kem chống nắng dành cho Da nhạy cảm:&lt;/span&gt;&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Điều mà các bạn có làn da nhạy cảm nên nhớ là tránh xa các loại kem chống nắng có thành phần Oxybenzone , benzophenone, &amp;nbsp;PABA, cồn . Hay nói 1 cách dễ nhớ hơn là tránh xa kem chống nắng hóa học( Sunscreen) .&amp;nbsp; Bạn nên tìm tới các loại kem chống nắng vật lý (Sunblock) vì thành phần tương đối là an toàn. Và nhớ vì là da nhạy cảm nên trước khi thử bất cứ mỹ phẩm gì đều phải test trước 1 ít lên vùng quai hàm nhé&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Loại kem chống nắng phù hợp với da nhạy cảm&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;&lt;a data-mce-=&quot;&quot; href=&quot;http://trumhangmy.com/kem-chong-nang-vat-ly-blue-lizard&quot; target=&quot;_blank&quot;&gt;Kem chống nắng vật lý Blue Lizard SPF 30&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;&lt;em style=&quot;line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;2. Kem chống nắng dành cho Da dầu (nhờn)&lt;/span&gt;&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Da dầu bình thườngđã khó chịu lắm rồimà còn phải chịu thêm 1 lớp kem nhớp nháp, dày bí và dính chặt lên da thì khó chịu vô cùng. Rồi dầu, mồ hôi hòa vào với kem chống nắng loang lổ đầy mặt, nghĩ tới thôi đã kinh dị rồi. Vì thế với bạn nào da dầu nên chọn các loại kem chống nắng có công thức không chứa dầu, không gây nhờn . Cách đơn giản để nhận biết là nhìn trên bao bì có chữ “oil –free” hoặc “ no sebum”. Bên cạnh đó, các loại chống nắng dạng xịt, gel, nước cũng là 1 sự lựa chọn cho bạn&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Nếu da bạn là da dầu nhưng không quá nhạy cảm thì các loại kem chống nắng hóa học với ưu điểm khô thoáng, thấm nhanh , mỏng nhẹ cũng là 1 sự lựa chọn hợp lý&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Các loại kem chống nắng phù hợp với da nhờn &amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;&lt;a data-mce-=&quot;&quot; href=&quot;http://trumhangmy.com/Kem-chong-nang-Neutrogena-Pure-Free-Liquid-Sunscreen&quot; target=&quot;_blank&quot;&gt;Neutrogena Pure &amp;amp; Free Liquid Sunscreen&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;line-height: 21.8181819915772px; font-size: 14pt;&quot;&gt;&lt;a data-mce-=&quot;&quot; href=&quot;http://trumhangmy.com/kem-chong-nang-clarins-day-screen-spf-40&quot; target=&quot;_blank&quot;&gt;Clarins Day Screen SPF 40 UVA-UVB Oil-free&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;&lt;em style=&quot;line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;3. Kem chống nắng dành cho Da mụn&lt;/span&gt;&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Da mụn vô cùng đỏng đảnh và&amp;nbsp; cực khó chọn kem chống nắng. Thú thật Thi có người bạn cũng yêu thích chăm sóc da, cũng biết được tầm quan trọng của kem chống nắng. Khổ cái bôi bất cứ loại kem chống nắng nào mụn cũng thi nhau nổi ầm ầm chả kiêng dè mặc dù bạn ấy cũng nghiên cứu kỹ và dùng hàng của những brand nổi tiếng.&amp;nbsp; Vì thế những bạn nào da mụn cần thận trọng trong vấn đề lựa chọn kem chống nắng.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Bạn nên chọn các sản phẩm chống nắng có chữ “Non-Comedogenic” không gây bít lỗ chân lông).Và đừng bao giờ chọn kem chống nắng hóa học, Bởi trong kem chống nắng hóa học thường có cồn, PABA, oxybenzone cực không tốt cho làn da mụn. Tránh xa các sản phẩm có mùi thơm vì nó có thể làm tình trạng mụn của bạn thảm hại hơn đó.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Lời khuyên ở đây là các loại kem chống nắng vật lý sẽ phù hợp hơn&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Kem chống nắng phù hợp da mụn&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;&lt;a data-mce-=&quot;&quot; href=&quot;http://trumhangmy.com/Kem-chong-nang-La-Roche-Posay-Anthelios-60-Ultra-Light-Sunscreen-Fluid&quot; target=&quot;_blank&quot;&gt;La Roche-Posay Anthelios 60 Ultra Light Sunscreen Fluid&lt;/a&gt;&lt;/span&gt;&lt;span style=&quot;line-height: 21.8181819915772px;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;&lt;em&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;4. Kem chống nắng dành cho Da khô&lt;/span&gt;&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Bạn nên chọn các loại kem chống nắng có chất dưỡng ẩm cho da nhé. Tuy nhiên, mặc dù trong kem chống nắng có chất dưỡng ẩm rồi nhưng bạn vẫn nên sử dụng thêm kem dưỡng trước khi dùng kem chống nắng nhé. Vì da khô là dễ bị nhăn, dễ bị lão hóa lắm đó. Nên khâu dưỡng da buộc phải chăm thật kỹ nha các nàng&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Sản phẩm chống nắng phù hợp với da khô&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;a data-mce-=&quot;&quot; href=&quot;http://trumhangmy.com/kem-duong-am-chong-nang-neutrogena-healthy-defense-daily-moisturizer-spf-50-with-helioplex&quot; target=&quot;_blank&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;Neutrogena Healthy Defense Daily Moisturizer SPF 50 with Helioplex&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;div class=&quot;mceTemp&quot; style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&amp;nbsp;&lt;/div&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;&lt;em&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;5. Kem chống nắng khi đi bơi&lt;/span&gt;&lt;/em&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span data-mce-style=&quot;font-size: 14pt;&quot; style=&quot;font-size: 14pt;&quot;&gt;“Water Resistant” hoặc “Water Proof” chính là từ khóa để lựa chọn kem chống nắng khi đi bơi. Những loại này sẽ giúp da bạn chống nắng khi xuống nước với khoảng thời gian lâu hơn kem chống nắng thông thường vì em nó không thấm nước mà lị. Tuy nhiên. Nhớ bôi lại sau 1-2h để đạt hiệu quả chống nắng tốt nhất nhé&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;6. Các loại kem chống nắng dùng cho body tốt:&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Kem chống nắng dạng xịt Neutrogena Ultra Sheer Body Mist Sunscreen Broad Spectrum SPF 70&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Georgia, ''Times New Roman'', ''Bitstream Charter'', Times, serif; font-size: 16px; line-height: 21.8181819915772px;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;a data-mce-=&quot;&quot; href=&quot;http://kenhduongda.com/wp-admin/%20Kem%20ch%E1%BB%91ng%20n%E1%BA%AFng%20d%E1%BA%A1ng%20x%E1%BB%8Bt%20Neutrogena%20Ultra%20Sheer%20Body%20Mist%20Sunscreen%20Broad%20Spectrum%20SPF%2070%20%20Kem%20ch%E1%BB%91ng%20n%E1%BA%AFng%20Neutrogena%20Ultra%20Sheer%20Sunscreen%20SPF%2045%20Banana%20Boat%20Sport%20Sunscreen%20SPF%2030%20177ml&quot; target=&quot;_blank&quot;&gt;Kem chống nắng Neutrogena Ultra Sheer Sunscreen SPF 45&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Hướng dẫn chọn lựa kem chống nắng phù hợp với từng loại da', '');

-- --------------------------------------------------------

--
-- Table structure for table `news_related`
--

CREATE TABLE IF NOT EXISTS `news_related` (
  `news_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_related`
--

INSERT INTO `news_related` (`news_id`, `related_id`) VALUES
(15, 16),
(15, 17),
(16, 15),
(16, 17),
(17, 15),
(17, 16);

-- --------------------------------------------------------

--
-- Table structure for table `news_tag`
--

CREATE TABLE IF NOT EXISTS `news_tag` (
  `news_tag_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `tag` varchar(32) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_tag`
--

INSERT INTO `news_tag` (`news_tag_id`, `news_id`, `language_id`, `tag`) VALUES
(55, 9, 1, 'hướng dẫn sử dụng'),
(54, 9, 1, 'sữa rửa mặt'),
(5, 10, 1, 'làm trắng da'),
(9, 11, 1, 'bột yến mạch'),
(8, 11, 1, 'trắng da'),
(109, 12, 1, 'sáng da'),
(108, 12, 1, 'mặt nạ'),
(107, 12, 1, 'trà xanh'),
(106, 12, 1, 'dưỡng da'),
(105, 12, 1, 'trị mụn'),
(104, 12, 1, 'chống lão hóa'),
(103, 12, 1, 'nhỏ lỗ chân lông'),
(116, 13, 1, 'Bí quyết sử dụng dầu xả'),
(149, 15, 1, 'khuyến mãi 8-3'),
(140, 21, 1, 'kem chống nắng'),
(117, 13, 1, 'dầu xả'),
(102, 12, 1, 'chống nếp nhăn'),
(148, 15, 1, 'lời chúc người yêu 8-3'),
(146, 16, 1, 'lời chúc vợ 8-3'),
(143, 17, 1, 'khuyến mãi 8-3'),
(142, 17, 1, 'lời chúc mẹ 8-3'),
(145, 16, 1, 'khuyến mãi 8-3'),
(139, 21, 1, 'ung thư da'),
(135, 22, 1, 'kem chống nắng'),
(141, 21, 1, 'sử dụng kem chống nắng'),
(144, 17, 1, 'lời chúc 8-3'),
(147, 16, 1, 'lời chúc 8-3'),
(150, 15, 1, 'lời chúc 8-3');

-- --------------------------------------------------------

--
-- Table structure for table `news_to_category`
--

CREATE TABLE IF NOT EXISTS `news_to_category` (
  `news_id` int(11) NOT NULL,
  `news_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_to_category`
--

INSERT INTO `news_to_category` (`news_id`, `news_category_id`) VALUES
(9, 8),
(10, 8),
(11, 8),
(12, 8),
(13, 10),
(15, 17),
(16, 17),
(17, 17),
(21, 8);

-- --------------------------------------------------------

--
-- Table structure for table `news_to_layout`
--

CREATE TABLE IF NOT EXISTS `news_to_layout` (
  `news_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `news_to_store`
--

CREATE TABLE IF NOT EXISTS `news_to_store` (
  `news_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `news_to_store`
--

INSERT INTO `news_to_store` (`news_id`, `store_id`) VALUES
(9, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(15, 0),
(16, 0),
(17, 0),
(21, 0),
(22, 0);

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option`
--

INSERT INTO `option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 2),
(2, 'checkbox', 3),
(4, 'text', 4),
(5, 'select', 1),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 1),
(12, 'date', 1);

-- --------------------------------------------------------

--
-- Table structure for table `option_description`
--

CREATE TABLE IF NOT EXISTS `option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option_description`
--

INSERT INTO `option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `option_value`
--

CREATE TABLE IF NOT EXISTS `option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option_value`
--

INSERT INTO `option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `option_value_description`
--

CREATE TABLE IF NOT EXISTS `option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `option_value_description`
--

INSERT INTO `option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(48, 1, 11, 'Large'),
(47, 1, 11, 'Medium'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(32) NOT NULL,
  `payment_company_id` varchar(32) NOT NULL,
  `payment_tax_id` varchar(32) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(32) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_company_id`, `payment_tax_id`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://localhost/trumhangmy/', 1, 1, 'bui huu doan', 'bui huu doan', 'doan.buihuu@gmail.com', '01232424', '', 'bui huu doan', 'bui huu doan', '', '', '', 'hochiminh', '', 'hochiminh', '', 'Bahrain', 17, 'Capital', 315, '', 'Thu tiền khi giao hàng', 'cod', 'bui huu doan', 'bui huu doan', '', 'hochiminh', '', 'hochiminh', '', 'Bahrain', 17, 'Capital', 315, '', 'Phí vận chuyển cố định', 'flat.flat', '', 150000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2014-10-18 11:36:40', '2014-10-18 11:36:40'),
(3, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'dsgsdgdsg', '', 'empty@localhost', '2152155125', '', 'dsgsdgdsg', '', '', '', '', 'dsgdsgdsgds', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'dsgsdgdsg', '', '', 'dsgdsgdsgds', '', '', '', '', 0, '', 0, '', 'Miễn phí vận chuyển', 'free.free', '', 500000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '116.100.61.252', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2014-10-30 20:20:14', '2014-10-30 20:20:14'),
(4, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 2, 1, 'Hứa Hiếu', '', 'huatronghieu@gmail.com', '0983035311', '', 'Hứa Hiếu', '', '', '', '', '13/9M Thống Nhất P11 Gò Vấp HCM', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Hứa Hiếu', '', '', '13/9M Thống Nhất P11 Gò Vấp HCM', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 200000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '1.53.135.129', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36', 'en-US,en;q=0.8', '2014-11-07 05:14:34', '2014-11-07 05:14:34'),
(11, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'admin', '', 'empty@localhost', '0124412421', '', 'admin', '', '', '', '', 'hochiminh', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'admin', '', '', 'hochiminh', '', '', '', '', 0, '', 0, '', 'Miễn phí vận chuyển', 'free.free', '', 1400000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '116.100.87.23', '', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2236.0 Safari/537.36', 'en-US,en;q=0.8,vi;q=0.6', '2014-12-07 18:00:03', '2014-12-07 18:00:03'),
(12, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 4, 1, 'Phạm Hải Yến', '', 'haiyen504@yahoo.com', '0903240681', '', 'Phạm Hải Yến', '', '', '', '', 'KĐT Xa La, Hà Nội', '', '', '', '', 0, '', 0, '', 'Thu tiền khi giao hàng', 'cod', 'Phạm Hải Yến', '', '', 'KĐT Xa La, Hà Nội', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 350000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '113.20.116.121', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2438.3 Safari/537.36', 'en-US,en;q=0.8', '2015-07-06 14:22:16', '2015-07-16 13:26:18'),
(13, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Lộc', '', 'empty@localhost', '0909792970', '', 'Lộc', '', '', '', '', '366 Nguyễn Trãi, Phường 8, Quận 5, HCM', '', '', '', '', 0, '', 0, '', 'Thu tiền khi giao hàng', 'cod', 'Lộc', '', '', '366 Nguyễn Trãi, Phường 8, Quận 5, HCM', '', '', '', '', 0, '', 0, '', 'Miễn phí vận chuyển', 'free.free', '', 650000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '14.169.157.84', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36', 'en-US,en;q=0.8', '2015-07-17 05:18:45', '2015-07-17 05:18:45'),
(14, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Nguyễn Hữu Thắng', '', 'empty@localhost', '0972100972', '', 'Nguyễn Hữu Thắng', '', '', '', '', 'DT402 Khánh Long-Tân Phước Khánh-Tân Uyên-Bình Dương', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Nguyễn Hữu Thắng', '', '', 'DT402 Khánh Long-Tân Phước Khánh-Tân Uyên-Bình Dương', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 330000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '27.74.70.159', '', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36', 'en-US,en;q=0.8,vi;q=0.6', '2015-07-27 02:53:55', '2015-07-27 02:53:55'),
(18, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 7, 1, 'Nguyen Huy Thuy An', '', 'thuyan0707@yahoo.com', '0938078202', '', 'Nguyen Huy Thuy An', '', '', '', '', '36/70/14 Duong D2, F25, Q Binh Thanh, Tp HCM', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Nguyen Huy Thuy An', '', '', '36/70/14 Duong D2, F25, Q Binh Thanh, Tp HCM', '', '', '', '', 0, '', 0, '', 'Miễn phí vận chuyển', 'free.free', '', 1300000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '123.20.248.136', '123.20.248.136', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2015-09-23 21:03:11', '2015-09-23 21:03:11'),
(21, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 8, 1, 'Trinh', '', 'trinh114@yahoo.com', '0906913786', '', 'Trinh', '', '', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Trinh', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 750000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.248.53.208', '171.248.53.208', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53', 'vi-vn', '2015-10-05 12:21:22', '2015-10-05 12:21:38'),
(17, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 7, 1, 'Nguyen Huy Thuy An', '', 'thuyan0707@yahoo.com', '0938078202', '', 'Nguyen Huy Thuy An', '', '', '', '', '36/70/14 Duong D2, F25, Q Binh Thanh, Tp HCM', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Nguyen Huy Thuy An', '', '', '36/70/14 Duong D2, F25, Q Binh Thanh, Tp HCM', '', '', '', '', 0, '', 0, '', 'Miễn phí vận chuyển', 'free.free', '', 1300000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '123.20.248.136', '123.20.248.136', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2015-09-23 21:00:58', '2015-09-23 21:01:00'),
(19, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 8, 1, 'Trinh', '', 'trinh114@yahoo.com', '0906913786', '', 'Trinh', '', '', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Trinh', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 750000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.248.53.208', '171.248.53.208', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53', 'vi-vn', '2015-10-05 12:02:25', '2015-10-05 12:02:25'),
(20, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 8, 1, 'Trinh', '', 'trinh114@yahoo.com', '0906913786', '', 'Trinh', '', '', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Trinh', '', '', 'Khối F, lô N, chung cư Bình Khánh, f.An Phú, Quận 2', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 750000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.248.53.208', '171.248.53.208', 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53', 'vi-vn', '2015-10-05 12:03:12', '2015-10-05 12:03:12'),
(22, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Đinh Thảo', '', 'empty@localhost', '0938625093', '', 'Đinh Thảo', '', '', '', '', 'Nhà Xinh - 109 Tôn Dật Tiên, Phường Tân Phú, Quận 7, Tp. HCM', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Đinh Thảo', '', '', 'Nhà Xinh - 109 Tôn Dật Tiên, Phường Tân Phú, Quận 7, Tp. HCM', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Vui lòng giao hàng trong giờ hành chánh giúp mình. Xin cảm ơn.<br>', 340000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '115.75.5.116', '115.75.5.116', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', 'en-US,en;q=0.8', '2015-10-07 11:44:51', '2015-10-07 11:44:51'),
(23, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Đinh Thảo', '', 'empty@localhost', '0938625093', '', 'Đinh Thảo', '', '', '', '', 'Nhà Xinh - 109 Tôn Dật Tiên, Phường Tân Phú, Quận 7, Tp. HCM', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Đinh Thảo', '', '', 'Nhà Xinh - 109 Tôn Dật Tiên, Phường Tân Phú, Quận 7, Tp. HCM', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Vui lòng giao hàng trong giờ hành chánh giúp mình. Xin cảm ơn.<br>', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '115.75.5.116', '115.75.5.116', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', 'en-US,en;q=0.8', '2015-10-07 11:45:19', '2015-10-07 11:45:20'),
(24, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 9, 1, 'Tran Phuong Thuy', '', 'phuongthuy_67@yahoo.com', '0945 050 052', '', 'Tran Phuong Thuy', '', '', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Tran Phuong Thuy', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 340000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '14.177.218.137', '14.177.218.137', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0', 'zh-CN,en-US;q=0.7,en;q=0.3', '2015-11-08 21:56:35', '2015-11-08 21:56:35'),
(25, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 9, 1, 'Tran Phuong Thuy', '', 'phuongthuy_67@yahoo.com', '0945 050 052', '', 'Tran Phuong Thuy', '', '', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Tran Phuong Thuy', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '210.86.231.190', '210.86.231.190', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0', 'en-US,en;q=0.5', '2015-11-09 12:32:31', '2015-11-09 13:05:59'),
(26, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 9, 1, 'Tran Phuong Thuy', '', 'phuongthuy_67@yahoo.com', '0945 050 052', '', 'Tran Phuong Thuy', '', '', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Tran Phuong Thuy', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '210.86.231.190', '210.86.231.190', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0', 'en-US,en;q=0.5', '2015-11-09 13:10:12', '2015-11-09 13:10:17'),
(27, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 9, 1, 'Tran Phuong Thuy', '', 'phuongthuy_67@yahoo.com', '0945 050 052', '', 'Tran Phuong Thuy', '', '', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Tran Phuong Thuy', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '210.86.231.190', '210.86.231.190', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0', 'en-US,en;q=0.5', '2015-11-09 14:09:08', '2015-11-09 14:09:15'),
(28, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 9, 1, 'Tran Phuong Thuy', '', 'phuongthuy_67@yahoo.com', '0945 050 052', '', 'Tran Phuong Thuy', '', '', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Tran Phuong Thuy', '', '', '18 Hoàng Quốc Việt, Cầu Giấy, Hà Nội', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 340000.0000, 7, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '210.86.231.190', '210.86.231.190', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:42.0) Gecko/20100101 Firefox/42.0', 'en-US,en;q=0.5', '2015-11-09 14:11:31', '2015-11-16 00:11:31'),
(29, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Phung Thi Thu Hien', '', 'empty@localhost', '0988660242', '', 'Phung Thi Thu Hien', '', '', '', '', '44 Dang Thi Nhu, quan 1', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Phung Thi Thu Hien', '', '', '44 Dang Thi Nhu, quan 1', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 750000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.237.51.103', '171.237.51.103', 'Mozilla/5.0 (iPad; CPU OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H321 Safari/600.1.4', 'en-us', '2015-11-12 21:17:26', '2015-11-12 21:17:26'),
(30, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Phung thi thu hien', '', 'empty@localhost', '0988660242', '', 'Phung thi thu hien', '', '', '', '', '44 dang thi nhu', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Phung thi thu hien', '', '', '44 dang thi nhu', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 750000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.237.135.203', '171.237.135.203', 'Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1', 'en-us', '2015-11-13 21:19:30', '2015-11-13 21:19:30'),
(31, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Nguyễn Thị Ngọc Anh', '', 'empty@localhost', '0912614168', '', 'Nguyễn Thị Ngọc Anh', '', '', '', '', 'Số 6/D26. Phố Nguyễn Bình. Phường Đổng Quốc Bình. Quận Ngô Quyền, Hải Phòng', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Nguyễn Thị Ngọc Anh', '', '', 'Số 6/D26. Phố Nguyễn Bình. Phường Đổng Quốc Bình. Quận Ngô Quyền, Hải Phòng', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 1300000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '123.26.172.58', '123.26.172.58', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36', 'en-US,en;q=0.8', '2015-11-15 09:16:23', '2015-11-15 09:16:23'),
(32, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Lê Thị Minh Huê', '', 'empty@localhost', '0938550336', '', 'Lê Thị Minh Huê', '', '', '', '', 'phòng 08-08, lầu 8, chung cư B1 trường sa, phường 17, quận bình thạnh', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Lê Thị Minh Huê', '', '', 'phòng 08-08, lầu 8, chung cư B1 trường sa, phường 17, quận bình thạnh', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Vui lòng gọi trước khi đến. Giao hàng càng sớm càng tốt. Cám ơn<br>', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '113.176.61.200', '113.176.61.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/600.6.3 (KHTML, like Gecko) Version/8.0.6 Safari/600.6.3', 'vi-vn', '2015-11-17 10:25:38', '2015-11-17 10:25:38'),
(33, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Lê Thị Minh Huê', '', 'empty@localhost', '0938550336', '', 'Lê Thị Minh Huê', '', '', '', '', 'phòng 08-08, lầu 8, chung cư B1 trường sa, phường 17, quận bình thạnh', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Lê Thị Minh Huê', '', '', 'phòng 08-08, lầu 8, chung cư B1 trường sa, phường 17, quận bình thạnh', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Vui lòng gọi trước khi đến. Giao hàng càng sớm càng tốt. Cám ơn<br>', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '113.176.61.200', '113.176.61.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/600.6.3 (KHTML, like Gecko) Version/8.0.6 Safari/600.6.3', 'vi-vn', '2015-11-17 10:39:18', '2015-11-17 10:39:19'),
(34, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Lê Thị Minh Huê', '', 'empty@localhost', '0938550336', '', 'Lê Thị Minh Huê', '', '', '', '', '08-08 chung cư B1 Trường Sa, phường 17, quận Bình Thạnh, Hồ Chí MInh', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Lê Thị Minh Huê', '', '', '08-08 chung cư B1 Trường Sa, phường 17, quận Bình Thạnh, Hồ Chí MInh', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Giao hàng càng sớm càng tốt. Vui lòng gọi trước khi đến giao. CÁM ƠN.<br>', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '180.93.173.200', '180.93.173.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/600.6.3 (KHTML, like Gecko) Version/8.0.6 Safari/600.6.3', 'vi-vn', '2015-11-17 13:10:33', '2015-11-17 13:10:34'),
(35, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'nguyễn thị thanh loan', '', 'empty@localhost', '0949568199', '', 'nguyễn thị thanh loan', '', '', '', '', 'so61 Trưng Nữ vương, P1, TP vĩnh long, tinh Vĩnh Long', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'nguyễn thị thanh loan', '', '', 'so61 Trưng Nữ vương, P1, TP vĩnh long, tinh Vĩnh Long', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'có thể giao hàng từ thứ 2 đến thứ 6<br>', 350000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.235.9.9', '171.235.9.9', 'Mozilla/5.0 (Windows NT 5.1; rv:42.0) Gecko/20100101 Firefox/42.0', 'vi-VN,vi;q=0.8,en-US;q=0.5,en;q=0.3', '2015-11-23 19:14:13', '2015-11-23 19:14:14'),
(36, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'nguyễn thị thanh loan', '', 'empty@localhost', '0949568199', '', 'nguyễn thị thanh loan', '', '', '', '', 'số 5 Trưng Nữ vương, P1, TP vĩnh long, tinh Vĩnh Long', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'nguyễn thị thanh loan', '', '', 'số 5 Trưng Nữ vương, P1, TP vĩnh long, tinh Vĩnh Long', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'có thể giao hàng từ thứ 2 đến thứ 6<br>', 350000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.235.9.9', '171.235.9.9', 'Mozilla/5.0 (Windows NT 5.1; rv:42.0) Gecko/20100101 Firefox/42.0', 'vi-VN,vi;q=0.8,en-US;q=0.5,en;q=0.3', '2015-11-23 19:15:48', '2015-11-23 19:15:48'),
(37, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Oanh', '', 'empty@localhost', '0908369383', '', 'Oanh', '', '', '', '', 'Số 25 A, hẻm 129 Đường Tây Hoà, Phước Long A, Q9, HCM', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Oanh', '', '', 'Số 25 A, hẻm 129 Đường Tây Hoà, Phước Long A, Q9, HCM', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Giao hàng gấp trong ngày thứ 6,27/11/2015. Vui lòng gọi điện xác nhận sớm<br>', 340000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '171.233.126.148', '171.233.126.148', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36', 'en-US,en;q=0.8', '2015-11-26 21:52:11', '2015-11-26 21:52:11'),
(38, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 12, 1, 'Trương Thị Hoa Lý', '', 'hoalykt@yahoo.com.vn', '0932562678', '', 'Trương Thị Hoa Lý', '', '', '', '', '180-192 Nguyễn Công Trứ, Phường Nguyễn Thái Bình, Quận 1, TP. Hồ Chí Minh', '', '', '', '', 0, '', 0, '', 'PayPal', 'pp_standard', 'Trương Thị Hoa Lý', '', '', '180-192 Nguyễn Công Trứ, Phường Nguyễn Thái Bình, Quận 1, TP. Hồ Chí Minh', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', '', 210000.0000, 0, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '27.75.160.6', '27.75.160.6', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2015-12-14 22:43:50', '2015-12-14 22:43:50'),
(42, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'phạm ngân', '', 'empty@localhost', '0908583959', '', 'phạm ngân', '', '', '', '', '01 lý nam đế, thị trấn hóc môn, huyện hóc môn (phòng tài nguyên môi trường)', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'phạm ngân', '', '', '01 lý nam đế, thị trấn hóc môn, huyện hóc môn (phòng tài nguyên môi trường)', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'Vui lòng giao trong giờ hành chính từ 9g đến 4g chièu<br>', 370000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '27.3.0.106', '27.3.0.106', 'Mozilla/5.0 (Linux; Android 5.1.1; SAMSUNG SM-N920C Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.4 Chrome/38.0.2125.102 Mobile Safari/537.36', 'vi-VN,vi;q=0.8,en-US;q=0.6,en;q=0.4', '2016-02-16 19:11:12', '2016-02-16 19:11:13'),
(41, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'huỳnh ngọc huyền', '', 'empty@localhost', '0937980025', '', 'huỳnh ngọc huyền', '', '', '', '', '12 nguyễn hiền, phường 4, quận 3', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'huỳnh ngọc huyền', '', '', '12 nguyễn hiền, phường 4, quận 3', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'giao hàng giờ hành chính, gọi trước kho giao<br>', 650000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '115.79.38.223', '115.79.38.223', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36', 'vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2', '2016-02-02 10:43:07', '2016-02-02 10:43:18'),
(43, 0, 'INV-2013-00', 0, 'TRÙM HÀNG MỸ', 'http://trumhangmy.com/', 0, 1, 'Chị Linh', '', 'empty@localhost', '0918027592', '', 'Chị Linh', '', '', '', '', '1017 Hồng Bàng, P12, Q6', '', '', '', '', 0, '', 0, '', '<img src="https://www.nganluong.vn/webskins/skins/nganluong/images/nl.gif" /> Online payment with NgânLượng <a  target="_blank" ', 'nganluong', 'Chị Linh', '', '', '1017 Hồng Bàng, P12, Q6', '', '', '', '', 0, '', 0, '', 'Phí vận chuyển cố định', 'flat.flat', 'giao gấp<br>', 165000.0000, 2, 0, 0.0000, 1, 1, 'GBP', 1.00000000, '118.69.225.144', '118.69.225.144', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36', 'en-US,en;q=0.8', '2016-02-22 14:02:03', '2016-02-22 14:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `order_download`
--

CREATE TABLE IF NOT EXISTS `order_download` (
  `order_download_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `remaining` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_field`
--

CREATE TABLE IF NOT EXISTS `order_field` (
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` int(128) NOT NULL,
  `value` text NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_fraud`
--

CREATE TABLE IF NOT EXISTS `order_fraud` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `country_match` varchar(3) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `high_risk_country` varchar(3) NOT NULL,
  `distance` int(11) NOT NULL,
  `ip_region` varchar(255) NOT NULL,
  `ip_city` varchar(255) NOT NULL,
  `ip_latitude` decimal(10,6) NOT NULL,
  `ip_longitude` decimal(10,6) NOT NULL,
  `ip_isp` varchar(255) NOT NULL,
  `ip_org` varchar(255) NOT NULL,
  `ip_asnum` int(11) NOT NULL,
  `ip_user_type` varchar(255) NOT NULL,
  `ip_country_confidence` varchar(3) NOT NULL,
  `ip_region_confidence` varchar(3) NOT NULL,
  `ip_city_confidence` varchar(3) NOT NULL,
  `ip_postal_confidence` varchar(3) NOT NULL,
  `ip_postal_code` varchar(10) NOT NULL,
  `ip_accuracy_radius` int(11) NOT NULL,
  `ip_net_speed_cell` varchar(255) NOT NULL,
  `ip_metro_code` int(3) NOT NULL,
  `ip_area_code` int(3) NOT NULL,
  `ip_time_zone` varchar(255) NOT NULL,
  `ip_region_name` varchar(255) NOT NULL,
  `ip_domain` varchar(255) NOT NULL,
  `ip_country_name` varchar(255) NOT NULL,
  `ip_continent_code` varchar(2) NOT NULL,
  `ip_corporate_proxy` varchar(3) NOT NULL,
  `anonymous_proxy` varchar(3) NOT NULL,
  `proxy_score` int(3) NOT NULL,
  `is_trans_proxy` varchar(3) NOT NULL,
  `free_mail` varchar(3) NOT NULL,
  `carder_email` varchar(3) NOT NULL,
  `high_risk_username` varchar(3) NOT NULL,
  `high_risk_password` varchar(3) NOT NULL,
  `bin_match` varchar(10) NOT NULL,
  `bin_country` varchar(2) NOT NULL,
  `bin_name_match` varchar(3) NOT NULL,
  `bin_name` varchar(255) NOT NULL,
  `bin_phone_match` varchar(3) NOT NULL,
  `bin_phone` varchar(32) NOT NULL,
  `customer_phone_in_billing_location` varchar(8) NOT NULL,
  `ship_forward` varchar(3) NOT NULL,
  `city_postal_match` varchar(3) NOT NULL,
  `ship_city_postal_match` varchar(3) NOT NULL,
  `score` decimal(10,5) NOT NULL,
  `explanation` text NOT NULL,
  `risk_score` decimal(10,5) NOT NULL,
  `queries_remaining` int(11) NOT NULL,
  `maxmind_id` varchar(8) NOT NULL,
  `error` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_history`
--

CREATE TABLE IF NOT EXISTS `order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(5) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_history`
--

INSERT INTO `order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(8, 12, 2, 1, '', '2015-07-06 14:22:32'),
(9, 12, 2, 1, 'Chào Yến,\n\nShop nhận được thông tin bạn đã order 1 kcn Neutrogena tại shop,\nBạn ở HN nên vui lòng ck giúp shop tiền hàng và ship ( Tổng cộng : 380k)\nSau đó nt cho shop 0933383093 shop sẽ giao hàng cho bạn nhé :)', '2015-07-16 13:26:18'),
(10, 13, 2, 1, '', '2015-07-17 05:18:45'),
(11, 14, 2, 1, '', '2015-07-27 02:53:55'),
(14, 17, 2, 1, '', '2015-09-23 21:01:00'),
(15, 21, 2, 1, '', '2015-10-05 12:21:38'),
(16, 23, 2, 1, '', '2015-10-07 11:45:20'),
(17, 25, 2, 1, '', '2015-11-09 13:05:59'),
(18, 26, 2, 1, '', '2015-11-09 13:10:17'),
(19, 27, 2, 1, '', '2015-11-09 14:09:15'),
(20, 28, 2, 1, '', '2015-11-09 14:11:37'),
(21, 31, 2, 1, '', '2015-11-15 09:16:23'),
(22, 28, 7, 1, 'Hết hàng', '2015-11-16 00:11:31'),
(23, 32, 2, 1, '', '2015-11-17 10:25:38'),
(24, 33, 2, 1, '', '2015-11-17 10:39:19'),
(25, 34, 2, 1, '', '2015-11-17 13:10:34'),
(26, 35, 2, 1, '', '2015-11-23 19:14:14'),
(27, 37, 2, 1, '', '2015-11-26 21:52:11'),
(31, 42, 2, 1, '', '2016-02-16 19:11:13'),
(30, 41, 2, 1, '', '2016-02-02 10:43:18'),
(32, 43, 2, 1, '', '2016-02-22 14:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `order_option`
--

CREATE TABLE IF NOT EXISTS `order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE IF NOT EXISTS `order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(6, 1, 50, 'Product 1', 'Product 1', 1, 150000.0000, 150000.0000, 0.0000, 0),
(9, 3, 56, 'Dịch vụ Shipping', 'S01', 1, 500000.0000, 500000.0000, 0.0000, 0),
(13, 4, 59, 'Body Lotion Japanese Cherry Blossom', 'BB02', 1, 200000.0000, 200000.0000, 0.0000, 0),
(22, 11, 58, 'Clarins UV Plus HP Day Screen High Protection SPF 40 UVA-UVB Oil-free', 'KCN02', 1, 1400000.0000, 1400000.0000, 0.0000, 0),
(23, 12, 79, 'Kem Chống Nắng Neutrogena Pure &amp; Free Liquid Sunscreen', 'KCN01', 1, 350000.0000, 350000.0000, 0.0000, 0),
(24, 13, 94, 'Kem chống nắng Cilinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40', 'KCN09', 1, 650000.0000, 650000.0000, 0.0000, 0),
(25, 14, 106, 'Sữa rửa mặt Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub - 198ml', 'SRM05', 1, 330000.0000, 330000.0000, 0.0000, 0),
(33, 17, 58, 'Kem chống nắng Clarins Day Screen SPF 40 UVA-UVB Oil-free', 'KCN02', 1, 1300000.0000, 1300000.0000, 0.0000, 0),
(35, 18, 58, 'Kem chống nắng Clarins Day Screen SPF 40 UVA-UVB Oil-free', 'KCN02', 1, 1300000.0000, 1300000.0000, 0.0000, 0),
(37, 19, 93, 'Kem chống nắng EltaMD UV Clear SPF 46 Broad Spectrum sunscreen', 'KCN08', 1, 750000.0000, 750000.0000, 0.0000, 0),
(39, 20, 93, 'Kem chống nắng EltaMD UV Clear SPF 46 Broad Spectrum sunscreen', 'KCN08', 1, 750000.0000, 750000.0000, 0.0000, 0),
(40, 21, 93, 'Kem chống nắng EltaMD UV Clear SPF 46 Broad Spectrum sunscreen', 'KCN08', 1, 750000.0000, 750000.0000, 0.0000, 0),
(41, 22, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(42, 23, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(45, 24, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(49, 25, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(53, 26, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(54, 27, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(55, 28, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(56, 29, 93, 'Kem chống nắng EltaMD UV Clear SPF 46 Broad Spectrum sunscreen', 'KCN08', 1, 750000.0000, 750000.0000, 0.0000, 0),
(57, 30, 93, 'Kem chống nắng EltaMD UV Clear SPF 46 Broad Spectrum sunscreen', 'KCN08', 1, 750000.0000, 750000.0000, 0.0000, 0),
(58, 31, 58, 'Kem chống nắng Clarins Day Screen SPF 40 UVA-UVB Oil-free', 'KCN02', 1, 1300000.0000, 1300000.0000, 0.0000, 0),
(59, 32, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(60, 33, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(61, 34, 80, 'Kem chống nắng vật lý Blue Lizard SPF 30', 'KCN03', 1, 340000.0000, 340000.0000, 0.0000, 0),
(62, 35, 79, 'Kem Chống Nắng Neutrogena Pure &amp; Free Liquid Sunscreen SPF 50', 'KCN01', 1, 350000.0000, 350000.0000, 0.0000, 0),
(63, 36, 79, 'Kem Chống Nắng Neutrogena Pure &amp; Free Liquid Sunscreen SPF 50', 'KCN01', 1, 350000.0000, 350000.0000, 0.0000, 0),
(64, 37, 98, 'Neutrogena Healthy Defense Daily Moisturizer SPF 50 with Helioplex', 'KCN11', 1, 340000.0000, 340000.0000, 0.0000, 0),
(66, 38, 59, 'Body Lotion Japanese Cherry Blossom', 'BB02', 1, 210000.0000, 210000.0000, 0.0000, 0),
(70, 42, 99, 'Kem chống nắng dạng xịt Neutrogena Ultra Sheer Body Mist Sunscreen Broad Spectrum SPF 70', 'KCN12', 1, 370000.0000, 370000.0000, 0.0000, 0),
(69, 41, 94, 'Kem chống nắng Cilinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40', 'KCN09', 1, 650000.0000, 650000.0000, 0.0000, 0),
(71, 43, 86, 'Mascara Cover Girl - Clump Crusher', 'M02', 1, 165000.0000, 165000.0000, 0.0000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_recurring`
--

CREATE TABLE IF NOT EXISTS `order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `profile_name` varchar(255) NOT NULL,
  `profile_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `profile_reference` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_recurring_transaction`
--

CREATE TABLE IF NOT EXISTS `order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired');

-- --------------------------------------------------------

--
-- Table structure for table `order_total`
--

CREATE TABLE IF NOT EXISTS `order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_total`
--

INSERT INTO `order_total` (`order_total_id`, `order_id`, `code`, `title`, `text`, `value`, `sort_order`) VALUES
(12, 1, 'total', 'Tổng cộng :', '150,000 VNĐ', 150000.0000, 9),
(11, 1, 'sub_total', 'Thành tiền:', '150,000 VNĐ', 150000.0000, 1),
(26, 4, 'total', 'Tổng cộng :', '200,000 Đ', 200000.0000, 9),
(25, 4, 'sub_total', 'Thành tiền:', '200,000 Đ', 200000.0000, 1),
(18, 3, 'total', 'Tổng cộng :', '500,000 VNĐ', 500000.0000, 9),
(17, 3, 'sub_total', 'Thành tiền:', '500,000 VNĐ', 500000.0000, 1),
(43, 11, 'sub_total', 'Thành tiền:', '1,400,000 Đ', 1400000.0000, 1),
(44, 11, 'total', 'Tổng cộng :', '1,400,000 Đ', 1400000.0000, 9),
(45, 12, 'sub_total', 'Thành tiền:', '350,000 Đ', 350000.0000, 1),
(46, 12, 'total', 'Tổng cộng :', '350,000 Đ', 350000.0000, 9),
(47, 13, 'sub_total', 'Thành tiền:', '650,000 Đ', 650000.0000, 1),
(48, 13, 'total', 'Tổng cộng :', '650,000 Đ', 650000.0000, 9),
(49, 14, 'sub_total', 'Thành tiền:', '330,000 Đ', 330000.0000, 1),
(50, 14, 'total', 'Tổng cộng :', '330,000 Đ', 330000.0000, 9),
(65, 17, 'sub_total', 'Thành tiền:', '1,300,000 Đ', 1300000.0000, 1),
(66, 17, 'total', 'Tổng cộng :', '1,300,000 Đ', 1300000.0000, 9),
(70, 18, 'total', 'Tổng cộng :', '1,300,000 Đ', 1300000.0000, 9),
(69, 18, 'sub_total', 'Thành tiền:', '1,300,000 Đ', 1300000.0000, 1),
(73, 19, 'sub_total', 'Thành tiền:', '750,000 Đ', 750000.0000, 1),
(74, 19, 'total', 'Tổng cộng :', '750,000 Đ', 750000.0000, 9),
(77, 20, 'sub_total', 'Thành tiền:', '750,000 Đ', 750000.0000, 1),
(78, 20, 'total', 'Tổng cộng :', '750,000 Đ', 750000.0000, 9),
(79, 21, 'sub_total', 'Thành tiền:', '750,000 Đ', 750000.0000, 1),
(80, 21, 'total', 'Tổng cộng :', '750,000 Đ', 750000.0000, 9),
(81, 22, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(82, 22, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(83, 23, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(84, 23, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(90, 24, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(89, 24, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(97, 25, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(98, 25, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(105, 26, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(106, 26, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(107, 27, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(108, 27, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(109, 28, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(110, 28, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(111, 29, 'sub_total', 'Thành tiền:', '750,000 Đ', 750000.0000, 1),
(112, 29, 'total', 'Tổng cộng :', '750,000 Đ', 750000.0000, 9),
(113, 30, 'sub_total', 'Thành tiền:', '750,000 Đ', 750000.0000, 1),
(114, 30, 'total', 'Tổng cộng :', '750,000 Đ', 750000.0000, 9),
(115, 31, 'sub_total', 'Thành tiền:', '1,300,000 Đ', 1300000.0000, 1),
(116, 31, 'total', 'Tổng cộng :', '1,300,000 Đ', 1300000.0000, 9),
(117, 32, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(118, 32, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(119, 33, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(120, 33, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(121, 34, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(122, 34, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(123, 35, 'sub_total', 'Thành tiền:', '350,000 Đ', 350000.0000, 1),
(124, 35, 'total', 'Tổng cộng :', '350,000 Đ', 350000.0000, 9),
(125, 36, 'sub_total', 'Thành tiền:', '350,000 Đ', 350000.0000, 1),
(126, 36, 'total', 'Tổng cộng :', '350,000 Đ', 350000.0000, 9),
(127, 37, 'sub_total', 'Thành tiền:', '340,000 Đ', 340000.0000, 1),
(128, 37, 'total', 'Tổng cộng :', '340,000 Đ', 340000.0000, 9),
(131, 38, 'sub_total', 'Thành tiền:', '210,000 Đ', 210000.0000, 1),
(132, 38, 'total', 'Tổng cộng :', '210,000 Đ', 210000.0000, 9),
(140, 42, 'total', 'Tổng cộng :', '370,000 Đ', 370000.0000, 9),
(141, 43, 'sub_total', 'Thành tiền:', '165,000 Đ', 165000.0000, 1),
(139, 42, 'sub_total', 'Thành tiền:', '370,000 Đ', 370000.0000, 1),
(137, 41, 'sub_total', 'Thành tiền:', '650,000 Đ', 650000.0000, 1),
(138, 41, 'total', 'Tổng cộng :', '650,000 Đ', 650000.0000, 9),
(142, 43, 'total', 'Tổng cộng :', '165,000 Đ', 165000.0000, 9);

-- --------------------------------------------------------

--
-- Table structure for table `order_voucher`
--

CREATE TABLE IF NOT EXISTS `order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pavoslidergroups`
--

CREATE TABLE IF NOT EXISTS `pavoslidergroups` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pavoslidergroups`
--

INSERT INTO `pavoslidergroups` (`id`, `title`, `params`) VALUES
(1, 'Slide', 'a:28:{s:5:"title";s:5:"Slide";s:5:"delay";s:4:"5000";s:9:"fullwidth";s:0:"";s:5:"width";s:3:"980";s:6:"height";s:3:"461";s:12:"touch_mobile";s:1:"1";s:13:"stop_on_hover";s:1:"1";s:12:"shuffle_mode";s:1:"0";s:14:"image_cropping";s:1:"0";s:11:"shadow_type";s:1:"2";s:14:"show_time_line";s:1:"1";s:18:"time_line_position";s:3:"top";s:16:"background_color";s:6:"#00000";s:6:"margin";s:0:"";s:7:"padding";s:0:"";s:16:"background_image";s:1:"0";s:14:"background_url";s:0:"";s:14:"navigator_type";s:4:"none";s:16:"navigator_arrows";s:16:"verticalcentered";s:16:"navigation_style";s:5:"round";s:17:"offset_horizontal";s:1:"0";s:15:"offset_vertical";s:2:"20";s:14:"show_navigator";s:1:"0";s:20:"hide_navigator_after";s:3:"200";s:15:"thumbnail_width";s:3:"100";s:16:"thumbnail_height";s:2:"50";s:16:"thumbnail_amount";s:1:"5";s:17:"hide_screen_width";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `pavosliderlayers`
--

CREATE TABLE IF NOT EXISTS `pavosliderlayers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `params` text NOT NULL,
  `layersparams` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pavosliderlayers`
--

INSERT INTO `pavosliderlayers` (`id`, `title`, `parent_id`, `group_id`, `params`, `layersparams`, `image`, `status`, `position`) VALUES
(11, 'Slide 4', 0, 1, 'a:16:{s:2:"id";s:2:"11";s:15:"slider_group_id";s:1:"1";s:12:"slider_title";s:7:"Slide 4";s:13:"slider_status";s:1:"1";s:17:"slider_transition";s:7:"turnoff";s:11:"slider_slot";s:1:"7";s:15:"slider_rotation";s:1:"0";s:15:"slider_duration";s:3:"300";s:12:"slider_delay";s:1:"0";s:11:"slider_link";s:0:"";s:16:"slider_thumbnail";s:0:"";s:15:"slider_usevideo";s:1:"0";s:14:"slider_videoid";s:0:"";s:16:"slider_videoplay";s:1:"0";s:9:"slider_id";s:2:"11";s:12:"slider_image";s:38:"data/Banner-Slide 23-6-15/Slide1 b.jpg";}', 'O:8:"stdClass":1:{s:6:"layers";a:2:{i:0;a:20:{s:16:"layer_video_type";s:7:"youtube";s:14:"layer_video_id";s:0:"";s:18:"layer_video_height";s:3:"200";s:17:"layer_video_width";s:3:"300";s:17:"layer_video_thumb";s:0:"";s:8:"layer_id";i:1;s:13:"layer_content";s:36:"data/Banner-Slide 23-6-15/Slide1.png";s:10:"layer_type";s:5:"image";s:11:"layer_class";s:0:"";s:13:"layer_caption";s:17:"Your Image Here 1";s:15:"layer_animation";s:4:"fade";s:12:"layer_easing";s:11:"easeOutExpo";s:11:"layer_speed";s:3:"350";s:9:"layer_top";s:1:"0";s:10:"layer_left";s:1:"0";s:13:"layer_endtime";s:1:"0";s:14:"layer_endspeed";s:3:"300";s:18:"layer_endanimation";s:4:"auto";s:15:"layer_endeasing";s:7:"nothing";s:10:"time_start";s:3:"400";}i:1;a:20:{s:16:"layer_video_type";s:7:"youtube";s:14:"layer_video_id";s:0:"";s:18:"layer_video_height";s:3:"200";s:17:"layer_video_width";s:3:"300";s:17:"layer_video_thumb";s:0:"";s:8:"layer_id";i:2;s:13:"layer_content";s:38:"data/Banner-Slide 23-6-15/Slide1 b.jpg";s:10:"layer_type";s:5:"image";s:11:"layer_class";s:0:"";s:13:"layer_caption";s:17:"Your Image Here 2";s:15:"layer_animation";s:4:"fade";s:12:"layer_easing";s:11:"easeOutExpo";s:11:"layer_speed";s:3:"350";s:9:"layer_top";s:1:"0";s:10:"layer_left";s:1:"0";s:13:"layer_endtime";s:1:"0";s:14:"layer_endspeed";s:3:"300";s:18:"layer_endanimation";s:4:"auto";s:15:"layer_endeasing";s:7:"nothing";s:10:"time_start";s:3:"800";}}}', 'data/Banner-Slide 23-6-15/Slide1 b.jpg', 1, 0),
(12, 'Slide 5', 0, 1, 'a:16:{s:2:"id";s:2:"12";s:15:"slider_group_id";s:1:"1";s:12:"slider_title";s:7:"Slide 5";s:13:"slider_status";s:1:"1";s:17:"slider_transition";s:7:"turnoff";s:11:"slider_slot";s:1:"7";s:15:"slider_rotation";s:1:"0";s:15:"slider_duration";s:3:"300";s:12:"slider_delay";s:1:"0";s:11:"slider_link";s:0:"";s:16:"slider_thumbnail";s:0:"";s:15:"slider_usevideo";s:1:"0";s:14:"slider_videoid";s:0:"";s:16:"slider_videoplay";s:1:"0";s:9:"slider_id";s:2:"12";s:12:"slider_image";s:38:"data/Banner-Slide 23-6-15/Slide2 b.jpg";}', 'O:8:"stdClass":1:{s:6:"layers";a:1:{i:0;a:20:{s:16:"layer_video_type";s:7:"youtube";s:14:"layer_video_id";s:0:"";s:18:"layer_video_height";s:3:"200";s:17:"layer_video_width";s:3:"300";s:17:"layer_video_thumb";s:0:"";s:8:"layer_id";i:1;s:13:"layer_content";s:38:"data/Banner-Slide 23-6-15/Slide2 b.jpg";s:10:"layer_type";s:5:"image";s:11:"layer_class";s:0:"";s:13:"layer_caption";s:17:"Your Image Here 1";s:15:"layer_animation";s:4:"fade";s:12:"layer_easing";s:11:"easeOutExpo";s:11:"layer_speed";s:3:"350";s:9:"layer_top";s:1:"0";s:10:"layer_left";s:1:"0";s:13:"layer_endtime";s:1:"0";s:14:"layer_endspeed";s:3:"300";s:18:"layer_endanimation";s:4:"auto";s:15:"layer_endeasing";s:7:"nothing";s:10:"time_start";s:3:"400";}}}', 'data/Banner-Slide 23-6-15/Slide2 b.jpg', 1, 0),
(13, 'Slide 6', 0, 1, 'a:16:{s:2:"id";s:2:"13";s:15:"slider_group_id";s:1:"1";s:12:"slider_title";s:7:"Slide 6";s:13:"slider_status";s:1:"1";s:17:"slider_transition";s:7:"turnoff";s:11:"slider_slot";s:1:"7";s:15:"slider_rotation";s:1:"0";s:15:"slider_duration";s:3:"300";s:12:"slider_delay";s:1:"0";s:11:"slider_link";s:0:"";s:16:"slider_thumbnail";s:0:"";s:15:"slider_usevideo";s:1:"0";s:14:"slider_videoid";s:0:"";s:16:"slider_videoplay";s:1:"0";s:9:"slider_id";s:2:"13";s:12:"slider_image";s:38:"data/Banner-Slide 23-6-15/Slide3 b.jpg";}', 'O:8:"stdClass":1:{s:6:"layers";a:1:{i:0;a:20:{s:16:"layer_video_type";s:7:"youtube";s:14:"layer_video_id";s:0:"";s:18:"layer_video_height";s:3:"200";s:17:"layer_video_width";s:3:"300";s:17:"layer_video_thumb";s:0:"";s:8:"layer_id";i:1;s:13:"layer_content";s:38:"data/Banner-Slide 23-6-15/Slide3 b.jpg";s:10:"layer_type";s:5:"image";s:11:"layer_class";s:0:"";s:13:"layer_caption";s:17:"Your Image Here 1";s:15:"layer_animation";s:4:"fade";s:12:"layer_easing";s:11:"easeOutExpo";s:11:"layer_speed";s:3:"350";s:9:"layer_top";s:1:"0";s:10:"layer_left";s:1:"0";s:13:"layer_endtime";s:1:"0";s:14:"layer_endspeed";s:3:"300";s:18:"layer_endanimation";s:4:"auto";s:15:"layer_endeasing";s:7:"nothing";s:10:"time_start";s:3:"400";}}}', 'data/Banner-Slide 23-6-15/Slide3 b.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(13) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL,
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `date_added`, `date_modified`, `viewed`) VALUES
(61, 'BB04', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Lotion/Wild Madagascar Vanilla.jpg', 12, 1, 290000.0000, 0, 0, '2014-10-27', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-06 20:01:45', '2014-12-04 22:17:40', 1707),
(60, 'BB03', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Lotion/Twilight Woods.jpg', 12, 1, 290000.0000, 0, 0, '2014-10-27', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-06 20:01:45', '2014-12-04 22:17:32', 1670),
(57, 'BB01', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Lotion/Dark Kiss.jpg', 12, 1, 220000.0000, 0, 0, '2014-10-27', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-10-27 23:54:53', '2016-01-20 13:54:36', 2090),
(59, 'BB02', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Lotion/Japanese Cherry Blossom.jpg', 12, 1, 290000.0000, 0, 0, '2014-10-27', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-06 20:01:39', '2015-01-09 18:12:13', 3091),
(58, 'KCN02', '', '', '', '', '', '', '', 4, 5, 'data/My pham/Kem chong nang Clarins.JPG', 14, 1, 1700000.0000, 0, 0, '2014-10-31', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-01 13:01:23', '2015-08-02 04:17:33', 7283),
(62, 'BM01', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Mist/Dark Kiss(1).jpg', 12, 1, 290000.0000, 0, 0, '2014-11-06', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-07 02:16:12', '2014-12-04 22:17:53', 1797),
(63, 'BM02', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Mist/Japanese Cherry Blossom(1).jpg', 12, 1, 290000.0000, 0, 0, '2014-11-06', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-07 02:16:23', '2014-12-04 22:18:03', 1445),
(64, 'BM03', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Mist/Twilight Woods(1).jpg', 12, 1, 290000.0000, 0, 0, '2014-11-06', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-07 02:16:33', '2014-12-04 22:18:12', 1334),
(65, 'BM04', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Mist/Wild Madagascar Vanilla(1).jpg', 12, 1, 290000.0000, 0, 0, '2014-11-06', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-07 02:16:33', '2014-12-04 22:18:21', 1447),
(66, 'BG04', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Golden Autumn Citrus.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:33:26', '2014-11-09 23:48:11', 1231),
(67, 'BG02', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Dancing Waters.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:06', '2014-11-09 23:45:56', 1394),
(68, 'BG01', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Aqua Blossom.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:12', '2014-11-09 23:48:54', 1433),
(69, 'BG05', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Island Margarita.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:12', '2014-11-09 23:50:00', 1325),
(70, 'BG06', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Ocean for Men.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:19', '2014-11-09 23:51:04', 1302),
(71, 'BG07', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Pumpkin Cupcake.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:19', '2014-11-09 23:51:52', 1104),
(72, 'BG08', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Snow Kissed Berry.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:19', '2014-11-09 23:52:50', 1157),
(73, 'BG03', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Fresh Sparkling Snow.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:44:19', '2014-11-09 23:47:01', 1384),
(74, 'BG09', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_Warm Vanilla Sugar.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:53:13', '2014-11-09 23:54:00', 1284),
(75, 'BG10', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_White Pear & Fig.jpg', 12, 1, 35000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:53:13', '2014-11-09 23:55:06', 1161),
(76, 'BG11', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_5-Pack Classic Faves.jpg', 12, 1, 150000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:53:13', '2014-11-18 23:56:18', 1366),
(77, 'BG12', '', '', '', '', '', '', '', 1000, 5, 'data/BBW/Hand soap/HG_5-Pack Fall Fresh Picked.jpg', 12, 1, 150000.0000, 0, 0, '2014-11-09', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-09 23:53:13', '2014-11-09 23:57:43', 1300),
(80, 'KCN03', '', '', '', '', '', '', '', 2, 5, 'data/My pham/Kem chong nang Blue Lizard.JPG', 20, 1, 340000.0000, 0, 0, '2014-11-17', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-18 08:32:29', '2015-07-26 08:45:36', 4275),
(79, 'KCN01', '', '', '', '', '', '', '', 8, 5, 'data/My pham/Neutrogena Pure and Free Liquid, SPF 50, 1.4 Ounce.JPG', 15, 1, 350000.0000, 0, 0, '2014-11-15', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-16 09:56:00', '2015-07-27 05:07:16', 2983),
(81, 'KCN04', '', '', '', '', '', '', '', 10, 5, 'data/My pham/Kem chong nang Neutrogena.JPG', 15, 1, 350000.0000, 0, 0, '2014-11-15', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-11-18 23:32:46', '2015-01-14 11:36:38', 3549),
(82, 'S01', '', '', '', '', '', '', '', 100, 5, 'data/My pham/Son Loreal mau moi.JPG', 19, 1, 200000.0000, 0, 0, '2014-12-29', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2014-12-30 08:59:26', '2015-01-14 11:40:33', 749),
(85, 'M01', '', '', '', '', '', '', '', 10, 5, 'data/My pham/Mascara Maybelline.JPG', 0, 1, 165000.0000, 0, 0, '2015-01-13', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-01-14 12:19:37', '2015-01-14 12:28:45', 1157),
(83, 'TN01', '', '', '', '', '', '', '', 10, 5, 'data/My pham/Toner Earth Science.JPG', 16, 1, 290000.0000, 0, 0, '2015-01-12', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-01-13 01:06:52', '2015-01-13 18:27:22', 1645),
(84, 'GC01', '', '', '', '', '', '', '', 100, 5, 'data/iBlue/san pham giam can an toan iBlue.jpg', 0, 1, 1700000.0000, 0, 0, '2015-01-13', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-01-14 05:29:04', '2015-01-14 10:25:40', 785),
(86, 'M02', '', '', '', '', '', '', '', 9, 5, 'data/My pham/Mascara CoverGirl Clump Crusher.JPG', 0, 1, 165000.0000, 0, 0, '2015-01-13', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-01-14 12:28:05', '0000-00-00 00:00:00', 1516),
(87, 'KCN05', '', '', '', '', '', '', '', 100, 5, '', 0, 1, 0.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 0, '2015-06-23 15:26:33', '2015-07-17 15:39:20', 70),
(93, 'KCN08', '', '', '', '', '', '', '', 99, 5, 'data/My pham/20150529_112638.jpg', 0, 1, 750000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 15:55:49', '2015-07-16 23:50:18', 2534),
(92, 'KCN07', '', '', '', '', '', '', '', 100, 5, 'data/My pham/20150717_151202.jpg', 0, 1, 750000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 15:40:09', '2015-07-24 07:07:06', 1516),
(91, 'KCN06', '', '', '', '', '', '', '', 100, 5, '', 0, 1, 0.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 0, '2015-06-23 15:32:29', '2015-07-17 15:39:42', 57),
(94, 'KCN09', '', '', '', '', '', '', '', 98, 5, 'data/My pham/kem-chong-nang-Clinique-Super-City-Block-Oil-Free-Daily-Face-Protector-Broad-Spectrum-SPF-40.png', 0, 1, 650000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:04:51', '2015-07-17 15:12:10', 4054),
(99, 'KCN12', '', '', '', '', '', '', '', 99, 5, 'data/My pham/20150724_133706.jpg', 0, 1, 370000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:26:11', '2016-04-29 10:53:32', 2141),
(107, 'KCN10', '', '', '', '', '', '', '', 100, 5, 'data/My pham/banana boat.jpg', 0, 1, 350000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:49:26', '2015-07-16 22:43:03', 1959),
(98, 'KCN11', '', '', '', '', '', '', '', 99, 5, 'data/My pham/kem chong nang Neutrogena Healthy Defense Daily Moisturizer SPF 50 with Helioplex.jpg', 0, 1, 340000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:19:01', '2015-07-17 15:40:17', 1410),
(101, 'KCN13', '', '', '', '', '', '', '', 100, 5, 'data/My pham/KCN_laroche60.jpg', 0, 1, 800000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:29:50', '2015-06-23 17:11:30', 2529),
(102, 'SRM02', '', '', '', '', '', '', '', 100, 5, 'data/My pham/Sua-rua-mat-CeraVe.png', 0, 1, 380000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:33:38', '2015-07-16 23:43:14', 3785),
(104, 'SRM03', '', '', '', '', '', '', '', 100, 5, 'data/My pham/Suagiammun.jpg', 0, 1, 260000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:38:22', '2015-07-16 22:52:23', 9829),
(105, 'SRM04', '', '', '', '', '', '', '', 100, 5, 'data/My pham/Sua rua mat tri mun Neutrogena Oil-Free Acne Wash 2.jpg', 15, 1, 350000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:40:35', '2015-07-16 22:48:34', 2166),
(106, 'SRM05', '', '', '', '', '', '', '', 99, 5, 'data/My pham/Sua rua mat tri mun dau den Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub.jpg', 0, 1, 330000.0000, 0, 0, '2015-06-22', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, '2015-06-23 16:43:41', '2015-07-16 23:44:55', 2549);

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE IF NOT EXISTS `product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_description`
--

CREATE TABLE IF NOT EXISTS `product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_description`
--

INSERT INTO `product_description` (`product_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `tag`) VALUES
(57, 1, 'Body Lotion Dark Kiss ', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath &amp;amp; Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;là một nhãn hiệu chuyên về các sản phẩm dưỡng thể và chăm sóc da của tập đoàn&lt;/span&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;&amp;nbsp;&lt;span style=&quot;color:#EE82EE;&quot;&gt;Victoria’s Secret&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;. Thế mạnh của&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath And Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;không chỉ ở sản phẩm chất lượng giúp duy trì làn da mềm mại, tươi trẻ, mà còn ở các loại&amp;nbsp;mùi hương đa dạng và đầy sức hút của bản thân sản phẩm.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;SHEA &amp;amp; VITAMIN E BODY LOTION&lt;/strong&gt; Sữa dưỡng thể toàn thân cung cấp vitamin E và khoáng chất giúp giữ ẩm thường xuyên cho làn da bằng một lớp sữa mịn màng sẽ giúp bạn đánh bật vẻ xù xì, thô ráp do thời tiết lạnh hay nắng gắt gây ra và có được vẻ ngoài rạng rỡ, tươi trẻ. Ngoài ra, với tinh chất vitamin E trong sữa dưỡng thể giúp phục hồi làn da khô ráp trở nên mềm mịn và tươi sáng&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;sau khi tắm xong, lau khô người, lấy một lượng sữa dưỡng ẩm vừa đủ bôi lên khắp cơ thể, vừa bôi vừa nhẹ nhàng mát xa làn da của bạn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Khuyên dùng:&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân, xịt thơm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Loại sản phẩm: Dưỡng da&lt;/strong&gt;&lt;br /&gt;\r\n&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&lt;br /&gt;\r\n&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Dung tích: 236ml&lt;br style=&quot;font-family: Arial !important;&quot; /&gt;\r\nNơi sản xuất:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;/USA&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, có hóa đơn mua hàng từ Mỹ', '', 'Bath-Body-Works, Body-Lotion-Kem-duong-da'),
(83, 1, 'Toner Earth Science - Aloe Vera Complexion Toner &amp; Freshener', '&lt;h1 style=&quot;color: rgb(34, 34, 34); font-family: Arial, Verdana, sans-serif; line-height: normal;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;Em toner này mới được lăng xê làm mưa làm gió trên thị trường. Dễ hiểu thôi bởi vì quá ư là tốt vượt&amp;nbsp;&lt;span class=&quot;text_exposed_show&quot;&gt;ngoài mong đợi không còn gì để khen&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;\r\n\r\n&lt;h2 style=&quot;color: rgb(34, 34, 34); font-family: Arial, Verdana, sans-serif; line-height: normal;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;Toner Earth Science được rate cao ngất trời.&lt;/span&gt;&lt;/h2&gt;\r\n\r\n&lt;h3 style=&quot;color: rgb(34, 34, 34); font-family: Arial, Verdana, sans-serif; line-height: normal;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;Cũng giống như em sữa rửa mặt em toner Earth Science thành phần &quot;sạch&quot;, độ pH ổn giúp cân bằng được độ pH, da nhạy cảm dùng rất ok, thiên nhiên ngoài mong đợi Thấm nhanh, ko nhờn. Bôi lên mát ơi là mát sảng khoái vô cùng. Tóm lại là đỉnh của đỉnh&lt;/span&gt;&lt;/h3&gt;\r\n\r\n&lt;h4 style=&quot;color: rgb(34, 34, 34); font-family: Arial, Verdana, sans-serif; line-height: normal;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;Một sản phẩm toner độc đáo không cồn, làm sạch và mềm da với độ pH thích hợp của nó trong khi loại bỏ tất cả dấu vết cuối cùng của bụi bẩn, trang điểm và các tạp chất. Điều này làm sống lại, làm mát giúp se lỗ chân lông. Nhưng nó lại không làm khô, và thực sự giúp dưỡng ẩm cho da với sự phong phú của 85% lô hội, Panthenol, Hyaluronic Acid và biển Tảo bẹ. Độ&amp;nbsp;&lt;/span&gt;pH 5.5.&lt;/h4&gt;\r\n', 'Shop bán Toner Earth Science xách tay giá rẻ,chất lượng tốt,có hóa đơn đảm bảo', '', 'toner,earth science'),
(59, 1, 'Body Lotion Japanese Cherry Blossom', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath &amp;amp; Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;là một nhãn hiệu chuyên về các sản phẩm dưỡng thể và chăm sóc da của tập đoàn&lt;/span&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;&amp;nbsp;&lt;span style=&quot;color:#EE82EE;&quot;&gt;Victoria’s Secret&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;. Thế mạnh của&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath And Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;không chỉ ở sản phẩm chất lượng giúp duy trì làn da mềm mại, tươi trẻ, mà còn ở các loại&amp;nbsp;mùi hương đa dạng và đầy sức hút của bản thân sản phẩm.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;SHEA &amp;amp; VITAMIN E BODY LOTION&lt;/strong&gt; Sữa dưỡng thể toàn thân cung cấp vitamin E và khoáng chất giúp giữ ẩm thường xuyên cho làn da bằng một lớp sữa mịn màng sẽ giúp bạn đánh bật vẻ xù xì, thô ráp do thời tiết lạnh hay nắng gắt gây ra và có được vẻ ngoài rạng rỡ, tươi trẻ. Ngoài ra, với tinh chất vitamin E trong sữa dưỡng thể giúp phục hồi làn da khô ráp trở nên mềm mịn và tươi sáng&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;sau khi tắm xong, lau khô người, lấy một lượng sữa dưỡng ẩm vừa đủ bôi lên khắp cơ thể, vừa bôi vừa nhẹ nhàng mát xa làn da của bạn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Khuyên dùng:&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân, xịt thơm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Loại sản phẩm: Dưỡng da&lt;/strong&gt;&lt;br /&gt;\r\n&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&amp;nbsp;&lt;/span&gt;Japanese Cherry Blossom&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Dung tích: 236ml&lt;br style=&quot;font-family: Arial !important;&quot; /&gt;\r\nNơi sản xuất:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;/USA&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, J', 'Bath &amp; Body Works, Japanese Cherry Blossom, Body Lotion - Kem dưỡng da'),
(60, 1, 'Body Lotion Twilight Woods', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath &amp;amp; Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;là một nhãn hiệu chuyên về các sản phẩm dưỡng thể và chăm sóc da của tập đoàn&lt;/span&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;&amp;nbsp;&lt;span style=&quot;color:#EE82EE;&quot;&gt;Victoria’s Secret&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;. Thế mạnh của&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath And Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;không chỉ ở sản phẩm chất lượng giúp duy trì làn da mềm mại, tươi trẻ, mà còn ở các loại&amp;nbsp;mùi hương đa dạng và đầy sức hút của bản thân sản phẩm.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;SHEA &amp;amp; VITAMIN E BODY LOTION&lt;/strong&gt; Sữa dưỡng thể toàn thân cung cấp vitamin E và khoáng chất giúp giữ ẩm thường xuyên cho làn da bằng một lớp sữa mịn màng sẽ giúp bạn đánh bật vẻ xù xì, thô ráp do thời tiết lạnh hay nắng gắt gây ra và có được vẻ ngoài rạng rỡ, tươi trẻ. Ngoài ra, với tinh chất vitamin E trong sữa dưỡng thể giúp phục hồi làn da khô ráp trở nên mềm mịn và tươi sáng&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;sau khi tắm xong, lau khô người, lấy một lượng sữa dưỡng ẩm vừa đủ bôi lên khắp cơ thể, vừa bôi vừa nhẹ nhàng mát xa làn da của bạn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Khuyên dùng:&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân, xịt thơm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Loại sản phẩm: Dưỡng da&lt;/strong&gt;&lt;br /&gt;\r\n&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&amp;nbsp;&lt;/span&gt;Twilight Woods&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Dung tích: 236ml&lt;br style=&quot;font-family: Arial !important;&quot; /&gt;\r\nNơi sản xuất:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;/USA&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, T', 'Bath &amp; Body Works,Twilight Woods'),
(61, 1, 'Body Lotion Wild Madagascar Vanilla', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath &amp;amp; Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;là một nhãn hiệu chuyên về các sản phẩm dưỡng thể và chăm sóc da của tập đoàn&lt;/span&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;&amp;nbsp;&lt;span style=&quot;color:#EE82EE;&quot;&gt;Victoria’s Secret&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;. Thế mạnh của&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 16px; color: rgb(0, 0, 255);&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Bath And Body Works&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 16px;&quot;&gt;&amp;nbsp;không chỉ ở sản phẩm chất lượng giúp duy trì làn da mềm mại, tươi trẻ, mà còn ở các loại&amp;nbsp;mùi hương đa dạng và đầy sức hút của bản thân sản phẩm.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;SHEA &amp;amp; VITAMIN E BODY LOTION&lt;/strong&gt; Sữa dưỡng thể toàn thân cung cấp vitamin E và khoáng chất giúp giữ ẩm thường xuyên cho làn da bằng một lớp sữa mịn màng sẽ giúp bạn đánh bật vẻ xù xì, thô ráp do thời tiết lạnh hay nắng gắt gây ra và có được vẻ ngoài rạng rỡ, tươi trẻ. Ngoài ra, với tinh chất vitamin E trong sữa dưỡng thể giúp phục hồi làn da khô ráp trở nên mềm mịn và tươi sáng&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;sau khi tắm xong, lau khô người, lấy một lượng sữa dưỡng ẩm vừa đủ bôi lên khắp cơ thể, vừa bôi vừa nhẹ nhàng mát xa làn da của bạn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Khuyên dùng:&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân, xịt thơm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Loại sản phẩm: Dưỡng da&lt;/strong&gt;&lt;br /&gt;\r\n&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&amp;nbsp;&lt;strong&gt;Wild Madagascar Vanilla&lt;/strong&gt;&lt;/span&gt;&lt;br /&gt;\r\n&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;Dung tích: 236ml&lt;br style=&quot;font-family: Arial !important;&quot; /&gt;\r\nNơi sản xuất:&amp;nbsp;&lt;/strong&gt;Bath &amp;amp; Body Works®&lt;strong style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; font-size: 15px; line-height: 20.5499992370605px;&quot;&gt;/USA&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, W', 'Bath &amp; Body Works'),
(62, 1, 'Fragrance Mist Dark Kiss', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;&lt;/span&gt;Lắc đều, hướng vòi xịt vào vị trí cần xịt, cách cơ thể khoảng 15 – 20 cm. Xịt ở những vùng cơ thể tập trung nhiều mạch máu, da ẩm và nhạy cảm như cổ tay, sau gáy, giữa ngực, để hương thơm có thể khuếch tán đồng thời cũng giữ mùi được lâu hơn. Hạn chế xịt trực tiếp lên quần áo và đồ trang sức.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Khuyên dùng:&lt;/span&gt;&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Quy cách: chai 236ml oz 8fl Bảo&amp;nbsp; quản: nơi khô ráo, thoáng mát, tránh ánh nắng trực tiếp.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Xuất xứ: Mỹ.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Sản xuất bởi: Bath &amp;amp; Body Works &amp;nbsp;- Dark Kiss&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, l', 'Fragrance Mist Dark Kiss,Bath &amp; Body Works'),
(63, 1, 'Fragrance Mist Japanese Cherry Blossom', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;&lt;/span&gt;Lắc đều, hướng vòi xịt vào vị trí cần xịt, cách cơ thể khoảng 15 – 20 cm. Xịt ở những vùng cơ thể tập trung nhiều mạch máu, da ẩm và nhạy cảm như cổ tay, sau gáy, giữa ngực, để hương thơm có thể khuếch tán đồng thời cũng giữ mùi được lâu hơn. Hạn chế xịt trực tiếp lên quần áo và đồ trang sức.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Khuyên dùng:&lt;/span&gt;&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Quy cách: chai 236ml oz 8fl Bảo&amp;nbsp; quản: nơi khô ráo, thoáng mát, tránh ánh nắng trực tiếp.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Xuất xứ: Mỹ.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Sản xuất bởi: Bath &amp;amp; Body Works &amp;nbsp;- &lt;/span&gt;&lt;strong&gt;Japanese Cherry Blossom&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, J', 'Fragrance Mist Japanese Cherry Blossom,Bath &amp; Body Works'),
(64, 1, 'Fragrance Mist Twilight Woods', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;&lt;/span&gt;Lắc đều, hướng vòi xịt vào vị trí cần xịt, cách cơ thể khoảng 15 – 20 cm. Xịt ở những vùng cơ thể tập trung nhiều mạch máu, da ẩm và nhạy cảm như cổ tay, sau gáy, giữa ngực, để hương thơm có thể khuếch tán đồng thời cũng giữ mùi được lâu hơn. Hạn chế xịt trực tiếp lên quần áo và đồ trang sức.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Khuyên dùng:&lt;/span&gt;&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Quy cách: chai 236ml oz 8fl Bảo&amp;nbsp; quản: nơi khô ráo, thoáng mát, tránh ánh nắng trực tiếp.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Xuất xứ: Mỹ.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Sản xuất bởi: Bath &amp;amp; Body Works &amp;nbsp;- &lt;/span&gt;&lt;strong&gt;Twilight Woods&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works,Body Mist Bath&amp;Body Works,Hand Lotion Bath&amp;Body Works,Hand wash Bath&amp;Body Works,Hand cream Bath&amp;Body Works,Perfume Bath&amp;Body Works,kem dưỡng Bath&amp;Body Works,nước rửa tay Bath&amp;Body Works,Twilight ', 'Fragrance Mist Twilight Woods,Bath &amp; Body Works'),
(65, 1, 'Fragrance Mist Wild Madagascar Vanilla', '&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Hướng dẫn sử dụng:&amp;nbsp;&lt;/span&gt;Lắc đều, hướng vòi xịt vào vị trí cần xịt, cách cơ thể khoảng 15 – 20 cm. Xịt ở những vùng cơ thể tập trung nhiều mạch máu, da ẩm và nhạy cảm như cổ tay, sau gáy, giữa ngực, để hương thơm có thể khuếch tán đồng thời cũng giữ mùi được lâu hơn. Hạn chế xịt trực tiếp lên quần áo và đồ trang sức.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Khuyên dùng:&lt;/span&gt;&amp;nbsp;Để giữ mùi hương lâu bền hơn và cho làn da hoàn hảo, bạn nên dùng kèm với các sản phẩm cùng dòng như sữa tắm, tẩy tế bào chết toàn thân, dưỡng ẩm toàn thân.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Quy cách: chai 236ml oz 8fl Bảo&amp;nbsp; quản: nơi khô ráo, thoáng mát, tránh ánh nắng trực tiếp.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Xuất xứ: Mỹ.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;font-family: Arial; margin: 0px; padding: 0px 0px 10px; border: 0px; font-stretch: normal; font-size: 0.9em; line-height: 20px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;font-family: Candara, Helvetica, sans-serif; margin: 0px; padding: 0px; border: 0px; font-stretch: normal; line-height: 20.5499992370605px;&quot;&gt;Sản xuất bởi: Bath &amp;amp; Body Works &amp;nbsp;- &lt;/span&gt;&lt;strong&gt;Wild Madagascar Vanilla&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion Bath&amp;Body Works, Body Mist Bath&amp;Body Works, Hand Lotion Bath&amp;Body Works, Hand wash Bath&amp;Body Works, Hand cream Bath&amp;Body Works, Perfume Bath&amp;Body Works, kem dưỡng Bath&amp;Body Works, nước rửa tay Bath&amp;Body Works, l', 'Fragrance Mist Wild Madagascar Vanilla,Bath &amp; Body Works'),
(66, 1, 'Gel rửa tay Golden Autumn Citrus', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(67, 1, 'Gel rửa tay Dancing Waters', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(68, 1, 'Gel rửa tay Aqua Blossom', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(70, 1, 'Gel rửa tay Ocean for Men', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel');
INSERT INTO `product_description` (`product_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `tag`) VALUES
(71, 1, 'Gel rửa tay Pumpkin Cupcake', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(72, 1, 'Gel rửa tay Snow Kissed Berry', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(73, 1, 'Gel rửa tay Fresh Sparkling Snow', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(69, 1, 'Gel rửa tay Island Margarita', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(74, 1, 'Gel rửa tay Warm Vanilla Sugar', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(75, 1, 'Gel rửa tay White Pear &amp; Fig', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(76, 1, 'Gel rửa tay 5-Pack Classic Faves', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(77, 1, 'Gel rửa tay 5-Pack Fall Fresh Picked', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Gel rửa tay khô&lt;/strong&gt; (Hand Gel) là một sản phẩm ngày càng quen thuộc trong cuộc sống hiện đại. Với tính năng nhanh chóng mang lại cho bạn một đôi tay thật sạch khỏe và thơm mát mùi hương hoa quả tự nhiên, sản phẩm này vô cùng tiện dụng và có thể đồng hành cùng các nhân viên văn phòng hiện đại đến công sở. Đặc biệt, sản phẩm này còn rất cần thiết cho các chuyến đi xa. Bạn sẽ cảm nhận được sự khác biệt khi đi du lịch, chỉ trong vòng 1 phút, tất cả những mùi khó chịu như mùi hải sản, mùi hành, mùi tanh hay thậm chí những vết dầu mỡ, gel tóc… sẽ biến mất, thay vào đó là một cảm giác thật mát và sạch với mùi hương quyến rũ của các loại hoa hay mùi ngọt ngào của trái cây nhiệt đới mà không hề cần sự trợ giúp của nước.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;font-family: Arial; color: rgb(20, 24, 35); font-size: 13px; line-height: 18px;&quot; /&gt;\r\n&lt;span style=&quot;color: rgb(20, 24, 35); font-family: Arial; line-height: 18px;&quot;&gt;&lt;strong&gt;Cách dùng&lt;/strong&gt;: lấy một lượng nhỏ xoa đều lên tay, không cần rửa lại với nước.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Loại sản phẩm: Dưỡng da &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nhãn hiệu:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works® &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Dung tích: 29ml &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Arial; line-height: normal;&quot;&gt;Nơi sản xuất:&amp;nbsp;&lt;strong&gt;Bath &amp;amp; Body Works®/USA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop cung cấp sỉ và lẻ tất cả sản phẩm của Bath&amp;Body Works  như : body lotion, body cream, body mist, perfume với giá cả cạnh tranh nhất, Shop cung cấp hàng Bath&amp;Body Works  nhập từ Mỹ, không phải hàng VNXK hay TQ, Shop cung cấp hàng Victoria Secr', 'Body Lotion, Body Mist, Hand Lotion, Hand wash,Hand cream, Perfume, kem dưỡng, nước rửa tay, lotion Bath&amp;Body Works, nước hoa, xịt toàn thân, hàng nhập khuyến mãi, mỹ phẩm khuyến mãi, mỹ phẩm giảm giá, mỹ phẩm xách tay,phân phối mỹ phẩm giá rẻ,bỏ sỉ m', 'Bath &amp; Body Works, Hand Gel'),
(80, 1, 'Kem chống nắng vật lý Blue Lizard SPF 30', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$16:0&quot; style=&quot;font-family: Arial; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;&lt;strong&gt;Kem chống nắng vật lý Blue Lizard SPF 30&lt;/strong&gt; dành riêng cho mặt với chỉ số hóa chất có hại bằng 0, tuyệt đối không gây hại cho sức khỏe. Đặc biệt, không nhờn và để lại vết cặn trắng trên da sau khi thoa. Nắp tube sau khi tiếp xúc với tia UV sẽ đổi sang màu xanh, giúp bạn nhận biết mình đang tiếp xúc với các tia nắng có hại. Thật là thông minh và đáng yêu.&lt;/span&gt;&lt;br data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$17:0&quot; style=&quot;font-family: Arial; color: rgb(62, 69, 76); line-height: 15.3599996566772px; white-space: pre-wrap; background-color: rgb(247, 247, 247);&quot; /&gt;\r\n&lt;br data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$19:0&quot; style=&quot;font-family: Arial; color: rgb(62, 69, 76); line-height: 15.3599996566772px; white-space: pre-wrap; background-color: rgb(247, 247, 247);&quot; /&gt;\r\n&lt;span data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$20:0&quot; style=&quot;font-family: Arial; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;&lt;strong&gt;Cách dùng: &lt;/strong&gt;Bôi một lượng vừa đủ lên da. Vì là kem chống nắng vật lý nên không cần bôi lại trong ngày.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;&lt;span data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$4:0&quot; style=&quot;font-family: Arial; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;Chi tiết:&lt;/span&gt;&lt;/strong&gt;&lt;br data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$7:0&quot; style=&quot;font-size: 16px; font-family: Arial; color: rgb(62, 69, 76); line-height: 15.3599996566772px; white-space: pre-wrap; background-color: rgb(247, 247, 247);&quot; /&gt;\r\n&lt;span data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$8:0&quot; style=&quot;font-family: Arial; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;- Dung tích: &lt;strong&gt;85ml&lt;/strong&gt;&lt;/span&gt;&lt;br data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$11:0&quot; style=&quot;font-size: 16px; font-family: Arial; color: rgb(62, 69, 76); line-height: 15.3599996566772px; white-space: pre-wrap; background-color: rgb(247, 247, 247);&quot; /&gt;\r\n&lt;span data-reactid=&quot;.5.$mid=11416325139514=230750573d62405ae30.2:0.0.0.0.0.0.$end:0:$12:0&quot; style=&quot;font-family: Arial; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;- &lt;strong&gt;Made in Australia.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Kem chống nắng vật lý Blue Lizard SPF 30 dành riêng cho mặt với chỉ số hóa chất có hại bằng 0, tuyệt đối không gây hại cho sức khỏe. ', 'mỹ phẩm xách tay,mỹ phẩm giá rẻ,mỹ phẩm chính hãng,chuyên bán mỹ phẩm,cung cấp mỹ phẩm,kem chống nắng,kem nền,kem trắng da,phấn phủ,phấn má hồng,son môi,phấn mắt,kem dưỡng da,kem chống lão hóa da,lotion,chì kẻ mắt,mascara,bấm mi,son dưỡng,son kem,son thỏi', 'Blue Lizard,kem chống nắng'),
(81, 1, 'Kem chống nắng Neutrogena Ultra Sheer Sunscreen SPF 45', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Kem chống nắng &lt;strong&gt;Neutrogena Ultra Sheer Dry-Touch&lt;/strong&gt; được các bác sĩ da liễu công nhận về hiệu quả chống nắng và lun nằm trong top 1 những sản phẩm kem chống nắng tốt nhất thế giới do tạp chí &lt;strong&gt;Cleo Maga Zine&lt;/strong&gt;, tạp chí &lt;strong&gt;Allure&lt;/strong&gt; bình chọn.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Đây là dòng kem chống nắng huyền thoại của &lt;strong&gt;Neutrogena&lt;/strong&gt;. Mình dùng em này cách đây 4&amp;nbsp;năm khi còn là sinh viên. Và em nó vẫn mãi là huyền thoại trong lòng mình. Không thể nào phủ nhận được độ tốt và &lt;strong&gt;SIÊU TIẾT KIỆM, SIÊU KINH TẾ, SIÊU TỐT&lt;/strong&gt; của em nó.&amp;nbsp; Đặc biệt, loại Neutrogena của mình có chất &lt;strong&gt;Helioplex&lt;/strong&gt;, là 1 loại chất chống nắng mới mà Neutrogela đi đầu trong việc sử dụng nó vào sản phẩm chống nắng. &lt;strong&gt;Helioplex&lt;/strong&gt; được đại diện cho nền công nghệ kem chống nắng ổn định, bảo vệ hoàn hảo cho làn da. Sự có mặt của &lt;strong&gt;Helioplex&lt;/strong&gt; trong kem chống nắng bạn dang dùng, bạn có thể yên tâm&amp;nbsp; chắc chắn rằng bạn đang được bảo về hoàn hảo khi tiếp xúc với ánh nắng mặt trời&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Ngày trước mình xài &lt;strong&gt;Neutrogena&lt;/strong&gt; còn chưa tìm ra dc cái chất &lt;strong&gt;Helioplex&lt;/strong&gt; này nữa mà đã thấy tốt lắm lắm rồi. Nói chung, nếu bạn là sinh viên, học sinh muốn tìm 1 loại kem chống nắng phù hợp túi tiền, an toàn cho da, bảo vệ da tốt, thì kem chống nắng &lt;strong&gt;Neutrogena Ultra Sheer Dry-Touch&lt;/strong&gt; là 1 sự lựa chọn vô cùng sáng suốt cho bạn&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;strong&gt;HDSD&lt;/strong&gt;: - Lắc đều trước khi dùng&lt;/span&gt;&lt;/p&gt;\r\n', 'Kem chống nắng Neutrogena Ultra Sheer Dry-Touch được các bác sĩ da liễu công nhận về hiệu quả chống nắng và lun nằm trong top 1 những sản phẩm kem chống nắng tốt nhất thế giới', '', 'kem chống nắng,neutrogena'),
(82, 1, 'Son L''Oreal Colour Riche Caresse Stick', '&lt;p&gt;&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px; color: rgb(117, 117, 117);&quot;&gt;Được đánh giá &lt;strong&gt;4/5&lt;/strong&gt; trên makeupalley. Em này kiểu dáng vô cùng sang trọng. Ánh vàng lóng la lóng lánh nhìn y như cây YSL luôn. Rất hợp với việc tặng cho cô bạn thân đáng yêu hay một nữa của chàng.&lt;/span&gt;&lt;br style=&quot;color: rgb(117, 117, 117); font-family: georgia, ''times new roman''; font-size: 16px; line-height: 25.6000003814697px;&quot; /&gt;\r\n&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px; color: rgb(117, 117, 117);&quot;&gt;Vì em ấy thuộc &lt;strong&gt;dòng son mới nhất&lt;/strong&gt; của Loreal. Màu sắc phải gọi là đẹp rạng ngời mà ko chói lóa. Màu xinh xắn dễ thương yêu lắm luôn .&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px; color: rgb(117, 117, 117);&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/My pham/loreal_colour_caresse_lipstick_review.jpg&quot; style=&quot;width: 500px; height: 375px;&quot; /&gt;&lt;/span&gt;&lt;br style=&quot;color: rgb(117, 117, 117); font-family: georgia, ''times new roman''; font-size: 16px; line-height: 25.6000003814697px;&quot; /&gt;\r\n&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px; color: rgb(117, 117, 117);&quot;&gt;Có nhiều dưỡng chất chống nắng &lt;strong&gt;Shinni color&lt;/strong&gt; làm cho đôi môi của bạn mượt mà và quyến rũ tuy nhiên lại không làm môi bóng lưỡng mất tự nhiên mà lên môi lại&lt;strong&gt; matte vào môi&lt;/strong&gt;, lâu phai và không trôi nữa.&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;color: rgb(117, 117, 117); font-family: georgia, ''times new roman''; font-size: 16px; line-height: 25.6000003814697px;&quot; /&gt;\r\n&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px; color: rgb(117, 117, 117);&quot;&gt;Yêu lắm luôn! Bạn nào đã dùng em này rồi thì tới 90% sẽ quay lại sắm thêm vài em nữa&amp;nbsp;&lt;/span&gt;&lt;br style=&quot;color: rgb(117, 117, 117); font-family: georgia, ''times new roman''; font-size: 16px; line-height: 25.6000003814697px;&quot; /&gt;\r\n&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px; color: rgb(117, 117, 117);&quot;&gt;Một vài màu được ưa thích:&lt;/span&gt;&lt;br style=&quot;color: rgb(117, 117, 117); font-family: georgia, ''times new roman''; font-size: 16px; line-height: 25.6000003814697px;&quot; /&gt;\r\n&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px;&quot;&gt;172 Blushing Sequin: hồng neon dễ thương&lt;/span&gt;&lt;/span&gt;&lt;br style=&quot;color: rgb(117, 117, 117); font-family: georgia, ''times new roman''; font-size: 16px; line-height: 25.6000003814697px;&quot; /&gt;\r\n&lt;span style=&quot;color:#FFA500;&quot;&gt;&lt;span style=&quot;border: 0px; font-family: georgia, ''times new roman''; font-size: large; font-stretch: inherit; line-height: 25.6000003814697px; vertical-align: baseline; margin: 0px; padding: 0px;&quot;&gt;177 Fiery Veil: cam quyến rũ&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Shop mỹ phẩm xách tay bán Mẫu son Loreal mới nhất,hàng xách tay,có hóa đơn,chính hãng,giá cả cạnh tranh tại hcm', 'mỹ phẩm xách tay,mỹ phẩm giá rẻ,mỹ phẩm chính hãng,chuyên bán mỹ phẩm,cung cấp mỹ phẩm,kem chống nắng,kem nền,kem trắng da,phấn phủ,phấn má hồng,son môi,phấn mắt,kem dưỡng da,kem chống lão hóa da,lotion,chì kẻ mắt,mascara,bấm mi,son dưỡng,son kem,son thỏi', 'Son,Loreal'),
(84, 1, 'Giảm cân iBlue - Giảm cân an toàn nhập từ Mỹ', '&lt;h1&gt;&lt;strong&gt;Sản phẩm giảm cân an toàn iBlue – Giúp giảm cân nhanh,an toàn và hiệu quả&lt;/strong&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Đây là sản phẩm giảm cân an toàn được điều chế từ thảo mộc và trái cây(những thành phần tự nhiên) tuyệt đối an toàn. Ngoài tác dụng giảm cân còn được các vận động viên thường xuyên sử dụng để bổ sung năng lượng trong thi đấu thể thao.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Trong iBlue có chiết xuất Catesin của trà xanh giúp tăng cường trao đổi chất Lipid, kích thích quá trình sinh nhiệt làm tiêu hao lượng mỡ thừa trong cơ thể.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Trong iBlue có Guarana là chiết xuất từ hạt một loại trái cây chỉ có tại Brazil có tác dụng cung cấp năng lượng phát sinh theo thời gian, giúptăng và thúc đẩy quá trình đốt chất béo.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Ngoài ra iBlue còn có 1gr chất xơ, giúp những người thừa cân có thể cắt bớt khẩu phần ăn hàng ngày mà vẫn không thấy đói.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Sản phẩm giảm cân iBlue còn hay ở chỗ chỉ giúp người mong muốn giảm cân đốt cháy mỡ thừa tập trung ở vùng bụng và đùi chứ không làm ảnh hưởng đến vòng ngực và vòng mông.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;iBlue còn giúp tạo các nhóm cơ, săn chắc cơ, đẩy nhanh quá trình hình thành các nhóm cơ nếu kết hợp tập luyện thể hình và các bài tập đúng cách&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h1&gt;&lt;strong&gt;Sản phẩm giảm cân iBLUE&amp;nbsp;- Sử dụng hiệu quả với người bị bệnh TIỂU ĐƯỜNG, giảm lượng mỡ trong máu&lt;/strong&gt;&lt;/h1&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;iBLUE tốt cho người bị bệnh tiểu đường vì có vị ngọt từ Lá.&amp;nbsp;Cung cấp cho người giảm cân đường nhưng không phải là đường thông thường, nó có dạng giống đường giảm cân (Diet Sugar). Chiết xuất từ cây Stevia có thể giúp bệnh nhân tiểu đường hạ đường huyết. Khi cơ thể bạn hấp thu 1000mg stevioside hàng ngày sẽ làm giảm tới 18% lượng đường trong máu&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Sản phẩm giảm cân iBLUE ngoài giúp giảm cân còn giúp ổn định huyết áp cho người bị huyết áp cao và giúp làm giảm lượng mỡ trong máu. Giúp giảm thiểu nguy cơ mỡ trong máu và nguy cơ bệnh cao huyết áp. Hỗ trợ rất tốt cho bài tập HIIT.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;h3&gt;&lt;strong&gt;Mình đã sử dụng và đã thành công, Mình muốn chia sẻ cho các bạn sản phẩm giảm cân an toàn này để chúng ta đều có một thân hình như mong muốn,tạo thêm niềm tin trong cuộc sống. Các bạn có thể tham khảo thêm bài viết về &lt;a href=&quot;http://trumhangmy.com/lam-dep/giam-can/chia-se-kinh-nghiem-ban-than-giam-can-9-kg-trong-1-thang-ruoi-co-hinh-anh-that.html&quot;&gt;Quá trình giảm cân thành công của mình: Giảm 9kg trong vòng 1 tháng rưỡi&lt;/a&gt;, cơ thể săn chắc lại,không ốm yếu như các loại thuốc giảm cân trôi nổi, không hại cơ thể.&lt;/strong&gt;&lt;/h3&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;&lt;a href=&quot;http://trumhangmy.com/lam-dep/giam-can/chia-se-kinh-nghiem-ban-than-giam-can-9-kg-trong-1-thang-ruoi-co-hinh-anh-that.html&quot;&gt;&lt;img alt=&quot;giam can an toan iBlue&quot; src=&quot;http://trumhangmy.com/image/data/iBlue/Diet.jpg&quot; style=&quot;width: 800px; height: 600px;&quot; /&gt;&lt;/a&gt;​&lt;/strong&gt;&lt;/p&gt;\r\n', 'Giảm cân an toàn từ Mỹ,đã có nhiều KH đánh giá tốt,giảm cân nhanh không cần ăn kiêng,giảm cân cho dân văn phòng,giảm cân cho dân kinh doanh hay uống bia', '', 'iBlue, giảm cân'),
(85, 1, 'Mascara Maybelline - Full N Soft Waterproof', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;Em Mascara&amp;nbsp;này cực kỳ nổi tiếng trong các dòng mascara rồi nhé. Seach review đâu đâu cũng thấy kh&lt;span class=&quot;text_exposed_show&quot;&gt;en wá trời khen luôn, ko một&amp;nbsp;lời chê. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;&lt;span class=&quot;text_exposed_show&quot;&gt;Em này&amp;nbsp;còn đứng số 1 trong tất cả các loại mascara được review trên makeupalley. Cháy hàng,&amp;nbsp;khó khăn lắm mới về được em này.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;&lt;span class=&quot;text_exposed_show&quot;&gt;Tác dụng làm cực kỳ dày mi, dài mi,và còn cong mi, nhưng lại ko hề làm mi khô cứng giả tạo như các loại mascara khác, mi mềm nhẹ tự nhiên y như là sinh ra đã đẹp sẵn tự nhiên vậy. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;&lt;span class=&quot;text_exposed_show&quot;&gt;Và còn chống thấm nước nữa, nên mặc kề mồ hôi mắt vẫn ko thành gấu trúc nhoa.... &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;&lt;span class=&quot;text_exposed_show&quot;&gt;Tha hồ cho các nàng đi lừa tình nhé... Chớp chớp.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Mascara Maybelline Full N Soft Waterproof  làm cực kỳ dày mi, dài mi,và còn cong mi, nhưng lại ko hề làm mi khô cứng giả tạo,mi mềm nhẹ tự nhiên,chống thấm nước', '', 'Maybelline,Mascara'),
(86, 1, 'Mascara Cover Girl - Clump Crusher', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;Em Mascara Cover Girl&amp;nbsp;này rất HOT, về 1 đợt 30 cây mà các nàng order liên tục giờ mới có hàng bán&lt;span class=&quot;text_exposed_show&quot;&gt; tiếp cho các nàng.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span class=&quot;userContent&quot;&gt;&lt;span class=&quot;text_exposed_show&quot;&gt;Con em mình bảo nó làm cho mi dày hơn cỡ....gấp đôi thôi&amp;nbsp;à ;))&amp;nbsp;chứ ko dày cộm mất tự nhiên nha. Và còn làm mi dài ra nữa. Thích dài thêm thì cứ chuốt nữa là dài tới bến luôn nha mấy nàng.&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Điểm đặc biệt của em nó là ở đầu cọ hình vòng cung, có tác dụng nâng mi và giữ cong mi cực tốt nhá (đầu cọ tuyệt quá nên đóng gói show cả ra ngoài). &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Mình chuốt cả ngày từ sáng đi làm mà tới chiều về mi vẫn còn cong mới kinh, trong khi đi làm ko bấm mi đâu nhoa, chỉ make up ...đơn giản thôi nha. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Thích điên lên được&amp;nbsp;á. Có em í là&amp;nbsp;đại tiện khỏi cần kẹp mi lỉnh kỉnh chi cho mệt mà vẫn có hàng mi dài dày mà vẫn vô cùng tự nhiên nha các nàng nha.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Em Mascara Cover Girl này rất HOT,làm chi mi dài,dày và giữ cong mi cực tốt,hàng mỹ phẩm xách tay chất lượng,có hóa đơn', '', 'Mascara,Cover Girl'),
(58, 1, 'Kem chống nắng Clarins Day Screen SPF 40 UVA-UVB Oil-free', '&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;Kem chống nắng Clarins UV Plus HP&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Là 1 trong những loại kem chống nắng đình đám nhất, rất nổi tiếng, được ca ngợi rất nhiều,&amp;nbsp; đáng mua và cực kỳ tốt. Mình hay gọi em nó là “&lt;strong&gt;kem chống nắng tốt nhất thế giới&lt;/strong&gt;”. Phù hợp cả với&amp;nbsp;làn da nhạy cảm, mong manh yếu đuối nhất nhé. Vì &lt;strong&gt;kem chống nắng&amp;nbsp;Clarins UV Plus SPF40&lt;/strong&gt; còn được các chuyên gia về da liễu&amp;nbsp;khuyên dùng cho cả phụ nữ&amp;nbsp; mang thai và những người đang phải&amp;nbsp;điều trị các vết thương trên da. Trong khi đó, phức hợp chống ô nhiễm độc quyền của&amp;nbsp;Clarins giúp da miễn nhiễm với tình trạng khói bụi và&amp;nbsp;không khí ô nhiễm,độ ẩm cao ở các&amp;nbsp;đô thị VN&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Một ưu điểm vượt trội là Clarins UV Plus HP&amp;nbsp; được tích hợp cả cơ chế chống nắng vật lý (chuyển hướng các tia cực tím, không cho chúng tác động đến da) và cơ chế chống nắng hóa học (không cho UVA và UVB xâm nhập sâu vào da), do đó, tạo thành lớp màng chắn ngăn chặn tình trạng sạm da, những đốm nám và hiện tượng da lão hóa sớm do ánh nắng mặt trời.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Vì thế nên &lt;em&gt;khả năng chống nắng&lt;/em&gt; của &lt;strong&gt;Clarins UV Plus HP&lt;/strong&gt; thì không phải băn khoăn. Ko bị bóng nhờn dù là da dầu. Chất kem lỏng, nhẹ, thoáng, ko nhờn dính, dễ dàng apply lên mặt mà ko gây bí, rít, thấm khá nhanh, bôi nhiều vẫn ko có tình trạng lợn cợn và bệt lại trên da. Sau khi bôi lên mặt thấy da mịn màng mướt mát, sáng sủa.&amp;nbsp;Dùng 1 thời gian sẽ thấy da trắng sáng hơn đó các bạn àh. ( đây chắc là khuyến mãi kèm theo )&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Em nó nổi tiếng và tốt thế nào chỉ cần seach google là ra hàng trăm ngàn phụ nữ vote 5 sao nhé. Nên mình&amp;nbsp;không cần phải quảng cáo nhiều ạ. Hãy dùng và cảm nhận để biết đồng tiền mình bỏ ra thật xứng đáng.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;Sản phẩm này mình mua trong store hơn 1triệu8/50ml. Đắt nhưng xắt ra miếng. Tuy nhiên vẫn&amp;nbsp;đau lòng quá nên quyết tâm nhập về giá tốt để chia sẻ cho các chị em yêu thích dòng sản phẩm này giống mình.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;***Mẫu kem chống nắng này Clarins có 3 màu, nhưng bên mình chỉ nhập về màu trắng (không màu), vì theo cảm nhận của mình thì&amp;nbsp;màu trắng khi thoa lên da không bị bết ra như 2 màu kia và không bị rít da...&lt;/span&gt;&lt;/em&gt;&lt;/p&gt;\r\n', 'Là 1 trong những loại kem chống nắng đình đám nhất, rất nổi tiếng, được ca ngợi rất nhiều,  đáng mua và cực kỳ tốt. Mình hay gọi em nó là “kem chống nắng tốt nhất thế giới”. Phù hợp cả với làn da nhạy cảm, mong manh yếu đuối nhất.', 'mỹ phẩm xách tay,mỹ phẩm giá rẻ,mỹ phẩm chính hãng,chuyên bán mỹ phẩm,cung cấp mỹ phẩm,kem chống nắng,kem nền,kem trắng da,phấn phủ,phấn má hồng,son môi,phấn mắt,kem dưỡng da,kem chống lão hóa da,lotion,chì kẻ mắt,mascara,bấm mi,son dưỡng,son kem,son thỏi', 'clarins,kem chống nắng'),
(87, 1, 'Kem chống nắng Eucerin Daily Protection Moisturizing Face Lotion, Broad Spectrum SPF 30', '&lt;p&gt;Kem chống nắng &lt;strong&gt;Eucerin Daily Protection&lt;/strong&gt; là dược mỹ phẩm của Pháp. Em nó còn EWG bình chọn là kem&amp;nbsp;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;chống nắng dưỡng ẩm tốt nhất đó ạ.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Em kem chống nắng &lt;strong&gt;Eucerin&lt;/strong&gt; này vừa là chống nắng vừa là dưỡng ẩm luôn nên các bạn chỉ cần bôi 1 lớp&amp;nbsp;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;kem đủ lượng thôi là đủ ko cần phải 2,3 lớp lích kích. &amp;nbsp;Thích hợp cho các bạn nào da nhạy cảm, da dễ dị&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;ứng và đặc biệt cho các bạn nào da có xu hướng thiên khô.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Chất kem màu trắng sữa, lỏng dạng lotion, bôi lên &amp;nbsp;rất nhẹ nhàng và nhanh chóng thấm vào da mà&amp;nbsp;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;không làm da bị nhờn dính, bí rít khó chịu, không để lại vệt trắng white cast tí nào luôn. &amp;nbsp;Da mình da dầu&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;nhưng mình xài vẫn vô tư nhé, ko làm mình vì thế nổi mụn thêm hay bí nhờn tí nào.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Mình dùng em này khi em ấy mới ra cách đây 4 năm, từ đó tới nay trên makeupalay , webtretho và các&amp;nbsp;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;diễn đàn làm đẹp các mẹ các chị khen em nó nức trời. &amp;nbsp;Chai to đùng dùng rất tốt cho mặt, dùng hoài ko&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;thấy hết mà giá rất hữu nghị. Quả là 1 sản phẩm vừa ngon , vừa rẻ mà hiệu quả và độ an toàn của em nó&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;thì ko chê vào chỗ nào được&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Nếu bạn nào là sinh viên, muốn tìm một loại kem chống nắng nhẹ nhàng, tiện lợi, dễ sử dụng, hiệu quá,&amp;nbsp;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;biệt là giá cả rát kinh tế thì kem chống nắng &lt;strong&gt;Eucerin&lt;/strong&gt; là 1 sự lựa chọn lý tưởng .&lt;/span&gt;&lt;/p&gt;\r\n', '', '', ''),
(92, 1, 'Kem chống nắng Solar Rx Therapeutic', '&lt;p dir=&quot;ltr&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Em kem chống nắng &lt;span style=&quot;color:#FF0000;&quot;&gt;&lt;strong&gt;Solar Rx Moisturizer Broad Spectrum SPF 30&lt;/strong&gt;&lt;/span&gt; này vô cùng đáng yêu và dễ thương &amp;nbsp;vì được EWG xếp độ an toàn loại 1. Nghĩa là thành phần vô cùng lành tính, em ấy không hề chứa hương liệu hay cồn gì. Nên rất an toàn với làn da mong manh của bạn .&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Kem chống nắng &lt;/span&gt;&lt;strong style=&quot;color: rgb(255, 0, 0); font-size: 18px; line-height: 20.7999992370605px;&quot;&gt;Solar Rx Moisturizer Broad Spectrum SPF 30&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt; với thành phần chính tận hơn 20% là kẽm oxit nên khả năng chống nắng vô cùng tốt.Bảo vệ da bạn khỏi tác hại của các tia tử ngoại. Ngoài ra, kem còn có thật nhiều các thành phần để dưỡng ẩm cho da như carrot seed oil, avocado oil, shea butter.. &amp;nbsp;Vì vậy ,ngoài tác dụng chống nắng, em ấy còn có khả năng dưỡng ẩm cho da rất tốt. Nếu dùng kem chống nắng này bạn có thể dùng thẳng lên mặt mà không cần kem dưỡng ẩm bên dưới nữa mà vẫn an toàn. &amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/My pham/20150717_151217.jpg&quot; style=&quot;width: 600px; height: 1067px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Chất kem ban đầu bạn sẽ hơi shock vì em nó rất đặc, nhưng chắc vì nhờ có nhiều thành phần dưỡng nên kem mềm &amp;nbsp;mại rất dễ tán và tan trên da, khi tan lên da xong là kem tiệp màu vào da chứ không hề để lại vệt trắng white cast khiến da trắng bệt như ma như các loại kem chống nắng vật lý khác. Đây chính là điểm cộng to đùng của bạn kem chống nắng &lt;/span&gt;&lt;strong style=&quot;color: rgb(255, 0, 0); font-size: 18px; line-height: 20.7999992370605px;&quot;&gt;Solar Rx Moisturizer Broad Spectrum SPF 30&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt; này. &amp;nbsp;Khi em nó đã yên vị trên da, thì 1 lúc sau sẽ thấm và bạn chỉ cần phủ 1 ít phấn khoáng lên mặt là xinh tươi khỏi phải bàn&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Tóm lại. Kem chống nắng&amp;nbsp;&lt;/span&gt;&lt;strong style=&quot;color: rgb(255, 0, 0); font-size: 18px; line-height: 20.7999992370605px;&quot;&gt;Solar Rx Moisturizer Broad Spectrum SPF 30&amp;nbsp;&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;sẽ rất tuyệt vời cho những bạn đang tìm kiếm 1 loại kem chống nắng vật lý&amp;nbsp;dùng lâu dài, lành tính&amp;nbsp;và độ an toàn cao, không làm mặt trắng xóa sau khi bôi, có chất dưỡng da.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Phù hợp với các bạn từ da thường tới da khô. Da dầu muốn sử dụng em này tì nhớ phủ thêm phấn cho mặt thêm xinh và che khuyết điểm nhẹ nha&lt;/span&gt;&lt;/p&gt;\r\n', 'kem chống nắng Solar Rx Therapeutic này vô cùng đáng yêu và dễ thương  vì được EWG xếp độ an toàn loại 1. Nghĩa là thành phần vô cùng lành tính, em ấy không hề chứa hương liệu hay cồn gì. Nên rất an toàn với làn da mong manh của bạn .', '', 'Solar RX,kem chống nắng,spf 30');
INSERT INTO `product_description` (`product_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `tag`) VALUES
(91, 1, 'Kem chống nắng Marie Veronique moisturizing face sceen', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1f8b-a4da-be1f-4c0975562c7b&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em kem chống nắng có màu &lt;strong&gt;Marie Veronique&lt;/strong&gt; này trong cộng đồng mỹ phẩm thiên nhiên cực cực kỳ là nổi tiếng và được yêu. Thành phần của em nó hầu như “sạch”, toàn là chiết xuất từ các loại dầu thiên nhiên tốt cho da như jojoba oil, emu oil, rose hip oil, vitamin E... &amp;nbsp;mà hoàn toàn ko hề có các thành phần hóa học gây hại như “talc, propylene glycol, parapen. Vì vậy nên that yên tâm khi dùng em nó lâu dài. &lt;strong&gt;Marie Veronique&lt;/strong&gt; được EWG xếp độ an toàn loại 1 nữa đó nha. Nghĩa là rất rất an toàn cho da. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1f8b-a4da-be1f-4c0975562c7b&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em kem chống nắng &lt;strong&gt;Marie Veronique&lt;/strong&gt; có màu nhẹ, nên tất nhiên khi thoa lên sẽ làm da bạn đẹp hơn,sáng sủa trắng trẻo xinh xắn hơn tùy màu bạn chọn là sáng hay tối, và còn cover được kha khá khuyết điểm đấy ạ. Em ấy làm đồng đều màu da rất tốt, khiến da dầu loang lỗ chỗ đậm chỗ nhạt như mình bôi lên sáng đẹp hẳn như da đẹp tự nhiên vậy, lại &amp;nbsp;không bóng nhờn, bôi lên da đẹp hẳn mà còn rất matte. Hiệu ứng matte này mình chưa tìm được ở sản phẩm chống nắng nào tốt như vậ. Yêu nhất luôn &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1f8b-a4da-be1f-4c0975562c7b&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Mỗi buổi sáng mình dùng 1 lớp đủ lượng kem chống nắng &lt;strong&gt;Marie Veronique &lt;/strong&gt;này lên da ( cách dùng kem chống nắng đủ lượng xem ở đây), rồi phủ 1 lớp phấn nhẹ, thế là da đã sáng sủa, xinh đẹp lắm lắm rồi. thêm tí son tí lông mày nữa là xinh khỏi bàn&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1f8b-a4da-be1f-4c0975562c7b&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Có 2 màu: Extra Light và Light tint&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', '', '', ''),
(93, 1, 'Kem chống nắng EltaMD UV Clear SPF 46 Broad Spectrum sunscreen', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Kem chống nắng &lt;/span&gt;&lt;span style=&quot;color:#B22222;&quot;&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;E&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color:#B22222;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;ltaMD UV Clear &lt;/span&gt;SPF 46 Broad Spectrum sunscreen&lt;/strong&gt;&lt;/span&gt;&amp;nbsp;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;đình đám nổi tiếng từ xưa và tới tận bây giờ. Được rất nhiều các bloger , bác sĩ hay người bán hàng recommend và cả ngàn review 5 sao &amp;nbsp;của người dùng tặng cho em nó. &amp;nbsp;Hàng đã về cho các chị em đây. Khó khăn lắm mới về kịp đủ cho các chị em vì nhập về bao nhiêu hết bấy nhiêu&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;EltaMD&lt;/strong&gt; là 1 hãng &amp;nbsp;dược mỹ phẩm của Mỹ. Và từ khi em &amp;nbsp;kem chống nắng &lt;/span&gt;&lt;span style=&quot;color:#B22222;&quot;&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;EltaMD UV Clear SPF 46 Broad Spectrum&amp;nbsp;sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; này ra đời đã làm nên tên tuôi của hãng đình đám luôn. Nói không ngoa đây chính là sản phẩm đinh, sản phẩm làm nên tên tuổi của cả 1 tập đoàn và mình chắc chắn rằng 98% ai dùng cũng phải yêu ngay em ấy . 2% kia chắc là các bạn chưa dùng kem chống nắng nên không biết so sánh thế nào. Vì sao ư? &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em này có thành phần quá tốt và lành tính. Có lượng Kẽm oxit &amp;nbsp;khá là cao so với các kem chống nắng hiện có trên thị trường vì thế khả năng chống nắng cũng tốt hơn rất nhiều, chống nắng phổ rộng luôn nha, &amp;nbsp;chống được UVA, và UVB . Bảo vệ da bạn an toàn dưới các tia có hại&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;À quên mất điều quan trọng đây này, Kem chống nắng &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot; style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px;&quot;&gt;&lt;span style=&quot;color: rgb(178, 34, 34);&quot;&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;E&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px; color: rgb(178, 34, 34);&quot;&gt;&lt;strong&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;ltaMD UV Clear &lt;/span&gt;SPF 46 Broad Spectrum sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; còn có cả Niacinamide 5% có khả năng giảm vết thâm, nám, giảm mụn, giúp da căng khỏe và đều màu hơn &amp;nbsp;. Trong thành phần kem chống nắng &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot; style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px;&quot;&gt;&lt;span style=&quot;color: rgb(178, 34, 34);&quot;&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;E&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px; color: rgb(178, 34, 34);&quot;&gt;&lt;strong&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;ltaMD UV Clear &lt;/span&gt;SPF 46 Broad Spectrum sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&amp;nbsp;còn có Vitamin E. HA. Vitamin B3 giúp giảm nám, làm trắng, dưỡng ẩm ,chống lão hóa , phục hồi da, trị mụn, giảm dầu… Dùng lâu dài da bạn chỉ có càng ngày càng đẹp sáng đẹp mịn màng thêm thôi. Phù hợp với các loại da, rất thích hợp cho da mụn và da nhạy cảm. Và chỉ duy nhất trong em &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot; style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px;&quot;&gt;&lt;span style=&quot;color: rgb(178, 34, 34);&quot;&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;E&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px; color: rgb(178, 34, 34);&quot;&gt;&lt;strong&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;ltaMD UV Clear &lt;/span&gt;SPF 46 Broad Spectrum sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; này mới làm được điều tuyệt vời đó thôi nhé&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;img alt=&quot;Kem chống nắng EltaMD UV Clear SPF 40 Broad Spectrum sunscreen &quot; src=&quot;http://trumhangmy.com/image/data/My pham/20150529_112638.jpg&quot; style=&quot;width: 600px; height: 281px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 1.38; white-space: pre-wrap; background-color: transparent;&quot;&gt;Chất kem màu trắng sữa , dạng cream nhưng không đặc mà xốp xốp và &amp;nbsp;mềm mịn. Xoa lên da mát rười rượi &amp;nbsp;thích lắm, rất dễ dàng apply và chỉ cần vỗ 1 lúc là lớp kem xốp mịn màng ấy tan ngay thấm vào da. Không hề bóng nhẫy mặt mà để lại 1 lớp bóng &amp;nbsp;mờ và chỉ &amp;nbsp;2,3 phút sau là thấm như không có gì trên mặt ( điều này không quan trọng vì bạn lúc nào cũng cần apply chống nắng trước khi ra nắng mà) , không hề nhờn dính bí rít tí nào, da khô ráo, mát mẻ, sáng khoái. &amp;nbsp;Dùng cả ngày về da vẫn mềm mại mượt mà, giảm tiết dầu thấy rõ luôn và không nổi mụn. Với thành phần lành tính tốt như vậy, bạn sẽ hạn chế được việc dị ứng, bí rít, lên mụn . kem có cả dưỡng ẩm dưỡng da nên da bạn nào dầu rửa mặt xong là bụp thẳng em nó lên không cần dưỡng ẩm cũng được.&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Bạn mình hay nổi mụn, nhưng vẫn cố sống cố chết dùng kem chống nắng mặc dù bị nổi mụn. Coi việc nổi mụn khi dùng kem chống nắng là việc hiển nhiên vì nó bảo thà nổi mụn còn hơn không dùng kem chống nắng. &amp;nbsp;Vậy mà khi dùng tới em này thì thật bất ngờ, chả hiểu sao dùng được chai thì mụn có dấu hiệu giảm dần dần, nghĩa là em kem chống nắng này không hề gây thêm mụn cho da rất dễ bị nổi mụn &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Thêm 1 tác dụng nữa mình hóng được, là với bạn nào hay trang điểm, thì em kem chống nắng &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot; style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px;&quot;&gt;&lt;span style=&quot;color: rgb(178, 34, 34);&quot;&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;E&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: arial, helvetica, sans-serif; font-size: 18px; line-height: 24.8400001525879px; color: rgb(178, 34, 34);&quot;&gt;&lt;strong&gt;&lt;span style=&quot;vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;ltaMD UV Clear &lt;/span&gt;SPF 46 Broad Spectrum sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&amp;nbsp;này rất thích hợp là lớp chống nắng cho lớp trang điểm vì nó được chế tạo phù hợp với môi trường nhiệt độ cao và ẩm, nó giống như lớp mặt nạ cân bằng da cho bạn, nên lớp kem này sẽ không bị chảy làm ảnh hưởng tới lớp trang điểm, thậm chí còn làm cho lớp trang điểm giữ được lâu hơn. Biết nói thế nào nhỉ, có thể xem &amp;nbsp;tác dụng này của em ấy giống như primer vậy đó. Thích lắm &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-family:arial,helvetica,sans-serif;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-400450bf-1fa1-33e3-da8b-f90e51142325&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;So với giá tiền của em thì chất lượng em ấy mang lại hoàn toàn xứng đáng. Được lợi đủ đường. Vừa chống nắng, vừa dưỡng đẹp da, vừa giảm mụn… bla..bla.. &amp;nbsp;&amp;nbsp;Chả trách sao các bạn đua nhau mua hàng, hàng về liên tục&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Kem chống nắng EltaMD UV Clear đình đám nổi tiếng từ xưa và tới tận bây giờ. Được rất nhiều các bloger , bác sĩ hay người bán hàng recommend và cả ngàn review 5 sao  của người dùng tặng cho em nó.  Hàng đã về cho các chị em đây. Khó khăn lắm mới về kịp đủ', '', 'kem chống nắng,Elta MD, SPF 46'),
(94, 1, 'Kem chống nắng Cilinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fa7-cfa4-3447-d13704fe03fb&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Kem chống nắng &lt;/span&gt;&lt;span style=&quot;color:#40E0D0;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Clinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; là dạng kem chống nắng vật lý. &lt;strong&gt;SPF 40&lt;/strong&gt; và đúng như cái tên của nó &lt;strong&gt;“Broad Spectrum”&lt;/strong&gt; &amp;nbsp;chống nắng phổ rộng luôn. &amp;nbsp;Nghĩa là chống được cả tia UVA, UVB, giúp da bạn an toàn dưới các tia UV có hại. Và công thức thì không dầu, thích hợp cho mọi loại da, kể cả da bạn nào bị mụn dùng vẫn tốt nhé. Đặc biệt em này có thể dùng luôn cho phần da mắt nhạy cảm mong manh luôn. Vậy là đỡ tốn 1 khoản mua kem chống nắng riêng cho vùng mắt, để tiền đầu tư vào các tình yêu khác nhỉ.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fa7-cfa4-3447-d13704fe03fb&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Kem chống nắng &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 18px; line-height: 24.8400001525879px; color: rgb(64, 224, 208);&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Clinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; này có thành phần rất tốt, ít silicon. Cộng thêm nhiều thành phần chiết xuất giúp dưỡng ẩm cho da ( vì thế nên da bạn nào da dầu thì sau khi rửa mặt có thể bôi thẳng em nó lên mặt mà không cần kem dưỡng ẩm cũng được), và thành phần có các chất chống oxi hóa, bảo vệ da khỏi tác hại của môi trường, giống như da được mặc 1 lớp áo giáp vậy. Về công dụng này thì ngoài em này còn có kem chống nắng &lt;strong&gt;Clarins HP UV&lt;/strong&gt; plush (xem ở đây) nhưng giá thành có phần đắt đỏ hơn &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;img alt=&quot;Kem chống nắng Clinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40 &quot; src=&quot;http://trumhangmy.com/image/data/My pham/Clinique spf 40 2.png&quot; style=&quot;width: 402px; height: 464px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fa7-cfa4-3447-d13704fe03fb&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Chất kem dạng lotion, không hề đặc cứng nên rất dễ dàng apply lên da và mỏng nhẹ vô cùng. Khi bôi lên mặt hơi dính 1 xíu, nhưng &amp;nbsp;chỉ cần 2 phút sau là &amp;nbsp;đã thấm ngay vô mặt. Da mặt mịn màng căng mướt mượt mà mà không có cám giác đang bôi kem chống nắng lên mặt. Các bạn bôi theo kiểu vỗ vỗ vào da, thì kem sẽ thấm nhanh ơi là nhanh, không hề bịt rít lại nhé. Kem chống nắng &lt;strong&gt;Clinique Super City Block&lt;/strong&gt; &amp;nbsp;có màu da nhẹ, &amp;nbsp;nên khi dùng sẽ làm da đều màu, sáng da, cover được 1 ít lỗ chân lông và khuyết điểm nhẹ vì thế trông như da đẹp tự nhiên vậy. Và là kem có màu da nên sẽ không để lại vệt trắng white cast trên mặt bạn, mà thay vào đó giống như lớp found nhẹ cover da, khiến da bạn đều màu, trắng trẻo xinh xắn hơn. Các bạn có thể dùng thay kem lót luôn nếu không muốn lích kích nhiều lớp nhé. &amp;nbsp;Mùi hương em nó thì &amp;nbsp;nhẹ nhàng tự nhiên yêu ơi là yêu chứ không thơm nồng nặc mùi hương liệu hóa chất nhưng mà cũng bay rất nhanh thôi . À mà em này coi vậy chứ kiềm dầu khá lắm đấy nhé. Bôi từ sáng mà tới trưa mặt cũng không đổ dầu lên láng như mọi ngày. Bình thường 1 ngày mình dặm phấn lại 3,4 lần thì giờ chỉ cần dặm 1 lần thôi nhé&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fa7-cfa4-3447-d13704fe03fb&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Bản thân mình, buổi sáng sau khi rửa mặt và dùng toner cân bằng da dưỡng ẩm, là mình chơi thẳng em &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size: 18px; line-height: 24.8400001525879px; color: rgb(64, 224, 208);&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Clinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; này lên mặt luôn. ( bạn nào kỹ có thể dùng 1 ít dưỡng ẩm cấp nước) &amp;nbsp;Lấy 1 lượng kem vừa đủ ra tay và vỗ đều nhẹ nhàng lên da mặt, ( với mình thì mình bôi luôn cà vùng da mắt luôn nhé vì kem này dịu nhẹ), khoảng 30 giây là kem thấm đều hết vô da rồi, và 2 - &amp;nbsp;4 phút sau là da mình mướt rượt, mềm mại , con ruội đầu lên trượt chân té luôn. Hi hi. Sau đó mình phủ thêm 1 lớp phấn bột khoảng lên. Thế là xinh đẹp tự nhiên mà ai cũng tưởng da là đẹp sẵn vậy. Mà em này kiềm dầu khá tốt, không làm da bị đổ dầu nhiều nữa nhé. 1 ngày mình chỉ cần touch up phấn phủ vào đầu giờ chiều là ok xinh đẹp luôn. Có ngày mình chả touch up gì hết mà vẫn đẹp bình thường, hi hi&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Kem chống nắng Clinique Super City Block Oil-Free Daily Face Protector Broad Spectrum SPF 40 là dạng kem chống nắng vật lý. SPF 40 và đúng như cái tên của nó “Broad Spectrum”  chống nắng phổ rộng luôn. ', '', 'Clinique,kem chống nắng,SPF 40'),
(99, 1, 'Kem chống nắng dạng xịt Neutrogena Ultra Sheer Body Mist Sunscreen Broad Spectrum SPF 70', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Là một trong những thành viên hot và đình đám của gia đình kem chống nắng &lt;strong&gt;Neutrogena.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Xịt chống nắng &lt;/span&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Ultra Sheer Body Mist Sunscreen Broad Spectrum SPF 70&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; &amp;nbsp;dạng xịt rất là tiện lợi.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Xit một phát lên cơ thể hoặc mặt , là có thể dễ dàng blend ra da và thẫm thấu rất nhanh vào da mà không gây b&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; white-space: pre-wrap; font-size: 18px; line-height: 1.38; background-color: transparent;&quot;&gt;í nhờn hay tắc nghẽn lỗ chân lông và mụn. &amp;nbsp;Đặc biệt khi vừa xịt lên cơ thể, bạn sẽ cảm thấy cực kỳ sảng&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;khoái ,relax, mát mẻ chứ không nặng trịch bóng nhờn khó chịu nha.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/My pham/20150724_133706.jpg&quot; style=&quot;width: 600px;&quot; /&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Thành phần em &lt;/span&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Ultra Sheer Body Mist Sunscreen này có Helioplex&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;, là 1 chất chống nắng mà khi&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Có nó trong thành phần kem chống nắng thì bạn có thể yên tâm là da bạn được bảo vệ hoàn hảo dưới ánh nắng mặt trời,&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;, giúp bạn chống nắng phổ rộng luôn nha, tia nào cũng chấp hết.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height: 1.38; margin-top: 0pt; margin-bottom: 10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Với &lt;/span&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;SPF 70&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;. Bạn có thể yên tâm dùng được hàng ngày trong thành pố và khi đi biển đều được hết nhé vì em nó&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fbd-251e-aa1b-74643089c43e&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;không thấm nước và siêu chống mồ hôi luôn, tha hồ thoải mái vận động không sợ chảy và mất tác dụng, hi hi&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Là một trong những thành viên hot và đình đám của gia đình kem chống nắng Neutrogena. Xịt chống nắng Neutrogena Ultra Sheer Body Mist Sunscreen Broad Spectrum SPF 70  dạng xịt rất là tiện lợi.', '', 'neutrogena,kem chống nắng,xịt chống nắng, spf 70'),
(101, 1, 'Kem chống nắng La Roche-Posay Anthelios 60 Ultra Light Sunscreen Fluid', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc0-6ca6-ab96-d8fe57e058f3&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Hãng này nổi tiếng nhất về kem chống nắng rồi, kem chống nắng của hãng luôn được liệt vào hàng siêu phẩm, bom tấn các kiểu. &amp;nbsp;Riêng em kem chống nắng &lt;strong&gt;La Roche-Posay Anthelios 60 Ultra Light Sunscreen Fluid&lt;/strong&gt; này là &amp;nbsp;loại đã làm nên tên tuổi cho hãng và nổi đỉnh đám. &amp;nbsp;còn được giải thường &amp;nbsp;Best Of Beauty do Allure bình chọn nữa nha. Được bác sĩ da liễu khuyên dùng&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc0-6ca6-ab96-d8fe57e058f3&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Thành phần khỏi phải chê. Có cả 1 hệ thống chống nắng Cell-ox Shield ( trong này toàn là những thành phần chống nắng đỉnh của đỉnh), Bên cạnh đó, với SPF 60 và công nghệ mới của La Roche, giúp cho lớp chống nắng được giữ lâu và bên , ổn định dưới ánh nắng, nên có thể bảo vệ lâu hơn và kéo dài thời gian chống nắng hơn. Có cả Senna Alatala là phức hợp chất chống oxi hóa mạnh, có tác &amp;nbsp;dụng bảo vệ tế bào của da, giúp bảo vệ da ở cấp độ tế bào luôn đó,lại &amp;nbsp;còn chống oxi hóa, chống nắng phổ rộng được UVA. UVB, &amp;nbsp;bảo vệ &amp;nbsp;da hoàn hảo tuyệt với luôn. Độ an toàn của em nó đươc liệt vào mức an toàn cấp độ 3, khỏi sợ có thành phần độc hại nha&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc0-6ca6-ab96-d8fe57e058f3&quot;&gt;&lt;span style=&quot;font-size: 14.6666666666667px; font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Chất kem siêu lỏng, &amp;nbsp;thấm vào da nhanh, không để lại vệt trắng whitecast, không bóng nhờn bí mà lại để lại “ matte finnish ” giống như trên thân chai quảng cáo, không thấm nước, nhẹ dịu an toàn cho da., da nhạy cảm dùng tốt hết luôn, Nói chung là ưng lắm. Mỗi lần hết kem chống nắng là kem chống nắng &lt;strong&gt;La Roche-Posay Anthelios 60 Ultra Light Sunscreen Fluid&lt;/strong&gt; luôn nằm trong top list&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', '', '', ''),
(105, 1, 'Sữa rửa mặt trị mụn Neutrogena Oil-Free Acne Wash', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fca-dae9-8af8-6d2f39ea6de9&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em sữa rửa mặt &lt;/span&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; là sữa rửa mặt số 1 dành cho da dầu và bị mụn trứng cá. Là sản phẩm trị mụn bán chạy nhất của hãng (Best Seller í) &amp;nbsp;Bạn nào da dầu nhiều và bị mụn, cam kết với các bạn khi dùng 1 thời gian da sẽ giảm mụn thấy rõ, mụn đang sứng cũng đỡ và ngăn ngừa mụn mới phát sinh luôn. Em nó còn được Allure bình chọn là sữa rửa mặt trị mụn tốt nhất luôn đó nha &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fca-dae9-8af8-6d2f39ea6de9&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Sữa rửa trị mụn &lt;/span&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; chính là sản phảm “ đinh” về rửa trị mụn trứng cá của hãng. Chất sữa rửa mặt dạng gel trong, dùng rất tiện và tiết kiệm.Được chiết xuất từ tinh chất hoa cúc. Thành phần lại chứa Salicylic Acid giúp ngăn ngừa mụn hiệu quả từ trong trứng, và thổi bay mụn có sẵn, giảm nhờn, se lỗ chân lông…&amp;nbsp;Công thức oil- free – không dầu sẽ không làm tắc nghẽn da, giúp da sạch, thông thoáng, kiềm dầu. &lt;/span&gt;&lt;br class=&quot;kix-line-break&quot; /&gt;\r\n&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Được các bác sĩ da liễu khuyên dùng. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Các sản phẩm của &lt;strong&gt;Neutrogena&lt;/strong&gt; đều rất tốt cho da mụn, vì &lt;strong&gt;Neutrogena &lt;/strong&gt;là 1 trong 3 thương hiệu nổi tiếng nhất về điều trị mụn trên Thế Giới&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Em sữa rửa mặt Neutrogena Oil-Free Acne Wash là sữa rửa mặt số 1 dành cho da dầu và bị mụn trứng cá. Là sản phẩm trị mụn bán chạy nhất của hãng ( Best Seller í)  Bạn nào da dầu nhiều và bị mụn, cam kết với các bạn khi dùng 1 thời gian da sẽ giảm mụn thấy ', '', 'neutrogena, sữa rửa mặt, trị mụn'),
(104, 1, 'Sữa rửa mặt giảm mụn, giảm đỏ Neutrogena Oil-Free Acne Wash Redness Soothing Facial Cleanser', '&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc8-899e-87b5-c1b368524000&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em sữa rửa mặt &lt;/span&gt;&lt;span style=&quot;color:#00FF00;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash Redness Soothing Facial Cleanser &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;với màu xah lá dịu mát sẽ mang lại cho bạn làn da dịu mát y như vậy luôn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Da bạn nào mà bị mụn ửng đỏ nè, dầu nè, hoặc cảm giác nóng bừng bừng do mụn, thì em &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color:#00FF00;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash Redness Soothing Facial Cleanser&lt;/strong&gt; &lt;/span&gt;&lt;/span&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;với thành phần được chiết xuất thì cú La Mã và Lô Hội sẽ giúp làm dịu da, kể cả da nhạy cảm, mong manh cũng không sao hết. Ngoài ra, em nó còn có Acid Salicylic giúp ngừa mụn hiệu quả, giảm nhờn, se lỗ chân lông…&amp;nbsp;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Công thức oil- free – không dầu sẽ không làm tắc nghẽn da, giúp da sạch, thông thoáng, kiềm dầu. Da bạn sẽ từ từ sáng ra, khỏe ra, giảm mụn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Được các bác sĩ da liễu khuyên dùng. Các sản phẩm của Neutrogena đều rất tốt cho da mụn, vì &lt;strong&gt;Neutrogena &lt;/strong&gt;là 1 trong 3 thương hiệu nổi tiếng nhất về điều trị mụn trên Thế Giới.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Da bạn nào mà bị mụn ửng đỏ nè, dầu nè, hoặc cảm giác nóng bừng bừng do mụn, thì em Neutrogena Oil-Free Acne Wash Redness Soothing Facial Cleanser với thành phần được chiết xuất thì cú La Mã và Lô Hội sẽ giúp làm dịu da, kể cả da nhạy cảm, mong manh cũng ', '', 'neutrogena, sữa rửa mặt, trị mụn'),
(107, 1, 'Kem chống nắng Banana Boat Protect &amp; Hydrate Sunscreen Lotion 2in1 Broad Spectrum SPF 30', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fd2-2c73-f39c-522fac6d37ef&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Banaba Boat &lt;/strong&gt;ở Việt Nam mình thông dụng nhất là các loại kem chống nắng. Còn ở nước ngoài thì nó là hãng chuyên chăm sóc da tiếp xúc với nắng ( kem phơi da, kem chống nắn, kem after sun gì cũng có hết). Và em kem chống nắng &lt;/span&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Banana Boat Protect &amp;amp; Hydrate Sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; này là 1 trong những loại kem chống nắng nổi tiếng và theo mình là đáng mua nhất của hãng&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fd2-2c73-f39c-522fac6d37ef&quot;&gt;&lt;span style=&quot;color:#FF8C00;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Banana Boat Protect &amp;amp; Hydrate Sunscreen&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; là loại kem chống nắng mới ra của hãng. Và hình như cái sau có xu hướng tốt hơn cái trước hay sao í mà em này tốt hầu như không có điểm trừ luôn&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fd2-2c73-f39c-522fac6d37ef&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Banana Boat Protect &amp;amp; Hydrate Sunscreen &lt;/strong&gt;thuộc dạng kem chống nắng hóa học, trên bao bì còn ghi &lt;strong&gt;Broad Spectrum &lt;/strong&gt;chống nắng phổ rộng luôn nên bảo vệ da được tác hại của cả UVA và UVB thật là thích. Thành phần rất ư là tốt với lô hội, Vitamin E. C giúp dưỡng ẩm da mềm mịn nè, làm sáng da nè,chống ô xi hóa lão hóa các kiểu, làm dịu da cháy nắng nè và dưỡng da rất tốt luôn, da bạn nào lỡ có nhạy cảm dùng được luôn nhá. Và vì kem có chất dưỡng ẩm nên nếu da bạn nào dầu thì sau khi rửa mặt và toner cân bằng da có thể apply trực tiếp em nó lên mặt luôn mà không phải dưỡng ẩm&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fd2-2c73-f39c-522fac6d37ef&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em kem chống nắng &lt;strong&gt;Banana Boat Protect &amp;amp; Hydrate Sunscreen&lt;/strong&gt; &amp;nbsp;này nhìn cái chai là đã yêu ơi là yêu. Mùa hè nhìn vô là thấy xanh mướt mát rồi nha. Chai màu trong suốt, nhìn được cả kem bên trong 2 màu xanh lá- &amp;nbsp;trắng quện đều với nhau đẹp và nghệ thuật lắm luôn, hi hi. (hồi trước mình còn định lấy đũa khuấy cho nó đều nhau mới ghê). Chất kem mềm và mướt, không đặc cứng như cream mà nghiêng về dạng lotion. Mùi lô hội thơm ngan ngát mát mẻ giữa trưa hè luôn. &amp;nbsp;Khi bôi lên da bạn sẽ cảm nhận ngay cảm giác mát lạnh của lô hội , bôi tới đâu mát tới đó và tan rất nhanh vô da. Khi bạn bôi lên da vỗ đều, chỉ 1 phút sau là kem đã thấm hoàn toàn, trả lại cho bạn làn da đủ ẩm ,mịn màng và rất mát mẻ, không hề bị bóng nhờn hay bí rít. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fd2-2c73-f39c-522fac6d37ef&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Sản phẩm này rất dịu nhẹ, và an toàn cho da nhạy cảm, không gây kích ứng nên có thể dùng bôi mặt tốt nhé. Vì bản thân chỉ số &lt;strong&gt;SPF 30&lt;/strong&gt; này dùng để bôi mặt mà. Tuy nhiên giá cả em nó thì rất rất là kinh tế luôn Chai to đùng 177ml, các bạn có thể kết hợp vừa dùng cho mặt vừa dung cho body để cơ thể trắng đều luôn nhé, dùng tẹt ga cả mùa hè. &lt;strong&gt;SPF 30&lt;/strong&gt; là chỉ số lý tưởng để chống nắng hằng ngày trong thành phố đấy ạ. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fd2-2c73-f39c-522fac6d37ef&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;À mà quên nữa. em này là water resistant, không thấm nước và có thể hoạt đông tốt tới 80 phút dưới nước luôn. Vậy là tha hồ thích hợp cho mùa hè hay đổ mồ hôi mà khôg sợ em nó bị trôi chèm nhẹp giảm hiệu quả nha. Đây quả là sản phẩm lý tưởng cho mùa hè cả về hiệu quả và giá cả. Shop đã dày công nghiên cứu và dùng thử để giới thiệu với các bạn sản phẩm tốt như kem chống năng &lt;strong&gt;Banana Boat Protect &amp;amp; Hydrate Sunscreen &lt;/strong&gt;&amp;nbsp;thế này. Còn chờ gì nữa mà không sắm ngay 1 em cho mùa hè này&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Banana Boat Protect &amp; Hydrate Sunscreen là loại kem chống nắng mới ra của hãng. Và hình như cái sau có xu hướng tốt hơn cái trước hay sao í mà em này tốt hầu như không có điểm trừ luôn', '', 'Banana Boat, kem chống nắng,spf 30');
INSERT INTO `product_description` (`product_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `tag`) VALUES
(106, 1, 'Sữa rửa mặt Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub - 198ml', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fcd-dace-e880-06fd786eadb8&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Là dạng gel hồng, có nhưng hạt rất nhỏ và mịn, &amp;nbsp;Sữa rửa mặt trị mụn &lt;/span&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;có thể nhẹ nhàng rửa sạch da bạn, đánh bay cả mụn trứng cá và đặc biệt là mụn đầu đen luôn. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fcd-dace-e880-06fd786eadb8&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Được chiết xuất từ bưởi và vit C, &lt;/span&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; có bọt nhẹ nhàng len lỏi vào từng lỗ chân lông, giúp làm sạch sâu làn da, thu nhỏ lỗ chân lông., tẩy tế bào chết cho làn da sáng đẹp , mịn màng. Acid Salicylic giúp &amp;nbsp;trị mụn trứng cá và đánh bật mụn đầu làm sạch và. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;img alt=&quot;sua-rua-mat-Neutrogena-Oil-Free-Acne-Wash-Pink-Grapefruit-Foaming-Scrub-198ml&quot; src=&quot;http://trumhangmy.com/image/data/My pham/Sua rua mat tri mun dau den Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub.jpg&quot; style=&quot;width: 600px; height: 1067px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fcd-dace-e880-06fd786eadb8&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Dùng sản phẩm &lt;/span&gt;&lt;span style=&quot;color:#EE82EE;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;, chỉ cần 1 bước rửa mặt là có thể tích hợp được 2 sản phẩm là sữa rửamặt và nước hoa hồng luôn, bạn không cần phải dùng nước hoa hồng sau đó mà da vẫn sạch và ẩm mịn.&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Là dạng gel hồng, có nhưng hạt rất nhỏ và mịn,  Sữa rửa mặt trị mụn Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub có thể nhẹ nhàng rửa sạch da bạn, đánh bay cả mụn trứng cá và đặc biệt là mụn đầu đen luôn. ', '', 'neutrogena, sữa rửa mặt, trị mụn'),
(102, 1, 'Sữa rửa mặt CaraVe Foaming Facial Cleanser dành cho da thường- da dầu', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc4-2e3d-f38d-4d76fecfb551&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Sữa rửa mặt &lt;/span&gt;&lt;span style=&quot;color:#40E0D0;&quot;&gt;&lt;span style=&quot;font-family: Calibri; vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt;CaraVe Foaming Facial Cleanser&lt;/strong&gt; &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;thì quá là nổi tiếng. Chỉ cần google seach là cả mấy chục ngàn review khen tới tấp. Được rate 4.5/5 trên Drugstore &amp;nbsp;Chả trách mới nhập em này về mà mấy bạn sang mua ào ào làm mình tò mò phải thử dùng em ấy. Và kết quả là mê em ấy lết bánh vì tất cả những gì em nó mang lại&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc4-2e3d-f38d-4d76fecfb551&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Thành phần của Sữa rửa mặt &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;strong style=&quot;color: rgb(64, 224, 208); font-family: Calibri; font-size: 18px; line-height: 24.8400001525879px; white-space: pre-wrap;&quot;&gt;CaraVe Foaming Facial Cleanser&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;strong&gt; &lt;/strong&gt;quá là yêu. Không có Fragrance luôn. Mà còn &amp;nbsp;có thần dược Vitamin B3 giúp trắng da, ngừa lão hóa, ngừa mụn, giảm stress cho da. Có cả Ceramide,HA,&amp;nbsp;giúp giữ ẩm, tái tạo, bảo vệ da, ngừa mất nước, hạn chế vi khuẩn xâm nhập vô da .Giúp da sau khi rửa xong không những sạch mà thậm chí còn mềm mượt và trơn láng đã lắm luôn. &amp;nbsp;&amp;nbsp;Đặc biệt độ pH chuẩn 5.5 ( độ pH quan trọng với da như thế nào thì mọi người cũng biết rồi đó xem ở đây) Da của bạn sẽ thật sự khỏe mạnh nếu dùng loại sữa rửa mặt có độ pH chuẩn như thế này&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc4-2e3d-f38d-4d76fecfb551&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Chất sữa dạng gel trong lỏng. Không mùi. (Da bạn nào nhạy cảm thì thích hợp lắm đó ). &amp;nbsp;Khi đánh lên có tạo ra bọt. Mình rất thích sản phẩm nào có tạo bọt vì như thế mới có cảm giác sạch sẽ được. Em nó rửa sạch hết mọi bụi bẩn trên da nhưng không hề làm da bị khô căng cứng như các loại rửa mặt tạo bọt thông thường, ngược lại dạ rất mềm mượt vì vô số các thành phần giữ ẩm mình kể ở trên. Rất thích hợp để sử dụng hàng ngày vì độ nhẹ dịu của nó&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/My pham/Sua-rua-mat-CeraVe.png&quot; style=&quot;width: 800px; height: 534px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc4-2e3d-f38d-4d76fecfb551&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Sữa rửa mặt &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;strong style=&quot;color: rgb(64, 224, 208); font-family: Calibri; font-size: 18px; line-height: 24.8400001525879px; white-space: pre-wrap;&quot;&gt;CaraVe Foaming Facial Cleanser&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt; thích hợp cho da nhạy cảm, yếu, cần được bảo vệ và phục hồi. Được các bác sĩ da liễu khuyên dùng . &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fc4-2e3d-f38d-4d76fecfb551&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Về giá thành thì khỏi phải nói. Với những gì em nó mang lại, và với dung tích to đùng đoàng 355 ml, xài điên đảo cả hơn nửa năm thì giá cả của Sữa rửa mặt &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;strong style=&quot;color: rgb(64, 224, 208); font-family: Calibri; font-size: 18px; line-height: 24.8400001525879px; white-space: pre-wrap;&quot;&gt;CaraVe Foaming Facial Cleanser &lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;hết sức là hợp lý. Còn chờ gì nữa mà không mua ngay một em &amp;nbsp;về để nâng niu chiều chuộng làn da bạn&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Sữa rửa mặt CaraVe Foaming Facial Cleanser  thì quá là nổi tiếng. Chỉ cần google seach là cả mấy chục ngàn review khen tới tấp. Được rate 4.5/5 trên Drugstore  Chả trách mới nhập em này về mà mấy bạn sang mua ào ào làm mình tò mò phải thử dùng em ấy. Và k', '', ''),
(98, 1, 'Neutrogena Healthy Defense Daily Moisturizer SPF 50 with Helioplex', '&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fb7-d88f-958d-ca67dc4b66ac&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Kem dưỡng ẩm chống nắng &lt;strong&gt;Neutrogena Healthy Defense Daily Moisturizer&lt;/strong&gt; là sản phẩm được tích hợp cả 2 công dụng, vưà dưỡng ẩm và vưa chống nắng rất tốt với chỉ số chống nắng lên tới &lt;strong&gt;SPF 50&lt;/strong&gt; thích hợp để bảo vệ da hang ngày. Em nó còn được giải Editor''s Choice của &amp;nbsp;Allure nữa nha. Em này luôn nằm trong top list kem chống nắng được các bác sĩ khuyên dùng vì rất tốt cho da. &lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fb7-d88f-958d-ca67dc4b66ac&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Em kem chống nắng dưỡng ẩm &lt;strong&gt;Neutrogena Healthy Defense Daily Moisturizer &lt;/strong&gt;này thành phần rất dễ thương. Không chứa hượng liệu . Có Vitamin E giúp chống lão hóa nè, dưỡng ẩm nè, tạo ra lớp màng ngăn sự ô nhiễm môi trường tác động tới da. Đặc biệt là em ấy thành phần chứa Helioplex, Chỉ cần thành phần có chất Helioplex này thôi là có thể yên tâm da được bảo vệ an toàn dưới ánh nắng. ( đọc thong tin vè chất Helioplex tại đây) , chống nắng phổ rộng,UVA, UVB &amp;nbsp;chống lại được tuốt luốt, lại không gây mụn và không làm da bị dị ứng, quá là an toàn cho da&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;http://trumhangmy.com/image/data/My pham/kem chong nang Neutrogena Healthy Defense Daily Moisturizer SPF 50 with Helioplex.jpg&quot; style=&quot;width: 600px; height: 1067px;&quot; /&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p dir=&quot;ltr&quot; style=&quot;line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fb7-d88f-958d-ca67dc4b66ac&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Chất kem &lt;strong&gt;Neutrogena Healthy Defense Daily Moisturizer&lt;/strong&gt; xốp, nhìn thì có vẻ đặc nhưng không hề cứng, có thể dễ dàng tán và kem bám đều lên da vô cùng nhẹ nhàng. Lúc đầu mới vỗ kem lên mặt, có cảm giác bóng và hơi nhờn, nhưng yên tâm, chỉ 1 lúc sau là kem thấm vào da, không nhờn dính, &amp;nbsp;tan vào da luôn như không có gì &amp;nbsp;&amp;nbsp;và bạn sẽ không còn cảm giác nặng nề như lúc đầu nữa mà thay vào đó là &amp;nbsp;làn da ẩm mịn cả ngày luôn. SPF 50 chống nắng phổ rộng, quá đủ cho 1 làn da cần bảo vệ an toàn&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span id=&quot;docs-internal-guid-f5329f57-1fb7-d88f-958d-ca67dc4b66ac&quot;&gt;&lt;span style=&quot;font-family: Calibri; color: rgb(0, 0, 0); vertical-align: baseline; white-space: pre-wrap; background-color: transparent;&quot;&gt;Nếu có em này trong tay, công việc sửa soạn buổi sáng của bạn tiết kiệm rất nhiều thời gian. &amp;nbsp;Sáng sau khi rưả mặt mình bôi &lt;strong&gt;Neutrogena Healthy Defense Daily Moisturizer&lt;/strong&gt; 1 lượng vừa đủ, thế là vừa chống nắng vừa dưỡng ẩm. Chờ kem thấm mình chỉ việc bôi lớp BB hoặc CC cream yêu thích, phủ tí phấn là xinh đẹp. BB, CC cream của mình cũng có chỉ số chống nắng luôn, phấn phủ cũng vậy, để hỗ trợ thêm cho em dưỡng ẩm kia thôi. Mình làm văn phòng, cũng không cần gì nhiều&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Kem dưỡng ẩm chống nắng Neutrogena Healthy Defense Daily Moisturizer là sản phẩm được tích hợp cả 2 công dụng, vưà dưỡng ẩm và vưa chống nắng rất tốt với chỉ số chống nắng lên tới SPF 50 thích hợp để bảo vệ da hang ngày. Em nó còn được giải Editor''s Choic', '', 'kem chống nắng,Neutrogena,SPF 50'),
(79, 1, 'Kem Chống Nắng Neutrogena Pure &amp; Free Liquid Sunscreen SPF 50', '&lt;p&gt;&lt;span style=&quot;color:#0000FF;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt; này lấy về dc vài ngày là người quen hốt hết sạch . Còn 2 em duy nhất nè. Em &lt;/span&gt;&lt;/span&gt;&lt;strong style=&quot;color: rgb(0, 0, 255); line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 18px;&quot;&gt;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&lt;/span&gt;&lt;/strong&gt;0&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt; chuyên dùng cho mặt, chất lỏng, nhẹ như lotion í. Nên có thể dễ dàng bôi lên da. Không gây nặng hay bí da, cũng không hề làm da bị bóng như mấy loại kem chống nắng khác nha. Không làm da đổ dầu luôn. Quá tuyệt vời, mong đợi gì hơn chứ. Em &lt;/span&gt;&lt;/span&gt;&lt;strong style=&quot;color: rgb(0, 0, 255); line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 18px;&quot;&gt;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt; này tốt cho da dầu, da nhạy cảm nữa nha. Có thể dùng như lớp lót, dùng 1 mình em í vẫn làm mặt sáng sủa tự nhiên mà ko thấy dầu nhờn nhé. Thêm tí son vô là xinh thôi rồi luôn.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;img alt=&quot;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&quot; id=&quot;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&quot; longdesc=&quot;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&quot; src=&quot;http://trumhangmy.com/image/data/My pham/20150724_133735.jpg&quot; style=&quot;width: 600px; height: 800px;&quot; /&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;Thêm vào đó, thành phần kem lại vô cùng tư nhiên và an toàn. Không nước hoa, thuốc nhuộm, dầu, và các thành phần hóa học gây kích thích, rất tốt cho làn da nhạy cảm! - Ít gây dị ứng - Không thấm nước, không thấm mồ hôi - Không hương - Không làm tắc nghẽn lỗ chân lông hoặc gây mụn - Không dầu, PABA &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;HDSD : Lắc chai trước khi dùng. Bôi kem 15 phút trước khi ra nắng. Để bảo vệ thêm, bôi lại sau khi bơi, đổ mồ hôi quá nhiều, lau bằng khăn hoặc phơi nắng nhiều. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p style=&quot;line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;Thành phần hoạt động của &lt;/span&gt;&lt;/span&gt;&lt;strong style=&quot;color: rgb(0, 0, 255); line-height: 20.7999992370605px;&quot;&gt;&lt;span style=&quot;font-size: 18px;&quot;&gt;Kem Chống Nắng Neutrogena Pure &amp;amp; Free Liquid Sunscreen SPF 50&lt;/span&gt;&lt;/strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;span style=&quot;color: rgb(55, 62, 77); font-family: helvetica, arial, sans-serif; line-height: 15.3599996566772px; white-space: pre-wrap;&quot;&gt;: Titanium Dioxide 5%, Zinc Oxide 3%&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'Kem Chống Nắng Neutrogena Pure &amp; Free Liquid Sunscreen SPF 50 này lấy về dc vài ngày là người quen hốt hết sạch . Còn 2 em duy nhất nè. Em Kem Chống Nắng Neutrogena Pure &amp; Free Liquid Sunscreen SPF 500 chuyên dùng cho mặt, chất lỏng, nhẹ như lotio', 'mỹ phẩm xách tay,mỹ phẩm giá rẻ,mỹ phẩm chính hãng,chuyên bán mỹ phẩm,cung cấp mỹ phẩm,kem chống nắng,kem nền,kem trắng da,phấn phủ,phấn má hồng,son môi,phấn mắt,kem dưỡng da,kem chống lão hóa da,lotion,chì kẻ mắt,mascara,bấm mi,son dưỡng,son kem,son thỏi', 'kem chống nắng,neutrogena');

-- --------------------------------------------------------

--
-- Table structure for table `product_discount`
--

CREATE TABLE IF NOT EXISTS `product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=447 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_filter`
--

CREATE TABLE IF NOT EXISTS `product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2738 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2580, 84, 'data/iBlue/san pham giam can an toan iBlue.jpg', 0),
(2667, 79, 'data/My pham/Neutrogena Pure and Free Liquid, SPF 50, 1.4 Ounce.JPG', 0),
(2729, 57, 'data/Hoa don/Hoa don mua hang tu My.JPG', 0),
(2728, 57, 'data/My pham/May rua mat neutrogena.JPG', 0),
(2727, 57, 'data/My pham/Phan phu purminerals.JPG', 0),
(2726, 57, 'data/My pham/Mascara Maybelline.JPG', 0),
(2725, 57, 'data/My pham/Mascara CoverGirl Clump Crusher.JPG', 0),
(2724, 57, 'data/My pham/Lan khu mui Lady Speed Stick.JPG', 0),
(2723, 57, 'data/My pham/Son EOS.JPG', 0),
(2722, 57, 'data/My pham/Son hu Carmex.JPG', 0),
(2721, 57, 'data/My pham/Lan khu mui BAN.JPG', 0),
(2720, 57, 'data/My pham/Kem chong nang Neutrogena.JPG', 0),
(2719, 57, 'data/My pham/Kem danh rang Crest.JPG', 0),
(2718, 57, 'data/My pham/Kem chong nang Blue Lizard.JPG', 0),
(2717, 57, 'data/My pham/Ke mat Eyes Liner Wet n Wild.JPG', 0),
(2716, 57, 'data/My pham/Kem chong nang Clarins.JPG', 0),
(2715, 57, 'data/My pham/Eyes Liner HD.JPG', 0),
(2668, 58, 'data/My pham/Clarins UV Plus HP Day Screen High Protection SPF 40 UVA-UVB Oil-free.jpg', 0),
(2587, 82, 'data/My pham/Son Loreal moi nhat.JPG', 0),
(2714, 57, 'data/My pham/Eyes Liner Kiss.JPG', 0),
(2713, 57, 'data/Hoa don/Hoa don mua hang tu My 1.JPG', 0),
(2712, 57, 'data/My pham/Eye Liner Revlon.JPG', 0),
(2711, 57, 'data/Hoa don/Hoa don mua hang tu My 8.JPG', 0),
(2584, 81, 'data/My pham/Neutrogena Ultra Sheer Sunscreen SPF 45 Twin Pack 6.0 Ounce.jpg', 0),
(2665, 80, 'data/My pham/Blue Lizard Sunscreen Sensitive chemical frr, Fragrance free, SPF 30+, 3fl.oz.jpg', 0),
(2624, 85, 'data/My pham/Mascara Maybelline Full n Soft .JPG', 0),
(2623, 86, 'data/My pham/Covergirl-Clump-Crusher-by-Lashblast-Mascara.jpg', 0),
(2649, 102, 'data/My pham/SRM_Carave.jpg', 0),
(2639, 105, 'data/My pham/Suatrimun_oil_free.jpg', 0),
(2640, 104, 'data/My pham/Suagiammun.jpg', 0),
(2633, 101, 'data/My pham/KCN_laroche60.jpg', 0),
(2655, 93, 'data/My pham/20150529_112651.jpg', 0),
(2637, 107, 'data/My pham/banana boat.jpg', 0),
(2638, 107, 'data/My pham/banana-boat-sport-performance-sunscreen-lotion SPF30.jpg', 0),
(2650, 106, 'data/My pham/Sua rua mat tri mun dau den Neutrogena Oil-Free Acne Wash Pink Grapefruit Foaming Scrub 2.jpg', 0),
(2654, 93, 'data/My pham/20150529_112709.jpg', 0),
(2653, 93, 'data/My pham/20150529_112720.jpg', 0),
(2658, 98, 'data/My pham/kcn_neutrogenaspf50.jpg', 0),
(2659, 98, 'data/My pham/kem chong nang Neutrogena Healthy Defense Daily Moisturizer SPF 50 with Helioplex 2.jpg', 0),
(2662, 92, 'data/My pham/20150717_151217.jpg', 0),
(2737, 99, 'data/My pham/20150724_133656.jpg', 0),
(2710, 57, 'data/My pham/Eye Liner Revlon 1.JPG', 0),
(2709, 57, 'data/Hoa don/Hoa don mua hang tu My 7.JPG', 0),
(2708, 57, 'data/Hoa don/Hoa don mua hang tu My 9.JPG', 0),
(2707, 57, 'data/Hoa don/Hoa don mua hang tu My 12.JPG', 0),
(2706, 57, 'data/Hoa don/Hoa don mua hang tu My 5.JPG', 0),
(2705, 57, 'data/Hoa don/Hoa don mua hang tu My 4.JPG', 0),
(2704, 57, 'data/Hoa don/Hoa don mua hang tu My 6.JPG', 0),
(2703, 57, 'data/Hoa don/Hoa don mua hang tu My 3.JPG', 0),
(2730, 57, 'data/Hoa don/Hoa don mua hang tu My 2.JPG', 0),
(2731, 57, 'data/VictoriaSecrect/Body Lotion Victoria Secrect.JPG', 0),
(2732, 57, 'data/VictoriaSecrect/Kem duong the Body Lotion Victoria Secrect.JPG', 0),
(2733, 57, 'data/VictoriaSecrect/Sua duong the Victoria Secrect.JPG', 0),
(2734, 57, 'data/VictoriaSecrect/Nuoc hoa Victoria Secrect.JPG', 0),
(2735, 57, 'data/Hoa don/Hoa don mua hang tu My 11.JPG', 0),
(2736, 57, 'data/Hoa don/Hoa don mua hang tu My 10.JPG', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_option`
--

CREATE TABLE IF NOT EXISTS `product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_value`
--

CREATE TABLE IF NOT EXISTS `product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_profile`
--

CREATE TABLE IF NOT EXISTS `product_profile` (
  `product_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_recurring`
--

CREATE TABLE IF NOT EXISTS `product_recurring` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_related`
--

CREATE TABLE IF NOT EXISTS `product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_related`
--

INSERT INTO `product_related` (`product_id`, `related_id`) VALUES
(57, 59),
(57, 60),
(57, 61),
(58, 79),
(58, 80),
(58, 81),
(58, 87),
(58, 91),
(58, 92),
(58, 93),
(58, 94),
(58, 98),
(58, 99),
(58, 101),
(58, 107),
(59, 57),
(59, 60),
(59, 61),
(60, 57),
(60, 59),
(60, 61),
(61, 57),
(61, 59),
(61, 60),
(62, 63),
(62, 64),
(62, 65),
(63, 62),
(63, 64),
(63, 65),
(64, 62),
(64, 63),
(64, 65),
(65, 62),
(65, 63),
(65, 64),
(66, 67),
(66, 68),
(66, 69),
(66, 70),
(66, 71),
(66, 72),
(66, 73),
(66, 74),
(66, 75),
(66, 76),
(66, 77),
(67, 66),
(67, 68),
(67, 69),
(67, 70),
(67, 71),
(67, 72),
(67, 73),
(67, 74),
(67, 75),
(67, 76),
(67, 77),
(68, 66),
(68, 67),
(68, 69),
(68, 70),
(68, 71),
(68, 72),
(68, 73),
(68, 74),
(68, 75),
(68, 76),
(68, 77),
(69, 66),
(69, 67),
(69, 68),
(69, 70),
(69, 71),
(69, 72),
(69, 73),
(69, 74),
(69, 75),
(69, 76),
(69, 77),
(70, 66),
(70, 67),
(70, 68),
(70, 69),
(70, 71),
(70, 72),
(70, 73),
(70, 74),
(70, 75),
(70, 76),
(70, 77),
(71, 66),
(71, 67),
(71, 68),
(71, 69),
(71, 70),
(71, 72),
(71, 73),
(71, 74),
(71, 75),
(71, 76),
(71, 77),
(72, 66),
(72, 67),
(72, 68),
(72, 69),
(72, 70),
(72, 71),
(72, 73),
(72, 74),
(72, 75),
(72, 76),
(72, 77),
(73, 66),
(73, 67),
(73, 68),
(73, 69),
(73, 70),
(73, 71),
(73, 72),
(73, 74),
(73, 75),
(73, 76),
(73, 77),
(74, 66),
(74, 67),
(74, 68),
(74, 69),
(74, 70),
(74, 71),
(74, 72),
(74, 73),
(74, 75),
(74, 76),
(74, 77),
(75, 66),
(75, 67),
(75, 68),
(75, 69),
(75, 70),
(75, 71),
(75, 72),
(75, 73),
(75, 74),
(75, 76),
(75, 77),
(76, 66),
(76, 67),
(76, 68),
(76, 69),
(76, 70),
(76, 71),
(76, 72),
(76, 73),
(76, 74),
(76, 75),
(76, 77),
(77, 66),
(77, 67),
(77, 68),
(77, 69),
(77, 70),
(77, 71),
(77, 72),
(77, 73),
(77, 74),
(77, 75),
(77, 76),
(79, 58),
(79, 80),
(79, 81),
(79, 87),
(79, 91),
(79, 92),
(79, 93),
(79, 94),
(79, 98),
(79, 99),
(79, 101),
(79, 107),
(80, 58),
(80, 79),
(80, 81),
(80, 87),
(80, 91),
(80, 92),
(80, 93),
(80, 94),
(80, 98),
(80, 99),
(80, 101),
(80, 107),
(81, 58),
(81, 79),
(81, 80),
(81, 87),
(81, 91),
(81, 92),
(81, 93),
(81, 94),
(81, 98),
(81, 99),
(81, 101),
(81, 107),
(83, 102),
(83, 104),
(83, 105),
(83, 106),
(85, 86),
(86, 85),
(87, 58),
(87, 79),
(87, 80),
(87, 81),
(87, 91),
(87, 92),
(87, 93),
(87, 94),
(87, 98),
(87, 99),
(87, 101),
(87, 107),
(91, 58),
(91, 79),
(91, 80),
(91, 81),
(91, 87),
(91, 92),
(91, 93),
(91, 94),
(91, 98),
(91, 99),
(91, 101),
(91, 107),
(92, 58),
(92, 79),
(92, 80),
(92, 81),
(92, 87),
(92, 91),
(92, 93),
(92, 94),
(92, 98),
(92, 99),
(92, 101),
(92, 107),
(93, 58),
(93, 79),
(93, 80),
(93, 81),
(93, 87),
(93, 91),
(93, 92),
(93, 94),
(93, 98),
(93, 99),
(93, 101),
(93, 107),
(94, 58),
(94, 79),
(94, 80),
(94, 81),
(94, 87),
(94, 91),
(94, 92),
(94, 93),
(94, 98),
(94, 99),
(94, 101),
(94, 107),
(98, 58),
(98, 79),
(98, 80),
(98, 81),
(98, 87),
(98, 91),
(98, 92),
(98, 93),
(98, 94),
(98, 99),
(98, 101),
(98, 107),
(99, 58),
(99, 79),
(99, 80),
(99, 81),
(99, 87),
(99, 91),
(99, 92),
(99, 93),
(99, 94),
(99, 98),
(99, 101),
(99, 107),
(101, 58),
(101, 79),
(101, 80),
(101, 81),
(101, 87),
(101, 91),
(101, 92),
(101, 93),
(101, 94),
(101, 98),
(101, 99),
(101, 107),
(102, 83),
(102, 104),
(102, 105),
(102, 106),
(104, 83),
(104, 102),
(104, 105),
(104, 106),
(105, 83),
(105, 102),
(105, 104),
(105, 106),
(106, 83),
(106, 102),
(106, 104),
(106, 105),
(107, 58),
(107, 79),
(107, 80),
(107, 81),
(107, 87),
(107, 91),
(107, 92),
(107, 93),
(107, 94),
(107, 98),
(107, 99),
(107, 101);

-- --------------------------------------------------------

--
-- Table structure for table `product_reward`
--

CREATE TABLE IF NOT EXISTS `product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=1034 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_reward`
--

INSERT INTO `product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(1031, 57, 2, 0),
(823, 59, 2, 0),
(1030, 57, 1, 0),
(822, 59, 1, 0),
(1027, 58, 2, 0),
(1026, 58, 1, 0),
(787, 60, 2, 0),
(786, 60, 1, 0),
(789, 61, 2, 0),
(788, 61, 1, 0),
(791, 62, 2, 0),
(790, 62, 1, 0),
(793, 63, 2, 0),
(792, 63, 1, 0),
(795, 64, 2, 0),
(794, 64, 1, 0),
(797, 65, 2, 0),
(796, 65, 1, 0),
(713, 66, 2, 0),
(712, 66, 1, 0),
(709, 67, 2, 0),
(708, 67, 1, 0),
(715, 68, 2, 0),
(714, 68, 1, 0),
(719, 69, 2, 0),
(718, 69, 1, 0),
(721, 70, 2, 0),
(720, 70, 1, 0),
(723, 71, 2, 0),
(722, 71, 1, 0),
(725, 72, 2, 0),
(724, 72, 1, 0),
(711, 73, 2, 0),
(710, 73, 1, 0),
(735, 74, 2, 0),
(734, 74, 1, 0),
(737, 75, 2, 0),
(736, 75, 1, 0),
(783, 76, 2, 0),
(782, 76, 1, 0),
(741, 77, 2, 0),
(740, 77, 1, 0),
(1025, 79, 2, 0),
(1024, 79, 1, 0),
(1021, 80, 2, 0),
(1020, 80, 1, 0),
(853, 81, 2, 0),
(852, 81, 1, 0),
(861, 82, 2, 0),
(860, 82, 1, 0),
(839, 83, 2, 0),
(838, 83, 1, 0),
(845, 84, 2, 0),
(844, 84, 1, 0),
(869, 85, 2, 0),
(868, 85, 1, 0),
(866, 86, 1, 0),
(867, 86, 2, 0),
(1007, 87, 2, 0),
(1006, 87, 1, 0),
(999, 93, 2, 0),
(998, 93, 1, 0),
(1015, 92, 2, 0),
(1014, 92, 1, 0),
(1009, 91, 2, 0),
(1008, 91, 1, 0),
(1001, 94, 2, 0),
(1000, 94, 1, 0),
(977, 107, 2, 0),
(976, 107, 1, 0),
(1033, 99, 2, 0),
(1032, 99, 1, 0),
(1011, 98, 2, 0),
(1010, 98, 1, 0),
(967, 101, 2, 0),
(966, 101, 1, 0),
(993, 102, 2, 0),
(992, 102, 1, 0),
(981, 104, 2, 0),
(980, 104, 1, 0),
(979, 105, 2, 0),
(978, 105, 1, 0),
(995, 106, 2, 0),
(994, 106, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_special`
--

CREATE TABLE IF NOT EXISTS `product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=532 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_special`
--

INSERT INTO `product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(531, 57, 1, 1, 180000.0000, '0000-00-00', '0000-00-00'),
(529, 58, 1, 0, 1300000.0000, '0000-00-00', '0000-00-00'),
(517, 59, 1, 1, 210000.0000, '0000-00-00', '0000-00-00'),
(505, 60, 1, 1, 210000.0000, '0000-00-00', '0000-00-00'),
(506, 61, 1, 1, 210000.0000, '0000-00-00', '0000-00-00'),
(507, 62, 1, 1, 230000.0000, '0000-00-00', '0000-00-00'),
(508, 63, 1, 1, 230000.0000, '0000-00-00', '0000-00-00'),
(509, 64, 1, 1, 230000.0000, '0000-00-00', '0000-00-00'),
(510, 65, 1, 1, 230000.0000, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE IF NOT EXISTS `product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_category`
--

INSERT INTO `product_to_category` (`product_id`, `category_id`) VALUES
(57, 61),
(58, 64),
(59, 61),
(60, 61),
(61, 61),
(62, 62),
(63, 62),
(64, 62),
(65, 62),
(66, 66),
(67, 66),
(68, 66),
(69, 66),
(70, 66),
(71, 66),
(72, 66),
(73, 66),
(74, 66),
(75, 66),
(76, 66),
(77, 66),
(79, 64),
(80, 64),
(81, 64),
(83, 73),
(84, 71),
(85, 68),
(86, 68),
(87, 64),
(91, 64),
(92, 64),
(93, 64),
(94, 64),
(98, 64),
(99, 64),
(101, 64),
(102, 73),
(104, 73),
(105, 73),
(106, 73),
(107, 64);

-- --------------------------------------------------------

--
-- Table structure for table `product_to_download`
--

CREATE TABLE IF NOT EXISTS `product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_layout`
--

CREATE TABLE IF NOT EXISTS `product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_store`
--

CREATE TABLE IF NOT EXISTS `product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_to_store`
--

INSERT INTO `product_to_store` (`product_id`, `store_id`) VALUES
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(98, 0),
(99, 0),
(101, 0),
(102, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0);

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `profile_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) unsigned NOT NULL,
  `cycle` int(10) unsigned NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) unsigned NOT NULL,
  `trial_cycle` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_description`
--

CREATE TABLE IF NOT EXISTS `profile_description` (
  `profile_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return`
--

CREATE TABLE IF NOT EXISTS `return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_action`
--

CREATE TABLE IF NOT EXISTS `return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_action`
--

INSERT INTO `return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `return_history`
--

CREATE TABLE IF NOT EXISTS `return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_reason`
--

CREATE TABLE IF NOT EXISTS `return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_reason`
--

INSERT INTO `return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `return_status`
--

CREATE TABLE IF NOT EXISTS `return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `return_status`
--

INSERT INTO `return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`review_id`, `product_id`, `customer_id`, `author`, `text`, `rating`, `status`, `date_added`, `date_modified`) VALUES
(2, 57, 0, 'thao', 'Bath &amp; Body Works là một nhãn hiệu chuyên về các sản phẩm dưỡng thể và chăm sóc da của tập đoàn Victoria’s Secret. Thế mạnh của Bath And Body Works không chỉ ở sản phẩm chất lượng giúp duy trì làn da mềm mại, tươi trẻ, mà còn ở các loại mùi hương đa dạng và đầy sức hút của bản thân sản phẩm.', 5, 1, '2016-01-10 14:17:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `group` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6962 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `store_id`, `group`, `key`, `value`, `serialized`) VALUES
(1200, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(5, 0, 'total', 'total_sort_order', '9', 0),
(6, 0, 'total', 'total_status', '1', 0),
(1199, 0, 'sub_total', 'sub_total_status', '1', 0),
(5619, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0),
(6488, 0, 'cod', 'cod_status', '1', 0),
(6647, 0, 'flat', 'flat_sort_order', '1', 0),
(6646, 0, 'flat', 'flat_status', '1', 0),
(6645, 0, 'flat', 'flat_geo_zone_id', '0', 0),
(6644, 0, 'flat', 'flat_tax_class_id', '9', 0),
(6467, 0, 'carousel', 'carousel_module', 'a:1:{i:0;a:9:{s:9:"banner_id";s:1:"8";s:5:"limit";s:1:"5";s:6:"scroll";s:1:"1";s:5:"width";s:3:"170";s:6:"height";s:3:"170";s:9:"layout_id";s:1:"1";s:8:"position";s:11:"content_top";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(6468, 0, 'carousel1', 'carousel1_module', 'a:1:{i:0;a:9:{s:9:"banner_id";s:2:"14";s:5:"limit";s:1:"3";s:6:"scroll";s:1:"1";s:5:"width";s:3:"400";s:6:"height";s:3:"300";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"8";}}', 1),
(6643, 0, 'flat', 'flat_cost', '30000', 0),
(1201, 0, 'tax', 'tax_status', '1', 0),
(4332, 0, 'featured', 'featured_product', '', 0),
(4331, 0, 'featured', 'product', '', 0),
(6461, 0, 'promo_banner', 'promo_banner_module', 'a:1:{i:0;a:11:{s:5:"width";s:3:"800";s:6:"height";s:3:"600";s:5:"image";s:28:"data/slideshow/KD-Online.png";s:4:"link";s:58:"http://trumhangmy.com/ho-tro-khoi-nghiep-kinh-doanh-online";s:9:"layout_id";s:1:"1";s:11:"language_id";s:1:"0";s:6:"status";s:1:"0";s:14:"display_always";s:1:"1";s:10:"new_window";s:1:"1";s:8:"position";s:11:"content_top";s:10:"sort_order";i:-1;}}', 1),
(1202, 0, 'tax', 'tax_sort_order', '', 0),
(5618, 0, 'free_checkout', 'free_checkout_status', '1', 0),
(5617, 0, 'free_checkout', 'free_checkout_order_status_id', '2', 0),
(6950, 0, 'config', 'config_error_display', '1', 0),
(6951, 0, 'config', 'config_error_log', '1', 0),
(6952, 0, 'config', 'config_error_filename', 'error.txt', 0),
(6953, 0, 'config', 'config_google_analytics', '&lt;script&gt;\r\n  (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,''script'',''//www.google-analytics.com/analytics.js'',''ga'');\r\n\r\n  ga(''create'', ''UA-57974199-1'', ''auto'');\r\n  ga(''send'', ''pageview'');\r\n\r\n&lt;/script&gt;', 0),
(4734, 0, 'nganluong', 'nganluong_sort_order', '3', 0),
(4733, 0, 'nganluong', 'nganluong_status', '1', 0),
(4732, 0, 'nganluong', 'nganluong_order_status_id', '2', 0),
(6940, 0, 'config', 'config_secure', '0', 0),
(6941, 0, 'config', 'config_shared', '0', 0),
(6942, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai''hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(6949, 0, 'config', 'config_compression', '0', 0),
(6948, 0, 'config', 'config_encryption', '160fb6ad4b85e88f0b45960bbe63c1a7', 0),
(6947, 0, 'config', 'config_password', '1', 0),
(6943, 0, 'config', 'config_seo_url', '1', 0),
(6944, 0, 'config', 'config_file_extension_allowed', 'txt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc\r\nrtf\r\nxls\r\nppt\r\nodt\r\nods', 0),
(6945, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/jpeg\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/vnd.microsoft.icon\r\nimage/tiff\r\nimage/tiff\r\nimage/svg+xml\r\nimage/svg+xml\r\napplication/zip\r\napplication/x-rar-compressed\r\napplication/x-msdownload\r\napplication/vnd.ms-cab-compressed\r\naudio/mpeg\r\nvideo/quicktime\r\nvideo/quicktime\r\napplication/pdf\r\nimage/vnd.adobe.photoshop\r\napplication/postscript\r\napplication/postscript\r\napplication/postscript\r\napplication/msword\r\napplication/rtf\r\napplication/vnd.ms-excel\r\napplication/vnd.ms-powerpoint\r\napplication/vnd.oasis.opendocument.text\r\napplication/vnd.oasis.opendocument.spreadsheet', 0),
(6946, 0, 'config', 'config_maintenance', '0', 0),
(4731, 0, 'nganluong', 'nganluong_receiver', 'huatronghieu@gmail.com', 0),
(2416, 0, 'pavsliderlayer', 'pavsliderlayer_module', 'a:1:{i:0;a:5:{s:8:"group_id";s:1:"1";s:9:"layout_id";s:1:"1";s:8:"position";s:11:"content_top";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}}', 1),
(6529, 0, 'featured2', 'product', 's', 0),
(6530, 0, 'featured2', 'featured2_product', '58,79,81,80,102,104,106,105', 0),
(6531, 0, 'featured2', 'featured2_module', 'a:1:{i:0;a:7:{s:5:"limit";s:1:"9";s:11:"image_width";s:3:"270";s:12:"image_height";s:3:"203";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}}', 1),
(5454, 0, 'simple', 'id', '', 0),
(5455, 0, 'simple', 'label_en', '', 0),
(5456, 0, 'simple', 'object_type', 'order', 0),
(5457, 0, 'simple', 'object_field', '', 0),
(5458, 0, 'simple', 'type', 'text', 0),
(5459, 0, 'simple', 'place', 'customer', 0),
(5460, 0, 'simple', 'date_min', '', 0),
(5461, 0, 'simple', 'date_start', '', 0),
(5462, 0, 'simple', 'date_max', '', 0),
(5463, 0, 'simple', 'date_end', '', 0),
(5464, 0, 'simple', 'values_en', '', 0),
(5465, 0, 'simple', 'validation_type', '0', 0),
(5466, 0, 'simple', 'validation_min', '', 0),
(5467, 0, 'simple', 'validation_max', '', 0),
(5468, 0, 'simple', 'validation_regexp', '', 0),
(5469, 0, 'simple', 'validation_error_en', '', 0),
(5470, 0, 'simple', 'save_to', '', 0),
(5471, 0, 'simple', 'mask', '', 0),
(5472, 0, 'simple', 'placeholder_en', '', 0),
(5473, 0, 'simple', 'simple_registration_view_email_confirm', '0', 0),
(5474, 0, 'simple', 'simple_registration_generate_password', '0', 0),
(5475, 0, 'simple', 'simple_registration_password_confirm', '0', 0),
(5476, 0, 'simple', 'simple_registration_password_length_min', '4', 0),
(5477, 0, 'simple', 'simple_registration_password_length_max', '20', 0),
(5478, 0, 'simple', 'simple_registration_agreement_id', '0', 0),
(5479, 0, 'simple', 'simple_registration_agreement_checkbox', '1', 0),
(5480, 0, 'simple', 'simple_registration_agreement_checkbox_init', '0', 0),
(5481, 0, 'simple', 'simple_registration_captcha', '0', 0),
(5482, 0, 'simple', 'simple_registration_subscribe', '2', 0),
(5483, 0, 'simple', 'simple_registration_subscribe_init', '1', 0),
(5484, 0, 'simple', 'simple_registration_view_customer_type', '0', 0),
(5485, 0, 'simple', 'simple_set_registration', 'a:1:{s:5:"group";a:2:{i:1;s:82:"header_main,main_email,main_firstname,main_telephone,header_address,main_address_1";i:2;s:82:"header_main,main_email,main_firstname,main_telephone,header_address,main_address_1";}}', 1),
(5486, 0, 'simple', 'simple_account_view_customer_type', '0', 0),
(5487, 0, 'simple', 'simple_set_account_info', 'a:1:{s:5:"group";a:2:{i:1;s:67:"header_main,main_email,main_firstname,main_telephone,header_address";i:2;s:67:"header_main,main_email,main_firstname,main_telephone,header_address";}}', 1),
(5498, 0, 'simple', 'simple_customer_two_column', '1', 0),
(5497, 0, 'simple', 'simple_googleapi_key', '', 0),
(5496, 0, 'simple', 'simple_googleapi', '0', 0),
(5495, 0, 'simple', 'simple_shipping_titles', 'a:2:{s:4:"flat";a:3:{s:4:"show";s:1:"0";s:5:"title";a:1:{s:2:"en";s:9:"Flat Rate";}s:11:"description";a:1:{s:2:"en";s:0:"";}}s:4:"free";a:3:{s:4:"show";s:1:"0";s:5:"title";a:1:{s:2:"en";s:13:"Free Shipping";}s:11:"description";a:1:{s:2:"en";s:0:"";}}}', 1),
(5488, 0, 'simple', 'simple_set_account_address', 'a:1:{s:5:"group";a:2:{i:1;s:29:"main_firstname,main_address_1";i:2;s:29:"main_firstname,main_address_1";}}', 1),
(5489, 0, 'simple', 'simple_joomla_path', '', 0),
(5490, 0, 'simple', 'simple_joomla_route', '', 0),
(5491, 0, 'simple', 'simple_header_tag', 'h3', 0),
(5492, 0, 'simple', 'simple_headers', 'a:2:{s:11:"header_main";a:3:{s:2:"id";s:11:"header_main";s:5:"label";a:1:{s:2:"en";s:10:"Thông tin";}s:5:"place";s:8:"customer";}s:14:"header_address";a:3:{s:2:"id";s:14:"header_address";s:5:"label";a:1:{s:2:"en";s:12:"Địa chỉ";}s:5:"place";s:8:"customer";}}', 1),
(5493, 0, 'simple', 'restore', '0', 0),
(5494, 0, 'simple', 'simple_payment_titles', 'a:4:{s:3:"cod";a:3:{s:4:"show";s:1:"0";s:5:"title";a:1:{s:2:"en";s:16:"Cash On Delivery";}s:11:"description";a:1:{s:2:"en";s:0:"";}}s:13:"free_checkout";a:3:{s:4:"show";s:1:"0";s:5:"title";a:1:{s:2:"en";s:13:"Free Checkout";}s:11:"description";a:1:{s:2:"en";s:0:"";}}s:9:"nganluong";a:3:{s:4:"show";s:1:"0";s:5:"title";a:1:{s:2:"en";s:56:"Thanh toán trực tuyến an toàn qua NgânLượng.vn";}s:11:"description";a:1:{s:2:"en";s:0:"";}}s:11:"pp_standard";a:3:{s:4:"show";s:1:"0";s:5:"title";a:1:{s:2:"en";s:15:"PayPal Standard";}s:11:"description";a:1:{s:2:"en";s:0:"";}}}', 1),
(5453, 0, 'simple', 'simple_fields_main', 'a:15:{s:10:"main_email";a:15:{s:2:"id";s:10:"main_email";s:5:"label";a:1:{s:2:"en";s:6:"E-mail";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"3";s:17:"validation_regexp";s:56:"/^[a-z0-9_\\.\\-]{1,20}@[a-z0-9\\.\\-]{1,20}\\.[a-z]{2,4}$/si";s:16:"validation_error";a:1:{s:2:"en";s:35:"E-Mail không đúng định dạng";}s:7:"save_to";s:5:"email";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:14:"main_firstname";a:17:{s:2:"id";s:14:"main_firstname";s:5:"label";a:1:{s:2:"en";s:17:"Họ  &amp; Tên ";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"2";s:14:"validation_min";s:1:"1";s:14:"validation_max";s:2:"30";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:49:"Họ  &amp; Tên  phải từ 1 tới 30 ký tự";}s:7:"save_to";s:9:"firstname";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:13:"main_lastname";a:17:{s:2:"id";s:13:"main_lastname";s:5:"label";a:1:{s:2:"en";s:9:"Last Name";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"2";s:14:"validation_min";s:1:"1";s:14:"validation_max";s:2:"30";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:46:"Last Name must be between 1 and 32 characters!";}s:7:"save_to";s:8:"lastname";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:14:"main_telephone";a:17:{s:2:"id";s:14:"main_telephone";s:5:"label";a:1:{s:2:"en";s:15:"Điện thoại";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"2";s:14:"validation_min";s:1:"3";s:14:"validation_max";s:2:"32";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:47:"Điện thoại phải từ 3 tới 32 ký tự";}s:7:"save_to";s:9:"telephone";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:8:"main_fax";a:17:{s:2:"id";s:8:"main_fax";s:5:"label";a:1:{s:2:"en";s:3:"Fax";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"0";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:0:"";}s:7:"save_to";s:3:"fax";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:12:"main_company";a:17:{s:2:"id";s:12:"main_company";s:5:"label";a:1:{s:2:"en";s:7:"Company";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"0";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:0:"";}s:7:"save_to";s:7:"company";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:15:"main_company_id";a:17:{s:2:"id";s:15:"main_company_id";s:5:"label";a:1:{s:2:"en";s:10:"Company ID";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"0";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:0:"";}s:7:"save_to";s:10:"company_id";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:11:"main_tax_id";a:17:{s:2:"id";s:11:"main_tax_id";s:5:"label";a:1:{s:2:"en";s:6:"Tax ID";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"0";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:0:"";}s:7:"save_to";s:6:"tax_id";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:14:"main_address_1";a:17:{s:2:"id";s:14:"main_address_1";s:5:"label";a:1:{s:2:"en";s:12:"Địa chỉ";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"2";s:14:"validation_min";s:1:"3";s:14:"validation_max";s:3:"128";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:45:"Địa chỉ phải từ 3 tới 128 ký tự";}s:7:"save_to";s:9:"address_1";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:14:"main_address_2";a:17:{s:2:"id";s:14:"main_address_2";s:5:"label";a:1:{s:2:"en";s:14:"Address Line 2";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"0";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:0:"";}s:7:"save_to";s:9:"address_2";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:13:"main_postcode";a:17:{s:2:"id";s:13:"main_postcode";s:5:"label";a:1:{s:2:"en";s:8:"Postcode";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"2";s:14:"validation_min";s:1:"2";s:14:"validation_max";s:2:"10";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:45:"Postcode must be between 2 and 10 characters!";}s:7:"save_to";s:8:"postcode";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:15:"main_country_id";a:17:{s:2:"id";s:15:"main_country_id";s:5:"label";a:1:{s:2:"en";s:7:"Country";}s:4:"type";s:6:"select";s:6:"values";s:9:"countries";s:4:"init";s:1:"0";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:15:"validation_type";s:1:"4";s:16:"validation_error";a:1:{s:2:"en";s:23:"Please select a country";}s:7:"save_to";s:10:"country_id";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:12:"main_zone_id";a:17:{s:2:"id";s:12:"main_zone_id";s:5:"label";a:1:{s:2:"en";s:4:"Zone";}s:4:"type";s:6:"select";s:6:"values";s:5:"zones";s:4:"init";s:1:"0";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:15:"validation_type";s:1:"4";s:16:"validation_error";a:1:{s:2:"en";s:22:"Please select a region";}s:7:"save_to";s:7:"zone_id";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:9:"main_city";a:17:{s:2:"id";s:9:"main_city";s:5:"label";a:1:{s:2:"en";s:4:"City";}s:4:"type";s:4:"text";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"2";s:14:"validation_min";s:1:"2";s:14:"validation_max";s:3:"128";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:42:"City must be between 2 and 128 characters!";}s:7:"save_to";s:4:"city";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}s:12:"main_comment";a:17:{s:2:"id";s:12:"main_comment";s:5:"label";a:1:{s:2:"en";s:10:"Nội dung";}s:4:"type";s:8:"textarea";s:6:"values";a:1:{s:2:"en";s:0:"";}s:4:"init";s:0:"";s:8:"date_min";s:0:"";s:10:"date_start";s:0:"";s:8:"date_max";s:0:"";s:8:"date_end";s:0:"";s:15:"validation_type";s:1:"0";s:14:"validation_min";s:0:"";s:14:"validation_max";s:0:"";s:17:"validation_regexp";s:0:"";s:16:"validation_error";a:1:{s:2:"en";s:0:"";}s:7:"save_to";s:7:"comment";s:4:"mask";s:0:"";s:11:"placeholder";a:1:{s:2:"en";s:0:"";}}}', 1),
(5451, 0, 'simple', 'simple_group_payment', 'a:2:{i:1;s:0:"";i:2;s:0:"";}', 1),
(5452, 0, 'simple', 'simple_group_shipping', 'a:2:{i:1;s:0:"";i:2;s:0:"";}', 1),
(1530, 0, 'ocarthitcounter', 'ocart_hit_counter_fake_online', '', 0),
(1531, 0, 'ocarthitcounter', 'ocart_hit_counter_template', 'ocart4', 0),
(1532, 0, 'ocarthitcounter', 'ocarthitcounter_module', 'a:4:{i:0;a:5:{s:5:"title";s:7:"Visitor";s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}i:1;a:5:{s:5:"title";s:7:"Visitor";s:9:"layout_id";s:1:"7";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}i:2;a:5:{s:5:"title";s:7:"Visitor";s:9:"layout_id";s:1:"8";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}i:4;a:5:{s:5:"title";s:7:"Visitor";s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}}', 1),
(1528, 0, 'yahoo', 'yahoo_code', 'codocmotminh_bhd', 0),
(1529, 0, 'yahoo', 'yahoo_module', 'a:4:{i:0;a:4:{s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}i:1;a:4:{s:9:"layout_id";s:1:"7";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}i:2;a:4:{s:9:"layout_id";s:1:"8";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}i:4;a:4:{s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(6939, 0, 'config', 'config_fraud_status_id', '7', 0),
(6937, 0, 'config', 'config_fraud_key', '', 0),
(6938, 0, 'config', 'config_fraud_score', '', 0),
(6936, 0, 'config', 'config_fraud_detection', '0', 0),
(6935, 0, 'config', 'config_alert_emails', '', 0),
(3814, 0, 'latest', 'latest_module', 'a:3:{i:0;a:7:{s:5:"limit";s:1:"3";s:11:"image_width";s:3:"138";s:12:"image_height";s:3:"138";s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}i:1;a:7:{s:5:"limit";s:1:"3";s:11:"image_width";s:3:"138";s:12:"image_height";s:3:"138";s:9:"layout_id";s:1:"7";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}i:2;a:7:{s:5:"limit";s:1:"3";s:11:"image_width";s:3:"138";s:12:"image_height";s:3:"138";s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(4205, 0, 'yahoomessenger', 'yahoomessenger_code', 'huatronghieu', 0),
(4206, 0, 'yahoomessenger', 'yahoomessenger_style', '2', 0),
(4207, 0, 'yahoomessenger', 'yahoomessenger_module', 'a:4:{i:0;a:4:{s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}i:1;a:4:{s:9:"layout_id";s:1:"7";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}i:2;a:4:{s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}i:3;a:4:{s:9:"layout_id";s:1:"4";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}}', 1),
(3815, 0, 'bestseller', 'bestseller_module', 'a:3:{i:0;a:7:{s:5:"limit";s:1:"3";s:11:"image_width";s:3:"138";s:12:"image_height";s:3:"138";s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}i:1;a:7:{s:5:"limit";s:1:"3";s:11:"image_width";s:3:"138";s:12:"image_height";s:3:"138";s:9:"layout_id";s:1:"7";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}i:2;a:7:{s:5:"limit";s:1:"3";s:11:"image_width";s:3:"138";s:12:"image_height";s:3:"138";s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}}', 1),
(6961, 0, 'fblikebox', 'fblikebox_module', 'a:1:{i:0;a:4:{s:9:"layout_id";s:1:"1";s:8:"position";s:11:"content_top";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"3";}}', 1),
(6960, 0, 'fblikebox', 'fblikebox_column_connections', '', 0),
(6959, 0, 'fblikebox', 'fblikebox_column_height', '260', 0),
(6958, 0, 'fblikebox', 'fblikebox_column_width', '435', 0),
(6957, 0, 'fblikebox', 'fblikebox_content_connections', '', 0),
(6956, 0, 'fblikebox', 'fblikebox_content_height', '260', 0),
(6955, 0, 'fblikebox', 'fblikebox_content_width', '435', 0),
(6954, 0, 'fblikebox', 'fblikebox_code', 'https://www.facebook.com/trumhangmy', 0),
(6934, 0, 'config', 'config_account_mail', '0', 0),
(6933, 0, 'config', 'config_alert_mail', '0', 0),
(6932, 0, 'config', 'config_smtp_timeout', '5', 0),
(6931, 0, 'config', 'config_smtp_port', '25', 0),
(6930, 0, 'config', 'config_smtp_password', '', 0),
(6929, 0, 'config', 'config_smtp_username', '', 0),
(6928, 0, 'config', 'config_smtp_host', '', 0),
(6927, 0, 'config', 'config_mail_parameter', '', 0),
(6926, 0, 'config', 'config_mail_protocol', 'mail', 0),
(6925, 0, 'config', 'config_ftp_status', '0', 0),
(6924, 0, 'config', 'config_ftp_root', '', 0),
(6923, 0, 'config', 'config_ftp_password', '', 0),
(6922, 0, 'config', 'config_ftp_username', '', 0),
(6921, 0, 'config', 'config_ftp_port', '21', 0),
(6920, 0, 'config', 'config_ftp_host', 'localhost', 0),
(6919, 0, 'config', 'config_image_cart_height', '47', 0),
(6918, 0, 'config', 'config_image_cart_width', '47', 0),
(6917, 0, 'config', 'config_image_wishlist_height', '47', 0),
(6916, 0, 'config', 'config_image_wishlist_width', '47', 0),
(6915, 0, 'config', 'config_image_compare_height', '90', 0),
(6912, 0, 'config', 'config_image_related_width', '270', 0),
(6914, 0, 'config', 'config_image_compare_width', '90', 0),
(5448, 0, 'simple', 'shipping_code_for_shipping_1', '', 0),
(5449, 0, 'simple', 'shipping_code_for_shipping_2', '', 0),
(5450, 0, 'simple', 'simple_links', 'a:4:{s:3:"cod";s:0:"";s:13:"free_checkout";s:0:"";s:9:"nganluong";s:0:"";s:11:"pp_standard";s:0:"";}', 1),
(5447, 0, 'simple', 'shipping_code_for_customer_2', '', 0),
(5446, 0, 'simple', 'shipping_code_for_customer_1', '', 0),
(5441, 0, 'simple', 'simple_show_shipping_address', '1', 0),
(5442, 0, 'simple', 'simple_show_shipping_address_same_init', '1', 0),
(5443, 0, 'simple', 'simple_show_shipping_address_same_show', '1', 0),
(5444, 0, 'simple', 'simple_shipping_view_address_select', '1', 0),
(5445, 0, 'simple', 'simple_set_checkout_address', 'a:3:{s:5:"group";a:2:{i:1;s:29:"main_firstname,main_address_1";i:2;s:29:"main_firstname,main_address_1";}s:8:"shipping";a:2:{i:1;a:2:{s:4:"flat";s:0:"";s:4:"free";s:0:"";}i:2;a:2:{s:4:"flat";s:0:"";s:4:"free";s:0:"";}}s:7:"payment";a:2:{i:1;a:4:{s:3:"cod";s:0:"";s:13:"free_checkout";s:0:"";s:9:"nganluong";s:0:"";s:11:"pp_standard";s:0:"";}i:2;a:4:{s:3:"cod";s:0:"";s:13:"free_checkout";s:0:"";s:9:"nganluong";s:0:"";s:11:"pp_standard";s:0:"";}}}', 1),
(6913, 0, 'config', 'config_image_related_height', '203', 0),
(6911, 0, 'config', 'config_image_additional_height', '74', 0),
(6910, 0, 'config', 'config_image_additional_width', '74', 0),
(6909, 0, 'config', 'config_image_product_height', '165', 0),
(6908, 0, 'config', 'config_image_product_width', '220', 0),
(6906, 0, 'config', 'config_image_popup_width', '800', 0),
(6907, 0, 'config', 'config_image_popup_height', '600', 0),
(6905, 0, 'config', 'config_image_thumb_height', '225', 0),
(6903, 0, 'config', 'config_image_category_height', '150', 0),
(6904, 0, 'config', 'config_image_thumb_width', '300', 0),
(6902, 0, 'config', 'config_image_category_width', '150', 0),
(6901, 0, 'config', 'config_icon', 'data/logo/Logo-vuong.png', 0),
(6900, 0, 'config', 'config_logo', 'data/logo/lo.png', 0),
(6899, 0, 'config', 'config_return_status_id', '2', 0),
(6898, 0, 'config', 'config_return_id', '0', 0),
(6897, 0, 'config', 'config_commission', '5', 0),
(6895, 0, 'config', 'config_stock_status_id', '5', 0),
(6526, 0, 'featured1', 'product', '', 0),
(6896, 0, 'config', 'config_affiliate_id', '4', 0),
(6894, 0, 'config', 'config_stock_checkout', '0', 0),
(6893, 0, 'config', 'config_stock_warning', '0', 0),
(6892, 0, 'config', 'config_stock_display', '0', 0),
(6891, 0, 'config', 'config_complete_status_id', '5', 0),
(5735, 0, 'news_category', 'news_category_module', 'a:3:{i:0;a:4:{s:9:"layout_id";s:1:"4";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}i:1;a:4:{s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}i:2;a:4:{s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}}', 1),
(6487, 0, 'cod', 'cod_geo_zone_id', '5', 0),
(6534, 0, 'free', 'free_status', '1', 0),
(5438, 0, 'simple', 'simple_customer_view_address_select', '0', 0),
(5439, 0, 'simple', 'simple_customer_view_customer_type', '0', 0),
(5440, 0, 'simple', 'simple_set_checkout_customer', 'a:3:{s:5:"group";a:2:{i:1;s:107:"header_main,main_email,main_firstname,main_telephone,split_split,header_address,main_address_1,main_comment";i:2;s:107:"header_main,main_email,main_firstname,main_telephone,split_split,header_address,main_address_1,main_comment";}s:8:"shipping";a:2:{i:1;a:2:{s:4:"flat";s:0:"";s:4:"free";s:0:"";}i:2;a:2:{s:4:"flat";s:0:"";s:4:"free";s:0:"";}}s:7:"payment";a:2:{i:1;a:4:{s:3:"cod";s:0:"";s:13:"free_checkout";s:0:"";s:9:"nganluong";s:0:"";s:11:"pp_standard";s:0:"";}i:2;a:4:{s:3:"cod";s:0:"";s:13:"free_checkout";s:0:"";s:9:"nganluong";s:0:"";s:11:"pp_standard";s:0:"";}}}', 1),
(5437, 0, 'simple', 'simple_customer_view_login', '0', 0),
(5435, 0, 'simple', 'simple_customer_action_subscribe', '2', 0),
(5436, 0, 'simple', 'simple_customer_view_customer_subscribe_init', '1', 0),
(5433, 0, 'simple', 'simple_customer_view_password_length_min', '4', 0),
(5434, 0, 'simple', 'simple_customer_view_password_length_max', '20', 0),
(5432, 0, 'simple', 'simple_customer_view_password_confirm', '0', 0),
(5431, 0, 'simple', 'simple_customer_generate_password', '0', 0),
(5430, 0, 'simple', 'simple_customer_view_customer_register_init', '0', 0),
(5428, 0, 'simple', 'simple_customer_view_email', '0', 0),
(5429, 0, 'simple', 'simple_customer_view_email_confirm', '0', 0),
(5426, 0, 'simple', 'simple_show_will_be_registered', '0', 0),
(5427, 0, 'simple', 'simple_customer_action_register', '0', 0),
(5425, 0, 'simple', 'simple_customer_hide_if_logged', '1', 0),
(5423, 0, 'simple', 'simple_payment_view_autoselect_first', '1', 0),
(5424, 0, 'simple', 'simple_payment_view_address_full', 'a:4:{s:3:"cod";s:1:"0";s:13:"free_checkout";s:1:"0";s:9:"nganluong";s:1:"0";s:11:"pp_standard";s:1:"0";}', 1),
(5421, 0, 'simple', 'simple_payment_methods_hide', '0', 0),
(5422, 0, 'simple', 'simple_payment_view_address_empty', '1', 0),
(5420, 0, 'simple', 'simple_shipping_view_address_full', 'a:2:{s:4:"flat";s:1:"0";s:4:"free";s:1:"0";}', 1),
(5419, 0, 'simple', 'simple_shipping_view_autoselect_first', '0', 0),
(5418, 0, 'simple', 'simple_shipping_view_address_empty', '0', 0),
(5417, 0, 'simple', 'simple_shipping_view_title', '0', 0),
(5414, 0, 'simple', 'simple_common_view_help_id', '0', 0),
(5415, 0, 'simple', 'simple_common_view_help_text', '0', 0),
(5416, 0, 'simple', 'simple_shipping_methods_hide', '0', 0),
(5413, 0, 'simple', 'simple_common_view_agreement_checkbox_init', '0', 0),
(5412, 0, 'simple', 'simple_common_view_agreement_checkbox', '0', 0),
(5411, 0, 'simple', 'simple_common_view_agreement_text', '0', 0),
(5410, 0, 'simple', 'simple_common_view_agreement_id', '0', 0),
(5409, 0, 'simple', 'simple_max_weight', '', 0),
(5408, 0, 'simple', 'simple_min_weight', '', 0),
(5407, 0, 'simple', 'simple_max_quantity', '', 0),
(5406, 0, 'simple', 'simple_min_quantity', '', 0),
(4342, 0, 'category', 'category_module', 'a:4:{i:0;a:4:{s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}i:1;a:4:{s:9:"layout_id";s:1:"2";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}i:2;a:4:{s:9:"layout_id";s:1:"7";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"1";}i:3;a:4:{s:9:"layout_id";s:1:"4";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(6469, 0, 'banner', 'banner_module', 'a:3:{i:0;a:7:{s:9:"banner_id";s:2:"12";s:5:"width";s:3:"260";s:6:"height";s:3:"240";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}i:1;a:7:{s:9:"banner_id";s:2:"11";s:5:"width";s:3:"435";s:6:"height";s:3:"260";s:9:"layout_id";s:1:"1";s:8:"position";s:11:"content_top";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"4";}i:2;a:7:{s:9:"banner_id";s:2:"13";s:5:"width";s:3:"260";s:6:"height";s:3:"240";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"5";}}', 1),
(6465, 0, 'newslatest', 'newslatest_module', 'a:1:{i:0;a:10:{s:5:"limit";s:1:"6";s:16:"limitdescription";s:3:"150";s:11:"image_width";s:2:"80";s:12:"image_height";s:2:"80";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:11:"description";s:1:"0";s:11:"imagestatus";s:1:"0";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"7";}}', 1),
(4226, 0, 'voucher', 'voucher_status', '1', 0),
(4227, 0, 'voucher', 'voucher_sort_order', '4', 0),
(6533, 0, 'free', 'free_geo_zone_id', '0', 0),
(6486, 0, 'cod', 'cod_order_status_id', '2', 0),
(6889, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(6890, 0, 'config', 'config_order_status_id', '1', 0),
(6888, 0, 'config', 'config_order_edit', '100', 0),
(6470, 0, 'special', 'special_module', 'a:1:{i:0;a:7:{s:5:"limit";s:1:"5";s:11:"image_width";s:2:"80";s:12:"image_height";s:2:"80";s:9:"layout_id";s:1:"3";s:8:"position";s:11:"column_left";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(5403, 0, 'simple', 'simple_show_weight', '0', 0),
(5404, 0, 'simple', 'simple_min_amount', '', 0),
(5405, 0, 'simple', 'simple_max_amount', '', 0),
(5402, 0, 'simple', 'simple_use_total', '0', 0),
(5401, 0, 'simple', 'simple_comment_target', '', 0),
(5400, 0, 'simple', 'simple_customer_group_id_after_reg', '1', 0),
(5399, 0, 'simple', 'simple_type_of_selection_of_group', 'radio', 0),
(5398, 0, 'simple', 'simple_geoip_mode', '1', 0),
(5397, 0, 'simple', 'simple_checkout_asap_for_logged', '1', 0),
(5396, 0, 'simple', 'simple_checkout_asap_for_not_logged', '0', 0),
(5388, 0, 'simple', 'simple_steps', '0', 0),
(5389, 0, 'simple', 'simple_steps_summary', '0', 0),
(5390, 0, 'simple', 'simple_debug', '0', 0),
(5391, 0, 'simple', 'simple_disable_guest_checkout', '0', 0),
(5392, 0, 'simple', 'simple_empty_email', '', 0),
(5393, 0, 'simple', 'simple_set_for_reload', 'main_email,main_firstname,main_telephone,split_split,main_address_1,main_comment', 0),
(5394, 0, 'simple', 'simple_use_cookies', '0', 0),
(5395, 0, 'simple', 'simple_show_back', '0', 0),
(5387, 0, 'simple', 'simple_common_template', '{cart}{help}{customer}{shipping}{payment}{agreement}{payment_form}', 0),
(5736, 0, 'google_sitemap', 'google_sitemap_status', '1', 0),
(6887, 0, 'config', 'config_checkout_id', '5', 0),
(6885, 0, 'config', 'config_cart_weight', '1', 0),
(6886, 0, 'config', 'config_guest_checkout', '1', 0),
(6883, 0, 'config', 'config_customer_price', '0', 0),
(6884, 0, 'config', 'config_account_id', '3', 0),
(6881, 0, 'config', 'config_customer_group_id', '1', 0),
(6532, 0, 'free', 'free_total', '1000000', 0),
(6485, 0, 'cod', 'cod_total', '0.01', 0),
(6489, 0, 'cod', 'cod_sort_order', '5', 0),
(6527, 0, 'featured1', 'featured1_product', '94,99,93,101,92,102,104,106,105', 0),
(6528, 0, 'featured1', 'featured1_module', 'a:1:{i:0;a:7:{s:5:"limit";s:1:"9";s:11:"image_width";s:3:"260";s:12:"image_height";s:3:"200";s:9:"layout_id";s:1:"1";s:8:"position";s:14:"content_bottom";s:6:"status";s:1:"1";s:10:"sort_order";s:1:"2";}}', 1),
(6535, 0, 'free', 'free_sort_order', '2', 0),
(6882, 0, 'config', 'config_customer_group_display', 'a:1:{i:0;s:1:"1";}', 1),
(6880, 0, 'config', 'config_customer_online', '0', 0),
(6879, 0, 'config', 'config_tax_customer', 'shipping', 0),
(6878, 0, 'config', 'config_tax_default', 'shipping', 0),
(6877, 0, 'config', 'config_vat', '0', 0),
(6876, 0, 'config', 'config_tax', '1', 0),
(6875, 0, 'config', 'config_voucher_max', '1000', 0),
(6874, 0, 'config', 'config_voucher_min', '1', 0),
(6873, 0, 'config', 'config_download', '1', 0),
(6872, 0, 'config', 'config_review_status', '1', 0),
(6871, 0, 'config', 'config_product_count', '1', 0),
(6870, 0, 'config', 'config_admin_limit', '20', 0),
(6869, 0, 'config', 'config_catalog_limit', '15', 0),
(6868, 0, 'config', 'config_weight_class_id', '1', 0),
(6867, 0, 'config', 'config_length_class_id', '1', 0),
(6866, 0, 'config', 'config_currency_auto', '1', 0),
(6865, 0, 'config', 'config_currency', 'GBP', 0),
(6860, 0, 'config', 'config_layout_id', '4', 0),
(6861, 0, 'config', 'config_country_id', '230', 0),
(6862, 0, 'config', 'config_zone_id', '3780', 0),
(6863, 0, 'config', 'config_language', 'en', 0),
(6864, 0, 'config', 'config_admin_language', 'en', 0),
(6859, 0, 'config', 'config_meta_description', 'Shop chuyên cung cấp sỉ và lẻ các mặt hàng mỹ phẩm xách tay,hàng hóa xách tay từ Mỹ,đảm bảo chất lượng,có hóa đơn mua hàng từ Mỹ', 0),
(6857, 0, 'config', 'config_fax', '', 0),
(6858, 0, 'config', 'config_title', 'Shop mỹ phẩm xách tay từ Mỹ ở HCM | Trùm Hàng Mỹ', 0),
(6856, 0, 'config', 'config_telephone', '093.8088.893 - 098.3035.311', 0),
(6855, 0, 'config', 'config_email', 'trumhangmy@gmail.com', 0),
(6854, 0, 'config', 'config_address', '102/121 - LÊ VĂN THỌ - P11 - GÒ VẤP - HCM\r\n', 0),
(6852, 0, 'config', 'config_name', 'TRÙM HÀNG MỸ', 0),
(6853, 0, 'config', 'config_owner', 'trumhangmy.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `simple_custom_data`
--

CREATE TABLE IF NOT EXISTS `simple_custom_data` (
  `object_type` tinyint(4) NOT NULL,
  `object_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `simple_custom_data`
--

INSERT INTO `simple_custom_data` (`object_type`, `object_id`, `customer_id`, `data`) VALUES
(2, 1, 1, 'a:0:{}'),
(3, 1, 1, 'a:0:{}'),
(1, 1, 1, 'a:0:{}'),
(1, 2, 0, 'a:0:{}'),
(1, 3, 0, 'a:0:{}'),
(2, 2, 2, 'a:0:{}'),
(3, 2, 2, 'a:0:{}'),
(1, 4, 2, 'a:0:{}'),
(1, 5, 2, 'a:0:{}'),
(1, 6, 2, 'a:0:{}'),
(1, 7, 0, 'a:0:{}'),
(1, 8, 0, 'a:0:{}'),
(1, 9, 0, 'a:0:{}'),
(1, 10, 0, 'a:0:{}'),
(1, 11, 0, 'a:0:{}'),
(2, 3, 3, 'a:0:{}'),
(3, 3, 3, 'a:0:{}'),
(2, 4, 4, 'a:0:{}'),
(3, 4, 4, 'a:0:{}'),
(1, 12, 4, 'a:0:{}'),
(1, 13, 0, 'a:0:{}'),
(1, 14, 0, 'a:0:{}'),
(1, 15, 0, 'a:0:{}'),
(2, 5, 5, 'a:0:{}'),
(3, 5, 5, 'a:0:{}'),
(1, 16, 5, 'a:0:{}'),
(2, 6, 6, 'a:0:{}'),
(3, 6, 6, 'a:0:{}'),
(2, 7, 7, 'a:0:{}'),
(3, 7, 7, 'a:0:{}'),
(1, 17, 7, 'a:0:{}'),
(1, 18, 7, 'a:0:{}'),
(2, 8, 8, 'a:0:{}'),
(3, 8, 8, 'a:0:{}'),
(1, 19, 8, 'a:0:{}'),
(1, 20, 8, 'a:0:{}'),
(1, 21, 8, 'a:0:{}'),
(1, 22, 0, 'a:0:{}'),
(1, 23, 0, 'a:0:{}'),
(2, 9, 9, 'a:0:{}'),
(3, 9, 9, 'a:0:{}'),
(1, 24, 9, 'a:0:{}'),
(1, 25, 9, 'a:0:{}'),
(1, 26, 9, 'a:0:{}'),
(1, 27, 9, 'a:0:{}'),
(1, 28, 9, 'a:0:{}'),
(1, 29, 0, 'a:0:{}'),
(1, 30, 0, 'a:0:{}'),
(1, 31, 0, 'a:0:{}'),
(2, 10, 10, 'a:0:{}'),
(3, 10, 10, 'a:0:{}'),
(1, 32, 0, 'a:0:{}'),
(1, 33, 0, 'a:0:{}'),
(1, 34, 0, 'a:0:{}'),
(1, 35, 0, 'a:0:{}'),
(1, 36, 0, 'a:0:{}'),
(1, 37, 0, 'a:0:{}'),
(2, 11, 11, 'a:0:{}'),
(3, 11, 11, 'a:0:{}'),
(2, 12, 12, 'a:0:{}'),
(3, 12, 12, 'a:0:{}'),
(1, 38, 12, 'a:0:{}'),
(2, 13, 13, 'a:0:{}'),
(3, 13, 13, 'a:0:{}'),
(1, 39, 0, 'a:0:{}'),
(2, 14, 14, 'a:0:{}'),
(3, 14, 14, 'a:0:{}'),
(2, 15, 15, 'a:0:{}'),
(3, 15, 15, 'a:0:{}'),
(1, 40, 0, 'a:0:{}'),
(1, 41, 0, 'a:0:{}'),
(1, 42, 0, 'a:0:{}'),
(1, 43, 0, 'a:0:{}'),
(2, 16, 16, 'a:0:{}'),
(3, 16, 16, 'a:0:{}');

-- --------------------------------------------------------

--
-- Table structure for table `stock_status`
--

CREATE TABLE IF NOT EXISTS `stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_status`
--

INSERT INTO `stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'Còn hàng'),
(8, 1, 'Pre-Order'),
(5, 1, 'Hết hàng'),
(6, 1, 'Hàng đang về');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE IF NOT EXISTS `store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tax_class`
--

CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_class`
--

INSERT INTO `tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed Stuff', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate`
--

CREATE TABLE IF NOT EXISTS `tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_rate`
--

INSERT INTO `tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (17.5%)', 17.5000, 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', 2.0000, 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate_to_customer_group`
--

CREATE TABLE IF NOT EXISTS `tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_rate_to_customer_group`
--

INSERT INTO `tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_rule`
--

CREATE TABLE IF NOT EXISTS `tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_rule`
--

INSERT INTO `tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `url_alias`
--

CREATE TABLE IF NOT EXISTS `url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1182 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `url_alias`
--

INSERT INTO `url_alias` (`url_alias_id`, `query`, `keyword`) VALUES
(1179, 'information_id=4', 'huong-dan-mua-hang'),
(1009, 'category_id=60', 'do-lot-pink'),
(854, 'news/all', 'tin-tuc'),
(1037, 'news_id=9', 'huong-dan-su-dung-sua-rua-mat-dung-cach.html'),
(911, 'news_id=10', 'phuong-phap-lam-trang-da-mat-trong-15-ngay.html'),
(811, 'product/special', 'khuyen-mai'),
(794, 'common/home', 'index.php'),
(795, 'information/contact', 'lien-he'),
(796, 'information/sitemap', 'site-map'),
(797, 'account/login', 'tai-khoan'),
(798, 'account/order', 'don-hang'),
(799, 'account/address', 'dia-chi'),
(800, 'account/simpleedit', 'thong-tin-tai-khoan'),
(1008, 'category_id=59', 'victoria-s-secret'),
(1010, 'category_id=25', 'bath-and-body-works'),
(860, 'news_category_id=7', 'lam-dep'),
(1060, 'news_category_id=8', 'duong-da-lam-dep-da'),
(814, 'manufacturer_id=12', 'bath-and-body-works'),
(813, 'manufacturer_id=13', 'pink'),
(978, 'product_id=62', 'xit-toan-than-dark-kiss'),
(915, 'news_id=11', 'lam-trang-da-tu-a-den-z-cung-bot-yen-mach.html'),
(1061, 'news_category_id=10', 'duong-toc-lam-dep-toc'),
(1013, 'information_id=7', 'danh-sach-website-chon-loc'),
(825, 'information_id=3', 'khuyen_mai'),
(892, 'category_id=61', 'body_lotion'),
(893, 'category_id=62', 'body-mist'),
(1066, 'category_id=64', 'kem-chong-nang'),
(922, 'news_category_id=9', 'thoi-trang'),
(1174, 'information_id=5', 'shipping'),
(980, 'product_id=64', 'xit-toan-than-Twilight-Woods'),
(979, 'product_id=63', 'xit-toan-than-cherry-blossom'),
(981, 'product_id=65', 'xit-toan-than-wild-madagascar-vanilla'),
(898, 'product_id=66', 'nuoc-rua-tay-Golden-Autumn-Citrus'),
(894, 'category_id=66', 'gel-rua-tay-kho'),
(896, 'product_id=67', 'nuoc-rua-tay-dancing-waters'),
(897, 'product_id=73', 'nuoc-rua-tay-Fresh-Sparkling-Snow'),
(899, 'product_id=68', 'nuoc-rua-tay-aqua-blossom'),
(901, 'product_id=69', 'nuoc-rua-tay-Island-Margarita'),
(902, 'product_id=70', 'nuoc-rua-tay-Ocean-for-Men'),
(903, 'product_id=71', 'nuoc-rua-tay-Pumpkin-Cupcake'),
(904, 'product_id=72', 'nuoc-rua-tay-Snow-Kissed-Berry'),
(905, 'product_id=74', 'nuoc-rua-tay-Warm-Vanilla-Sugar'),
(906, 'product_id=75', 'nuoc-rua-tay-White-Pear-Fig'),
(974, 'product_id=76', 'nuoc-rua-tay-5-Pack-Classic-Faves'),
(908, 'product_id=77', 'nuoc-rua-tay-5-Pack-Fall-Fresh-Picked'),
(989, 'information_id=8', 'ho-tro-khoi-nghiep-kinh-doanh-online'),
(977, 'product_id=61', 'kem-duong-toan-than-wild-madagascar-vanilla'),
(976, 'product_id=60', 'kem-duong-toan-than-twillight-woods'),
(1017, 'product_id=59', 'kem-duong-toan-than-japanese-cherry-blossom'),
(1176, 'product_id=57', 'kem-duong-toan-than-dark-kiss'),
(941, 'manufacturer_id=14', 'clarins'),
(942, 'manufacturer_id=15', 'Neutrogena'),
(943, 'manufacturer_id=16', 'earth-science'),
(944, 'manufacturer_id=17', 'minerals'),
(945, 'manufacturer_id=18', 'nyx'),
(946, 'manufacturer_id=19', 'loreal '),
(1038, 'news_id=12', 'phuong-phap-cho-da-mat-trang-hong-tu-nhien-voi-mat-na-tra-xanh.html'),
(1163, 'product_id=79', 'Kem-chong-nang-Neutrogena-Pure-Free-Liquid-Sunscreen-SPF-50'),
(1059, 'news_category_id=11', 'dang-dep'),
(1063, 'news_category_id=12', 'my-pham-trang-diem'),
(1064, 'news_category_id=13', 'phau-thuat-tham-my'),
(1072, 'news_category_id=14', 'noi-y'),
(955, 'news_category_id=15', 'phu-kien'),
(1158, 'product_id=80', 'kem-chong-nang-vat-ly-blue-lizard'),
(966, 'manufacturer_id=20', 'blue-lizard'),
(972, 'news_id=13', 'bi-quyet-su-dung-dau-xa-dung-cach-mang-lai-ve-dep-suon-muot-va-ong-a-cho-toc.html'),
(1047, 'product_id=81', 'kem-chong-nang-Neutrogena-Ultra-Sheer-Sunscreen-SPF45'),
(988, 'information_id=9', 'khuyen-mai-khai-truong-giang-sinh-va-nam-moi'),
(1166, 'product_id=58', 'kem-chong-nang-clarins-day-screen-spf-40'),
(1141, 'product_id=82', 'son-loreal-colour-riche-caresse-stick'),
(1070, 'category_id=67', 'son'),
(1067, 'category_id=68', 'mascara'),
(1065, 'category_id=69', 'Eyes-Liner'),
(1069, 'category_id=70', 'phan-phu'),
(1011, 'category_id=71', 'san-pham-giam-can'),
(1143, 'product_id=92', 'kem-chong-nang-Solar-Rx-moisturizer-broad-spectrum-spf-30'),
(1018, 'category_id=63', 'my-pham'),
(1042, 'news_id=14', 'chia-se-kinh-nghiem-giam-can-an-toan-giam-9kg-trong-1-thang-ruoi.html'),
(1062, 'news_category_id=16', 'giam-can'),
(1036, 'product_id=83', 'Toner-Earth-Science-Aloe-Vera-Complexion-Toner-Freshener'),
(1071, 'category_id=73', 'sua-rua-mat-toner'),
(1043, 'product_id=84', 'giam-can-an-toan-iblue'),
(1055, 'product_id=85', 'Mascara-Maybelline-Full-N-Soft-Waterproof'),
(1054, 'product_id=86', 'Mascara-Cover-Girl-Clump-Crusher'),
(1137, 'product_id=87', 'kem-chong-nang-Eucerin-Daily Protection-Moisturizing-Face-Lotion-Broad-Spectrum-SPF-30'),
(1138, 'product_id=91', 'kem-chong-nang-Marie-Veronique-moisturizing-face sceen'),
(1133, 'product_id=93', 'kem-chong-nang-EltaMD-UV-Clear-SPF-46-Broad-Spectrum-sunscreen'),
(1134, 'product_id=94', 'kem-chong-nang-Clinique-Super-City-Block-Oil-Free-Daily-Face-Protector-Broad-Spectrum-SPF-40'),
(1122, 'product_id=107', 'kem-chong-nang-Banana-Boat-Protect-Hydrate-Sunscreen-Lotion-2in1-Broad-Spectrum-SPF-30'),
(1181, 'product_id=99', 'kem-chong-nang-dang-xit-Neutrogena-Ultra-Sheer-Body-Mist-Sunscreen-Broad-Spectrum-SPF-70'),
(1139, 'product_id=98', 'Neutrogena-Healthy-Defense-Daily-Moisturizer-SPF-50-with-Helioplex'),
(1117, 'product_id=101', 'kem-chong-nang-La-Roche-Posay-Anthelios-60-Ultra-Light-Sunscreen-Fluid'),
(1130, 'product_id=102', 'sua-rua-mat-CaraVe-Foaming-Facial-Cleanser-danh-cho-da-thuong-da-dau'),
(1124, 'product_id=104', 'sua-rua-mat-giam-mun-giam-do-Neutrogena-Oil-Free-Acne-Wash-Redness-Soothing-Facial-Cleanser'),
(1123, 'product_id=105', 'sua-rua-mat-tri-mun-Neutrogena-Oil-Free-Acne-Wash'),
(1131, 'product_id=106', 'sua-rua-mat-Neutrogena-Oil-Free-Acne-Wash-Pink-Grapefruit-Foaming-Scrub-198ml'),
(1180, 'news_id=22', 'huong-dan-chon-kem-chong-nang-cho-tung-loai-da.html'),
(1178, 'information_id=10', 'tuyen-ctv-va-dai-ly-ban-hang-my');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '491b28342112f26fc7165c4366e54e994fb1386b', 'b15c434d7', 'Hiếu', 'Hứa', 'huatronghieu@gmail.com', '', '14.161.17.5', 1, '2014-08-26 19:29:34'),
(2, 1, 'mimi', 'aa333e4a1fb5429b4255697dda9e4f0ab51e1ce0', '64941888c', 'Mi', 'Mi', 'duythi@gmail.com', '', '1.53.135.138', 1, '2014-10-31 09:55:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Top Administrator', 'a:2:{s:6:"access";a:169:{i:0;s:14:"amazon/listing";i:1;s:14:"amazon/product";i:2;s:16:"amazonus/listing";i:3;s:16:"amazonus/product";i:4;s:17:"catalog/attribute";i:5;s:23:"catalog/attribute_group";i:6;s:16:"catalog/category";i:7;s:16:"catalog/download";i:8;s:14:"catalog/filter";i:9;s:19:"catalog/information";i:10;s:20:"catalog/manufacturer";i:11;s:12:"catalog/news";i:12;s:21:"catalog/news_category";i:13;s:20:"catalog/news_comment";i:14;s:14:"catalog/option";i:15;s:15:"catalog/product";i:16;s:15:"catalog/profile";i:17;s:14:"catalog/review";i:18;s:18:"common/filemanager";i:19;s:23:"common/filemanager_page";i:20;s:13:"design/banner";i:21;s:19:"design/custom_field";i:22;s:13:"design/layout";i:23;s:12:"ebay/profile";i:24;s:13:"ebay/template";i:25;s:14:"extension/feed";i:26;s:17:"extension/manager";i:27;s:16:"extension/module";i:28;s:17:"extension/openbay";i:29;s:17:"extension/payment";i:30;s:18:"extension/shipping";i:31;s:15:"extension/total";i:32;s:16:"feed/google_base";i:33;s:19:"feed/google_sitemap";i:34;s:20:"localisation/country";i:35;s:21:"localisation/currency";i:36;s:21:"localisation/geo_zone";i:37;s:21:"localisation/language";i:38;s:25:"localisation/length_class";i:39;s:25:"localisation/order_status";i:40;s:26:"localisation/return_action";i:41;s:26:"localisation/return_reason";i:42;s:26:"localisation/return_status";i:43;s:25:"localisation/stock_status";i:44;s:22:"localisation/tax_class";i:45;s:21:"localisation/tax_rate";i:46;s:25:"localisation/weight_class";i:47;s:17:"localisation/zone";i:48;s:14:"module/account";i:49;s:16:"module/affiliate";i:50;s:29:"module/amazon_checkout_layout";i:51;s:13:"module/banner";i:52;s:17:"module/bestseller";i:53;s:15:"module/carousel";i:54;s:16:"module/carousel1";i:55;s:15:"module/category";i:56;s:18:"module/ebaydisplay";i:57;s:16:"module/fblikebox";i:58;s:15:"module/featured";i:59;s:16:"module/featured1";i:60;s:16:"module/featured2";i:61;s:13:"module/filter";i:62;s:18:"module/google_talk";i:63;s:20:"module/googleadsense";i:64;s:18:"module/information";i:65;s:13:"module/latest";i:66;s:20:"module/news_category";i:67;s:19:"module/newsfeatured";i:68;s:17:"module/newslatest";i:69;s:21:"module/pavsliderlayer";i:70;s:12:"module/popup";i:71;s:16:"module/pp_layout";i:72;s:19:"module/promo_banner";i:73;s:13:"module/simple";i:74;s:16:"module/slideshow";i:75;s:14:"module/special";i:76;s:12:"module/store";i:77;s:18:"module/tnt_newscat";i:78;s:14:"module/welcome";i:79;s:21:"module/yahoomessenger";i:80;s:14:"openbay/amazon";i:81;s:16:"openbay/amazonus";i:82;s:15:"openbay/openbay";i:83;s:12:"openbay/play";i:84;s:23:"payment/amazon_checkout";i:85;s:24:"payment/authorizenet_aim";i:86;s:21:"payment/bank_transfer";i:87;s:14:"payment/cheque";i:88;s:11:"payment/cod";i:89;s:21:"payment/free_checkout";i:90;s:22:"payment/klarna_account";i:91;s:22:"payment/klarna_invoice";i:92;s:14:"payment/liqpay";i:93;s:20:"payment/moneybookers";i:94;s:17:"payment/nganluong";i:95;s:14:"payment/nochex";i:96;s:15:"payment/paymate";i:97;s:16:"payment/paypoint";i:98;s:13:"payment/payza";i:99;s:26:"payment/perpetual_payments";i:100;s:18:"payment/pp_express";i:101;s:25:"payment/pp_payflow_iframe";i:102;s:14:"payment/pp_pro";i:103;s:21:"payment/pp_pro_iframe";i:104;s:17:"payment/pp_pro_pf";i:105;s:17:"payment/pp_pro_uk";i:106;s:19:"payment/pp_standard";i:107;s:15:"payment/sagepay";i:108;s:22:"payment/sagepay_direct";i:109;s:18:"payment/sagepay_us";i:110;s:19:"payment/twocheckout";i:111;s:28:"payment/web_payment_software";i:112;s:16:"payment/worldpay";i:113;s:10:"play/order";i:114;s:12:"play/product";i:115;s:27:"report/affiliate_commission";i:116;s:22:"report/customer_credit";i:117;s:22:"report/customer_online";i:118;s:21:"report/customer_order";i:119;s:22:"report/customer_reward";i:120;s:24:"report/product_purchased";i:121;s:21:"report/product_viewed";i:122;s:18:"report/sale_coupon";i:123;s:17:"report/sale_order";i:124;s:18:"report/sale_return";i:125;s:20:"report/sale_shipping";i:126;s:15:"report/sale_tax";i:127;s:14:"sale/affiliate";i:128;s:12:"sale/contact";i:129;s:11:"sale/coupon";i:130;s:13:"sale/customer";i:131;s:20:"sale/customer_ban_ip";i:132;s:19:"sale/customer_group";i:133;s:10:"sale/order";i:134;s:14:"sale/recurring";i:135;s:11:"sale/return";i:136;s:12:"sale/voucher";i:137;s:18:"sale/voucher_theme";i:138;s:15:"setting/setting";i:139;s:13:"setting/store";i:140;s:16:"shipping/auspost";i:141;s:17:"shipping/citylink";i:142;s:14:"shipping/fedex";i:143;s:13:"shipping/flat";i:144;s:13:"shipping/free";i:145;s:13:"shipping/item";i:146;s:23:"shipping/parcelforce_48";i:147;s:15:"shipping/pickup";i:148;s:19:"shipping/royal_mail";i:149;s:12:"shipping/ups";i:150;s:13:"shipping/usps";i:151;s:15:"shipping/weight";i:152;s:11:"tool/backup";i:153;s:14:"tool/error_log";i:154;s:12:"total/coupon";i:155;s:12:"total/credit";i:156;s:14:"total/handling";i:157;s:16:"total/klarna_fee";i:158;s:19:"total/low_order_fee";i:159;s:12:"total/reward";i:160;s:14:"total/shipping";i:161;s:15:"total/sub_total";i:162;s:9:"total/tax";i:163;s:11:"total/total";i:164;s:13:"total/voucher";i:165;s:9:"user/user";i:166;s:20:"user/user_permission";i:167;s:20:"module/googleadsense";i:168;s:14:"module/special";}s:6:"modify";a:169:{i:0;s:14:"amazon/listing";i:1;s:14:"amazon/product";i:2;s:16:"amazonus/listing";i:3;s:16:"amazonus/product";i:4;s:17:"catalog/attribute";i:5;s:23:"catalog/attribute_group";i:6;s:16:"catalog/category";i:7;s:16:"catalog/download";i:8;s:14:"catalog/filter";i:9;s:19:"catalog/information";i:10;s:20:"catalog/manufacturer";i:11;s:12:"catalog/news";i:12;s:21:"catalog/news_category";i:13;s:20:"catalog/news_comment";i:14;s:14:"catalog/option";i:15;s:15:"catalog/product";i:16;s:15:"catalog/profile";i:17;s:14:"catalog/review";i:18;s:18:"common/filemanager";i:19;s:23:"common/filemanager_page";i:20;s:13:"design/banner";i:21;s:19:"design/custom_field";i:22;s:13:"design/layout";i:23;s:12:"ebay/profile";i:24;s:13:"ebay/template";i:25;s:14:"extension/feed";i:26;s:17:"extension/manager";i:27;s:16:"extension/module";i:28;s:17:"extension/openbay";i:29;s:17:"extension/payment";i:30;s:18:"extension/shipping";i:31;s:15:"extension/total";i:32;s:16:"feed/google_base";i:33;s:19:"feed/google_sitemap";i:34;s:20:"localisation/country";i:35;s:21:"localisation/currency";i:36;s:21:"localisation/geo_zone";i:37;s:21:"localisation/language";i:38;s:25:"localisation/length_class";i:39;s:25:"localisation/order_status";i:40;s:26:"localisation/return_action";i:41;s:26:"localisation/return_reason";i:42;s:26:"localisation/return_status";i:43;s:25:"localisation/stock_status";i:44;s:22:"localisation/tax_class";i:45;s:21:"localisation/tax_rate";i:46;s:25:"localisation/weight_class";i:47;s:17:"localisation/zone";i:48;s:14:"module/account";i:49;s:16:"module/affiliate";i:50;s:29:"module/amazon_checkout_layout";i:51;s:13:"module/banner";i:52;s:17:"module/bestseller";i:53;s:15:"module/carousel";i:54;s:16:"module/carousel1";i:55;s:15:"module/category";i:56;s:18:"module/ebaydisplay";i:57;s:16:"module/fblikebox";i:58;s:15:"module/featured";i:59;s:16:"module/featured1";i:60;s:16:"module/featured2";i:61;s:13:"module/filter";i:62;s:18:"module/google_talk";i:63;s:20:"module/googleadsense";i:64;s:18:"module/information";i:65;s:13:"module/latest";i:66;s:20:"module/news_category";i:67;s:19:"module/newsfeatured";i:68;s:17:"module/newslatest";i:69;s:21:"module/pavsliderlayer";i:70;s:12:"module/popup";i:71;s:16:"module/pp_layout";i:72;s:19:"module/promo_banner";i:73;s:13:"module/simple";i:74;s:16:"module/slideshow";i:75;s:14:"module/special";i:76;s:12:"module/store";i:77;s:18:"module/tnt_newscat";i:78;s:14:"module/welcome";i:79;s:21:"module/yahoomessenger";i:80;s:14:"openbay/amazon";i:81;s:16:"openbay/amazonus";i:82;s:15:"openbay/openbay";i:83;s:12:"openbay/play";i:84;s:23:"payment/amazon_checkout";i:85;s:24:"payment/authorizenet_aim";i:86;s:21:"payment/bank_transfer";i:87;s:14:"payment/cheque";i:88;s:11:"payment/cod";i:89;s:21:"payment/free_checkout";i:90;s:22:"payment/klarna_account";i:91;s:22:"payment/klarna_invoice";i:92;s:14:"payment/liqpay";i:93;s:20:"payment/moneybookers";i:94;s:17:"payment/nganluong";i:95;s:14:"payment/nochex";i:96;s:15:"payment/paymate";i:97;s:16:"payment/paypoint";i:98;s:13:"payment/payza";i:99;s:26:"payment/perpetual_payments";i:100;s:18:"payment/pp_express";i:101;s:25:"payment/pp_payflow_iframe";i:102;s:14:"payment/pp_pro";i:103;s:21:"payment/pp_pro_iframe";i:104;s:17:"payment/pp_pro_pf";i:105;s:17:"payment/pp_pro_uk";i:106;s:19:"payment/pp_standard";i:107;s:15:"payment/sagepay";i:108;s:22:"payment/sagepay_direct";i:109;s:18:"payment/sagepay_us";i:110;s:19:"payment/twocheckout";i:111;s:28:"payment/web_payment_software";i:112;s:16:"payment/worldpay";i:113;s:10:"play/order";i:114;s:12:"play/product";i:115;s:27:"report/affiliate_commission";i:116;s:22:"report/customer_credit";i:117;s:22:"report/customer_online";i:118;s:21:"report/customer_order";i:119;s:22:"report/customer_reward";i:120;s:24:"report/product_purchased";i:121;s:21:"report/product_viewed";i:122;s:18:"report/sale_coupon";i:123;s:17:"report/sale_order";i:124;s:18:"report/sale_return";i:125;s:20:"report/sale_shipping";i:126;s:15:"report/sale_tax";i:127;s:14:"sale/affiliate";i:128;s:12:"sale/contact";i:129;s:11:"sale/coupon";i:130;s:13:"sale/customer";i:131;s:20:"sale/customer_ban_ip";i:132;s:19:"sale/customer_group";i:133;s:10:"sale/order";i:134;s:14:"sale/recurring";i:135;s:11:"sale/return";i:136;s:12:"sale/voucher";i:137;s:18:"sale/voucher_theme";i:138;s:15:"setting/setting";i:139;s:13:"setting/store";i:140;s:16:"shipping/auspost";i:141;s:17:"shipping/citylink";i:142;s:14:"shipping/fedex";i:143;s:13:"shipping/flat";i:144;s:13:"shipping/free";i:145;s:13:"shipping/item";i:146;s:23:"shipping/parcelforce_48";i:147;s:15:"shipping/pickup";i:148;s:19:"shipping/royal_mail";i:149;s:12:"shipping/ups";i:150;s:13:"shipping/usps";i:151;s:15:"shipping/weight";i:152;s:11:"tool/backup";i:153;s:14:"tool/error_log";i:154;s:12:"total/coupon";i:155;s:12:"total/credit";i:156;s:14:"total/handling";i:157;s:16:"total/klarna_fee";i:158;s:19:"total/low_order_fee";i:159;s:12:"total/reward";i:160;s:14:"total/shipping";i:161;s:15:"total/sub_total";i:162;s:9:"total/tax";i:163;s:11:"total/total";i:164;s:13:"total/voucher";i:165;s:9:"user/user";i:166;s:20:"user/user_permission";i:167;s:20:"module/googleadsense";i:168;s:14:"module/special";}}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_history`
--

CREATE TABLE IF NOT EXISTS `voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_theme`
--

CREATE TABLE IF NOT EXISTS `voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher_theme`
--

INSERT INTO `voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'data/demo/canon_eos_5d_2.jpg'),
(7, 'data/demo/gift-voucher-birthday.jpg'),
(6, 'data/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_theme_description`
--

CREATE TABLE IF NOT EXISTS `voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher_theme_description`
--

INSERT INTO `voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `weight_class`
--

CREATE TABLE IF NOT EXISTS `weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `weight_class`
--

INSERT INTO `weight_class` (`weight_class_id`, `value`) VALUES
(1, 1.00000000),
(2, 1000.00000000),
(5, 2.20460000),
(6, 35.27400000);

-- --------------------------------------------------------

--
-- Table structure for table `weight_class_description`
--

CREATE TABLE IF NOT EXISTS `weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `weight_class_description`
--

INSERT INTO `weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE IF NOT EXISTS `zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=4034 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M''Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu''a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark''unik''', 'GEG', 1),
(184, 11, 'Kotayk''', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik''', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots'' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore''s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel''skaya (Homyel'')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George''s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith''s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M''Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O''Hi', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chi', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovar-Bilogora', 'BB', 1),
(848, 53, 'City of Zagreb', 'CZ', 1),
(849, 53, 'Dubrovnik-Neretva', 'DN', 1),
(850, 53, 'Istra', 'IS', 1),
(851, 53, 'Karlovac', 'KA', 1),
(852, 53, 'Koprivnica-Krizevci', 'KK', 1),
(853, 53, 'Krapina-Zagorje', 'KZ', 1),
(854, 53, 'Lika-Senj', 'LS', 1),
(855, 53, 'Medimurje', 'ME', 1),
(856, 53, 'Osijek-Baranja', 'OB', 1),
(857, 53, 'Pozega-Slavonia', 'PS', 1),
(858, 53, 'Primorje-Gorski Kotar', 'PG', 1),
(859, 53, 'Sibenik', 'SI', 1),
(860, 53, 'Sisak-Moslavina', 'SM', 1),
(861, 53, 'Slavonski Brod-Posavina', 'SB', 1),
(862, 53, 'Split-Dalmatia', 'SD', 1),
(863, 53, 'Varazdin', 'VA', 1),
(864, 53, 'Virovitica-Podravina', 'VP', 1),
(865, 53, 'Vukovar-Srijem', 'VS', 1),
(866, 53, 'Zadar-Knin', 'ZK', 1),
(867, 53, 'Zagreb', 'ZA', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '''Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma''iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa''id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina''', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina''', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan Laani', 'AL', 1),
(1086, 72, 'Etela-Suomen Laani', 'ES', 1),
(1087, 72, 'Ita-Suomen Laani', 'IS', 1),
(1088, 72, 'Lansi-Suomen Laani', 'LS', 1),
(1089, 72, 'Lapin Lanani', 'LA', 1),
(1090, 72, 'Oulun Laani', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d''Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand''Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1428, 97, 'Bacs-Kiskun', 'BK', 1),
(1429, 97, 'Baranya', 'BA', 1),
(1430, 97, 'Bekes', 'BE', 1),
(1431, 97, 'Bekescsaba', 'BS', 1),
(1432, 97, 'Borsod-Abauj-Zemplen', 'BZ', 1),
(1433, 97, 'Budapest', 'BU', 1),
(1434, 97, 'Csongrad', 'CS', 1),
(1435, 97, 'Debrecen', 'DE', 1),
(1436, 97, 'Dunaujvaros', 'DU', 1),
(1437, 97, 'Eger', 'EG', 1),
(1438, 97, 'Fejer', 'FE', 1),
(1439, 97, 'Gyor', 'GY', 1),
(1440, 97, 'Gyor-Moson-Sopron', 'GM', 1),
(1441, 97, 'Hajdu-Bihar', 'HB', 1),
(1442, 97, 'Heves', 'HE', 1),
(1443, 97, 'Hodmezovasarhely', 'HO', 1),
(1444, 97, 'Jasz-Nagykun-Szolnok', 'JN', 1),
(1445, 97, 'Kaposvar', 'KA', 1),
(1446, 97, 'Kecskemet', 'KE', 1),
(1447, 97, 'Komarom-Esztergom', 'KO', 1),
(1448, 97, 'Miskolc', 'MI', 1),
(1449, 97, 'Nagykanizsa', 'NA', 1),
(1450, 97, 'Nograd', 'NO', 1),
(1451, 97, 'Nyiregyhaza', 'NY', 1),
(1452, 97, 'Pecs', 'PE', 1),
(1453, 97, 'Pest', 'PS', 1),
(1454, 97, 'Somogy', 'SO', 1),
(1455, 97, 'Sopron', 'SP', 1),
(1456, 97, 'Szabolcs-Szatmar-Bereg', 'SS', 1),
(1457, 97, 'Szeged', 'SZ', 1),
(1458, 97, 'Szekesfehervar', 'SE', 1),
(1459, 97, 'Szolnok', 'SL', 1),
(1460, 97, 'Szombathely', 'SM', 1),
(1461, 97, 'Tatabanya', 'TA', 1),
(1462, 97, 'Tolna', 'TO', 1),
(1463, 97, 'Vas', 'VA', 1),
(1464, 97, 'Veszprem', 'VE', 1),
(1465, 97, 'Zala', 'ZA', 1),
(1466, 97, 'Zalaegerszeg', 'ZZ', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Pondicherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1);
INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta''mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be''er Sheva', 'BS', 1),
(1613, 104, 'Bika''at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '''Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al ''Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa''', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa''', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma''an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P''yongan-bukto', 'PYB', 1),
(1769, 112, 'P''yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P''yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch''ungch''ong-bukto', 'CO', 1),
(1774, 113, 'Ch''ungch''ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch''on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t''ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al ''Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra''', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1819, 117, 'Aizkraukles Rajons', 'AIZ', 1),
(1820, 117, 'Aluksnes Rajons', 'ALU', 1),
(1821, 117, 'Balvu Rajons', 'BAL', 1),
(1822, 117, 'Bauskas Rajons', 'BAU', 1),
(1823, 117, 'Cesu Rajons', 'CES', 1),
(1824, 117, 'Daugavpils Rajons', 'DGR', 1),
(1825, 117, 'Dobeles Rajons', 'DOB', 1),
(1826, 117, 'Gulbenes Rajons', 'GUL', 1),
(1827, 117, 'Jekabpils Rajons', 'JEK', 1),
(1828, 117, 'Jelgavas Rajons', 'JGR', 1),
(1829, 117, 'Kraslavas Rajons', 'KRA', 1),
(1830, 117, 'Kuldigas Rajons', 'KUL', 1),
(1831, 117, 'Liepajas Rajons', 'LPR', 1),
(1832, 117, 'Limbazu Rajons', 'LIM', 1),
(1833, 117, 'Ludzas Rajons', 'LUD', 1),
(1834, 117, 'Madonas Rajons', 'MAD', 1),
(1835, 117, 'Ogres Rajons', 'OGR', 1),
(1836, 117, 'Preilu Rajons', 'PRE', 1),
(1837, 117, 'Rezeknes Rajons', 'RZR', 1),
(1838, 117, 'Rigas Rajons', 'RGR', 1),
(1839, 117, 'Saldus Rajons', 'SAL', 1),
(1840, 117, 'Talsu Rajons', 'TAL', 1),
(1841, 117, 'Tukuma Rajons', 'TUK', 1),
(1842, 117, 'Valkas Rajons', 'VLK', 1),
(1843, 117, 'Valmieras Rajons', 'VLM', 1),
(1844, 117, 'Ventspils Rajons', 'VSR', 1),
(1845, 117, 'Daugavpils', 'DGV', 1),
(1846, 117, 'Jelgava', 'JGV', 1),
(1847, 117, 'Jurmala', 'JUR', 1),
(1848, 117, 'Liepaja', 'LPK', 1),
(1849, 117, 'Rezekne', 'RZK', 1),
(1850, 117, 'Riga', 'RGA', 1),
(1851, 117, 'Ventspils', 'VSL', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale''s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha''s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al ''Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati''', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'JO', 1),
(1972, 129, 'Kedah', 'KE', 1),
(1973, 129, 'Kelantan', 'KL', 1),
(1974, 129, 'Labuan', 'LA', 1),
(1975, 129, 'Melaka', 'ME', 1),
(1976, 129, 'Negeri Sembilan', 'NS', 1),
(1977, 129, 'Pahang', 'PA', 1),
(1978, 129, 'Perak', 'PE', 1),
(1979, 129, 'Perlis', 'PR', 1),
(1980, 129, 'Pulau Pinang', 'PP', 1),
(1981, 129, 'Sabah', 'SA', 1),
(1982, 129, 'Sarawak', 'SR', 1),
(1983, 129, 'Selangor', 'SE', 1),
(1984, 129, 'Terengganu', 'TE', 1),
(1985, 129, 'Wilayah Persekutuan', 'WP', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke''s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa''id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2737, 176, 'Gomo-Altaysk', 'GO', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul''yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust''-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A''ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa''asaleleaga', 'FA', 1),
(2856, 181, 'Gaga''emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa''itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va''a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '''Asir', 'AS', 1),
(2882, 184, 'Ha''il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand'' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand'' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3031, 197, 'Ascension', 'A', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3033, 197, 'Tristan da Cunha', 'T', 1),
(3034, 199, 'A''ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa''iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa''iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1);
INSERT INTO `zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t''ou', 'NT', 1),
(3145, 206, 'P''eng-hu', 'PH', 1),
(3146, 206, 'P''ing-tung', 'PT', 1),
(3147, 206, 'T''ai-chung', 'TG', 1),
(3148, 206, 'T''ai-nan', 'TA', 1),
(3149, 206, 'T''ai-pei county', 'TP', 1),
(3150, 206, 'T''ai-tung', 'TT', 1),
(3151, 206, 'T''ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T''ai-chung', 'TH', 1),
(3157, 206, 'T''ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T''ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha''apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava''u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakir', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkasy', 'CK', 1),
(3481, 220, 'Chernihiv', 'CH', 1),
(3482, 220, 'Chernivtsi', 'CV', 1),
(3483, 220, 'Crimea', 'CR', 1),
(3484, 220, 'Dnipropetrovs''k', 'DN', 1),
(3485, 220, 'Donets''k', 'DO', 1),
(3486, 220, 'Ivano-Frankivs''k', 'IV', 1),
(3487, 220, 'Kharkiv Kherson', 'KL', 1),
(3488, 220, 'Khmel''nyts''kyy', 'KM', 1),
(3489, 220, 'Kirovohrad', 'KR', 1),
(3490, 220, 'Kiev', 'KV', 1),
(3491, 220, 'Kyyiv', 'KY', 1),
(3492, 220, 'Luhans''k', 'LU', 1),
(3493, 220, 'L''viv', 'LV', 1),
(3494, 220, 'Mykolayiv', 'MY', 1),
(3495, 220, 'Odesa', 'OD', 1),
(3496, 220, 'Poltava', 'PO', 1),
(3497, 220, 'Rivne', 'RI', 1),
(3498, 220, 'Sevastopol', 'SE', 1),
(3499, 220, 'Sumy', 'SU', 1),
(3500, 220, 'Ternopil''', 'TE', 1),
(3501, 220, 'Vinnytsya', 'VI', 1),
(3502, 220, 'Volyn''', 'VO', 1),
(3503, 220, 'Zakarpattya', 'ZK', 1),
(3504, 220, 'Zaporizhzhya', 'ZA', 1),
(3505, 220, 'Zhytomyr', 'ZH', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '''Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubayy', 'DU', 1),
(3511, 221, 'R''as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg''ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog''iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma''rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa''dah', 'SD', 1),
(3805, 235, 'San''a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta''izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3972, 222, 'Isle of Man', 'IOM', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4019, 243, 'Kosovo', 'KM', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4033, 230, 'Viet Nam', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `zone_to_geo_zone`
--

CREATE TABLE IF NOT EXISTS `zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zone_to_geo_zone`
--

INSERT INTO `zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(57, 222, 0, 3, '2010-02-26 22:33:24', '0000-00-00 00:00:00'),
(65, 222, 0, 4, '2010-12-15 15:18:13', '0000-00-00 00:00:00'),
(66, 230, 3780, 5, '2015-07-17 15:56:30', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `affiliate`
--
ALTER TABLE `affiliate`
  ADD PRIMARY KEY (`affiliate_id`);

--
-- Indexes for table `affiliate_transaction`
--
ALTER TABLE `affiliate_transaction`
  ADD PRIMARY KEY (`affiliate_transaction_id`);

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `attribute_description`
--
ALTER TABLE `attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `attribute_group`
--
ALTER TABLE `attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `attribute_group_description`
--
ALTER TABLE `attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `banner_image`
--
ALTER TABLE `banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `banner_image_description`
--
ALTER TABLE `banner_image_description`
  ADD PRIMARY KEY (`banner_image_id`,`language_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `category_description`
--
ALTER TABLE `category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `category_filter`
--
ALTER TABLE `category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `category_path`
--
ALTER TABLE `category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `category_to_layout`
--
ALTER TABLE `category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `category_to_store`
--
ALTER TABLE `category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `coupon_category`
--
ALTER TABLE `coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `coupon_history`
--
ALTER TABLE `coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `coupon_product`
--
ALTER TABLE `coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `custom_field`
--
ALTER TABLE `custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `custom_field_description`
--
ALTER TABLE `custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `custom_field_to_customer_group`
--
ALTER TABLE `custom_field_to_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `custom_field_value`
--
ALTER TABLE `custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `custom_field_value_description`
--
ALTER TABLE `custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_ban_ip`
--
ALTER TABLE `customer_ban_ip`
  ADD PRIMARY KEY (`customer_ban_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `customer_field`
--
ALTER TABLE `customer_field`
  ADD PRIMARY KEY (`customer_id`,`custom_field_id`,`custom_field_value_id`);

--
-- Indexes for table `customer_group`
--
ALTER TABLE `customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `customer_group_description`
--
ALTER TABLE `customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `customer_history`
--
ALTER TABLE `customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `customer_ip`
--
ALTER TABLE `customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `customer_online`
--
ALTER TABLE `customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `customer_reward`
--
ALTER TABLE `customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `customer_transaction`
--
ALTER TABLE `customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `download_description`
--
ALTER TABLE `download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `extension`
--
ALTER TABLE `extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `filter_description`
--
ALTER TABLE `filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `filter_group`
--
ALTER TABLE `filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `filter_group_description`
--
ALTER TABLE `filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `geo_zone`
--
ALTER TABLE `geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `information_description`
--
ALTER TABLE `information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `information_to_layout`
--
ALTER TABLE `information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `information_to_store`
--
ALTER TABLE `information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `layout`
--
ALTER TABLE `layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `layout_route`
--
ALTER TABLE `layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `length_class`
--
ALTER TABLE `length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `length_class_description`
--
ALTER TABLE `length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `manufacturer_to_store`
--
ALTER TABLE `manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`news_category_id`);

--
-- Indexes for table `news_category_description`
--
ALTER TABLE `news_category_description`
  ADD PRIMARY KEY (`news_category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `news_category_to_layout`
--
ALTER TABLE `news_category_to_layout`
  ADD PRIMARY KEY (`news_category_id`,`store_id`);

--
-- Indexes for table `news_category_to_store`
--
ALTER TABLE `news_category_to_store`
  ADD PRIMARY KEY (`news_category_id`,`store_id`);

--
-- Indexes for table `news_comment`
--
ALTER TABLE `news_comment`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `product_id` (`news_id`);

--
-- Indexes for table `news_description`
--
ALTER TABLE `news_description`
  ADD PRIMARY KEY (`news_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `news_related`
--
ALTER TABLE `news_related`
  ADD PRIMARY KEY (`news_id`,`related_id`);

--
-- Indexes for table `news_tag`
--
ALTER TABLE `news_tag`
  ADD PRIMARY KEY (`news_tag_id`);

--
-- Indexes for table `news_to_category`
--
ALTER TABLE `news_to_category`
  ADD PRIMARY KEY (`news_id`,`news_category_id`);

--
-- Indexes for table `news_to_layout`
--
ALTER TABLE `news_to_layout`
  ADD PRIMARY KEY (`news_id`,`store_id`);

--
-- Indexes for table `news_to_store`
--
ALTER TABLE `news_to_store`
  ADD PRIMARY KEY (`news_id`,`store_id`);

--
-- Indexes for table `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `option_description`
--
ALTER TABLE `option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `option_value`
--
ALTER TABLE `option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `option_value_description`
--
ALTER TABLE `option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_download`
--
ALTER TABLE `order_download`
  ADD PRIMARY KEY (`order_download_id`);

--
-- Indexes for table `order_field`
--
ALTER TABLE `order_field`
  ADD PRIMARY KEY (`order_id`,`custom_field_id`,`custom_field_value_id`);

--
-- Indexes for table `order_fraud`
--
ALTER TABLE `order_fraud`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_history`
--
ALTER TABLE `order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `order_option`
--
ALTER TABLE `order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`order_product_id`);

--
-- Indexes for table `order_recurring`
--
ALTER TABLE `order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `order_recurring_transaction`
--
ALTER TABLE `order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `order_total`
--
ALTER TABLE `order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `idx_orders_total_orders_id` (`order_id`);

--
-- Indexes for table `order_voucher`
--
ALTER TABLE `order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `pavoslidergroups`
--
ALTER TABLE `pavoslidergroups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pavosliderlayers`
--
ALTER TABLE `pavosliderlayers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_attribute`
--
ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `product_description`
--
ALTER TABLE `product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `product_discount`
--
ALTER TABLE `product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_filter`
--
ALTER TABLE `product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_option`
--
ALTER TABLE `product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `product_option_value`
--
ALTER TABLE `product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `product_profile`
--
ALTER TABLE `product_profile`
  ADD PRIMARY KEY (`product_id`,`profile_id`,`customer_group_id`);

--
-- Indexes for table `product_recurring`
--
ALTER TABLE `product_recurring`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `product_related`
--
ALTER TABLE `product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `product_reward`
--
ALTER TABLE `product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `product_special`
--
ALTER TABLE `product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_to_category`
--
ALTER TABLE `product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`);

--
-- Indexes for table `product_to_download`
--
ALTER TABLE `product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `product_to_layout`
--
ALTER TABLE `product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `product_to_store`
--
ALTER TABLE `product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- Indexes for table `profile_description`
--
ALTER TABLE `profile_description`
  ADD PRIMARY KEY (`profile_id`,`language_id`);

--
-- Indexes for table `return`
--
ALTER TABLE `return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `return_action`
--
ALTER TABLE `return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Indexes for table `return_history`
--
ALTER TABLE `return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `return_reason`
--
ALTER TABLE `return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Indexes for table `return_status`
--
ALTER TABLE `return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `simple_custom_data`
--
ALTER TABLE `simple_custom_data`
  ADD PRIMARY KEY (`object_type`,`object_id`,`customer_id`);

--
-- Indexes for table `stock_status`
--
ALTER TABLE `stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `tax_class`
--
ALTER TABLE `tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `tax_rate`
--
ALTER TABLE `tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `tax_rate_to_customer_group`
--
ALTER TABLE `tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `tax_rule`
--
ALTER TABLE `tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `url_alias`
--
ALTER TABLE `url_alias`
  ADD PRIMARY KEY (`url_alias_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `voucher_history`
--
ALTER TABLE `voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `voucher_theme`
--
ALTER TABLE `voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `voucher_theme_description`
--
ALTER TABLE `voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `weight_class`
--
ALTER TABLE `weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `weight_class_description`
--
ALTER TABLE `weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `zone_to_geo_zone`
--
ALTER TABLE `zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `affiliate`
--
ALTER TABLE `affiliate`
  MODIFY `affiliate_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `affiliate_transaction`
--
ALTER TABLE `affiliate_transaction`
  MODIFY `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attribute`
--
ALTER TABLE `attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `attribute_group`
--
ALTER TABLE `attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `banner_image`
--
ALTER TABLE `banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=378;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=252;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `coupon_history`
--
ALTER TABLE `coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon_product`
--
ALTER TABLE `coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `custom_field`
--
ALTER TABLE `custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `custom_field_value`
--
ALTER TABLE `custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `customer_ban_ip`
--
ALTER TABLE `customer_ban_ip`
  MODIFY `customer_ban_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_group`
--
ALTER TABLE `customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_history`
--
ALTER TABLE `customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_ip`
--
ALTER TABLE `customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `customer_reward`
--
ALTER TABLE `customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_transaction`
--
ALTER TABLE `customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `extension`
--
ALTER TABLE `extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=463;
--
-- AUTO_INCREMENT for table `filter`
--
ALTER TABLE `filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filter_group`
--
ALTER TABLE `filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `geo_zone`
--
ALTER TABLE `geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `layout`
--
ALTER TABLE `layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `layout_route`
--
ALTER TABLE `layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `length_class`
--
ALTER TABLE `length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `length_class_description`
--
ALTER TABLE `length_class_description`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `news_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `news_comment`
--
ALTER TABLE `news_comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_description`
--
ALTER TABLE `news_description`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `news_tag`
--
ALTER TABLE `news_tag`
  MODIFY `news_tag_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `option`
--
ALTER TABLE `option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `option_value`
--
ALTER TABLE `option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `order_download`
--
ALTER TABLE `order_download`
  MODIFY `order_download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_history`
--
ALTER TABLE `order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `order_option`
--
ALTER TABLE `order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `order_recurring`
--
ALTER TABLE `order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_recurring_transaction`
--
ALTER TABLE `order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `order_total`
--
ALTER TABLE `order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `order_voucher`
--
ALTER TABLE `order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pavoslidergroups`
--
ALTER TABLE `pavoslidergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pavosliderlayers`
--
ALTER TABLE `pavosliderlayers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `product_discount`
--
ALTER TABLE `product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=447;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2738;
--
-- AUTO_INCREMENT for table `product_option`
--
ALTER TABLE `product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT for table `product_option_value`
--
ALTER TABLE `product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `product_reward`
--
ALTER TABLE `product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1034;
--
-- AUTO_INCREMENT for table `product_special`
--
ALTER TABLE `product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=532;
--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `profile_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return`
--
ALTER TABLE `return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return_action`
--
ALTER TABLE `return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `return_history`
--
ALTER TABLE `return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return_reason`
--
ALTER TABLE `return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `return_status`
--
ALTER TABLE `return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6962;
--
-- AUTO_INCREMENT for table `stock_status`
--
ALTER TABLE `stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tax_class`
--
ALTER TABLE `tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tax_rate`
--
ALTER TABLE `tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `tax_rule`
--
ALTER TABLE `tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `url_alias`
--
ALTER TABLE `url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1182;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `voucher_history`
--
ALTER TABLE `voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `voucher_theme`
--
ALTER TABLE `voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `weight_class`
--
ALTER TABLE `weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `weight_class_description`
--
ALTER TABLE `weight_class_description`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4034;
--
-- AUTO_INCREMENT for table `zone_to_geo_zone`
--
ALTER TABLE `zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
