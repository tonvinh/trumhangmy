<?php
// HTTP
define('HTTP_SERVER', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');
define('HTTP_IMAGE', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/image/');

// HTTPS
define('HTTPS_SERVER', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');
define('HTTPS_IMAGE', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/image/');

define('PATH_DIR', HTTPS_SERVER . 'catalog/view/theme/default/');
define('PATH_JS', HTTPS_SERVER . 'catalog/view/javascript/');

// DIR
define('DIR_PATH', dirname(__FILE__). '/');
set_include_path(dirname(DIR_PATH));

define('DIR_APPLICATION', DIR_PATH . 'catalog/');
define('DIR_LANGUAGE', DIR_PATH . 'catalog/language/');
define('DIR_TEMPLATE', DIR_PATH . 'catalog/view/theme/');
define('DIR_SYSTEM', DIR_PATH . 'system/');
define('DIR_DATABASE', DIR_PATH . 'system/database/');
define('DIR_CACHE', DIR_PATH . 'system/cache/');
define('DIR_CONFIG', DIR_PATH . 'system/config/');
define('DIR_LOGS', DIR_PATH . 'system/logs/');
define('DIR_DOWNLOAD', DIR_PATH . 'download/');
define('DIR_IMAGE', DIR_PATH . 'image/');

// DB
require_once(DIR_DATABASE . 'database.php');
?>