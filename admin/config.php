<?php
// HTTP
define('HTTP_SERVER', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');
define('HTTP_CATALOG', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname(dirname($_SERVER['PHP_SELF'])), '/.\\') . '/');
define('HTTP_IMAGE', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname(dirname($_SERVER['PHP_SELF'])), '/.\\') . '/image/');

// HTTPS
define('HTTPS_SERVER', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');
define('HTTPS_IMAGE', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname(dirname($_SERVER['PHP_SELF'])), '/.\\') . '/image/');
define('HTTPS_CATALOG', 'http://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');

define('DIR_ADMIN', str_replace('\'', '/', realpath(dirname(__FILE__))) . '/');
define('DIR_ROOT', str_replace('\'', '/', realpath(dirname(dirname(__FILE__)))) . '/');

define('HTTPS_PATH', 		HTTP_SERVER. 'view/');
// DIR
define('DIR_APPLICATION', 	DIR_ADMIN);
define('DIR_LANGUAGE', 		DIR_ADMIN .'language/');
define('DIR_TEMPLATE', 		DIR_ADMIN .'view/template/');
define('DIR_SYSTEM', 		DIR_ROOT .'system/');
define('DIR_DATABASE', 		DIR_ROOT .'system/database/');
define('DIR_CONFIG', 		DIR_ROOT .'system/config/');
define('DIR_CACHE', 		DIR_ROOT .'system/cache/');
define('DIR_LOGS', 			DIR_ROOT .'system/logs/');
define('DIR_IMAGE', 		DIR_ROOT .'image/');
define('DIR_DOWNLOAD', 		DIR_ROOT .'download/');
define('DIR_CATALOG',		DIR_ROOT .'catalog/');

// DB
require_once(DIR_DATABASE . 'database.php');
?>