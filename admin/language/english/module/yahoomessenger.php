<?php
// Heading
$_['heading_title']       = 'Yahoo Messenger';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Yahoo Messenger!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_code']          = 'Yahoo Messenger Code:<br /><span class="help">Type nick yahoo, and name for show like that : yahooID1:name1;yahooID2:name2;</span>';
$_['entry_style']       = 'Yahoo Messenger Style (1-24)';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Yahoo Messenger!';
$_['error_code']          = 'Code Required';
$_['error_style']          = 'Yahoo Messenger Style Required (1-24)';

?>