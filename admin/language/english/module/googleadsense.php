<?php
// Heading
$_['heading_title']    = 'Google Adsense';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module google adsense!';
$_['text_left']        = 'Left';
$_['text_right']       = 'Right';

// Entry
$_['entry_google_client']   = 'Google ad client: <br /><span class="help">Login to your <a onclick="window.open(\'http://www.google.com/adsense/\');"><u>Google Adsense</u></a> account and after creating your web site profile copy and paste the adsense code into this field. <br />e.g. "pub-6128762133900691"</span>';
$_['entry_google_slot']   = 'Google ad slot: <br /><span class="help">e.g. "9710041367"</span>';
$_['entry_width']   = 'Width:';
$_['entry_height']   = 'Height:';
$_['entry_position']   = 'Position:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module google adsense!';
?>