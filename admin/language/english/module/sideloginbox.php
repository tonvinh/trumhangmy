<?php
// Heading
$_['heading_title']    		  = 'SideLoginBox';

// Text
$_['text_module']      		  = 'Modules';
$_['text_success']     		  = 'Success: You have modified the SideLoginBox module!';
$_['text_column_left']        = 'Left Column';
$_['text_column_right']       = 'Right Column';
$_['text_content_middle'] = 'Hedear Top';

// Entry
$_['entry_layout']        	  = 'Layout:';
$_['entry_position']     	  = 'Position:';
$_['entry_options']			  = 'Options:';
$_['entry_status']        	  = 'Status:';
$_['entry_sort_order']    	  = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify the SideLoginBox module!';



?>