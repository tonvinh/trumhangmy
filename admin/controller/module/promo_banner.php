<?php
class ControllerModulePromoBanner extends Controller {
	private $error = array(); 
	
	public function install() {
	}

	public function index() {
		$this->load->language('module/promo_banner');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$post_data = $this->request->post;

			// assign the content_top position to all banners
			if (isset($post_data['promo_banner_module'])) {
				foreach($post_data['promo_banner_module'] as $k=>$pd) {
					$post_data['promo_banner_module'][$k]['position'] = 'content_top';
					$post_data['promo_banner_module'][$k]['sort_order'] = -1;
				}
			}

			$this->model_setting_setting->editSetting('promo_banner', $post_data);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');

		$this->data['entry_dimension'] = $this->language->get('entry_dimension'); 
		$this->data['entry_image'] = $this->language->get('entry_image'); 
		$this->data['entry_url'] = $this->language->get('entry_url'); 
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_language'] = $this->language->get('entry_language');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_options'] = $this->language->get('entry_options');
		$this->data['entry_display_always'] = $this->language->get('entry_display_always');
		$this->data['entry_new_window'] = $this->language->get('entry_new_window');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_banner'] = $this->language->get('button_add_banner');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->data['token'] = $this->session->data['token'];

		$this->load->model('tool/image');
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['dimension'])) {
			$this->data['error_dimension'] = $this->error['dimension'];
		} else {
			$this->data['error_dimension'] = array();
		}

		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}

		if (isset($this->error['language'])) {
			$this->data['error_language'] = $this->error['language'];
		} else {
			$this->data['error_language'] = array();
		}
		
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/promo_banner', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/promo_banner', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$promo_banners = array();
		
		if (isset($this->request->post['promo_banner_module'])) {
			$promo_banners = $this->request->post['promo_banner_module'];
		} elseif ($this->config->get('promo_banner_module')) { 
			$promo_banners = $this->config->get('promo_banner_module');
		}

		foreach($promo_banners as $k=>$promo_banner) {
		
			if (!isset($promo_banner['display_always'])) {
				$promo_banners[$k]['display_always'] = 0;
			}
			
			if (!isset($promo_banner['new_window'])) {
				$promo_banners[$k]['new_window'] = 0;
			}
		
			if (empty($promo_banner['image'])) {
				$promo_banners[$k]['preview'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
			}
			else {
				$promo_banners[$k]['preview'] = $this->model_tool_image->resize($promo_banner['image'], 100, 100);
			}
		}

		$this->data['promo_banners'] = $promo_banners;
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();
		
		$this->data['languages'] = array();
		
		$this->data['languages'][] = array('language_id' => 0, 'name' => $this->language->get('text_all_languages'));
		
		foreach($languages as $language) {
			$this->data['languages'][] = array('language_id' => $language['language_id'], 'name' => $language['name']);
		}
		
		$this->template = 'module/promo_banner.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/promo_banner')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$layouts = array();

		if (isset($this->request->post['promo_banner_module'])) {
			foreach ($this->request->post['promo_banner_module'] as $key => $value) {
				if (!$value['width'] || !$value['height']) {
					$this->error['dimension'][$key] = $this->language->get('error_dimension');
				}

				if (!$value['image']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
				
				// allow one banner per layout per language
				if (isset($layouts[$value['layout_id']])) {
					if (!in_array($value['language_id'], $layouts[$value['layout_id']]) 
						&& !in_array(0, $layouts[$value['layout_id']]) 
						&& $value['language_id'] != 0) { // if 'All languages' previousl added do not allow anything else to be added ...
						$layouts[$value['layout_id']][] = $value['language_id'];
					}
					else {
						$this->error['language'][$key] = $this->language->get('error_language');
					}
				}
				else {
					$layouts[$value['layout_id']] = array( $value['language_id'] );
				}
				
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>