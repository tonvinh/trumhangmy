<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table id="module" class="list">
        <thead>
          <tr>
            <td class="left"><span class="required">*</span> <?php echo $entry_dimension; ?></td>
            <td class="left"><span class="required">*</span> <?php echo $entry_image; ?></td>
            <td class="left"><?php echo $entry_url; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
			<td class="left"><?php echo $entry_language; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
			<td class="left"><?php echo $entry_options; ?></td>
            <td>&nbsp;</td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($promo_banners as $promo_banner) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            <td class="left"><input type="text" name="promo_banner_module[<?php echo $module_row; ?>][width]" value="<?php echo $promo_banner['width']; ?>" size="3" />
              <input type="text" name="promo_banner_module[<?php echo $module_row; ?>][height]" value="<?php echo $promo_banner['height']; ?>" size="3" />
              <?php if (isset($error_dimension[$module_row])) { ?>
              <span class="error"><?php echo $error_dimension[$module_row]; ?></span>
              <?php } ?></td>

            <td class="center"><input type="hidden" name="promo_banner_module[<?php echo $module_row; ?>][image]" value="<?php echo $promo_banner['image']; ?>" id="image<?php echo $module_row; ?>"  />
              <img src="<?php echo $promo_banner['preview']; ?>" alt="" id="preview<?php echo $module_row; ?>" class="image" onclick="image_upload('image<?php echo $module_row; ?>', 'preview<?php echo $module_row; ?>');" />
              <?php if (isset($error_image[$module_row])) { ?>
              <span class="error"><?php echo $error_image[$module_row]; ?></span>
              <?php } ?></td>


            <td class="left"><input type="text" name="promo_banner_module[<?php echo $module_row; ?>][link]" value="<?php echo $promo_banner['link'];?>" /></td>
            <td class="left"><select name="promo_banner_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $promo_banner['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>

            <td class="left"><select name="promo_banner_module[<?php echo $module_row; ?>][language_id]">
                <?php foreach ($languages as $language) { ?>
                <?php if ($language['language_id'] == $promo_banner['language_id']) { ?>
                <option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if (isset($error_language[$module_row])) { ?>
              <span class="error"><?php echo $error_language[$module_row]; ?></span>
              <?php } ?></td>
			  
            <td class="left"><select name="promo_banner_module[<?php echo $module_row; ?>][status]">
                <?php if ($promo_banner['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
			  
            <td class="left">
				<?php if ($promo_banner['display_always']) { ?>
				<input type="checkbox" id="promo_banner_module_<?php echo $module_row; ?>_display_always" name="promo_banner_module[<?php echo $module_row; ?>][display_always]" value="1" checked="checked" /> 
                <?php } else { ?>
				<input type="checkbox" id="promo_banner_module_<?php echo $module_row; ?>_display_always" name="promo_banner_module[<?php echo $module_row; ?>][display_always]" value="1" /> 
                <?php } ?>
				<label for="promo_banner_module_<?php echo $module_row; ?>_display_always"><?php echo $entry_display_always; ?></label><br/>
				
				<?php if ($promo_banner['new_window']) { ?>
				<input type="checkbox" id="promo_banner_module_<?php echo $module_row; ?>_new_window" name="promo_banner_module[<?php echo $module_row; ?>][new_window]" value="1" checked="checked"/> 
                <?php } else { ?>
				<input type="checkbox" id="promo_banner_module_<?php echo $module_row; ?>_new_window" name="promo_banner_module[<?php echo $module_row; ?>][new_window]" value="1" /> 
                <?php } ?>
				<label for="promo_banner_module_<?php echo $module_row; ?>_new_window"><?php echo $entry_new_window; ?></label><br/>				
			</td>

			  
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="7">&nbsp;</td>
            <td class="left" style="min-width: 90px"><a onclick="addModule();" class="button"><span><?php echo $button_add_banner; ?></span></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="promo_banner_module[' + module_row + '][width]" value="" size="3" /> <input type="text" name="promo_banner_module[' + module_row + '][height]" value="" size="3" /></td>';
	html += '<td class="center"><input type="hidden" name="promo_banner_module[' + module_row + '][image]" value="" id="image' + module_row + '" /><img src="<?php echo $no_image; ?>" alt="" id="preview' + module_row + '" class="image" onclick="image_upload(\'image' + module_row + '\', \'preview' + module_row + '\');" /></td>';
	html += '<td class="left"><input type="text" name="promo_banner_module[' + module_row + '][link]" value="" /></td>';
	html += '    <td class="left"><select name="promo_banner_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="promo_banner_module[' + module_row + '][language_id]">';
	<?php foreach ($languages as $language) { ?>
	html += '      <option value="<?php echo $language['language_id']; ?>"><?php echo addslashes($language['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="promo_banner_module[' + module_row + '][status]">';
	html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
	html += '      <option value="0"><?php echo $text_disabled; ?></option>';
	html += '    </select></td>';

	html += '    <td class="left">';
	html += '      <input type="checkbox" id="promo_banner_module_' + module_row + '_display_always" name="promo_banner_module[' + module_row + '][display_always]"  value="1" />';
	html += '      <label for="promo_banner_module_' + module_row + '_display_always"><?php echo $entry_display_always; ?></label><br/>';
	html += '      <input type="checkbox" id="promo_banner_module_' + module_row + '_new_window" name="promo_banner_module[' + module_row + '][new_window]"  value="1" />';
	html += '      <label for="promo_banner_module_' + module_row + '_new_window"><?php echo $entry_new_window; ?></label><br/>';
	html += '    </td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<script type="text/javascript"><!--
function image_upload(field, preview) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>',
					type: 'POST',
					data: 'image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(data) {
						$('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" class="image" onclick="image_upload(\'' + field + '\', \'' + preview + '\');" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 700,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<div style="text-align:right">
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHTwYJKoZIhvcNAQcEoIIHQDCCBzwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBkWynaqTNofDnaFmJ6+67feEzYgWvVlitPg1vKS8AxlJBpzY8HqbUz+VV726+LliLuhjRjrFMnQiYLntv2GGo+s9tYlBALtdUB1ZrKqCMC6KqaHe+db31ZcJ+s77JzJu5AYKNpykeujnQ/0cyYdmAL0AA5Zd3dCE/lA1D8bSeLqjELMAkGBSsOAwIaBQAwgcwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIZImD+xjuC/2AgaigFKxQEw9pf9rHaLMi6PTWVNhUkxxEICaZNCbG6//mMcRxBuIG8FzmV7WOr2K/YB4XgI10AeJaRlGLMNFY1novOsOnRgOO1VAfcsICkMWSxfgV+RqlXiVpUaGxoW1A0w3DpORbELLDvud2hxJNrgdNimuNnMbzD0yIKjzJfpGYxNeRfCbxELc3cnTdRWre6y15bSERuquM6fRnKUDixKnCrAUaE6nG9JWgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xMjAzMTAxOTM0NTdaMCMGCSqGSIb3DQEJBDEWBBRf3Xcob9WHY5Tueyqw5htirJyfczANBgkqhkiG9w0BAQEFAASBgFeyFJJxvPHJMeCzq2C4El3EJUYBjEJ1JQsbY4pJtHo8Y56TjH47JFvdJJDlfXqKJ/QuVyE4n8QBXHtHuLHSRNBopkA+G/379AWgH9dSBosIhsRk6PtoIiUhzEe0wVkILsSqQh6gifPRRsKjJFwbBi/PFpCCgw6zXRYLf3TcicoM-----END PKCS7-----
	">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>
</div>
<?php echo $footer; ?>